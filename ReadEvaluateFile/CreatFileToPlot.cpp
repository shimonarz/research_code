
#include "CreatFileToPlotLogic.h"




int main2(int argc, char *argv[])
{
	// Main Command :
	// writeFileForPlot evaluate-result-file output-directory
	// writeFileForPlotAvgAll evaluate-result-folder output-directory
	
	if(argc < 1)
	{
		cout << "Command" << std::endl;
		cout << "writeFileForPlot evaluate-result-file output-directory . print all eval in file to different file" << std::endl;
		cout << "writeFileForPlotAvgOfFiles output-directory evaluate-result-file-List . print all eval in file to different file" << std::endl;
	}

	std::string command = argv[1]; 

	if (command=="writeFileForPlot")
	{
		writeFileForPlot(argc,argv);
	}
	if (command=="writeFileForPlotAvgOfFiles")
	{
		writeFileForPlotAvgOfFiles(argc,argv);
	}
	if (command=="crossValidationForRM310and100")
	{
		crossValidationForRM310and100(argc,argv);
	}
	if (command=="writeFileForPlotForFdocs")
	{
		ConfigurationType type = Retrival;
		writeFileForPlotForConfig(argc,argv,type);
	}
	if (command=="writeFileForPlotForFdocsBestoneAvg")
	{
		ConfigurationType type = Retrival;
		writeFileForPlotForConfigWithBestoneAvg(argc,argv,type);
	}
	if (command=="writeFileForPlotForW2v")
	{
		ConfigurationType type = W2vModel;
		writeFileForPlotForConfig(argc,argv,type);
	}
	return -1;
}