#include "CreatFileToPlotLogic.h"

#include <windows.h>


void getAllFileFromDirectory(string dirFullPath ,vector<string> *listOfFiles )
{
	WIN32_FIND_DATA file_data;
	HANDLE hFindFileNameEvaluation = FindFirstFile((dirFullPath+ "/*").c_str() , &file_data);
	if (hFindFileNameEvaluation != INVALID_HANDLE_VALUE) 
	{
		do 
		{
			string fileName = file_data.cFileName;
			if(fileName != "." && fileName != ".." )
			{
				std::cout << "file Name = " << fileName << std::endl ;
				string fullFileName = dirFullPath + "/" + fileName;
				listOfFiles->push_back(fullFileName);
			}
		}
		while (FindNextFile(hFindFileNameEvaluation, &file_data));
		FindClose(hFindFileNameEvaluation);
	}
}






void test_writeFileForPlot()
{
	int argc = 3;
	char* n_argv[] = { "ExeName", "writeFileForPlot" , "D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0/evaluateFolder/051.clusters.result.evaluate" ,"D:/Research/DataFromServer/OutputTests/test_writeFileForPlot"};
    writeFileForPlot(argc,n_argv);
}

void test_writeFileForPlotAvgOfFiles()
{
	
	int argc = 7;
	char* n_argv[] = { "ExeName", 
		"writeFileForPlot" , 
		"D:/Research/DataFromServer/OutputTests/test_writeFileForPlotAvgOfFiles",
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0/evaluateFolder/051.clusters.result.evaluate" ,
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0/evaluateFolder/052.clusters.result.evaluate" ,
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0/evaluateFolder/053.clusters.result.evaluate" ,
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0/evaluateFolder/054.clusters.result.evaluate" ,
		};
    writeFileForPlotAvgOfFiles(argc,n_argv);
}

void test_crossValidationForRM310and100()
{
	char a[][255] = {
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0/evaluateFolder", 
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.2/evaluateFolder", 
		/*"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.5/evaluateFolder", 
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.8/evaluateFolder", 
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_1/evaluateFolder", 
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_25_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0/evaluateFolder",
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_25_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.2/evaluateFolder", 
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_25_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.5/evaluateFolder", 
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_25_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.8/evaluateFolder", 
		"D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_25_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_1/evaluateFolder", */
	};

	vector<string> listOfFiles ;
	vector<string> listOfConfig ;
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0/evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.2/evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.5/evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.8/evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_1.0/evaluateFolder");

	int n = listOfConfig.size();
	for (int i = 0; i < n; i++)
	{
		string path = listOfConfig[i];
		getAllFileFromDirectory( path ,&listOfFiles );
	}

	crossValidationForRM310and100("D:/Research/DataFromServer/OutputTests/test_crossValidationForRM310and100/cv.txt",listOfFiles);
}

void test_crossValidationForRM310and100_2()
{

	vector<string> listOfFiles ;
	vector<string> listOfConfig ;
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/CB/AP/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0/evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/CB/AP/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.2/evaluateFolder");

	int n = listOfConfig.size();
	for (int i = 0; i < n; i++)
	{
		string path = listOfConfig[i];
		getAllFileFromDirectory( path ,&listOfFiles );
	}

	crossValidationForRM310and100("D:/Research/DataFromServer/OutputTests/test_CompareBetweenCBToCBRM3Models_WordComeFromRM1/cv.txt",listOfFiles);
}

void test_writeFileForPlotForFdocs()
{
	vector<string> listOfFiles ;
	vector<string> listOfConfig ;
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0/evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.2/evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.5/evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.8/evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_1.0/evaluateFolder");

	int n = listOfConfig.size();
	for (int i = 0; i < n; i++)
	{
		string path = listOfConfig[i];
		getAllFileFromDirectory( path ,&listOfFiles );
	}

	string fDocs = "10";
	string cvPath = "D:/Research/DataFromServer/OutputTests/test_crossValidationForRM310and100/cv.txt";
	string path_to_file = "D:/Research/DataFromServer/OutputTests/test_writeFileForPlotForFdocs";

	writeFileForPlotForFdocs( fDocs,  cvPath , path_to_file, &listOfFiles );
}

void test_writeFileForPlotForFdocsBestone()
{
	vector<string> listOfFiles ;
	vector<string> listOfConfig ;
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0/evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.2/evaluateFolder");
	//listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.5/evaluateFolder");
	//listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.8/evaluateFolder");
	//listOfConfig.push_back("D:/Research/DataFromServer/FIles/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_1.0/evaluateFolder");

	int n = listOfConfig.size();
	for (int i = 0; i < n; i++)
	{
		string path = listOfConfig[i];
		getAllFileFromDirectory( path ,&listOfFiles );
	}

	
	string fDocs = "10";
	string path_to_file = "D:/Research/DataFromServer/OutputTests/test_writeFileForPlotForFdocsBestone";
	string cvPath ="D:/Research/DataFromServer/OutputTests/test_crossValidationForRM310and100/cv.txt";
	writeFileForPlotForFdocsBestone( fDocs , cvPath , path_to_file, &listOfFiles );
}

void test_crossValidationForRM310and1002()
{
	vector<string> listOfFiles ;

	// /for all configurations
	HANDLE hFind;
	WIN32_FIND_DATA data;
	string pathToW2V = "D:/Research/DataFromServer/FIles/AP/";
	hFind = FindFirstFile((pathToW2V + "*").c_str(), &data);
	if (hFind != INVALID_HANDLE_VALUE) 
	{
		do 
		{
			string configDirName = data.cFileName;
			std::cout << "Configuration Name = " << configDirName << std::endl ;
			if(configDirName != "." && configDirName != ".." )
			{
				string dirFullPath = pathToW2V + configDirName  + "/uniform_evaluateFolder";
				getAllFileFromDirectory( dirFullPath ,&listOfFiles );
			}
		} 
		while (FindNextFile(hFind, &data));
		FindClose(hFind);
	}
	string path_to_file = "D:/Research/DataFromServer/OutputTests/test_crossValidationForRM310and100/cv2.txt";
	crossValidationForRM310and100_noAllToMemory(path_to_file,listOfFiles);

}


void main(int argc, char *argv[])
{
	//test_writeFileForPlot();
	//test_writeFileForPlotAvgOfFiles();
	//test_crossValidationForRM310and100();
	//test_writeFileForPlotForFdocs();
	//test_writeFileForPlotForFdocsBestone();
	//test_crossValidationForRM310and1002();
	test_crossValidationForRM310and100_2();

}












