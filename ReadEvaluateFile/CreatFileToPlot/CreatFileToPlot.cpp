
#include <string.h>
#include <set>
#include <string>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
//#include<utility>
#include <algorithm>    // std::min_element, std::max_element
#include <fstream>
#include <iostream>
#include <sstream>
#include <map>

using std::ifstream;
using std::ofstream;
using std::vector;
using std::string;
using std::map;
using std::cout;
using std::cerr;


typedef struct ResultFileLine
{
	string evalName;
	string queryName;
	double evalValue;
}ResultFileLine;



typedef struct QueryResult
{
	string queryName;
	double originalQueryValue;
	double rm1QueryValue;
	vector<std::pair<string,double>> result;
}QueryResult;

bool sort_pred(const std::pair<string,double>& left, const std::pair<string,double>& right)
{
    return left.second > right.second;
}

QueryResult* ReadFile(string fileName)
{
	QueryResult* file = new QueryResult();
	ifstream finput;
	
	finput.open ( fileName.c_str() ); 
	
	if (!finput.good())
	{
		std::cerr << "Can't open file: " << fileName << std::endl;
		return NULL;
	}
	

	bool isOriginalQuery = true;
	bool isORm1Query = false;

	while  ( finput.good() )
	{
		string line;
		getline (finput, line);
		if(line.size() == 0 ) continue;

		std::string qcopy( line);
		std::istringstream oss (qcopy);

		std::string map;
		oss >> map;
		if ( map.empty()) {
			return NULL;    
		}

		std::string queryName;
		oss >> queryName;
		if ( queryName.empty()) {
			return NULL;    
		}

		if(queryName.compare("all") == 0)
		{
			continue;
		}

		std::string mapValueStr;
		oss >> mapValueStr;
		if ( mapValueStr.empty()) {
			return NULL;    
		}

		double mapValue = atof(mapValueStr.c_str());

		if(isOriginalQuery)
		{
			file->queryName = queryName;
			file->originalQueryValue = mapValue;
			isOriginalQuery = false;
			continue;
		}

		if(queryName.compare(file->queryName + "_rm1") == 0)
		{
			file->rm1QueryValue = mapValue;
			continue;
		}
		
		file->result.push_back(std::make_pair(queryName, mapValue ) );	
	}

	std::sort(file->result.begin(), file->result.end(), sort_pred);

	finput.close();
	return file;
	
}

vector<ResultFileLine*>* ReadFileToVector(string fileName)
{
	vector<ResultFileLine*>* vec = new vector<ResultFileLine*>();
	ifstream finput;
	finput.open ( fileName.c_str() ); 
	if (!finput.good())
	{
		std::cerr << "Can't open file: " << fileName << std::endl;
		return NULL;
	}
	
	while  ( finput.good() )
	{
		string line;
		getline (finput, line);
		if(line.size() == 0 ) continue;

		ResultFileLine* evalline = new ResultFileLine();

		std::string qcopy( line);
		std::istringstream oss (qcopy);

		std::string eval;
		oss >> eval;
		if ( eval.empty()) {
			return NULL;    
		}

		std::string queryName;
		oss >> queryName;
		if ( queryName.empty()) {
			return NULL;    
		}


		std::string evalValueStr;
		oss >> evalValueStr;
		if ( evalValueStr.empty()) {
			return NULL;    
		}

		double evalValue = atof(evalValueStr.c_str());

		evalline->evalName = eval;
		evalline->queryName = queryName;
		evalline->evalValue = evalValue;
		
		vec->push_back(evalline);
	}

	finput.close();
	return vec;
}

map<string,vector<ResultFileLine*>*>* mapEval(vector<ResultFileLine*>* evalFile)
{
	map<string,vector<ResultFileLine*>*>  *evalMapFile = new map<string,vector<ResultFileLine*>*>();
	for(int i=0 ; i < evalFile->size() ; ++i)
	{
		ResultFileLine* fileLine = (*evalFile)[i];
		if( evalMapFile->find(fileLine->evalName) == evalMapFile->end()) // not found
		{
			vector<ResultFileLine*>* newVector = new vector<ResultFileLine*>();
			newVector->push_back(fileLine);
			evalMapFile->insert(std::make_pair<string,vector<ResultFileLine*>*> (fileLine->evalName, newVector));
		}
		else
		{
			map<string,vector<ResultFileLine*>*>::const_iterator  pos = evalMapFile->find(fileLine->evalName);
			vector<ResultFileLine*>* vec = pos->second;
			vec->push_back(fileLine);
		}
	}
	return evalMapFile;
}

void writeFileForPlot(string path_to_file, QueryResult* evalFile)
{
	ofstream of;
	of.open ( path_to_file.c_str(), std::ofstream::out ); 
	of << "# index, originalQueryValue, rm1QueryValue, clusterValue";

	double originalQueryValue = evalFile->originalQueryValue;
	double rm1QueryValue = evalFile->rm1QueryValue;
	
	for(int i=0; i<evalFile->result.size() ; i++)
	{
		double clusterValue = (*evalFile).result[i].second;
		of << i << " " <<  originalQueryValue  << " " << rm1QueryValue << " " <<  clusterValue << std::endl;
	}

	of.close();  

}

void writeFileForPlotByWval(map<string,QueryResult*> *res , string path_to_file, vector<string> *evalToPlot = NULL)
{
	if(evalToPlot == NULL)
	{
		typedef map<string,QueryResult*>::iterator it_type;
		for(it_type iterator = res->begin(); iterator != res->end(); iterator++) 
		{
			string path = path_to_file + "/" + iterator->second->queryName + "_" + iterator->first + ".dat";
			writeFileForPlot( path, iterator->second);	 
		}
		return;
	}

	for(int i=0; i<evalToPlot->size() ; i++)
	{
		string evalName = (*evalToPlot)[i];
		string path = path_to_file + "/" + evalName + ".dat";

		if( res->find(evalName) == res->end()) // not found
		{
			printf("Eval not found in map : %s" , evalName.c_str());
		}
		else
		{
			map<string,QueryResult*>::const_iterator  pos = res->find(evalName);
			QueryResult* queryResult = pos->second;
			writeFileForPlot( path, queryResult);
		}

	}
}


bool minSizeOfQueryName(ResultFileLine *i, ResultFileLine *j) { return i->queryName.size() < j->queryName.size(); }

QueryResult* getQueryResult(vector<ResultFileLine*> * resultFileLineVec,  string name )
{
	QueryResult* file = new QueryResult();
	ifstream finput;
	
	bool isOriginalQuery = true;
	bool isORm1Query = false;

	for( int i = 0 ; i< resultFileLineVec->size(); ++i)
	{
		ResultFileLine* resultFileLine = (*resultFileLineVec)[i];

		if(resultFileLine->queryName.compare("all") == 0)
		{
			continue;
		}

		if(resultFileLine->queryName.compare(name) == 0)
		{
			file->queryName = resultFileLine->queryName;
			file->originalQueryValue = resultFileLine->evalValue;
			continue;
		}

		if(resultFileLine->queryName.compare(name+ "_rm1") == 0)
		{
			file->rm1QueryValue = resultFileLine->evalValue;
			continue;
		}
		
		file->result.push_back(std::make_pair(resultFileLine->queryName, resultFileLine->evalValue ) );	
	}
	
	std::sort(file->result.begin(), file->result.end(), sort_pred);

	finput.close();
	return file;
	
}

map<string,QueryResult*>* getQueryResult(map<string,vector<ResultFileLine*>*> * file)
{
	map<string,QueryResult*>* res = new map<string,QueryResult*>();

	typedef map<string,vector<ResultFileLine*>*>::iterator it_type;
	for(it_type iterator = file->begin(); iterator != file->end(); iterator++) 
	{
		vector<ResultFileLine*> *resultFileLine = iterator->second;
		vector<ResultFileLine*>::iterator min = std::min_element(resultFileLine->begin(),resultFileLine->end(),minSizeOfQueryName);
		string name = (*min)->queryName;
		QueryResult* evalRes = getQueryResult(resultFileLine,  name );
		res->insert( std::make_pair<string,QueryResult*>(iterator->first,evalRes));
	}


	return res;
}


int main(int argc, char *argv[])
{
	if(argc != 3)
	{
		cout << "You must enter 2 parameters : (1) evaluate result file  (2) path to file rady to plot " << std::endl;
	}
	string fileName = argv[1];
	string path_to_file = argv[2];

	vector<ResultFileLine*>* evalFile = ReadFileToVector( fileName);
	map<string,vector<ResultFileLine*>*>  *file = mapEval(evalFile);
	map<string,QueryResult*> *res = getQueryResult(file);
	writeFileForPlotByWval(res,path_to_file);

	evalFile->clear();
	delete evalFile;
	file->clear();
	delete file;
	res->clear();
	delete res;

	return -1;
}