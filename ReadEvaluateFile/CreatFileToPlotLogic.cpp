

#include "CreatFileToPlotLogic.h"




// function for sort function
bool sort_pred(const std::pair<string,double>& left, const std::pair<string,double>& right){	return left.second > right.second;}



// Sum map function
double sumQueryRm310ResultMap (const double previous, const std::pair<std::string,QueryResult*>& p) { return previous + p.second->rm310QueryValue;}
double sumQueryRm3100ResultMap(const double previous, const std::pair<std::string,QueryResult*>& p) { return previous + p.second->rm3100QueryValue;}



// Write files functions

// write file that contain :  originalQueryValue, rm310QueryValue, rm3100QueryValue, clusterValue

void writeFileForPlot(string path_to_file, QueryResult* evalFile)
{

	ResultToPrint resultToPrint;
	double originalQueryValue = evalFile->originalQueryValue;
	double rm3_10 = evalFile->rm310QueryValue;
	double rm3_100 = evalFile->rm3100QueryValue;

	resultToPrint.listOfSingleValue.insert(std::make_pair<string,double>("originalQueryValue",originalQueryValue));
	resultToPrint.listOfSingleValue.insert(std::make_pair<string,double>("rm310QueryValue",rm3_10));
	resultToPrint.listOfSingleValue.insert(std::make_pair<string,double>("rm3100QueryValue",rm3_100));

	vector<double> clustersValues;
	int i;
	for(i=0; i<evalFile->result.size() ; i++)
	{
		double clusterValue = (*evalFile).result[i].second;
		clustersValues.push_back(clusterValue);
	}
	resultToPrint.listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("clusterValue",&clustersValues));
 
	resultToPrint.writeFileForPlot(path_to_file);
}


void writeCVFile(string path_to_file, std::map<string, CvResult>* cv)
{
	ofstream of;
	of.open ( path_to_file.c_str(), std::ofstream::out ); 
	of << "# evaluation , rm310cv, rm3100cv" << std::endl;

	std::map<string, CvResult>::iterator cvIterator;
	for (cvIterator = cv->begin(); cvIterator != cv->end(); ++cvIterator)
	{
		string eval = cvIterator->first;
		double rm3_10 = cvIterator->second.rm310Value;
		double rm3_100 = cvIterator->second.rm3100Value;
		of << eval << " " <<  rm3_10  << " " << rm3_100 << std::endl;
	}
	of.close();  
}


void writeFileForPlotByWval(EvaluateResultFile *res , string path_to_file, vector<string> *evalToPlot = NULL)
{
	if(evalToPlot == NULL)
	{
		typedef std::map<string,QueryResult*>::iterator it_type;
		for(it_type iterator = res->evalInFile.begin(); iterator != res->evalInFile.end(); iterator++) 
		{
			string path = path_to_file + "/" + iterator->second->queryName + "_" + iterator->first + ".dat";
			writeFileForPlot( path, iterator->second);	 
		}
		return;
	}

	for(int i=0; i<evalToPlot->size() ; i++)
	{
		string evalName = (*evalToPlot)[i];
		string path = path_to_file + "/" + evalName + ".dat";
		QueryResult* queryResult =  res->getEvaluateValuesInFile(evalName);
		if(queryResult != NULL)
		{
			writeFileForPlot( path, queryResult);
		}
	}
}

// set for query strut the contain all query parameters
// resultFileLineVec - define query the contain: original, rm310, rm3100, clusters
// queryNme - current query name
/*QueryResult* getQueryResult(vector<ResultFileLine*> * resultFileLineVec,  string queryNme )
{
	QueryResult* file = new QueryResult();

	for( int i = 0 ; i< resultFileLineVec->size(); ++i)
	{
		ResultFileLine* resultFileLine = (*resultFileLineVec)[i];

		if(resultFileLine->queryName.compare("all") == 0)
		{
			continue;
		}

		if(resultFileLine->queryName.compare(queryNme) == 0)
		{
			file->queryName = resultFileLine->queryName;
			file->originalQueryValue = resultFileLine->evalValue;
			continue;
		}

		if(resultFileLine->queryName.compare(queryNme+ "_rm3_10") == 0)
		{
			file->rm310QueryValue = resultFileLine->evalValue;
			continue;
		}

		if(resultFileLine->queryName.compare(queryNme+ "_rm3_100") == 0)
		{
			file->rm3100QueryValue = resultFileLine->evalValue;
			continue;
		}

		file->result.push_back(std::make_pair(resultFileLine->queryName, resultFileLine->evalValue ) );	
	}

	std::sort(file->result.begin(), file->result.end(), sort_pred);

	return file;
}*/




// get list of file line and return only the relevant query
// vec - file line
// queryNameList - queries to return 
/*vector<ResultFileLine*> ReduceResultFileLineVectorByQuery(vector<ResultFileLine*>* vec, vector<string>* queryNameList)
{
	std::vector<ResultFileLine*> elems;
	for (int i = 0; i < vec->size(); i++)
	{
		for (int j = 0; j < queryNameList->size() ; j++)
		{
			if((*vec)[i]->queryName == (*queryNameList)[j])
			{
				elems.push_back((*vec)[i]);
				break;
			}
		}
	}
	return elems;
}*/

// read set of file that for different configurations
// argv - file name to read
// configPerEvaluation - strut that contain in the end all files
// QueryNameSet - contain all queries name
void ReadAllFilesToMemory(vector<string> *filesName, ConfigPerEvaluation *configPerEvaluation,std::set<string>* QueryNameSet )
{
	int i=0;
	try {

#pragma region Read All files to memory  
		std::cout << "		Read file number : " << std::endl;
		// loop for each the file that we receive in the main parameters
		for( i=0; i<filesName->size() ; ++i)
		{
			std::cout << "\r" << "		Read file number :" << i <<  std::flush;
			// create new representation to the file
			string fileName = (*filesName)[i];

#pragma region check if file exist
			ifstream finput;
			finput.open ( fileName.c_str() ); 
			if (!finput.good())
			{
				std::cerr << "Can't open file: " << fileName << std::endl;
				continue;
			}
			finput.close();
#pragma endregion 

#pragma region extract from the file name the configuration name and the query name
			std::vector<std::string> splitedFile = split(fileName, '/'); // the file path is split by "/" note.
			string queryFullName = splitedFile[splitedFile.size()-1]; 
			std::vector<std::string> splitedQuery = split(queryFullName, '.');
			string query = splitedQuery[0];
			string config = splitedFile[splitedFile.size()-3];
#pragma endregion 

			std::cout << "\r" << "		Read file number :" << i << ", config = " << config   << ", query = " << query << std::flush;
			// add query name to the set
			QueryNameSet->insert(query);

			EvaluateResultFile evaluateResultFile;
			evaluateResultFile.init(fileName);

			// pass all evaluation in the current file
			std::map<string,QueryResult*>::iterator fileConfig; //  evaluateResultFile.evalInFile  define map : key =  evaluation parameter , value = query (cluster , rm310 , rm3100, oriental values of the evaluation parameter ) 
			for ( fileConfig = evaluateResultFile.evalInFile.begin(); fileConfig != evaluateResultFile.evalInFile.end(); fileConfig++ )
			{
				string evalName = fileConfig->first ; // evaluation name
				QueryResult* queryRes =	fileConfig->second ; // value of the file for the evaluation
				configPerEvaluation->add(config,evalName,queryRes->Copy());
			}
			evaluateResultFile.Clear();
		} 
		std::cout << "		calculate average " << std::endl;
		configPerEvaluation->calcAvg();
#pragma endregion Read All files to memory  
	}
	catch (...) {
		cout << "Exception occurred - file name " << (*filesName)[i] << std::endl;
	}
}


///ConfigurationType

void orderFileByConfig(vector<string> *fileNameList, ConfigurationType type , std::map<string,vector<string>*> *orderFile )
{
	int numberOfFiles = fileNameList->size();
	for(int i = 0 ; i < numberOfFiles ; ++i)
	{
		string fileName = (*fileNameList)[i];
		// extract from the file name the configuration name and the query name
		std::vector<std::string> splitedFile = split(fileName, '/'); // the file path is split by "/" note.
		string queryFullName = splitedFile[splitedFile.size()-1]; 
		std::vector<std::string> splitedQuery = split(queryFullName, '.');
		
		string query = splitedQuery[0];
		string config = splitedFile[splitedFile.size()-3];
		string w2vModel = splitedFile[splitedFile.size()-4];
		string index = splitedFile[splitedFile.size()-5];

		string key;
		switch (type)
		{
		case Index:
			key = index;
			break;
		case W2vModel:
			key = w2vModel;
			break;
		case Retrival:
			key = config;
			break;
		default:
			key = query;
			break;
		}

	    std::map< string , vector<string>* >::const_iterator  configIt = orderFile->find(key);
		if( configIt == orderFile->end() ) // config not found
		{
			vector<string>* vec = new vector<string>();
			vec->push_back(fileName);
		    orderFile->insert(std::make_pair<string,vector<string>*> (key, vec));
		}
		else
		{
			vector<string>* vec = configIt->second ;
			vec->push_back(fileName);
		}
	}
}

void orderFileByConfig(vector<string> *fileNameList,std::map<string,vector<string>*> *orderFile )
{
	int numberOfFiles = fileNameList->size();
	for(int i = 0 ; i < numberOfFiles ; ++i)
	{
		string fileName = (*fileNameList)[i];
		// extract from the file name the configuration name and the query name
		std::vector<std::string> splitedFile = split(fileName, '/'); // the file path is split by "/" note.
		string queryFullName = splitedFile[splitedFile.size()-1]; 
		std::vector<std::string> splitedQuery = split(queryFullName, '.');
		string query = splitedQuery[0];
		string config = splitedFile[splitedFile.size()-3];

	     std::map< string , vector<string>* >::const_iterator  configIt = orderFile->find(config);
		if( configIt == orderFile->end() ) // config not found
		{
			vector<string>* vec = new vector<string>();
			vec->push_back(fileName);
		    orderFile->insert(std::make_pair<string,vector<string>*> (config, vec));
		}
		else
		{
			vector<string>* vec = configIt->second ;
			vec->push_back(fileName);
		}
	}
}

void crossValidationForRM310and100(string file,vector<string> argv)
{

	ConfigPerEvaluation configPerEvaluation; // key is evaluation parameter (map, P@10..) , value is list of configuration (each configuration contain all query result)
	std::set<string> QueryNameSet;

	#pragma region Read All files to memory  
	std::cout << "		Start - Read All files to memory ." << std::endl;
	ReadAllFilesToMemory(&argv, &configPerEvaluation,&QueryNameSet);
	std::cout << "		End - Read All files to memory ." << std::endl;
	#pragma endregion Read All files to memory  
	  
	#pragma region Execute cross validation process 
	std::cout << "		Start - Execute cross validation process ." << std::endl;
	std::map<string,CvResult> cv ;

	// Do cross validation for each evaluation
	std::map<string, ConfigMap* > *configPerEvaluationMap = &configPerEvaluation.configPerEvaluation;
	typedef std::map<string,ConfigMap*>::iterator it_type;
	for(it_type configPerEvaluationIterator = configPerEvaluationMap->begin(); configPerEvaluationIterator != configPerEvaluationMap->end(); configPerEvaluationIterator++) 
	{
		std::map<string, QueryResult*> queryBestConfigForRm310 ;
		std::map<string, QueryResult*> queryBestConfigForRm3100;

		// Do cross validation for each query in evaluation
		std::set<string>::iterator QueryNameSetIterator;
		for (QueryNameSetIterator = QueryNameSet.begin(); QueryNameSetIterator != QueryNameSet.end(); ++QueryNameSetIterator)
		{
			string queryName = *QueryNameSetIterator; 

			// Train - calculate the maximum value of all configuration, the current query not included
			string maxConfigForRm310 = configPerEvaluationIterator->second->getMaxAvgConfigForRm310NotIncludeQuery(queryName);
			string maxConfigForRm3100 = configPerEvaluationIterator->second->getMaxAvgConfigForRm3100NotIncludeQuery(queryName);

			// Test  - get the value for the query in the chosen configuration
			QueryResult* queryResConfigForRm310 = configPerEvaluationIterator->second->getQueryResultPerConfiguration(maxConfigForRm310,queryName);
			QueryResult* queryResConfigForRm3100 = configPerEvaluationIterator->second->getQueryResultPerConfiguration(maxConfigForRm3100,queryName);

			// save the query and the test result
			queryBestConfigForRm310.insert(std::make_pair<string,QueryResult*>(queryName,queryResConfigForRm310));
			queryBestConfigForRm3100.insert(std::make_pair<string,QueryResult*>(queryName,queryResConfigForRm3100));
		}

		// average the result of the CV process
		double avgRm310 = 0;
		std::map<string, QueryResult*>::iterator queryBestConfigForRm310Iterator;
		for (queryBestConfigForRm310Iterator = queryBestConfigForRm310.begin(); queryBestConfigForRm310Iterator != queryBestConfigForRm310.end(); ++queryBestConfigForRm310Iterator)
		{
			avgRm310 += queryBestConfigForRm310Iterator->second->rm310QueryValue;
		}
		avgRm310 = avgRm310 / queryBestConfigForRm310.size();

		double avgRm3100 = 0;
		std::map<string, QueryResult*>::iterator queryBestConfigForRm3100Iterator;
		for (queryBestConfigForRm3100Iterator = queryBestConfigForRm3100.begin(); queryBestConfigForRm3100Iterator != queryBestConfigForRm3100.end(); ++queryBestConfigForRm3100Iterator)
		{
			avgRm3100 += queryBestConfigForRm3100Iterator->second->rm3100QueryValue;
		}
		avgRm3100 = avgRm3100 / queryBestConfigForRm3100.size();

		// Save the CV result for the evaluation parameter.
		CvResult cvRes;
		cvRes.rm3100Value = avgRm3100 ;
		cvRes.rm310Value = avgRm310 ;
		cv.insert(std::make_pair<string, CvResult>(configPerEvaluationIterator->first,cvRes));
	}
	std::cout << "		End - Execute cross validation process ." << std::endl;

	std::cout << "		Start - Write file that contain for all evaluation parameter the CV for rm3100 an rm310 ." << std::endl;
	// write file that contain for all evaluation parameter the CV for rm3100 an rm310
	writeCVFile(file,&cv); 
	std::cout << "		End - Write file that contain for all evaluation parameter the CV for rm3100 an rm310 ." << std::endl;

	configPerEvaluation.configPerEvaluation.clear();

	#pragma endregion Execute cross validation process 
}


void getOriginalQueryNames(vector<string> *configurationFiles , vector<string> *originalQueryNames)
{
	int length = configurationFiles->size();
	for (int i = 0; i < length; i++)
	{
		string fileName = (*configurationFiles)[i];
		EvaluateResultFile file;
		std::set<string>* queriesNames = file.GetQueryNames(fileName);
		string originalQuery = file.getOriginalQuery(queriesNames);
		originalQueryNames->push_back(originalQuery);
	}

}

void getEvaluationsNames(string fileName , vector<string> *EvaluationNames)
{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return ;
		}

		std::set<string> evalSet;
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			std::string qcopy( line);
			std::istringstream oss (qcopy);

			std::string eval;
			oss >> eval;
			if ( eval.empty()) {
				return ;    
			}
			evalSet.insert(eval);
		}

		finput.close();
		EvaluationNames->resize(evalSet.size());
		std::copy(evalSet.begin(), evalSet.end(), EvaluationNames->begin());

}

void crossValidationForRM310and100_noAllToMemory(string file,vector<string> argv)
{
	// get for each configuration all the file that belong to him.
	std::map<string,vector<string>*> orderFiles;
	orderFileByConfig(&argv,Retrival,&orderFiles); 

	vector<string> originalQueryNames;
	getOriginalQueryNames(orderFiles.begin()->second, &originalQueryNames);
	vector<string> evaluationNames;
	getEvaluationsNames( *(orderFiles.begin()->second->begin()) , &evaluationNames);
	  
	#pragma region Execute cross validation process 
	std::cout << "		Start - Execute cross validation process ." << std::endl;
	std::map<string,CvResult> cv ;

	
	FuzzyConfigMap configMap;
	configMap.init(&orderFiles);

	int evaluationlength = evaluationNames.size();
	for (int i = 0; i < evaluationlength; i++)
	{
		string evalName = evaluationNames[i];

		std::map<string, QueryResult*> queryBestConfigForRm310 ;
		std::map<string, QueryResult*> queryBestConfigForRm3100;

		// Do cross validation for each query in evaluation
		int originalQuerylength = originalQueryNames.size();
		for (int j = 0; j < originalQuerylength; j++)
		{
			string queryName = originalQueryNames[j];
			
			// Train - calculate the maximum value of all configuration, the current query not included
			string maxConfigForRm310 = configMap.getMaxAvgConfigForRm310NotIncludeQuery(evalName,queryName);
			string maxConfigForRm3100 = configMap.getMaxAvgConfigForRm3100NotIncludeQuery(evalName,queryName);

			// Test  - get the value for the query in the chosen configuration
			QueryResult* queryResConfigForRm310 = configMap.getQueryResultPerConfiguration(evalName,queryName,maxConfigForRm310);
			QueryResult* queryResConfigForRm3100 = configMap.getQueryResultPerConfiguration(evalName,queryName,maxConfigForRm3100);

			// save the query and the test result
			queryBestConfigForRm310.insert(std::make_pair<string,QueryResult*>(queryName,queryResConfigForRm310));
			queryBestConfigForRm3100.insert(std::make_pair<string,QueryResult*>(queryName,queryResConfigForRm3100));

		}
		// average the result of the CV process
		double avgRm310 = 0;
		std::map<string, QueryResult*>::iterator queryBestConfigForRm310Iterator;
		for (queryBestConfigForRm310Iterator = queryBestConfigForRm310.begin(); queryBestConfigForRm310Iterator != queryBestConfigForRm310.end(); ++queryBestConfigForRm310Iterator)
		{
			avgRm310 += queryBestConfigForRm310Iterator->second->rm310QueryValue;
		}
		avgRm310 = avgRm310 / queryBestConfigForRm310.size();

		double avgRm3100 = 0;
		std::map<string, QueryResult*>::iterator queryBestConfigForRm3100Iterator;
		for (queryBestConfigForRm3100Iterator = queryBestConfigForRm3100.begin(); queryBestConfigForRm3100Iterator != queryBestConfigForRm3100.end(); ++queryBestConfigForRm3100Iterator)
		{
			avgRm3100 += queryBestConfigForRm3100Iterator->second->rm3100QueryValue;
		}
		avgRm3100 = avgRm3100 / queryBestConfigForRm3100.size();

		// Save the CV result for the evaluation parameter.
		CvResult cvRes;
		cvRes.rm3100Value = avgRm3100 ;
		cvRes.rm310Value = avgRm310 ;
		cv.insert(std::make_pair<string, CvResult>(evalName,cvRes));

	}
	std::cout << "		End - Execute cross validation process ." << std::endl;

	std::cout << "		Start - Write file that contain for all evaluation parameter the CV for rm3100 an rm310 ." << std::endl;
	// write file that contain for all evaluation parameter the CV for rm3100 an rm310
	writeCVFile(file,&cv); 
	std::cout << "		End - Write file that contain for all evaluation parameter the CV for rm3100 an rm310 ." << std::endl;


	#pragma endregion Execute cross validation process 
	
}


// Command Functions

void writeFileForPlot(int argc, char *argv[])
{
	string fileName = argv[2];
	string path_to_file = argv[3];

	EvaluateResultFile evaluateResultFile;
	evaluateResultFile.init(fileName);

	writeFileForPlotByWval(&evaluateResultFile,path_to_file);
	
	evaluateResultFile.Clear();
}

void writeFileForPlotAvgOfFiles(int argc, char *argv[])
{
	string path_to_file = argv[2];
	int numberOfFile = 0;
	AvgEvaluateResultFiles avgEvaluateResultFiles;

	vector<string> fileNameList;
	for(int i=3; i<argc ; ++i)
	{
		fileNameList.push_back(argv[i]);
	}
	avgEvaluateResultFiles.init();
	avgEvaluateResultFiles.AvgFilesToAvg(&fileNameList);
	writeFileForPlotByWval(&avgEvaluateResultFiles.evaluateResultFile,path_to_file);
	avgEvaluateResultFiles.clear();
}

void crossValidationForRM310and100(int argc, char *argv[])
{
	string path_to_file = argv[2]; 
	vector<string> evaluateFiles;

	for(int i=3; i<argc ; ++i)
	{
		evaluateFiles.push_back(argv[i]);	
	}
	std::cout << "Start - cross Validation For RM310 and Rm3100 : There is " << evaluateFiles.size() << " files. The output will write to :" << path_to_file <<  std::endl;
	crossValidationForRM310and100(path_to_file,evaluateFiles);
}


void writeFileForPlotForFdocs(string fDocs, string cvPath ,string pathToDir, vector<string> *fileNameList )
{
	std::cout << "Start - get for each configuration all the file that belong to him." << std::endl;
	// get for each configuration all the file that belong to him.
	std::map<string,vector<string>*> orderFiles;
	orderFileByConfig(fileNameList,Retrival,&orderFiles); 
	
	std::cout << "After we get : " << orderFiles.size() << std::endl;
	

	std::cout << "Start - for each configuration calculate avg ." << std::endl;
	// for each configuration calculate avg 
	//r esultPerConfig contain for each configuration is avg -> 5 plot for fbOrigWeight = 0,0.2,0.5,0.8,1
	std::map<string,AvgEvaluateResultFiles*> resultPerConfig; /// key=configuration , vlaue= average all files in the configuration
	std::vector<AvgEvaluateResultFiles*> *avgEvaluateResultFilesList = new std::vector<AvgEvaluateResultFiles*>();
	map<string,vector<string>*>::iterator orderFilesIt;
	for(orderFilesIt = orderFiles.begin(); orderFilesIt != orderFiles.end(); orderFilesIt++) 
	{
		std::cout << "        Config Name : " << orderFilesIt->first << " Number of files" << orderFilesIt->second->size() << std::endl;
		vector<string> *localfileNameList = orderFilesIt->second; // list of evaluate files
		AvgEvaluateResultFiles* avgEvaluateResultFiles = new AvgEvaluateResultFiles();
		avgEvaluateResultFiles->init();
	    avgEvaluateResultFiles->AvgFilesToAvg(localfileNameList);
		resultPerConfig.insert(std::make_pair<string,AvgEvaluateResultFiles*> (orderFilesIt->first, avgEvaluateResultFiles)); 
		avgEvaluateResultFilesList->push_back(avgEvaluateResultFiles);
	}

	std::cout << "Start - avg of all configuration files." << std::endl;
	// avg of all configuration files
	AvgEvaluateResultFiles avgEvaluateResultFilesAllConfigFiles;
   // avgEvaluateResultFilesAllConfigFiles.AvgFilesToAvg(fileNameList);
	avgEvaluateResultFilesAllConfigFiles.init();
	avgEvaluateResultFilesAllConfigFiles.addAvgEvaluateResultFiles(avgEvaluateResultFilesList);

	std::cout << "Start - read cv file." << std::endl;
	// rm3 cv data
	Rm3CVFile rm3CVForEvaluations ; 
	rm3CVForEvaluations.init(cvPath);

	
	std::cout << "Start - evaluation parameter per fdocs configuration" << std::endl;
	// evaluation parameter per fdocs configuration
	std::map<string,ConfigKeyResult*> evaluationPerfDocsResult;
	std::set<string> *evaluationNames = &avgEvaluateResultFilesAllConfigFiles.evaluateResultFile.evaluationNames;
	
	std::vector<string> output(evaluationNames->size());
	std::copy(evaluationNames->begin(), evaluationNames->end(), output.begin());

	for (int i = 0; i < output.size(); i++)
	{
		string evalName = output[i];
		ConfigKeyResult* evalfDocsResult = new ConfigKeyResult();
		evalfDocsResult->configKey = fDocs;
		QueryResult*  resultPerEval = avgEvaluateResultFilesAllConfigFiles.getEvaluateValuesInFile(evalName);

		evalfDocsResult->originalQueryValue = resultPerEval->originalQueryValue;
		evalfDocsResult->rm310QueryValue = resultPerEval->rm310QueryValue;
		evalfDocsResult->rm3100QueryValue = resultPerEval->rm3100QueryValue;

		Rm3CVFileLine rm3 = rm3CVForEvaluations.getRm3CVFileLine(evalName);
		evalfDocsResult->cvRm310QueryValue = rm3.rm310CVValue;
		evalfDocsResult->cvRm3100QueryValue = rm3.rm3100CVValue;

		std::map<string,AvgEvaluateResultFiles*>::iterator resultPerConfigIterator;
		for(resultPerConfigIterator = resultPerConfig.begin(); resultPerConfigIterator != resultPerConfig.end(); resultPerConfigIterator++) 
	    {
			string configName = resultPerConfigIterator->first;

			#pragma region extract from the file name the configuration name and the query name
			std::vector<std::string> splitedFile = split(configName, '_'); // the file path is split by "/" note.
			string config = splitedFile[splitedFile.size()-2] + "_" + splitedFile[splitedFile.size()-1]; 
			#pragma endregion 

			AvgEvaluateResultFiles* clustres = resultPerConfigIterator->second;
			QueryResult*  resultPerEvalAndConfig = clustres->getEvaluateValuesInFile(evalName);
			vector<double> *data = resultPerEvalAndConfig->getClustresValues();
			evalfDocsResult->listOfClusters.insert(std::make_pair<string,vector<double>*>(config,data));
		}
		evaluationPerfDocsResult.insert(std::make_pair<string,ConfigKeyResult*>(evalName,evalfDocsResult));
	}

	/*
	std::set<string>::const_iterator setIt;
	for(setIt = evaluationNames->begin(); setIt != evaluationNames->end(); setIt++) 
	{
		string evalName;
		cout <<  *setIt ;
		
	}*/
	
	std::cout << "Start - write files" << std::endl;
	std::map<string,ConfigKeyResult*>::iterator  evaluationPerfDocsResultIterator;
	for(evaluationPerfDocsResultIterator = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIterator != evaluationPerfDocsResult.end(); evaluationPerfDocsResultIterator++) 
	{
		string evalName = evaluationPerfDocsResultIterator->first;
		ConfigKeyResult* fDocsResultToPlot = evaluationPerfDocsResultIterator->second;
		string path = pathToDir + "/" + evalName + ".dat";
		fDocsResultToPlot->writeFileForPlot(path); 
	}
}


void writeFileForPlotForFdocsBestone(string fDocs , string cvPath , string pathToDir, vector<string> *fileNameList )
{
	std::cout << "Start - get for each configuration all the file that belong to him." << std::endl;
	// get for each configuration all the file that belong to him.
	std::map<string,vector<string>*> orderFiles;
	orderFileByConfig(fileNameList,Retrival,&orderFiles); 
	std::cout << "After we get : " << orderFiles.size() << std::endl;
	

	std::cout << "Start - for each configuration calculate average ." << std::endl;
	// for each configuration calculate avg 
	//r esultPerConfig contain for each configuration is avg -> 5 plot for fbOrigWeight = 0,0.2,0.5,0.8,1
	std::map<string,AvgEvaluateResultFiles*> resultPerConfig; /// key=configuration , vlaue= average all files in the configuration
	vector<AvgEvaluateResultFiles*> avgEvaluateResultFilesList;
	map<string,vector<string>*>::iterator orderFilesIt;
	for(orderFilesIt = orderFiles.begin(); orderFilesIt != orderFiles.end(); orderFilesIt++) 
	{
		std::cout << "        Configuration Name : " << orderFilesIt->first << " Number of files : " << orderFilesIt->second->size() << std::endl;
		vector<string> *localfileNameList = orderFilesIt->second; // list of evaluate files
		AvgEvaluateResultFiles* avgEvaluateResultFiles = new AvgEvaluateResultFiles();
		avgEvaluateResultFiles->init();
	    avgEvaluateResultFiles->AvgFilesToAvg(localfileNameList);
		resultPerConfig.insert(std::make_pair<string,AvgEvaluateResultFiles*> (orderFilesIt->first, avgEvaluateResultFiles)); 
		avgEvaluateResultFilesList.push_back(avgEvaluateResultFiles);
	}

	std::cout << "Start - average of all configuration files." << std::endl;
	// avg of all configuration files
	AvgEvaluateResultFiles avgEvaluateResultFilesAllConfigFiles;
   // avgEvaluateResultFilesAllConfigFiles.AvgFilesToAvg(fileNameList);
	avgEvaluateResultFilesAllConfigFiles.init();
	avgEvaluateResultFilesAllConfigFiles.addAvgEvaluateResultFiles(&(avgEvaluateResultFilesList));

	Rm3CVFile rm3CVForEvaluations ; 
	if(cvPath != "NULL")
	{
		std::cout << "Start - read Cross Validation file." << std::endl;
		// rm3 cv data
		rm3CVForEvaluations.init(cvPath);
	}
	


	std::map<string,ResultToPrint*> evaluationPerfDocsResult;
	std::set<string> *evaluationNames = &avgEvaluateResultFilesAllConfigFiles.evaluateResultFile.evaluationNames;
	
	std::vector<string> output(evaluationNames->size());
	std::copy(evaluationNames->begin(), evaluationNames->end(), output.begin());

	std::cout << "Start - calculate best per query average." << std::endl;
	std::cout << "ResultPerConfig size " << resultPerConfig.size() <<  std::endl;
	BestoneAvg bestoneAvg;
	bestoneAvg.create_bestone_average( &resultPerConfig );
	std::cout << "ResultPerConfig size " << resultPerConfig.size() <<  std::endl;
	
	std::cout << "Start - built a evaluation parameter per configuration (before write the file)" << std::endl;	
	for (int i = 0; i < output.size(); i++)
	{
		string evalName = output[i];
		ResultToPrint* evalfDocsResult = new ResultToPrint();
		
		QueryResult*  resultPerEval = avgEvaluateResultFilesAllConfigFiles.getEvaluateValuesInFile(evalName);

		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("originalQueryValue",resultPerEval->originalQueryValue));
		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("rm310",resultPerEval->rm310QueryValue));
		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("rm3100",resultPerEval->rm3100QueryValue));

		double bestoneAvgRm310, bestoneAvgRm3100;
		bestoneAvg.getBestoneAvg(evalName , &(bestoneAvgRm310) , &(bestoneAvgRm3100));
		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("BestPerQueryAverageRm3-10",bestoneAvgRm310));
		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("BestPerQueryAverageRm3-100",bestoneAvgRm3100));
		
		if(cvPath != "NULL")
		{
			Rm3CVFileLine rm3 = rm3CVForEvaluations.getRm3CVFileLine(evalName);
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("CVRm3-10",rm3.rm310CVValue));
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("CVRm3-100",rm3.rm3100CVValue));
		}

		// for each configuration
		std::map<string,AvgEvaluateResultFiles*>::iterator resultPerConfigIterator;
		double maxRm10,maxRm100;
		maxRm10 = maxRm100 = std::numeric_limits<double>::min();
		std::cout << "ResultPerConfig size " << resultPerConfig.size() <<  std::endl;	
		for(resultPerConfigIterator = resultPerConfig.begin(); resultPerConfigIterator != resultPerConfig.end(); resultPerConfigIterator++) 
	    {
			string configName = resultPerConfigIterator->first;

			#pragma region extract from the file name the configuration name and the query name
			std::vector<std::string> splitedFile = split(configName, '_'); // the file path is split by "/" note.
			bool writeConfig = false;
			string config = "";
			for (int w = 0; w < (splitedFile.size() - 1) ; w++)
			{
				if(writeConfig)
				{
					config += splitedFile[w+1] + "_";
				}
				if( splitedFile[w] == "muRule")
				{
					writeConfig = true;
				}
			}
			#pragma endregion 

			AvgEvaluateResultFiles* clustres = resultPerConfigIterator->second;
			QueryResult*  resultPerEvalAndConfig = clustres->getEvaluateValuesInFile(evalName);
			std::cout << "QueryResult data - " << resultPerEvalAndConfig->toString() << std::endl;	

			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>(config,resultPerEvalAndConfig->getClustresValues()));
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>(config + "_" + "originalQueryValue",resultPerEvalAndConfig->originalQueryValue));
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>(config + "_" + "rm310",resultPerEvalAndConfig->rm310QueryValue));
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>(config + "_" + "rm3100",resultPerEvalAndConfig->rm3100QueryValue));

			maxRm10 = maxRm10 < resultPerEvalAndConfig->rm310QueryValue ? resultPerEvalAndConfig->rm310QueryValue : maxRm10;
			maxRm100 = maxRm100 < resultPerEvalAndConfig->rm3100QueryValue ? resultPerEvalAndConfig->rm3100QueryValue : maxRm100;
			
		}
		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("max_rm310",maxRm10));
		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("max_rm3100",maxRm100));

		evaluationPerfDocsResult.insert(std::make_pair<string,ResultToPrint*>(evalName,evalfDocsResult));
	}
	
	std::cout << "Start - write files" << std::endl;
	std::map<string,ResultToPrint*>::iterator  evaluationPerfDocsResultIterator;
	for(evaluationPerfDocsResultIterator = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIterator != evaluationPerfDocsResult.end(); evaluationPerfDocsResultIterator++) 
	{
		string evalName = evaluationPerfDocsResultIterator->first;
		ResultToPrint* fDocsResultToPlot = evaluationPerfDocsResultIterator->second;
		string path = pathToDir + "/" + evalName + ".dat";
		std::cout << "     write files for : " << evalName + ".dat   ,   " << fDocsResultToPlot->toString()<< std::endl;
		fDocsResultToPlot->writeFileForPlot(path); 
	}
}


void writeFileForPlotForW2V(string V2vModel, string cvPath ,string pathToDir, vector<string> *fileNameList )
{
	std::cout << "Start - get for each configuration all the file that belong to him." << std::endl;
	// get for each configuration all the file that belong to him.
	std::map<string,vector<string>*> orderFiles;
	orderFileByConfig(fileNameList,W2vModel,&orderFiles); 
	
	std::cout << "After we get : " << orderFiles.size() << std::endl;
	

	std::cout << "Start - for each configuration calculate avg ." << std::endl;
	// for each configuration calculate avg 
	//r esultPerConfig contain for each configuration is avg -> 5 plot for fbOrigWeight = 0,0.2,0.5,0.8,1
	std::map<string,AvgEvaluateResultFiles*> resultPerConfig; /// key=configuration , vlaue= average all files in the configuration
	vector<AvgEvaluateResultFiles*> avgEvaluateResultFilesList;
	map<string,vector<string>*>::iterator orderFilesIt;
	for(orderFilesIt = orderFiles.begin(); orderFilesIt != orderFiles.end(); orderFilesIt++) 
	{
		std::cout << "        W2v Model Name : " << orderFilesIt->first << " Number of files" << orderFilesIt->second->size() << std::endl;
		vector<string> *localfileNameList = orderFilesIt->second; // list of evaluate files
		AvgEvaluateResultFiles* avgEvaluateResultFiles = new AvgEvaluateResultFiles();
		avgEvaluateResultFiles->init();
	    avgEvaluateResultFiles->AvgFilesToAvg(localfileNameList);
		resultPerConfig.insert(std::make_pair<string,AvgEvaluateResultFiles*> (orderFilesIt->first, avgEvaluateResultFiles)); 
		avgEvaluateResultFilesList.push_back(avgEvaluateResultFiles);
	}

	std::cout << "Start - avg of all configuration files." << std::endl;
	// avg of all configuration files
	AvgEvaluateResultFiles avgEvaluateResultFilesAllConfigFiles;
   // avgEvaluateResultFilesAllConfigFiles.AvgFilesToAvg(fileNameList);
	avgEvaluateResultFilesAllConfigFiles.init();
	avgEvaluateResultFilesAllConfigFiles.addAvgEvaluateResultFiles(&avgEvaluateResultFilesList);

	std::cout << "Start - read cv file." << std::endl;
	// rm3 cv data
	Rm3CVFile rm3CVForEvaluations ; 
	rm3CVForEvaluations.init(cvPath);

	
	std::cout << "Start - evaluation parameter per fdocs configuration" << std::endl;
	// evaluation parameter per fdocs configuration
	std::map<string,ConfigKeyResult*> evaluationPerfDocsResult;
	std::set<string> *evaluationNames = &avgEvaluateResultFilesAllConfigFiles.evaluateResultFile.evaluationNames;
	
	std::vector<string> output(evaluationNames->size());
	std::copy(evaluationNames->begin(), evaluationNames->end(), output.begin());

	std::cout << "Start - evaluation name size " << output.size() << std::endl;

	for (int i = 0; i < output.size(); i++)
	{
		string evalName = output[i];
		
		ConfigKeyResult* evalfDocsResult = new ConfigKeyResult();
		evalfDocsResult->configKey = V2vModel;
		QueryResult*  resultPerEval = avgEvaluateResultFilesAllConfigFiles.getEvaluateValuesInFile(evalName);

		evalfDocsResult->originalQueryValue = resultPerEval->originalQueryValue;
		evalfDocsResult->rm310QueryValue = resultPerEval->rm310QueryValue;
		evalfDocsResult->rm3100QueryValue = resultPerEval->rm3100QueryValue;

		Rm3CVFileLine rm3 = rm3CVForEvaluations.getRm3CVFileLine(evalName);
		evalfDocsResult->cvRm310QueryValue = rm3.rm310CVValue;
		evalfDocsResult->cvRm3100QueryValue = rm3.rm3100CVValue;

		std::map<string,AvgEvaluateResultFiles*>::iterator resultPerConfigIterator;
		for(resultPerConfigIterator = resultPerConfig.begin(); resultPerConfigIterator != resultPerConfig.end(); resultPerConfigIterator++) 
	    {
			string configName = resultPerConfigIterator->first;

		/*	#pragma region extract from the file name the configuration name and the query name
			std::vector<std::string> splitedFile = split(configName, '_'); // the file path is split by "/" note.
			string config = splitedFile[splitedFile.size()-2] + "_" + splitedFile[splitedFile.size()-1]; 
			#pragma endregion */

			string config = configName;

			AvgEvaluateResultFiles* clustres = resultPerConfigIterator->second;
			QueryResult*  resultPerEvalAndConfig = clustres->getEvaluateValuesInFile(evalName);
			vector<double> *data = resultPerEvalAndConfig->getClustresValues();
			evalfDocsResult->listOfClusters.insert(std::make_pair<string,vector<double>*>(config,data));
		}
		evaluationPerfDocsResult.insert(std::make_pair<string,ConfigKeyResult*>(evalName,evalfDocsResult));
	}
	
	std::cout << "Start - write files" << std::endl;
	std::map<string,ConfigKeyResult*>::iterator  evaluationPerfDocsResultIterator;
	for(evaluationPerfDocsResultIterator = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIterator != evaluationPerfDocsResult.end(); evaluationPerfDocsResultIterator++) 
	{
		string evalName = evaluationPerfDocsResultIterator->first;
		ConfigKeyResult* fDocsResultToPlot = evaluationPerfDocsResultIterator->second;
		string path = pathToDir + "/" + evalName + ".dat";

		std::cout << " Create file " << path << std::endl;

		fDocsResultToPlot->writeFileForPlot(path); 
	}

	resultPerConfig.clear();
	avgEvaluateResultFilesList.clear();

}


// write a plot for each configuration
// cvPath - path to vc file for the w2v model
// fileNameList - list of evaluate file ( all file is with the same fbDocs)
void writeFileForPlotForConfig(int argc, char *argv[], ConfigurationType type)
{
	string configKey = argv[2];
	string cvPath = argv[3];
	string pathToDir = argv[4];
	vector<string> fileNameList;
	for(int i=5; i<argc ; ++i)
	{
		fileNameList.push_back(argv[i]);
	}
	
	std::cout << "Create plot for: \nconfigKey : " << configKey << "\n The cv file : " << cvPath << "\n numbers of evaluate files : " << fileNameList.size() << std::endl;
	
	switch (type)
	{
	case Index:
		break;
	case W2vModel:
		writeFileForPlotForW2V( configKey,  cvPath , pathToDir, &fileNameList );
		break;
	case Retrival:
		writeFileForPlotForFdocs( configKey,  cvPath , pathToDir, &fileNameList );
		break;
	default:
		break;
	}
}
	

// write a plot for each configuration
// cvPath - path to vc file for the w2v model
// fileNameList - list of evaluate file ( all file is with the same fbDocs)
void writeFileForPlotForConfigWithBestoneAvg(int argc, char *argv[], ConfigurationType type)
{
	string configKey = argv[2];
	string cvPath = argv[3];
	string pathToDir = argv[4];
	vector<string> fileNameList;
	for(int i=5; i<argc ; ++i)
	{
		fileNameList.push_back(argv[i]);
	}
	
	std::cout << "Create plot for: \nconfigKey : " << configKey << "\n numbers of evaluate files : " << fileNameList.size() << std::endl;
	
	switch (type)
	{
	case Index:
		break;
	case W2vModel:
		break;
	case Retrival:
		writeFileForPlotForFdocsBestone( configKey ,cvPath , pathToDir, &fileNameList );
		break;
	default:
		break;
	}
}






















/* Old Implementations'  */ 


// not use
QueryResult* ReadFile(string fileName)
{
	QueryResult* file = new QueryResult();
	ifstream finput;

	finput.open ( fileName.c_str() ); 

	if (!finput.good())
	{
		std::cerr << "Can't open file: " << fileName << std::endl;
		return NULL;
	}


	bool isOriginalQuery = true;

	while  ( finput.good() )
	{
		string line;
		getline (finput, line);
		if(line.size() == 0 ) continue;

		std::string qcopy( line);
		std::istringstream oss (qcopy);

		std::string map;
		oss >> map;
		if ( map.empty()) {
			return NULL;    
		}

		std::string queryName;
		oss >> queryName;
		if ( queryName.empty()) {
			return NULL;    
		}

		if(queryName.compare("all") == 0)
		{
			continue;
		}

		std::string mapValueStr;
		oss >> mapValueStr;
		if ( mapValueStr.empty()) {
			return NULL;    
		}

		double mapValue = atof(mapValueStr.c_str());

		if(isOriginalQuery)
		{
			file->queryName = queryName;
			file->originalQueryValue = mapValue;
			isOriginalQuery = false;
			continue;
		}

		if(queryName.compare(file->queryName + "_rm3_10") == 0)
		{
			file->rm310QueryValue = mapValue;
			continue;
		}

		if(queryName.compare(file->queryName + "_rm3_100") == 0)
		{
			file->rm3100QueryValue = mapValue;
			continue;
		}
		file->result.push_back(std::make_pair(queryName, mapValue ) );	
	}

	std::sort(file->result.begin(), file->result.end(), sort_pred);

	finput.close();
	return file;

}