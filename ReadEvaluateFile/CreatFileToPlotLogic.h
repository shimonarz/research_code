#ifndef CreatFileToPlotLogic_H
#define CreatFileToPlotLogic_H

#include <string.h>
#include <set>
#include <string>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>      // std::accumulate
#include <fstream>
#include <iostream>
#include <sstream>
#include <map>
#include <limits> 

using std::ifstream;
using std::ofstream;
using std::vector;
using std::string;
using std::map;
using std::cout;
using std::cerr;

#define SSTR( x ) static_cast< std::ostringstream & >( \
	( std::ostringstream() << std::dec << x ) ).str()

inline bool ends_with(std::string const & value, std::string const & ending)
{
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

inline string trimEnd(std::string const & str,std::string const & ending)
{
	size_t endpos = str.find_last_not_of(ending);
	if( string::npos != endpos )
	{
		return str.substr( 0, endpos+1 );
	}
	return str;
}

// generic function 
inline void split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
}

inline std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}


enum ConfigurationType
{
	Index,
	W2vModel,
	Retrival
};

// function for sort function
bool sort_pred(const std::pair<string,double>& left, const std::pair<string,double>& right);

typedef struct EvaluatResultFileLine
{
	string evalName;
	string queryName;
	double evalValue;

	EvaluatResultFileLine* clone()
	{
		EvaluatResultFileLine *newLine = new EvaluatResultFileLine();
		newLine->evalName = evalName;
		newLine->queryName = queryName;
		newLine->evalValue = evalValue;
		return newLine;

	}

}EvaluatResultFileLine;

typedef struct QueryResult
{
	string queryName;
	double originalQueryValue;
	double rm310QueryValue;
	double rm3100QueryValue;
	vector< std::pair<string,double> > result;

	void init(string name = NULL)
	{
		queryName = name;
		originalQueryValue = 0;
		rm3100QueryValue = 0;
		rm310QueryValue = 0;
	}

	// set for query strut the contain all query parameters
	// resultFileLineVec - define query the contain: original, rm310, rm3100, clusters
	// queryNme - current query name
	void init(vector<EvaluatResultFileLine*> * resultFileLineVec,  string queryNme)
	{

		for( int i = 0 ; i< resultFileLineVec->size(); ++i)
		{
			EvaluatResultFileLine* resultFileLine = (*resultFileLineVec)[i];

			if(resultFileLine->queryName.compare("all") == 0)
			{
				continue;
			}

			if(resultFileLine->queryName.compare(queryNme) == 0)
			{
				queryName = resultFileLine->queryName;
				originalQueryValue = resultFileLine->evalValue;
				continue;
			}

			if(resultFileLine->queryName.compare(queryNme+ "_rm3_10") == 0)
			{
				rm310QueryValue = resultFileLine->evalValue;
				continue;
			}

			if(resultFileLine->queryName.compare(queryNme+ "_rm3_100") == 0)
			{
				rm3100QueryValue = resultFileLine->evalValue;
				continue;
			}

			result.push_back(std::make_pair(resultFileLine->queryName, resultFileLine->evalValue ) );	
		}

		std::sort(result.begin(), result.end(), sort_pred);
	}

	void addToSum(QueryResult* value)
	{
		originalQueryValue += value->originalQueryValue;
		rm3100QueryValue += value->rm3100QueryValue;
		rm310QueryValue += value->rm310QueryValue;

		vector< std::pair<string,double> > *vec = &value->result;
		for(int j=0 ; j<vec->size() ; ++j)
		{
			result[j].second += (*vec)[j].second ;		
		}
	}

	void normalization(int numberOfFile)
	{
		originalQueryValue /= numberOfFile;
		rm3100QueryValue /= numberOfFile;
		rm310QueryValue /= numberOfFile;

		for(int j=0 ; j<result.size() ; ++j)
		{
			result[j].second /= numberOfFile;
		}
	}

	vector<double>* getClustresValues()
	{
		vector<double> *data = new vector<double>();
		for (int i = 0; i < result.size(); i++)
		{
			data->push_back(result[i].second);
		}
		return data;
	}

	QueryResult* Copy()
	{
		QueryResult* value = new QueryResult();
		value->queryName = queryName;
		value->originalQueryValue = originalQueryValue;
		value->rm310QueryValue = rm310QueryValue;
		value->rm3100QueryValue = rm3100QueryValue;

		for (int i = 0; i < result.size(); i++)
		{
			value->result.push_back(std::make_pair(result[i].first, result[i].second ) );
		}
		
		return value;
	}

	string toString()
	{
		return "Query-Result for Query - " + queryName + 
			" : OriginalQueryValue=" + SSTR(originalQueryValue) + 
			", rm310QueryValue=" + SSTR(rm310QueryValue) + 
			", rm3100QueryValue=" + SSTR(rm3100QueryValue) +
			", number of clusters =" + SSTR(result.size()) 
			;
	}

}QueryResult;

typedef struct EvaluateResultFile
{
	vector<EvaluatResultFileLine*> lines; // vector of all line in the file
	std::map<string,vector<EvaluatResultFileLine*>*> evalMapFile; // key= evaluation name , value= all the line with the same evaluation name;
	std::map<string, QueryResult*> evalInFile; // // key= evaluation name , value= all the data that connect to the evaluation is structure order;
	std::set<string> queriesNames;
	std::set<string> evaluationNames;

	void init(string fileName)
	{
		ReadEvaluateFileToVector(fileName);
		mapEval();
		setQueryResult();
	}

	void Clear()
	{
		std::map<string,vector<EvaluatResultFileLine*>*>::iterator it;
		for(it = evalMapFile.begin() ; it != evalMapFile.end() ; it ++)
		{
			it->second->clear();
			delete it->second;
		}
		evalMapFile.clear();
		
		for (int i = 0; i < lines.size(); i++)
		{
			delete lines[i];
		}
		lines.clear();
		std::map<string,QueryResult*>::iterator it2;
		for(it2 = evalInFile.begin() ; it2 != evalInFile.end() ; it2 ++)
		{
			delete it2->second;
		}
		evalInFile.clear();

		evaluationNames.clear();
		queriesNames.clear();
	}

	// read evaluate file (file that generate in trac_eval)
	void ReadEvaluateFileToVector(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return ;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			EvaluatResultFileLine* evalline = new EvaluatResultFileLine();

			std::string qcopy( line);
			std::istringstream oss (qcopy);

			std::string eval;
			oss >> eval;
			if ( eval.empty()) {
				return ;    
			}

			std::string queryName;
			oss >> queryName;
			if ( queryName.empty()) {
				return ;    
			}

			std::string evalValueStr;
			oss >> evalValueStr;
			if ( evalValueStr.empty()) {
				return ;    
			}

			double evalValue = atof(evalValueStr.c_str());

			evalline->evalName = eval;
			evalline->queryName = queryName;
			evalline->evalValue = evalValue;

			lines.push_back(evalline);

			queriesNames.insert(queryName);
		}

		finput.close();
	}

	std::set<string>* GetQueryNames(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return NULL;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			EvaluatResultFileLine* evalline = new EvaluatResultFileLine();

			std::string qcopy( line);
			std::istringstream oss (qcopy);

			std::string eval;
			oss >> eval;
			if ( eval.empty()) {
				return NULL;    
			}

			std::string queryName;
			oss >> queryName;
			if ( queryName.empty()) {
				return NULL;    
			}
			evalline->queryName = queryName;
			queriesNames.insert(queryName);
		}
		finput.close();
		return &queriesNames;
	}

	// return for each evaluation parameter (map, P@10..) vector that contain all queries
	// evalFile - vector that contain whole file - contain all queries
	void mapEval()
	{
		int len = lines.size();
		for(int i=0 ; i < len ; ++i)
		{
			EvaluatResultFileLine* fileLine = lines[i];
			if(fileLine->queryName == "all")
			{
				continue;
			}

			string evalName = fileLine->evalName;
			evaluationNames.insert(evalName);

			std::map<string,vector<EvaluatResultFileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
			if( pos == evalMapFile.end()) // not found
			{
				vector<EvaluatResultFileLine*>* newVector = new vector<EvaluatResultFileLine*>();
				newVector->push_back(fileLine);
				evalMapFile.insert(std::make_pair<string,vector<EvaluatResultFileLine*>*> (evalName, newVector));
			}
			else
			{
				vector<EvaluatResultFileLine*>* vec = pos->second;
				vec->push_back(fileLine);
			}
		}
	}

	void add(string evalName , QueryResult* data)
	{
		evaluationNames.insert(evalName);

		QueryResult* newQueryResult= new QueryResult();
		newQueryResult->queryName = evalName;
		newQueryResult->originalQueryValue = data->originalQueryValue;
		newQueryResult->rm3100QueryValue = data->rm3100QueryValue;
		newQueryResult->rm310QueryValue = data->rm310QueryValue;

		vector< std::pair<string,double> > *result = &data->result;
		for(int j=0 ; j<result->size() ; ++j)
		{
			newQueryResult->result.push_back(std::make_pair( SSTR( (j + 1) ).c_str() , (*result)[j].second ));
		}
		evalInFile.insert(std::make_pair<string,QueryResult*> (evalName, newQueryResult));
		
	}

	void addAndSum(string evalName , QueryResult* data)
	{
		std::map<string,QueryResult*>::const_iterator  pos = evalInFile.find(evalName);
		if( pos == evalInFile.end()) // not found
		{
			add( evalName ,  data);
		}
		else
		{
			QueryResult* queryResultFromMap = pos->second;
			queryResultFromMap->addToSum(data);
		}
	}

	void addAndSum(EvaluateResultFile *evaluateResultFile)
	{
		typedef std::map<string,QueryResult*>::iterator it_type;
		for(it_type iterator = evaluateResultFile->evalInFile.begin(); iterator != evaluateResultFile->evalInFile.end(); iterator++) 
		{
			string evalName = iterator->first;
			QueryResult* evalValues = iterator->second;
			addAndSum(evalName,evalValues);
		}
	}

	QueryResult* getEvaluateValuesInFile(string evalName)
	{
		std::map< string , QueryResult* >::const_iterator  pos = evalInFile.find(evalName);
		if( pos == evalInFile.end()) // not found
		{
			printf("Evaluate not found in map : %s" , evalName.c_str());
			return NULL;
		}
		else
		{
			return pos->second;
		}

	}

	double getEvaluateValuesInFile(string evalName,string queryName, bool *isOk)
	{
		*isOk = false;
		std::map<string,vector<EvaluatResultFileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
		if( pos == evalMapFile.end()) // not found
		{
			printf("Evaluate not found in map : %s" , evalName.c_str());
			
			return -1;
		}
		else
		{
			vector<EvaluatResultFileLine*>* vec = pos->second;
			int len = vec->size();
			for(int i =0 ; i<len ; i++)
			{
				EvaluatResultFileLine* line = (*vec)[i];
				if( line->queryName == queryName)
				{
					*isOk = true;
					return line->evalValue;
				}
			}
			// printf("Query not found in map : %s" , queryName.c_str());
			return -1;
		}

	}
	
	void normalizationAll(int numberOfFile)
	{
		typedef std::map<string,QueryResult*>::iterator it_type;
		for(it_type iterator = evalInFile.begin(); iterator != evalInFile.end(); iterator++) 
		{
			QueryResult* queryResultFromMap = iterator->second;
			queryResultFromMap->normalization(numberOfFile);
		}
	}

	void setQueryResult()
	{
		typedef std::map<string,vector<EvaluatResultFileLine*>*>::iterator it_type;
		for(it_type iterator = evalMapFile.begin(); iterator != evalMapFile.end(); iterator++) 
		{
			vector<EvaluatResultFileLine*> *resultFileLine = iterator->second;
			EvaluatResultFileLine* min = getMin(resultFileLine);
			string queryName = min->queryName;
			QueryResult *evalRes = new QueryResult();;
			evalRes->init(resultFileLine,  queryName); 
			evalInFile.insert( std::make_pair<string,QueryResult*>(iterator->first,evalRes));
		}
	}

	EvaluatResultFileLine* getMin(vector<EvaluatResultFileLine*> *resultFileLine)
	{
		vector<EvaluatResultFileLine*>::iterator it;
		EvaluatResultFileLine* min = (*resultFileLine)[0];
		for(it = resultFileLine->begin(); it != resultFileLine->end(); it++) 
		{
			if(min->queryName.size() > (*it)->queryName.size())
			{
				min = *it;
			}
		}
		return min;
	}

	string getOriginalQuery(std::set<string> *queries)
	{
		string min = *(queries->begin());
		std::set<string>::iterator it;
		for(it = queries->begin(); it != queries->end(); it++) 
		{
			if(min.size() > it->size())
			{
				min = *it;
			}
		}
		return min;
	}
	
	std::set<string>* getQueriesName()
	{
		return &queriesNames;
	}

	std::set<string>* getEvaluationNames()
	{
		return &evaluationNames;
	}

	vector<EvaluatResultFileLine*>* getEvaluatResultFileLine()
	{
		return &lines;
	}

}EvaluateResultFile;

typedef struct EvaluateResultFileForOneEvaluationParamter
{
	vector<EvaluatResultFileLine*> lines; // vector of all line in the file
	std::map<string,vector<EvaluatResultFileLine*>*> evalMapFile; // key= evaluation name , value= all the line with the same evaluation name;
	std::map<string, QueryResult*> evalInFile; // // key= evaluation name , value= all the data that connect to the evaluation is structure order;
	std::set<string> queriesNames;
	std::set<string> evaluationNames;

	void init(string fileName, string evalName)
	{
		ReadEvaluateFileToVector(fileName,evalName);
		mapEval();
		setQueryResult();
	}

	void Clear()
	{
		std::map<string,vector<EvaluatResultFileLine*>*>::iterator it;
		for(it = evalMapFile.begin() ; it != evalMapFile.end() ; it ++)
		{
			it->second->clear();
			delete it->second;
		}
		evalMapFile.clear();
		
		for (int i = 0; i < lines.size(); i++)
		{
			delete lines[i];
		}
		lines.clear();
		std::map<string,QueryResult*>::iterator it2;
		for(it2 = evalInFile.begin() ; it2 != evalInFile.end() ; it2 ++)
		{
			delete it2->second;
		}
		evalInFile.clear();

		evaluationNames.clear();
		queriesNames.clear();
	}

	// read evaluate file (file that generate in trac_eval)
	void ReadEvaluateFileToVector(string fileName,string evalName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return ;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			EvaluatResultFileLine* evalline = new EvaluatResultFileLine();

			std::string qcopy( line);
			std::istringstream oss (qcopy);

			std::string eval;
			oss >> eval;
			if ( eval.empty()) {
				return ;    
			}

			if(eval != evalName)
			{
				continue;
			}

			std::string queryName;
			oss >> queryName;
			if ( queryName.empty()) {
				return ;    
			}

			std::string evalValueStr;
			oss >> evalValueStr;
			if ( evalValueStr.empty()) {
				return ;    
			}

			double evalValue = atof(evalValueStr.c_str());

			evalline->evalName = eval;
			evalline->queryName = queryName;
			evalline->evalValue = evalValue;

			lines.push_back(evalline);

			queriesNames.insert(queryName);
		}

		finput.close();
	}

	// return for each evaluation parameter (map, P@10..) vector that contain all queries
	// evalFile - vector that contain whole file - contain all queries
	void mapEval()
	{
		int len = lines.size();
		for(int i=0 ; i < len ; ++i)
		{
			EvaluatResultFileLine* fileLine = lines[i];
			if(fileLine->queryName == "all")
			{
				continue;
			}

			string evalName = fileLine->evalName;
			evaluationNames.insert(evalName);

			std::map<string,vector<EvaluatResultFileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
			if( pos == evalMapFile.end()) // not found
			{
				vector<EvaluatResultFileLine*>* newVector = new vector<EvaluatResultFileLine*>();
				newVector->push_back(fileLine);
				evalMapFile.insert(std::make_pair<string,vector<EvaluatResultFileLine*>*> (evalName, newVector));
			}
			else
			{
				vector<EvaluatResultFileLine*>* vec = pos->second;
				vec->push_back(fileLine);
			}
		}
	}

	void add(string evalName , QueryResult* data)
	{
		evaluationNames.insert(evalName);

		QueryResult* newQueryResult= new QueryResult();
		newQueryResult->queryName = evalName;
		newQueryResult->originalQueryValue = data->originalQueryValue;
		newQueryResult->rm3100QueryValue = data->rm3100QueryValue;
		newQueryResult->rm310QueryValue = data->rm310QueryValue;

		vector< std::pair<string,double> > *result = &data->result;
		for(int j=0 ; j<result->size() ; ++j)
		{
			newQueryResult->result.push_back(std::make_pair( SSTR( (j + 1) ).c_str() , (*result)[j].second ));
		}
		evalInFile.insert(std::make_pair<string,QueryResult*> (evalName, newQueryResult));
		
	}

	void addAndSum(string evalName , QueryResult* data)
	{
		std::map<string,QueryResult*>::const_iterator  pos = evalInFile.find(evalName);
		if( pos == evalInFile.end()) // not found
		{
			add( evalName ,  data);
		}
		else
		{
			QueryResult* queryResultFromMap = pos->second;
			queryResultFromMap->addToSum(data);
		}
	}

	void addAndSum(EvaluateResultFile *evaluateResultFile)
	{
		typedef std::map<string,QueryResult*>::iterator it_type;
		for(it_type iterator = evaluateResultFile->evalInFile.begin(); iterator != evaluateResultFile->evalInFile.end(); iterator++) 
		{
			string evalName = iterator->first;
			QueryResult* evalValues = iterator->second;
			addAndSum(evalName,evalValues);
		}
	}

	QueryResult* getEvaluateValuesInFile(string evalName)
	{
		std::map< string , QueryResult* >::const_iterator  pos = evalInFile.find(evalName);
		if( pos == evalInFile.end()) // not found
		{
			printf("Evaluate not found in map : %s" , evalName.c_str());
			return NULL;
		}
		else
		{
			return pos->second;
		}

	}

	double getEvaluateValuesInFile(string evalName,string queryName, bool *isOk)
	{
		*isOk = false;
		std::map<string,vector<EvaluatResultFileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
		if( pos == evalMapFile.end()) // not found
		{
			printf("Evaluate not found in map : %s" , evalName.c_str());
			
			return -1;
		}
		else
		{
			vector<EvaluatResultFileLine*>* vec = pos->second;
			int len = vec->size();
			for(int i =0 ; i<len ; i++)
			{
				EvaluatResultFileLine* line = (*vec)[i];
				if( line->queryName == queryName)
				{
					*isOk = true;
					return line->evalValue;
				}
			}
			// printf("Query not found in map : %s" , queryName.c_str());
			return -1;
		}

	}
	
	void normalizationAll(int numberOfFile)
	{
		typedef std::map<string,QueryResult*>::iterator it_type;
		for(it_type iterator = evalInFile.begin(); iterator != evalInFile.end(); iterator++) 
		{
			QueryResult* queryResultFromMap = iterator->second;
			queryResultFromMap->normalization(numberOfFile);
		}
	}

	void setQueryResult()
	{
		typedef std::map<string,vector<EvaluatResultFileLine*>*>::iterator it_type;
		for(it_type iterator = evalMapFile.begin(); iterator != evalMapFile.end(); iterator++) 
		{
			vector<EvaluatResultFileLine*> *resultFileLine = iterator->second;
			EvaluatResultFileLine* min = getMin(resultFileLine);
			string queryName = min->queryName;
			QueryResult *evalRes = new QueryResult();;
			evalRes->init(resultFileLine,  queryName); 
			evalInFile.insert( std::make_pair<string,QueryResult*>(iterator->first,evalRes));
		}
	}

	EvaluatResultFileLine* getMin(vector<EvaluatResultFileLine*> *resultFileLine)
	{
		vector<EvaluatResultFileLine*>::iterator it;
		EvaluatResultFileLine* min = (*resultFileLine)[0];
		for(it = resultFileLine->begin(); it != resultFileLine->end(); it++) 
		{
			if(min->queryName.size() > (*it)->queryName.size())
			{
				min = *it;
			}
		}
		return min;
	}
	
	std::set<string>* getQueriesName()
	{
		return &queriesNames;
	}

	std::set<string>* getEvaluationNames()
	{
		return &evaluationNames;
	}

	vector<EvaluatResultFileLine*>* getEvaluatResultFileLine()
	{
		return &lines;
	}

}EvaluateResultFileForOneEvaluationParamter;

typedef struct AvgEvaluateResultFiles
{
	std::set<string> queriesNames;
	std::set<string> evaluationNames;

	vector<EvaluateResultFile*> EvaluateResultFiles;
	EvaluateResultFile evaluateResultFile;

	bool isInitialized;

	void init()
	{
		isInitialized = false;
	}

	#pragma region Add Path to files
	
	void AvgFilesToAvg(vector<string> *fileNameList)
	{
		int numberOfFile = fileNameList->size();
		for(int i=0; i<numberOfFile ; ++i)
		{
			string fileName = (*fileNameList)[i];
			AddFileToAvg(fileName);
		}
		normalizationAll(numberOfFile);
	}

	void AddFileToAvg(string fileName)
	{
		if(isInitialized == false)
		{
			evaluateResultFile.init(fileName);
			addData(&evaluateResultFile);
			EvaluateResultFiles.push_back(&evaluateResultFile);
			isInitialized = true;
		}
		else
		{
			EvaluateResultFile *localEvaluateResultFile = new EvaluateResultFile();
			localEvaluateResultFile->init(fileName);
			evaluateResultFile.addAndSum(localEvaluateResultFile);
			addData(localEvaluateResultFile);
			EvaluateResultFiles.push_back(localEvaluateResultFile);
		}
	}

	void addData(EvaluateResultFile *evaluateResultFiles)
	{
		std::set<string> *queriesNamesFormFile = evaluateResultFiles->getQueriesName();
		std::set<string>::iterator it;
		for (it = queriesNamesFormFile->begin(); it != queriesNamesFormFile->end(); ++it)
		{
			string q = *it; // Note the "*" here
			queriesNames.insert(q);
		}

		std::set<string> *evaluationNamesFormFile = evaluateResultFiles->getEvaluationNames();
		for (it = evaluationNamesFormFile->begin(); it != evaluationNamesFormFile->end(); ++it)
		{
			string q = *it; // Note the "*" here
			evaluationNames.insert(q);
		}
	}
	
	#pragma endregion Execute cross validation process 


	#pragma region Add AvgEvaluateResultFiles to files

	void addAvgEvaluateResultFiles(std::vector<AvgEvaluateResultFiles*> *avgEvaluateResultFilesList)
	{
		int numberOfElem = avgEvaluateResultFilesList->size();
		for(int i=0; i<numberOfElem ; ++i)
		{
			addEvaluationAndQueriesNamesAvg((*avgEvaluateResultFilesList)[i]);
			if(isInitialized == false)
			{
				isInitialized = true;
			}
			evaluateResultFile.addAndSum(&(*avgEvaluateResultFilesList)[i]->evaluateResultFile );
		}
		normalizationAll(numberOfElem);
	}

	void addEvaluationAndQueriesNamesAvg(AvgEvaluateResultFiles *avgEvaluateResultFiles)
	{
		std::set<string> *queriesNamesFormFile = avgEvaluateResultFiles->getQueriesName();
		std::set<string>::iterator it;
		for (it = queriesNamesFormFile->begin(); it != queriesNamesFormFile->end(); ++it)
		{
			string q = *it; // Note the "*" here
			queriesNames.insert(q);
		}

		std::set<string> *evaluationNamesFormFile = avgEvaluateResultFiles->getEvaluationNames();
		for (it = evaluationNamesFormFile->begin(); it != evaluationNamesFormFile->end(); ++it)
		{
			string q = *it; // Note the "*" here
			evaluationNames.insert(q);
		}
		vector<EvaluateResultFile*> *evaluateResultFiles = &avgEvaluateResultFiles->EvaluateResultFiles;
		int length = evaluateResultFiles->size();
		for (int i = 0; i < length; i++)
		{
			EvaluateResultFiles.push_back((*evaluateResultFiles)[i]);
		}
	}
	#pragma endregion Execute cross validation process 



	void clear()
	{
		evaluateResultFile.Clear();
		int length = EvaluateResultFiles.size();
		for (int i = 0; i < length; i++)
		{
			EvaluateResultFiles[i]->Clear();
		}
		EvaluateResultFiles.clear();
		queriesNames.clear();
		evaluationNames.clear();
	}

	void normalizationAll(int numberOfFile)
	{
		evaluateResultFile.normalizationAll(numberOfFile);
	}

	QueryResult* getEvaluateValuesInFile(string evalName)
	{
		return evaluateResultFile.getEvaluateValuesInFile(evalName);
	}

	double getEvaluateValuesInFile(string evalName , string queryName)
	{
		int length = EvaluateResultFiles.size();
		for (int i = 0; i < length; i++)
		{
			bool isOk;
			double value = EvaluateResultFiles[i]->getEvaluateValuesInFile(evalName,queryName,&isOk);
			if(isOk)
			{
				return value;
			}
		}
		//printf("Query not found in map : %s" , queryName.c_str());
		return -1;
	}

	std::set<string> *getQueriesName()
	{
		return &(queriesNames);
	}

	std::set<string> *getEvaluationNames()
	{
		return &(evaluationNames);
	}



}AvgEvaluateResultFiles;

typedef struct CvResult
{
	double rm310Value;
	double rm3100Value;
}CvResult;

typedef struct Config
{
	string configName;
	double rm310Sum;
	double rm3100Sum;
	double rm310Avg;
	double rm3100Avg;
	std::map<string ,QueryResult*> listOfQuery; // the key is query name 

	void init(string name)
	{
		configName = name;
		rm310Sum = 0;
		rm3100Sum = 0;
		rm310Avg = 0;
		rm3100Avg = 0;

	}

	void clear()
	{
		std::map<string ,QueryResult*>::iterator it;
		for(it = listOfQuery.begin() ; it !=  listOfQuery.end() ; it ++ )
		{
			delete it->second;
		}
		listOfQuery.clear();
	}
	
	void add(double rm310,double rm3100 , string queryName, QueryResult* queryResult)
	{
		rm310Sum += rm310;
		rm3100Sum += rm3100;
		listOfQuery.insert(std::make_pair<string,QueryResult*> (queryName, queryResult));
	}
	void calcAvg()
	{
		rm310Avg = rm310Sum / listOfQuery.size();
		rm3100Avg = rm3100Sum / listOfQuery.size();
	}
	double getRm310AvgNotIncludeQuery(string queryName)
	{
		QueryResult* queryRes = listOfQuery.find(queryName)->second	;
		return (rm310Sum - queryRes->rm310QueryValue) / (listOfQuery.size() - 1);
	}
	double getRm3100AvgNotIncludeQuery(string queryName)
	{
		QueryResult* queryRes = listOfQuery.find(queryName)->second	;
		return (rm3100Sum - queryRes->rm3100QueryValue) / (listOfQuery.size() - 1);
	}

	QueryResult* getQueryResult(string queryName)
	{
		return listOfQuery.find(queryName)->second	;
	}

}Config;

typedef struct ConfigMap
{
	std::map<string,Config*> listOfConfig; // the key is config name 

	void add(string configName, QueryResult* queryResult)
	{
		double rm310 = queryResult->rm310QueryValue;
		double rm3100 = queryResult->rm3100QueryValue;
		string queryName = queryResult->queryName;

		std::map< string , Config* >::const_iterator  configIt = listOfConfig.find(configName);
		if( configIt == listOfConfig.end() ) // config not found
		{
			Config* newConfig= new Config();
			newConfig->init(configName);
			newConfig->add(rm310, rm3100, queryName, queryResult);
			listOfConfig.insert(std::make_pair<string,Config*> (configName, newConfig));
		}
		else
		{
			Config* configFromMap = configIt->second ;
			configFromMap->add(rm310, rm3100, queryName, queryResult);
		}
	}
	void calcAvg()
	{
		typedef std::map<string,Config*>::iterator it_type;
		for(it_type iterator = listOfConfig.begin(); iterator != listOfConfig.end(); iterator++) {
			iterator->second->calcAvg();
		}
	}
	string getMaxAvgConfigForRm3100NotIncludeQuery(string queryName)
	{
		double maxValue = std::numeric_limits<double>::min();
		string maxConfig;

		std::map<string,Config*>::iterator listOfConfigIterator;
		for(listOfConfigIterator = listOfConfig.begin(); listOfConfigIterator != listOfConfig.end(); listOfConfigIterator++) 
		{
			double currentValue = listOfConfigIterator->second->getRm3100AvgNotIncludeQuery(queryName);
			if(currentValue > maxValue )
			{
				maxValue = currentValue;
				maxConfig = listOfConfigIterator->first;
			}
		}
		return maxConfig;
	}
	string getMaxAvgConfigForRm310NotIncludeQuery(string queryName)
	{
		double maxValue = std::numeric_limits<double>::min();
		string maxConfig;

		std::map<string,Config*>::iterator listOfConfigIterator;
		for(listOfConfigIterator = listOfConfig.begin(); listOfConfigIterator != listOfConfig.end(); listOfConfigIterator++) 
		{
			double currentValue = listOfConfigIterator->second->getRm310AvgNotIncludeQuery(queryName);
			if(currentValue > maxValue )
			{
				maxValue = currentValue;
				maxConfig = listOfConfigIterator->first;
			}
		}
		return maxConfig;
	}
	QueryResult* getQueryResultPerConfiguration(string maxConfigForRm310, string query)
	{
		std::map< string , Config* >::const_iterator  configIt = listOfConfig.find(maxConfigForRm310);
		return configIt->second->getQueryResult(query);
	}
}ConfigMap;

typedef struct ConfigPerEvaluation
{
	std::map<string, ConfigMap* > configPerEvaluation;
	void add(string configName, string evalName, QueryResult* queryRes)
	{
		std::map< string , ConfigMap* >::const_iterator  pos = configPerEvaluation.find(evalName);
		if( pos == configPerEvaluation.end()) // evaluation not found
		{ 
			ConfigMap* configMap = new ConfigMap();
			configMap->add(configName, queryRes);
			configPerEvaluation.insert(std::make_pair<string, ConfigMap* > (evalName, configMap));
		}
		else
		{
			ConfigMap* configMap = pos->second;
			configMap->add(configName, queryRes);
		}

	}
	void calcAvg()
	{
		typedef std::map<string,ConfigMap*>::iterator it_type;
		for(it_type iterator = configPerEvaluation.begin(); iterator != configPerEvaluation.end(); iterator++) {
			iterator->second->calcAvg();
		}
	}
}ConfigPerEvaluation;

typedef struct FuzzyConfigMap
{
	std::map<string,vector<string>*> *FilesPerConfig;

	std::map<string,Config*> listOfConfig; // the key is config name 

	void init(std::map<string,vector<string>*> *filesPerConfig)
	{
		FilesPerConfig = filesPerConfig;
	}

	void add(string configName, QueryResult* queryResult)
	{
		double rm310 = queryResult->rm310QueryValue;
		double rm3100 = queryResult->rm3100QueryValue;
		string queryName = queryResult->queryName;

		std::map< string , Config* >::const_iterator  configIt = listOfConfig.find(configName);
		if( configIt == listOfConfig.end() ) // config not found
		{
			Config* newConfig= new Config();
			newConfig->init(configName);
			newConfig->add(rm310, rm3100, queryName, queryResult);
			listOfConfig.insert(std::make_pair<string,Config*> (configName, newConfig));
		}
		else
		{
			Config* configFromMap = configIt->second ;
			configFromMap->add(rm310, rm3100, queryName, queryResult);
		}
	}
	
	void calcAvg()
	{
		typedef std::map<string,Config*>::iterator it_type;
		for(it_type iterator = listOfConfig.begin(); iterator != listOfConfig.end(); iterator++) {
			iterator->second->calcAvg();
		}
	}
	
	string getMaxAvgConfigForRm3100NotIncludeQuery(string evalName , string queryName)
	{
		return getMaxAvgConfigForRm310NotIncludeQuery( evalName ,  queryName , 1);
	}
	
	string getMaxAvgConfigForRm310NotIncludeQuery(string evalName ,string queryName)
	{
		return getMaxAvgConfigForRm310NotIncludeQuery( evalName ,  queryName , 0);
	}
	

	string getMaxAvgConfigForRm310NotIncludeQuery(string evalName , string queryName , int rm3Type)
	{
		double maxValue = std::numeric_limits<double>::min();
		string maxConfig;

		std::map<string,vector<string>*>::iterator listOfConfigFilesIterator;
		for(listOfConfigFilesIterator = FilesPerConfig->begin(); listOfConfigFilesIterator != FilesPerConfig->end(); listOfConfigFilesIterator++) 
		{
			Config config;
			config.init(listOfConfigFilesIterator->first);
			vector<string>* configListFiles = listOfConfigFilesIterator->second;
			for (int i = 0; i < configListFiles->size(); i++)
			{
				string fileName = (*configListFiles)[i];

				#pragma region extract from the file name the configuration name and the query name
				std::vector<std::string> splitedFile = split(fileName, '/'); // the file path is split by "/" note.
				string queryFullName = splitedFile[splitedFile.size()-1]; 
				std::vector<std::string> splitedQuery = split(queryFullName, '.');
				string query = splitedQuery[0];
				string configName = splitedFile[splitedFile.size()-3];
				#pragma endregion 

				EvaluateResultFileForOneEvaluationParamter evaluateResultFile;
				evaluateResultFile.init(fileName,evalName);

				std::map<string,QueryResult*>::iterator fileConfig = evaluateResultFile.evalInFile.find(evalName);
				if(fileConfig == evaluateResultFile.evalInFile.end() )
				{
					std::cout << "Cannot find evaluation parameter "<<  evalName << " in the file : " << fileName<<  std::endl;
					continue;
				}
				QueryResult* queryResult = fileConfig->second->Copy();
				config.add(queryResult->rm310QueryValue, queryResult->rm3100QueryValue , queryResult->queryName , queryResult);
				evaluateResultFile.Clear();
			}
			config.calcAvg();
			if(rm3Type == 1)
			{
				double currentValue = config.getRm3100AvgNotIncludeQuery(queryName);
				if(currentValue > maxValue )
				{
					maxValue = currentValue;
					maxConfig = listOfConfigFilesIterator->first;
				}
			}
			else
			{
				double currentValue = config.getRm310AvgNotIncludeQuery(queryName);
				if(currentValue > maxValue )
				{
					maxValue = currentValue;
					maxConfig = listOfConfigFilesIterator->first;
				}
			}
			
			config.clear();
		}
		return maxConfig;
	}

	QueryResult* getQueryResultPerConfiguration(string evalName , string query , string maxConfigForRm310)
	{
		std::map<string,vector<string>*>::iterator listOfConfigFilesIterator = FilesPerConfig->find(maxConfigForRm310);

		if(listOfConfigFilesIterator == FilesPerConfig->end())
		{
			return NULL;
		}

		vector<string>* configListFiles = listOfConfigFilesIterator->second;
		for (int i = 0; i < configListFiles->size(); i++)
		{
			string fileName = (*configListFiles)[i];
			if( fileName.find(query) )
			{
				EvaluateResultFileForOneEvaluationParamter evaluateResultFile;
				evaluateResultFile.init(fileName,evalName);

				std::map<string,QueryResult*>::iterator fileConfig = evaluateResultFile.evalInFile.find(evalName);
				if(fileConfig == evaluateResultFile.evalInFile.end() )
				{
					std::cout << "Cannot find evaluation parameter "<<  evalName << " in the file : " << fileName<<  std::endl;
					continue;
				}
				QueryResult* queryResult = fileConfig->second;
				return queryResult;
			}
		}
		return NULL;
	}

}FuzzyConfigMap;


typedef struct Rm3CVFileLine
{
	string evalName;
	double rm310CVValue;
	double rm3100CVValue;
}Rm3CVFileLine;

typedef struct Rm3CVFile
{
	std::map<string,Rm3CVFileLine> lines;
	void add(Rm3CVFileLine line)
	{
		lines.insert(std::make_pair<string,Rm3CVFileLine> (line.evalName, line));
	}
	Rm3CVFileLine getRm3CVFileLine(string evalName)
	{
		std::map< string , Rm3CVFileLine >::const_iterator  pos = lines.find(evalName);
		return pos->second;
	}
	void init(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			Rm3CVFileLine cvline ;

			std::string qcopy( line);
			std::istringstream oss (qcopy);

			std::string eval;
			oss >> eval;
			if ( eval.empty()) {
				return;    
			}

			std::string rm310CVValueStr;
			oss >> rm310CVValueStr;
			if ( rm310CVValueStr.empty()) {
				return;    
			}

			std::string erm3100CVValueStr;
			oss >> erm3100CVValueStr;
			if ( erm3100CVValueStr.empty()) {
				return;    
			}

			double rm310CVValue = atof(rm310CVValueStr.c_str());
			double rm3100CVValue = atof(erm3100CVValueStr.c_str());

			cvline.evalName = eval;
			cvline.rm310CVValue = rm310CVValue;
			cvline.rm3100CVValue = rm3100CVValue;

			add(cvline);
		}

		finput.close();
	}

}Rm3CVFile;

typedef struct ConfigKeyResult
{
	string configKey;
	double cvRm310QueryValue;
	double cvRm3100QueryValue;
	double originalQueryValue;
	double rm310QueryValue;
	double rm3100QueryValue;
	std::map< string,vector<double> *> listOfClusters;

	void writeFileForPlot( string path)
	{
		ofstream of;
		of.open ( path.c_str(), std::ofstream::out ); 
		string hading = "# index, originalQueryValue, rm310QueryValue, rm3100QueryValue, cv_rm310QueryValue, cv_rm3100QueryValue , " ;
		
		std::map<string,vector<double>*>::iterator it ;
		string clustersNems = "";
		for(it = listOfClusters.begin() ; it != listOfClusters.end() ; it++  )
		{
			clustersNems += it->first + ", ";
		}
		of << "# index, " << clustersNems <<  "originalQueryValue, rm310QueryValue, rm3100QueryValue, cv_rm310QueryValue, cv_rm3100QueryValue"   <<  std::endl;

		int length = listOfClusters.begin()->second->size();
		int i;
		for ( i = 0; i < length; i++)
		{
			string clustersValues = "";
			of << i  << " " ;
			for(it = listOfClusters.begin() ; it != listOfClusters.end() ; it++  )
			{
				vector<double>* values = it->second;
				of <<  (*values)[i]  << " ";
			}
			of <<  originalQueryValue << " " << rm310QueryValue << " "<<  rm3100QueryValue  << " "<<  cvRm310QueryValue << " "<<  cvRm3100QueryValue << std::endl;
		}

		// print the last line again - for plot 
		string clustersValues = "";
		of << i  << " " ;
		for(it = listOfClusters.begin() ; it != listOfClusters.end() ; it++  )
		{
			vector<double>* values = it->second;
			of <<  (*values)[i-1]  << " ";
		}
		of <<  originalQueryValue << " " << rm310QueryValue << " "<<  rm3100QueryValue  << " "<<  cvRm310QueryValue << " "<<  cvRm3100QueryValue << std::endl;
		


		of.close();
	}

}ConfigKeyResult;

typedef struct ResultToPrint
{
	std::map< string,double > listOfSingleValue;
	std::map< string,vector<double> *> listOfMultipleValue;

	void writeFileForPlot( string path)
	{
		ofstream of;
		std::cout << "Create file " << std::endl;
		of.open ( path.c_str(), std::ofstream::out ); 

		// Single Values + names
		std::map<string,double>::iterator listOfSingleValue_it ;
		string singleValueNems = "";
		string singleValue = "";
		std::cout << "Start write Single Values header " << std::endl;
		for(listOfSingleValue_it = listOfSingleValue.begin() ; listOfSingleValue_it != listOfSingleValue.end() ; listOfSingleValue_it++  )
		{
			singleValueNems +=  listOfSingleValue_it->first + " ,"   ;

			std::ostringstream strs;
			strs << listOfSingleValue_it->second;
			std::string str = strs.str();
			singleValue += str + " " ;
		}
		singleValueNems = trimEnd(singleValueNems,",");

	
		// Multiple Value names
		std::cout << "Start write Multiple Values header " << std::endl;
		std::map<string,vector<double>*>::iterator multipleValue_it ;
		string multipleValueNems = "";
		for(multipleValue_it = listOfMultipleValue.begin() ; multipleValue_it != listOfMultipleValue.end() ; multipleValue_it++  )
		{
			multipleValueNems += multipleValue_it->first + ", ";
		}
		multipleValueNems = trimEnd(multipleValueNems,",");

		// write the heading
		of << "# index, " << singleValueNems << " , " << multipleValueNems  <<  std::endl;

		int length = listOfMultipleValue.begin()->second->size();
		int i;
		string multipleValue = "";
		std::cout << "Start write to file. length =  " << length << std::endl;
		for ( i = 0; i < length; i++)
		{
			multipleValue = "";
			for(multipleValue_it = listOfMultipleValue.begin() ; multipleValue_it != listOfMultipleValue.end() ; multipleValue_it++  )
			{				
				std::cout << "index i = " << i << " . length =  " << multipleValue_it->second->size() << " " <<multipleValue_it->first << std::endl;
				vector<double>* values = multipleValue_it->second;
								
				std::ostringstream strs;
				strs << (*values)[i] ;
				std::string str = strs.str();
				multipleValue += str + " " ;
			}

			of << i  << " "  << singleValue << " " << multipleValue << std::endl;
		}

		// print the last line again - for plot 
		of << i  << " "  << singleValue << " " << multipleValue << std::endl;
		std::cout << "end write to file " << std::endl;
		of.close();
	}

	void writeOnlyMultipleValue(string path)
	{
		ofstream of;
		of.open ( path.c_str(), std::ofstream::out ); 
		// Multiple Value names
		std::map<string,vector<double>*>::iterator multipleValue_it ;
		string multipleValueNems = "";
		for(multipleValue_it = listOfMultipleValue.begin() ; multipleValue_it != listOfMultipleValue.end() ; multipleValue_it++  )
		{
			multipleValueNems += multipleValue_it->first + ", ";
		}
		multipleValueNems = trimEnd(multipleValueNems,",");

		// write the heading
		of << "# index, " << multipleValueNems <<  std::endl;

		int length = listOfMultipleValue.begin()->second->size();
		int i;
		string multipleValue = "";
		for ( i = 0; i < length; i++)
		{
			multipleValue = "";
			for(multipleValue_it = listOfMultipleValue.begin() ; multipleValue_it != listOfMultipleValue.end() ; multipleValue_it++  )
			{
				vector<double>* values = multipleValue_it->second;
								
				std::ostringstream strs;
				strs << (*values)[i] ;
				std::string str = strs.str();
				multipleValue += str + " " ;
			}

			of << i  << " "  << multipleValue << std::endl;
		}

		// print the last line again - for plot 
		of << i  << " "  << multipleValue << std::endl;
		of.close();
	}

	string toString()
	{
		return "ResultToPrint = listOfSingleValue size is : " + SSTR(listOfSingleValue.size()) + " listOfSingleValue size is : " +  SSTR(listOfMultipleValue.size());
	}

}ResultToPrint;

typedef struct Rm1Data
{
	double rm310CVValue;
	double rm3100CVValue;
}Rm1Data;

typedef struct BestoneAvg
{
	std::map<string,Rm1Data> AvgPerEval;

	bool getBestoneAvg(string evalName, double *rm310 , double *rm3100 )
	{
		std::map< string , Rm1Data >::const_iterator  pos = AvgPerEval.find(evalName);
		*rm3100 = pos->second.rm3100CVValue;
		*rm310 = pos->second.rm310CVValue;
		return true;
	}
	
	bool create_bestone_average(std::map<string,AvgEvaluateResultFiles*> *resultPerConfig )
	{
		AvgEvaluateResultFiles* first = (resultPerConfig->begin())->second;
		std::set<string> *evaluationNamesFromFile = first->evaluateResultFile.getEvaluationNames();
	
		std::vector<string> evaluationNames(evaluationNamesFromFile->size());
		std::copy(evaluationNamesFromFile->begin(), evaluationNamesFromFile->end(), evaluationNames.begin());

		std::set<string> *queriesNamesSet = first->getQueriesName();
		std::vector<string> queriesNames(queriesNamesSet->size());
		std::copy(queriesNamesSet->begin(), queriesNamesSet->end(), queriesNames.begin());


		string rm310 = "_rm3_10";
		string rm3100 = "_rm3_100";
	
		//Calc Avg for the max
		for (int i = 0; i < evaluationNames.size(); i++)
		{
			string evalName = evaluationNames[i];
			Rm1Data data;
			data.rm3100CVValue = 0;
			data.rm310CVValue = 0;

			int numberOfQueryForrm10 = 0;
			int numberOfQueryForrm100 = 0;
			// for each query, find the best value
			for(int q=0 ; q < queriesNames.size() ; q++)
			{
				string queryName = queriesNames[q];

				// check if the query that we check is rm3
				if ( ! ends_with(queryName,rm310)  && ! ends_with(queryName,rm3100) ) {
					continue;
				}

				double max =  std::numeric_limits<double>::min();

				// for each configuration, find the best value for current query and current evaluation parameter
				std::map<string,AvgEvaluateResultFiles*>::iterator  resultPerConfigIterator;
				for(resultPerConfigIterator = resultPerConfig->begin(); resultPerConfigIterator != resultPerConfig->end(); resultPerConfigIterator++) 
				{
					AvgEvaluateResultFiles *avgEvaluateResultFiles = resultPerConfigIterator->second;
					double  resultPerEval = avgEvaluateResultFiles->getEvaluateValuesInFile(evalName,queryName);
					max = max < resultPerEval ? resultPerEval : max;
				}
				if (ends_with(queryName,rm310) ) 
				{
					data.rm310CVValue += max;
					numberOfQueryForrm10++;
				}
				if (ends_with(queryName,rm3100) )
				{
					data.rm3100CVValue += max;
					numberOfQueryForrm100++;
				}
			}
			data.rm3100CVValue = data.rm3100CVValue / numberOfQueryForrm100 ;
			data.rm310CVValue =  data.rm310CVValue / numberOfQueryForrm10 ;
			AvgPerEval.insert(std::make_pair<string,Rm1Data>(evalName,data));
		}
		return true;
	}
}BestoneAvg;


void addItenToConfigMap(ConfigMap* configMap , string configName ,  Config*  config);
void addItemToMap();


void crossValidationForRM310and100(string f,vector<string> argv);
void crossValidationForRM310and100_noAllToMemory(string f,vector<string> argv);
void crossValidationForRM310and100(int argc, char *argv[]);



void writeFileForPlotAvgOfFiles(int argc, char *argv[]);
void writeFileForPlot(int argc, char *argv[]);
void writeFileForPlotForFdocs(string fDocs, string cvPath ,string path_to_file, vector<string> *fileNameList );
void writeFileForPlotForFdocsBestone(string fDocs , string cvPath , string pathToDir, vector<string> *fileNameList );
void writeFileForPlotForFdocs(int argc, char *argv[]);

void writeFileForPlotForConfig(int argc, char *argv[], ConfigurationType type);
void writeFileForPlotForConfigWithBestoneAvg(int argc, char *argv[], ConfigurationType type);
#endif