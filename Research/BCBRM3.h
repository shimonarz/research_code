#ifndef BCBRM3_H
#define BCBRM3_H

#include "CBRM3.h"
#include "GeneralFunctions.h"
#include "ModelConfiguration.h"
#include "DataStructer.h"
#include "WordInClusterProbability.h"
#include "ClustersFeature.h"

class BCBRM3_ModelConfiguration : public CBRM3_ModelConfiguration
{
public:
	vector< pair<string,double> > clustersFeatureFileList; //:file1 Weight1,file2 Weight2, ...

	void virtual Init(ModelConfiguration *modelConfiguration) 
	{
		CBRM3_ModelConfiguration::Init(modelConfiguration);
		clustersFeatureFileList = modelConfiguration->clustersFeatureFileList;
	}
};

// P(w|BC) = Sum _ c (  P(w|c)*P(c|q) ) 
typedef struct ClusterProbability
{
	QueriesClustersFeatures ClusterInQueryProbability; // P(c|q)
	WordInClusterProbabilityForAllQuery WordInClusterProbability; // P(w|c)
	std::map< string , double > FinalProbability; // key = term , value = P(w|BC)

	void Init(vector<string> *queriesTermsProbabilityInClustersFileList , vector< pair<string,double> > *clustersFeatureFileList )
	{
		InitOnlyWordInClusterProbability(queriesTermsProbabilityInClustersFileList);
		InitQueriesClustersFeatures(clustersFeatureFileList);
	}

	double GetWordInClusterProbability(string query , string term ) // Sum c { P(c|q)*P(w|c) }
	{
		map< string , vector<string> > *ClustersList = GetClustersList();
		map< string , vector<string> >::iterator it = ClustersList->find(query);
		double sum = 0;
		if( it != ClustersList->end() )
		{
			vector<string> *clusters = &it->second;
			for (int i = 0; i < clusters->size(); i++)
			{
				string cluster = (*clusters)[i];
				double featuresProbability = ClusterInQueryProbability.GetClusterProbability(cluster,query) ;//P(c|q)
				double termProbability = WordInClusterProbability.GetWordInClusterProbability(query ,  cluster ,  term); //P(w|c)
				sum += featuresProbability * termProbability;
			}
		}
		return  sum;
	}

	void Clear()
	{
		ClusterInQueryProbability.Clear();
		WordInClusterProbability.Clear();
	}

private :
	map< string , vector<string> > *GetClustersList()
	{
		return &ClusterInQueryProbability.ClustersList;
	}

	void InitOnlyWordInClusterProbability(vector<string> *queriesTermsProbabilityInClustersFileList)
	{
		WordInClusterProbability.init(queriesTermsProbabilityInClustersFileList);
	}

	void InitQueriesClustersFeatures(vector< pair<string,double> > *clustersFeatureFileList)
	{
		for (int i = 0; i < clustersFeatureFileList->size(); i++)
		{
			string featureFileName = (*clustersFeatureFileList)[i].first;
			double featureWeight = (*clustersFeatureFileList)[i].second;
			ClusterInQueryProbability.AddFeature(featureFileName , featureWeight);
		}
		ClusterInQueryProbability.SumAllClustersFeatures();
	}

}ClusterProbability;

class BCBRM3Query 
{
public:
	std::string QueryId;

	double OriginalQueryWeight;
	std::vector<Term*> OriginalQuery;
	
	double RM1TermsWeight;
	std::vector<Term*> RM1Terms;
	
	double ClustresTermsWeight;
	std::vector<Term*> ClustresTerms;

	void init()
	{
		QueryId = "";
		OriginalQueryWeight = RM1TermsWeight = ClustresTermsWeight = 0.0;
	}

	void InitOriginalQuery(string fullQuery)
	{
		// #combine( international organized crime )
		std::string qcopy( fullQuery);
		for ( size_t i=0; i<qcopy.size(); i++ )
		{
			if ( (qcopy[i]=='(')||(qcopy[i]==')')||(qcopy[i]==',')||(qcopy[i]=='"')||(qcopy[i]==':')) 
			{
				qcopy[i]=' ';
			}
		}

		vector<string> queryTerms = split(qcopy , ' ' );

		for (int i = 1; i < queryTerms.size(); i++)
		{
			Term* term = new Term(queryTerms[i]);
			OriginalQuery.push_back(term);
		}
	}

	void Clear()
	{
		int length = OriginalQuery.size();
		for (int i = 0; i < length; i++)
		{
			delete OriginalQuery[i];
		}
		OriginalQuery.clear();

		length = RM1Terms.size();
		for (int i = 0; i < length; i++)
		{
			delete RM1Terms[i];
		}
		RM1Terms.clear();

		length = ClustresTerms.size();
		for (int i = 0; i < length; i++)
		{
			delete ClustresTerms[i];
		}
		ClustresTerms.clear();
	}

	void ClearClustresExtended()
	{
		int length = ClustresTerms.size();
		for (int i = 0; i < length; i++)
		{
			delete ClustresTerms[i];
		}
		ClustresTerms.clear();
	}
};
// BCBRM3 (Boosting Cluster Base RM3) model
typedef struct BCBRM3Model
{
	QueriesFile Queries; // Queries 
	RM1Probability RM1_Probability; // P(w|RM1)
	ClusterProbability _clusterProbability; //	WordInClusterProbabilityForAllQuery WordInClusterProbability; // P(w|C)
	BCBRM3_ModelConfiguration ModelConfiguration; // ���� �� ������������ �� �����
	QueriesClusters ClustersTermsForAllQuery; // ���� �� �� �������� ������� ����

	void Init(BCBRM3_ModelConfiguration configuration)
	{
		FILE_LOG(logINFO) << "Init BCBRM3 model " ;
		ModelConfiguration = configuration;
		Queries.readFile(ModelConfiguration.queries);
		RM1_Probability.ReadFromFile(ModelConfiguration.RM1ProbabilityFile);
		_clusterProbability.Init( &ModelConfiguration.queriesTermsProbabilityInClustersFileList , &ModelConfiguration.clustersFeatureFileList);
		ClustersTermsForAllQuery.init(&ModelConfiguration.queriesClustersFileList);

		FILE_LOG(logINFO) << "Finish Init BCBRM3 model " ;
	}

	void BuildModel()
	{
		int k = 1;
		FILE_LOG(logINFO) << "Start Build retrieval files.";


		string queryOutputFile = ModelConfiguration.outputDir + "//queries.xml";

		ofstream of;
		of.open ( queryOutputFile.c_str(), std::ofstream::out ); 
		of << "<parameters>\n";

		string outputDir = ModelConfiguration.outputDir;
		map< string,string >::iterator query_it; // Queries
		for(query_it = Queries.Queries.begin() ; 
			query_it != Queries.Queries.end() ;
			++query_it)
		{
			string queryName = query_it->first;
			
			cout << '\r' << "	Extend Query " <<  queryName  << " - Progress : " << k <<  '/' <<  Queries.Queries.size() <<
				".     Progress Percent " <<  (int)(((double)k/(double)Queries.Queries.size())*100.0)  <<  "%" << std::flush;
		
			
			BCBRM3Query extendedQuery;

			//FILE_LOG(logDEBUG) << "" ;

			extendedQuery.InitOriginalQuery(query_it->second);
			//FILE_LOG(logDEBUG) << "extendedQuery.InitOriginalQuery(query_it->second)" ;
			extendedQuery.OriginalQueryWeight = ModelConfiguration.queriesWeight;
			//FILE_LOG(logDEBUG) << "extendedQuery.OriginalQueryWeight = ModelConfiguration.queriesWeight;" ;
			extendedQuery.RM1Terms = RM1_Probability.GetRM1Terms(queryName);
			//FILE_LOG(logDEBUG) << "extendedQuery.RM1Terms = RM1_Probability.GetRM1Terms(queryName);" ;
			extendedQuery.RM1TermsWeight = ModelConfiguration.rm1Weight;
			//FILE_LOG(logDEBUG) << "extendedQuery.RM1TermsWeight = ModelConfiguration.rm1Weight;" ;
			extendedQuery.ClustresTermsWeight = ModelConfiguration.termsProbabilityInClustersWeight;
			//FILE_LOG(logDEBUG) << "extendedQuery.ClustresTermsWeight = ModelConfiguration.termsProbabilityInClustersWeight;" ;

			vector<Term*> ClustresTerms;
			for (int i = 0; i < extendedQuery.RM1Terms.size(); i++)
			{
				Term* rm1Term = extendedQuery.RM1Terms[i];
				//FILE_LOG(logDEBUG) << "Term* rm1Term = extendedQuery.RM1Terms[i];" ;
				string term = rm1Term->value;
				//FILE_LOG(logDEBUG) << "string term = rm1Term->value;" ;

				double probability = _clusterProbability.GetWordInClusterProbability(queryName , term);
				//FILE_LOG(logDEBUG) << "double probability = _clusterProbability.GetWordInClusterProbability(queryName , term);" ;

				Term* t = new Term(term , probability);
				//FILE_LOG(logDEBUG) << "Term* t = new Term(term , probability);" ;

				ClustresTerms.push_back(t);
				//FILE_LOG(logDEBUG) << "ClustresTerms.push_back(t);" ;

			}

			Term::Normalize(&ClustresTerms);
			//FILE_LOG(logDEBUG) << "Term::Normalize(&ClustresTerms);" ;
			extendedQuery.ClustresTerms = ClustresTerms;
				
			writeExtendedQuery(&of,queryName,&extendedQuery);
			extendedQuery.ClearClustresExtended();

			/*for (int i = 0; i < ClustresTerms.size(); i++)
			{
				delete ClustresTerms[i];
			}*/
			ClustresTerms.clear();

			k++;
		}
		cout << '\n';

		of << "</parameters>";
		of.close(); 

	}
	


	
    void writeExtendedQuery(ofstream *of,string queryNumber,BCBRM3Query *expansion)
	{
		std::string query = buildQuery(expansion);

		*of << "<query>\n";
		*of << "<number>" + queryNumber + "</number>\n";
		*of << "<text>" + query + "</text>\n";
		*of << "</query>\n";
	}

	std::string buildQuery(BCBRM3Query *terms)
	{
		std::string result;

		double originalQueryWeight = terms->OriginalQueryWeight;
		double expansionQueryWeight = terms->ClustresTermsWeight + terms->RM1TermsWeight;

		char originalQueryWeightStr[40];
		sprintf(originalQueryWeightStr,"%.32f",originalQueryWeight);
		std::ostringstream originalQueryWeightStream;
		//originalQueryWeightStream.precision(4);
		//originalQueryWeightStream<<fixed;    // for fixed point notation
		originalQueryWeightStream << originalQueryWeightStr ;
		

		char expansionQueryWeightStr[40];
		sprintf(expansionQueryWeightStr,"%.32f",expansionQueryWeight);
		std::ostringstream expansionQueryWeightStream;
		expansionQueryWeightStream << expansionQueryWeightStr;

		if(terms->ClustresTermsWeight == 0 && terms->RM1TermsWeight == 0)
		{
			// no expansion 
			result = "#combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + " )" ;
		}
		else if(terms->ClustresTermsWeight == 0 && terms->RM1TermsWeight != 0)
		{
			// only Rm1 expansion
			result = "#weight( " + 
				originalQueryWeightStream.str() + " #combine( #combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + ") ) " + 
				expansionQueryWeightStream.str()  + " #weight(  " + Term::convertTermToString(terms->RM1Terms,true,true) + " ) " + " )";
		}
		else if( terms->ClustresTermsWeight != 0 && terms->RM1TermsWeight == 0)
		{
			// only Cluster expansion
			result = "#weight( " + 
				originalQueryWeightStream.str() + " #combine( #combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + ") ) " + 
				expansionQueryWeightStream.str()  + " #weight(  " + Term::convertTermToString(terms->ClustresTerms,true,true) + " ) " + " )";

		}
		else
		{
			// Rm1 and Cluster expansion
			std::vector<Term*> margeTerms;
			// rm1 values
			int length = terms->RM1Terms.size();
			for (int i = 0; i < length; i++)
			{
				Term* newTerm = terms->RM1Terms[i]->Copy();
				newTerm->weight = ( terms->RM1TermsWeight / expansionQueryWeight) * newTerm->weight;
				margeTerms.push_back(newTerm);
			}

			length = terms->ClustresTerms.size();
			for (int i = 0; i < length; i++)
			{
				Term* t = terms->ClustresTerms[i];
				
				// Search if the Term is already exist
				Term* foundT = NULL;
				std::vector<Term*>::iterator it =  margeTerms.begin();
				for (it =  margeTerms.begin() ; it != margeTerms.end(); it++)
				{
					if((*it)->value == t->value)
					{
						foundT = *it;
						break;
					}
				}

				// if Term is already exist
				if(foundT != NULL)
				{
					foundT->weight += ( terms->ClustresTermsWeight / expansionQueryWeight) * t->weight;
				}
				else
				{
					Term* newTerm = t->Copy();
					newTerm->weight = ( terms->ClustresTermsWeight / expansionQueryWeight) * newTerm->weight;
					margeTerms.push_back(newTerm);
				}

			}

			double sum = 0;
			for (int i = 0; i < margeTerms.size(); i++)
			{
				sum += margeTerms[i]->weight;
			}
			for (int i = 0; i < margeTerms.size(); i++)
			{
				margeTerms[i]->weight /= sum;
			}

			result = "#weight( " + 
				originalQueryWeightStream.str() + " #combine( #combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + ") ) " + 
				expansionQueryWeightStream.str()  + " #weight(  " + Term::convertTermToString(margeTerms,true,true) + " ) " + " )";

			// delete all new Terms that we create
			length = margeTerms.size();
			for (int i = 0; i < length; i++)
			{
				delete margeTerms[i];
			}
		}



		return result;
	}
	

	void Clear()
	{
		RM1_Probability.Clear();
		_clusterProbability.Clear();
		ClustersTermsForAllQuery.Clear();

	}

}BCBRM3Model;

#endif