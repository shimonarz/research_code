#ifndef GeneralFunctions_H
#define GeneralFunctions_H

#include <string.h>
#include <string>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <set>
#include <sstream>
#include <algorithm>
#include "math.h"
#include <stdarg.h>
#include <functional> 
#include <cctype>
#include <locale>
#include <stdlib.h>     /* abs */
#include "log.h"

using std::ifstream;
using std::ofstream;
using std::map;
using std::pair;
using std::vector;
using std::string;
using std::cout;
using std::cerr;



//#define SSTR( x ) static_cast< std::ostringstream & >( \
//( std::ostringstream() <<  x ) ).str()
//( std::ostringstream() << std::dec <<  x ) ).str()

enum ConfigurationType
{
	Model,
	Index,
	W2vModel,
	Retrival,
	InnerRetrival,
	Query
};

inline string SSTR(double vlaue)
{
	char str[45];
	sprintf(str,"%.32f",vlaue);
	std::ostringstream stream;
	stream << str ;
	return stream.str();
}

inline string SSTR_4(double vlaue)
{
	char str[45];
	sprintf(str,"%.4f",vlaue);
	std::ostringstream stream;
	stream << str ;
	return stream.str();
}


inline void split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
}

inline std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}

inline bool ends_with(std::string const & value, std::string const & ending)
{
	if (ending.size() > value.size()) return false;
	return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

inline bool start_with(std::string const & value, std::string const & start)
{
	if (start.size() > value.size()) return false;
	return ( value.substr(0, start.size()) == start);
}

inline string trimEnd(std::string const & str,std::string const & ending)
{
	size_t endpos = str.find_last_not_of(ending);
	if( string::npos != endpos )
	{
		return str.substr( 0, endpos+1 );
	}
	return str;
}
inline string trimFormStrToEnd(std::string const & str,std::string const & ending)
{
	size_t endpos = str.find(ending);
	if( string::npos != endpos )
	{
		return str.substr( 0, endpos );
	}
	return str;
}

static inline std::string trimStart(std::string const & str,std::string const & start) 
{

	size_t endpos = str.find_first_not_of(start);
	if( string::npos != endpos )
	{
		return str.substr( endpos+1 , str.size() );
	}
	return str;
}

struct descending_order_compare_DoublePair_by_second
{
	inline bool operator()(const std::pair<double,double>& p1, const std::pair<double,double>& p2)
	{
		if ( p1.second > p2.second ) return true;
		return false;
	}   
};

struct increasing_order_compare_DoublePair_by_second
{
	inline bool operator()(const std::pair<double,double>& p1, const std::pair<double,double>& p2)
	{
		if ( p1.second < p2.second ) return true;
		return false;
	}   
};

static bool sort_pred(const std::pair<int,double>& left, const std::pair<int,double>& right)
{
	return left.second > right.second;
}

static bool sort_string_double(const std::pair<string,double>& left, const std::pair<string,double>& right){	return left.second > right.second;}



inline double convertToDouble(std::string const& s)
{
	std::istringstream i(s);
	double x;
	if (!(i >> x))
		return -10;
	return x;
}

inline int convertToInt(std::string const& s)
{
	std::istringstream i(s);
	int x;
	if (!(i >> x))
		return 0;
	return x;
}

inline bool  convertToDouble(std::string const& s , double *value)
{
	std::istringstream i(s);
	*value = -2;
	double x;
	if (!(i >> x))
		return false;
	*value = x;
	return true;
}


inline string GetClusterQueryFullName(const std::string &_query, const std::string &_cluster)
{
	return _query + "_" + _cluster;
}

inline string getFileName(string fullFileName)
{
	vector<string> splited = split(fullFileName , '/');
	string fileName = splited [ splited.size() - 1 ];
	return fileName;
}

inline string getPathToFile_UnderScoreFormat(string fullFileName)
{
	vector<string> splited = split(fullFileName , '/');
	string name = "";
	for (int i = 0; i < splited.size() - 1; i++)
	{
		name += splited[i] +  "_";
	}
	return name;
}


inline string getFileName_Query(string fullFileName)
{
	return split( getFileName(fullFileName) , '.')[0];
}

inline string getClusterNameFormQueryClsuter(string queryClsuterName)
{
	return split( queryClsuterName , '_')[1];
}

inline void printVector(string name , vector<double> *x)
{
	int n = x->size();
	string toPrint = name + " : ";
	for(int i=0;i<n;i++)
	{
		toPrint += SSTR((*x)[i]) + " , " ;
	}
	cout << toPrint << std::endl;
}

inline string getMainConfie(string fullConfigName)
{
	const string end = "_Orig";
	return trimFormStrToEnd(fullConfigName,end);
}

inline void orderFileByConfig(vector<string> *fileNameList, ConfigurationType type , std::map<string,vector<string>*> *orderFile )
{
	int numberOfFiles = fileNameList->size();
	for(int i = 0 ; i < numberOfFiles ; ++i)
	{
		string fileName = (*fileNameList)[i];
		// extract from the file name the configuration name and the query name
		std::vector<std::string> splitedFile = split(fileName, '/'); // the file path is split by "/" note.
		string queryFullName = splitedFile[splitedFile.size()-1]; 
		string config = splitedFile[splitedFile.size()-3];
		string w2vModel = splitedFile[splitedFile.size()-4];
		string index = splitedFile[splitedFile.size()-5];
		string model = splitedFile[splitedFile.size()-6];

		std::vector<std::string> splitedQuery = split(queryFullName, '.');
		string query = splitedQuery[0];


		// /lv_local/home/shimonarz/Output/current_week_07_01_2017_new_rm3/AP_PSWR/AP.w2v.dim.300.win.16.neg.10/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_OrigWeight_0.2_clusterTermsWeight_0.5_rm1TermsWeight_.3/uniform_evaluateFolder/evalFile

		string key;
		switch (type)
		{
		case Model:
			key = model;
			break;
		case Index:
			key = index;
			break;
		case W2vModel:
			key = w2vModel;
			break;
		case Retrival:
			key = config;
			break;
		case InnerRetrival:
			key = "Orig";
			break;
		case Query:
			key = query;
			break;
		default:
			key = query;
			break;
		}
		std::map< string , vector<string>* >::const_iterator  configIt = orderFile->find(key);
		if( configIt == orderFile->end() ) // config not found
		{
			vector<string>* vec = new vector<string>();
			vec->push_back(fileName);
			orderFile->insert(std::make_pair<string,vector<string>*> (key, vec));
		}
		else
		{
			vector<string>* vec = configIt->second ;
			vec->push_back(fileName);
		}
	}
}

inline void orderFileByPos(vector<string> *fileNameList, int posFromTheEnd , std::map<string,vector<string>*> *orderFile )
{
	int numberOfFiles = fileNameList->size();
	for(int i = 0 ; i < numberOfFiles ; ++i)
	{
		string fileName = (*fileNameList)[i];
		// extract from the file name the configuration name and the query name
		std::vector<std::string> splitedFile = split(fileName, '/'); // the file path is split by "/" note.
		string key = splitedFile[splitedFile.size()-posFromTheEnd];;

		std::map< string , vector<string>* >::const_iterator  configIt = orderFile->find(key);
		if( configIt == orderFile->end() ) // config not found
		{
			vector<string>* vec = new vector<string>();
			vec->push_back(fileName);
			orderFile->insert(std::make_pair<string,vector<string>*> (key, vec));
		}
		else
		{
			vector<string>* vec = configIt->second ;
			vec->push_back(fileName);
		}
	}
}

inline string getConfigNaem(string name , string from = "muRule")
{
	std::vector<std::string> splitedFile = split(name, '_'); // the file path is split by "/" note.
	bool writeConfig = false;
	string str = "";
	for (unsigned int w = 0; w < (splitedFile.size() - 1) ; w++)
	{
		if(writeConfig)
		{
			str += splitedFile[w+1] + " ";
		}
		if( splitedFile[w] == from)
		{
			writeConfig = true;
		}
	}
	return str;
}

inline string getW2vModelName(vector<string> *wordToVecModelFileNames , string queryName )
{
	for (int i = 0 ; i < wordToVecModelFileNames->size()  ; ++i)
	{
		string w2vFileName = (*wordToVecModelFileNames)[i];
		string fileName =  getFileName(w2vFileName);
		if ( start_with(fileName,queryName) )
		{
			return w2vFileName;
		}
	}
	return "";
}


inline string getFileNameForQuery(string query,vector<string> *listOfFiles )
{
	int length = listOfFiles->size();
	for (int i = 0; i < length; i++)
	{
		string fileName = (*listOfFiles)[i];
		if (fileName.find(query) != std::string::npos) {
			return fileName;
		}
	}
	return NULL;
}

inline double correlation (vector<double> *x , vector<double> *y)
{
	if(x==NULL || y==NULL)
	{
		return -2;
	}
	if( x->size() == 0 || y->size() == 0)
	{
		return -3;
	}

	if(x->size() != y->size())
	{
		return -4;
	}

	vector<double> xx,xy,yy;
	double r , nr=0,dr_1=0,dr_2=0,dr_3=0,dr=0;
	double sum_y=0,sum_yy=0,sum_xy=0,sum_x=0,sum_xx=0;
	int i;
	int n = x->size();



	for(i=0;i<n;i++)
	{
		xx.push_back((*x)[i]*(*x)[i]);
		yy.push_back((*y)[i]*(*y)[i]);
	}

	for(i=0;i<n;i++)
	{
		sum_x+=(*x)[i];
		sum_y+=(*y)[i];
		sum_xx+= xx[i];
		sum_yy+=yy[i];
		sum_xy+= (*x)[i]*(*y)[i];
	}

	nr=(n*sum_xy)-(sum_x*sum_y);
	double sum_x2=sum_x*sum_x;
	double sum_y2=sum_y*sum_y;
	dr_1=(n*sum_xx)-sum_x2;
	dr_2=(n*sum_yy)-sum_y2;
	dr_3=dr_1*dr_2;

	if(dr_3 < 0)
	{
		return -5;
	}

	dr=sqrt(dr_3);

	if(dr == 0)
	{
		return -6;
	}
	r=(nr/dr);
	return r;
}

inline double correlation (double *x , double *y , int len)
{
	double* xx = new double[len];
	double* xy = new double[len];
	double* yy = new double[len];
	double r , nr=0,dr_1=0,dr_2=0,dr_3=0,dr=0;
	double sum_y=0,sum_yy=0,sum_xy=0,sum_x=0,sum_xx=0;
	int i;
	int n = len;

	for(i=0;i<n;i++)
	{
		xx[i]=x[i]*x[i];
		yy[i]=y[i]*y[i];
	}
	for(i=0;i<n;i++)
	{
		sum_x+=x[i];
		sum_y+=y[i];
		sum_xx+= xx[i];
		sum_yy+=yy[i];
		sum_xy+= x[i]*y[i];
	}
	nr=(n*sum_xy)-(sum_x*sum_y);
	double sum_x2=sum_x*sum_x;
	double sum_y2=sum_y*sum_y;
	dr_1=(n*sum_xx)-sum_x2;
	dr_2=(n*sum_yy)-sum_y2;
	dr_3=dr_1*dr_2;
	dr=sqrt(dr_3);
	r=(nr/dr);

	delete xx;
	delete xy;
	delete yy;

	return r;
}



inline double my_correlation (vector<double> *x , vector<double> *y)
{
	if(x==NULL || y==NULL)
	{
		return -2;
	}
	if( x->size() == 0 || y->size() == 0)
	{
		return -3;
	}

	if(x->size() != y->size())
	{
		return -4;
	}

	double xAvg , yAvg;

	int n = x->size();

	xAvg = yAvg = 0.0;
	for(int i=0;i<n;i++)
	{
		xAvg += (*x)[i];
		yAvg += (*y)[i];
	}
	xAvg = xAvg / n;
	yAvg = yAvg / n;


	double up , down1, down2 , down;
	up = down1 = down2 = 0.0;
	for(int i=0;i<n;i++)
	{
		up += ((*x)[i] - xAvg) * ((*y)[i] - yAvg);
		down1 += ((*x)[i] - xAvg) * ((*x)[i] - xAvg);
		down2 += ((*y)[i] - yAvg) * ((*y)[i] - yAvg);
	}

	down = down1 * down2;
	down=sqrt(down);

	if(up == 0)
	{
		return 0;
	}

	if(down == 0)
	{
		return -6;
	}
	double r =(up/down);
	return r;
}
#endif