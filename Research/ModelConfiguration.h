#ifndef ModelConfiguration_H
#define ModelConfiguration_H

#include "research.h"

// Model Configuration
class ModelConfiguration
{
public:
	string queries;
	double queriesWeight;
	string qrels;

	string rm1ProbabilityFile;
	double rm1Weight;

	string clustersFile;
	vector<string> queriesClustersFileList;
	vector<string> queriesTermsProbabilityInClustersFileList;
	vector< pair<string,double> > clustersFeatureFileList; //:file1 Weight1,file2 Weight2, ...
	double termsProbabilityInClustersWeight;
	string outputDir;

	string loopOnAllClustersWords;

	ModelConfiguration()
	{
		defult_number = (-5.0);
		queries = "";
		queriesWeight = defult_number;
		qrels = "";
		rm1ProbabilityFile = "";
		clustersFile = "";
		outputDir = "";
		rm1Weight = defult_number;
		termsProbabilityInClustersWeight = defult_number;
		loopOnAllClustersWords = "";
	}

	void Init(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
	
		if (!finput.good())
		{
			std::cerr << "Can't Model Configuration open file: " << fileName << std::endl;
			return;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			vector<string> lineSplit = split(line , main_delim );
			string configName = lineSplit[0];
			string configData = lineSplit[1];
			
			if(configName == "queries")
			{
				queries = configData;
			}else if(configName == "queriesWeight")
			{
				convertToDouble(configData,&queriesWeight);
			}else if(configName == "qrels")
			{
				qrels = configData;
			}else if(configName == "rm1ProbabilityFile")
			{
				rm1ProbabilityFile = configData;
			}else if(configName == "rm1Weight")
			{
				convertToDouble(configData,&rm1Weight);
			}else if(configName == "queriesClustersFileList")
			{
				queriesClustersFileList = split(configData , ',' );
			}else if(configName == "queriesTermsProbabilityInClustersFileList")
			{
				queriesTermsProbabilityInClustersFileList = split(configData , ',' );
			}else if(configName == "clustersFeatureFileList")
			{
				vector<string> clustersFeatureFilesWithWeight = split(configData , ',' );
				for (int i = 0; i < clustersFeatureFilesWithWeight.size() ; i++)
				{
					vector<string> vec = split(clustersFeatureFilesWithWeight[i] , ' ' );
					string featureFileName = vec[0];
					double featureWeight;
					convertToDouble(vec[1],&featureWeight);
					clustersFeatureFileList.push_back( std::make_pair<string,double>(featureFileName,featureWeight) );
				}
			}else if(configName == "termsProbabilityInClustersWeight")
			{
				convertToDouble(configData,&termsProbabilityInClustersWeight);
			}else if(configName == "outputDir")
			{
				outputDir = configData;
			}
			else if(configName == "clustersFile")
			{
				clustersFile = configData;
			}else if(configName == "loopOnAllClustersWords")
			{
				loopOnAllClustersWords = configData;
			}
			
		}

		finput.close();
	}

	void WriteModelConfigurationToFile(string fileName)
	{
		ofstream of;
		of.open ( fileName.c_str(), std::ofstream::out ); 
		if(queries != "")  of << "queries" << main_delim << queries << "\n";
		if(queriesWeight != -1.0)  of << "queriesWeight" << main_delim<< queriesWeight << "\n";
		if(qrels != "")  of << "qrels" << main_delim<< qrels << "\n";
		if(clustersFile != "")  of << "clustersFile" << main_delim << clustersFile << "\n";

		
		if(rm1ProbabilityFile != "")  of << "rm1ProbabilityFile" << main_delim<< rm1ProbabilityFile << "\n";
		if(rm1Weight != -1.0)  of << "rm1Weight" << main_delim<< rm1Weight << "\n";
		
		if(queriesClustersFileList.size() > 0)  of << "queriesClustersFileList" << main_delim << GetListOfStringToPrint(&queriesClustersFileList) << "\n";
		if(queriesTermsProbabilityInClustersFileList.size() > 0)  of << "queriesTermsProbabilityInClustersFileList" << main_delim << GetListOfStringToPrint(&queriesTermsProbabilityInClustersFileList) << "\n";
		if(clustersFeatureFileList.size() > 0)  of << "clustersFeatureFileList" << main_delim << GetPairListOfStringDoubleToPrint(&clustersFeatureFileList) << "\n";
		
		if(termsProbabilityInClustersWeight != -1.0)  of << "termsProbabilityInClustersWeight" << main_delim << termsProbabilityInClustersWeight << "\n";
		if(outputDir != "")  of << "outputDir" << main_delim << outputDir << "\n";
		if(loopOnAllClustersWords != "")  of << "loopOnAllClustersWords" << main_delim << loopOnAllClustersWords << "\n";
			
		of.close(); 
	}

	bool ValidateModelConfiguration()
	{
		if( ! CheckFile(queries,"queries") ) return false;
		if( ! CheckFile(qrels,"qrels") ) return false;
		if( ! CheckFile(rm1ProbabilityFile,"rm1ProbabilityFile") ) return false;
		if( ! CheckFile(outputDir,"outputDir") ) return false;
		if( ! CheckFile(clustersFile,"clustersFile") ) return false;
		
		// Probability
		double sum = ( (queriesWeight ==  defult_number) ? 0 : queriesWeight) +
					( (rm1Weight ==  defult_number) ? 0 : rm1Weight) +
					( (termsProbabilityInClustersWeight ==  defult_number) ? 0 : termsProbabilityInClustersWeight)  ;
		if(sum != 1) 
		{
			std::cout << "Validate-Model-Configuration Error - Probability is not sum to 1 " << std::endl;
			return false;
		}

		if(queriesClustersFileList.size() != 0)
		{
			if( !  CheckList(&queriesClustersFileList ,"queries Clusters File ") ) return false;
		}

		if(queriesTermsProbabilityInClustersFileList.size() != 0)
		{
			if( !  CheckList(&queriesTermsProbabilityInClustersFileList ,"queries Terms Probability In Clusters File") ) return false;
		}
	    if(clustersFeatureFileList.size() != 0)
		{
			if( !  CheckPairList(&clustersFeatureFileList ,"clusters Feature File") ) return false;
		}

		return true;	
	}
private:
	static const char main_delim = '#';
	double defult_number;
	string GetListOfStringToPrint(vector<string> *list)
	{
		string returnValue = "";
		for (int i = 0; i < list->size(); i++)
		{
			returnValue += (*list)[i] + ",";
		}
		returnValue = trimEnd(returnValue,",");
		return returnValue;
	}
	
	string GetPairListOfStringDoubleToPrint(vector< pair<string,double> > *list)
	{
		string returnValue = "";
		for (int i = 0; i < list->size(); i++)
		{
			returnValue += (*list)[i].first + " " + SSTR((*list)[i].second) + ",";
		}
		returnValue = trimEnd(returnValue,",");
		return returnValue;
	}


	bool CheckList(vector<string> *list , string listName)
	{
		for (int i = 0; i < list->size(); i++)
		{
			if( ! CheckFile((*list)[i] , listName) ) return false;
		}
		return true;
	}

	bool CheckPairList(vector< pair<string,double> > *list , string listName)
	{
		double sum = 0;
		for (int i = 0; i < list->size(); i++)
		{
			if( ! CheckFile((*list)[i].first , listName) ) return false;
			sum += (*list)[i].second;
		}
		if(sum != 1 )
		{
			return false;
		}
		return true;
	}

	bool CheckFile(string fileName , string filetype)
	{
		if(fileName != "")
		{
			ifstream finput;
			finput.open ( fileName.c_str() ); 
			if (!finput.good())
			{
				std::cout << "Validate-Model-Configuration Error - Can't open " << filetype << " file: " << fileName << std::endl;
				return false;
			}
			finput.close();
		}
		return true;
	}
};

class Base_ModelConfiguration
{
public:
	string queries;
	double queriesWeight;
	string qrels;
	string outputDir;

	void virtual Init(ModelConfiguration *modelConfiguration) 
	{
		queries = modelConfiguration->queries;
		queriesWeight = modelConfiguration->queriesWeight;
		qrels = modelConfiguration->qrels;
		outputDir = modelConfiguration->outputDir;
	}
};

class RM3_ModelConfiguration : public Base_ModelConfiguration
{
public:

	string RM1ProbabilityFile;
	double rm1Weight;

	void virtual Init(ModelConfiguration *modelConfiguration) 
	{
		Base_ModelConfiguration::Init(modelConfiguration);
		RM1ProbabilityFile = modelConfiguration->rm1ProbabilityFile;
		rm1Weight = modelConfiguration->rm1Weight;
	}
};

#endif