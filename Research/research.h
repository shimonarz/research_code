

/* 
 * File:   research.h
 * Author: Shimon Arzuan
 *
 * Created on April 16, 2016, 9:23 PM
 */

#ifndef Research_H
#define Research_H


#include <string.h>
#include <string>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <set>
#include <sstream>
#include <algorithm>
#include "math.h"
#include <stdarg.h>
#include <functional> 
#include <cctype>
#include <locale>
#include <stdlib.h>     /* abs */

#include "DataStructer.h"
#include "WordToVec.h"

using std::ifstream;
using std::ofstream;
using std::map;
using std::pair;
using std::vector;
using std::string;
using std::cout;
using std::cerr;




void CompareBetweenCBToCBRM3Models_WordComeFromRM1(string pathToDir,string cvPath , vector<string> *fileNameList);

void CreateClusterFileForAllQueries(string firstQueryExpensionFile,vector<string> wordToVecModelFileNameVec,int k,string outputDir );

void CreateClusterFileForQuery(FirstQueryExpension *firstQueryExpension,int k,WordToVecModel *wordToVecModel,string outputFile);

void CreateQrelFileForAllQueryClusers(vector<string> clustersfilesNames , string qrelsFileName , string outputDir);

void CreateQueriesFileWithWorkingSetDocno(string reRankingQueries, string outputDir ,vector<string> *clustersQueryFileList );

void CreateQueriesFileWithWorkingSetDocno(string reRankingQueries, string outputDir ,string currentClustersQueryFile );





#endif