#ifndef Evaluate_H
#define Evaluate_H

#include "DataStructer.h"

typedef struct EvaluatResultFileLine
{
	string evalName;
	string queryName;
	double evalValue;

	EvaluatResultFileLine* clone()
	{
		EvaluatResultFileLine *newLine = new EvaluatResultFileLine();
		newLine->evalName = evalName;
		newLine->queryName = queryName;
		newLine->evalValue = evalValue;
		return newLine;

	}

}EvaluatResultFileLine;

typedef struct QueryResult
{
	string queryName;
	double originalQueryValue;
	double rm310QueryValue;
	double rm3100QueryValue;
	vector< std::pair<string,double> > result;
	double avg;
	void init(string name = NULL)
	{
		queryName = name;
		originalQueryValue = 0;
		rm3100QueryValue = 0;
		rm310QueryValue = 0;
		avg=0;
	}

	// set for query strut the contain all query parameters
	// resultFileLineVec - define query the contain: original, rm310, rm3100, clusters
	// queryNme - current query name
	void init(vector<EvaluatResultFileLine*> * resultFileLineVec,  string queryNme)
	{
		init(queryNme);
		for( unsigned int i = 0 ; i< resultFileLineVec->size(); ++i)
		{
			EvaluatResultFileLine* resultFileLine = (*resultFileLineVec)[i];

			if(resultFileLine->queryName.compare("all") == 0)
			{
				continue;
			}

			if(resultFileLine->queryName.compare(queryNme) == 0)
			{
				queryName = resultFileLine->queryName;
				originalQueryValue = resultFileLine->evalValue;
				continue;
			}

			if(resultFileLine->queryName.compare(queryNme+ "_rm3_10") == 0)
			{
				rm310QueryValue = resultFileLine->evalValue;
				continue;
			}

			if(resultFileLine->queryName.compare(queryNme+ "_rm3_100") == 0)
			{
				rm3100QueryValue = resultFileLine->evalValue;
				continue;
			}

			result.push_back(std::make_pair(resultFileLine->queryName, resultFileLine->evalValue ) );	
		}

		std::sort(result.begin(), result.end(), sort_string_double);
		
		avg=0;

		if(result.size() == 0)
			return;

		for(unsigned int j=0 ; j<result.size() ; ++j)
		{
			avg += result[j].second;
		}
		avg /= result.size();

	}

	void addToSum(QueryResult* value)
	{
		originalQueryValue += value->originalQueryValue;
		rm3100QueryValue += value->rm3100QueryValue;
		rm310QueryValue += value->rm310QueryValue;

		avg=0;
		vector< std::pair<string,double> > *vec = &value->result;
		for(unsigned int j=0 ; j<vec->size() ; ++j)
		{
			result[j].second += (*vec)[j].second ;	
			avg += result[j].second;
		}
		avg /= result.size();
	}

	void normalization(int numberOfFile)
	{
		originalQueryValue /= numberOfFile;
		rm3100QueryValue /= numberOfFile;
		rm310QueryValue /= numberOfFile;

		avg=0;
		for(unsigned int j=0 ; j<result.size() ; ++j)
		{
			result[j].second /= numberOfFile;
			avg += result[j].second;
		}
		avg /= result.size();
	}

	vector<double>* getClustresValues()
	{
		vector<double> *data = new vector<double>();
		for (unsigned int i = 0; i < result.size(); i++)
		{
			data->push_back(result[i].second);
		}
		return data;
	}

	void getClustresValues( vector<double>* data)
	{
		for (unsigned int i = 0; i < result.size(); i++)
		{
			data->push_back(result[i].second);
		}
	}


	QueryResult* Copy()
	{
		QueryResult* value = new QueryResult();
		value->queryName = queryName;
		value->originalQueryValue = originalQueryValue;
		value->rm310QueryValue = rm310QueryValue;
		value->rm3100QueryValue = rm3100QueryValue;
		value->avg = avg;

		for (unsigned int i = 0; i < result.size(); i++)
		{
			value->result.push_back(std::make_pair(result[i].first, result[i].second ) );
		}

		return value;
	}

	string toString()
	{
		return "Query-Result for Query - " + queryName + 
			" : OriginalQueryValue=" + SSTR(originalQueryValue) + 
			", rm310QueryValue=" + SSTR(rm310QueryValue) + 
			", rm3100QueryValue=" + SSTR(rm3100QueryValue) +
			", number of clusters =" + SSTR(result.size()) 
			;
	}

}QueryResult;

typedef struct EvaluateResultFile
{
	vector<EvaluatResultFileLine*> lines; // vector of all line in the file
	std::map<string,vector<EvaluatResultFileLine*>*> evalMapFile; // key= evaluation name , value= all the line with the same evaluation name;
	std::map<string, QueryResult*> evalInFile; // // key= evaluation name , value= all the data that connect to the evaluation is structure order;
	std::set<string> queriesNames;
	std::set<string> evaluationNames;

	void init(string fileName)
	{
		ReadEvaluateFileToVector(fileName);
		mapEval();
		setQueryResult();
	}

	void Clear()
	{
		FILE_LOG(logDEBUG2) << "EvaluateResultFile.Clear - delete vector<EvaluatResultFileLine*> , size = " << lines.size();
		/*for (unsigned int i = 0; i < lines.size(); i++)
		{
			delete lines[i];
		}*/
		lines.clear();

		FILE_LOG(logDEBUG2) << "EvaluateResultFile.Clear - delete map<string,vector<EvaluatResultFileLine*>*>";
		std::map<string,vector<EvaluatResultFileLine*>*>::iterator it;
		for(it = evalMapFile.begin() ; it != evalMapFile.end() ; it ++)
		{
			FILE_LOG(logDEBUG2) << "EvaluateResultFile.Clear - delete map<string,vector<EvaluatResultFileLine*>*>  - 1. " << it->first << ", vec size" <<it->second->size(); 
			it->second->clear();
			FILE_LOG(logDEBUG2) << "EvaluateResultFile.Clear - delete map<string,vector<EvaluatResultFileLine*>*>  - 2 ";
			delete it->second; 
		}
		evalMapFile.clear();

		FILE_LOG(logDEBUG2) << "EvaluateResultFile.Clear - delete map<string,QueryResult*>";
		std::map<string,QueryResult*>::iterator it2;
		for(it2 = evalInFile.begin() ; it2 != evalInFile.end() ; it2 ++)
		{
			delete it2->second;
		}
		evalInFile.clear();

		evaluationNames.clear();
		queriesNames.clear();
	}

	// read evaluate file (file that generate in trac_eval)
	void ReadEvaluateFileToVector(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return ;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			EvaluatResultFileLine* evalline = new EvaluatResultFileLine();

			std::string qcopy( line);
			std::istringstream oss (qcopy);

			std::string eval;
			oss >> eval;
			if ( eval.empty()) {
				return ;    
			}

			std::string queryName;
			oss >> queryName;
			if ( queryName.empty()) {
				return ;    
			}

			std::string evalValueStr;
			oss >> evalValueStr;
			if ( evalValueStr.empty()) {
				return ;    
			}

			double evalValue = atof(evalValueStr.c_str());

			evalline->evalName = eval;
			evalline->queryName = queryName;
			evalline->evalValue = evalValue;

			lines.push_back(evalline);

			queriesNames.insert(queryName);
		}

		finput.close();
	}

	std::set<string>* GetQueryNames(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return NULL;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			std::string qcopy( line);
			std::istringstream oss (qcopy);

			std::string eval;
			oss >> eval;
			if ( eval.empty()) {
				return NULL;    
			}

			std::string queryName;
			oss >> queryName;
			if ( queryName.empty()) {
				return NULL;    
			}

			queriesNames.insert(queryName);
		}
		finput.close();
		return &queriesNames;
	}

	// return for each evaluation parameter (map, P@10..) vector that contain all queries
	// evalFile - vector that contain whole file - contain all queries
	void mapEval()
	{
		int len = lines.size();
		for(int i=0 ; i < len ; ++i)
		{
			EvaluatResultFileLine* fileLine = lines[i];
			if(fileLine->queryName == "all")
			{
				continue;
			}

			string evalName = fileLine->evalName;
			evaluationNames.insert(evalName);

			std::map<string,vector<EvaluatResultFileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
			if( pos == evalMapFile.end()) // not found
			{
				vector<EvaluatResultFileLine*>* newVector = new vector<EvaluatResultFileLine*>();
				newVector->push_back(fileLine);
				evalMapFile.insert(std::make_pair<string,vector<EvaluatResultFileLine*>*> (evalName, newVector));
			}
			else
			{
				vector<EvaluatResultFileLine*>* vec = pos->second;
				vec->push_back(fileLine);
			}
		}
	}

	void add(string evalName , QueryResult* data)
	{
		evaluationNames.insert(evalName);

		QueryResult* newQueryResult= new QueryResult();
		newQueryResult->queryName = evalName;
		newQueryResult->originalQueryValue = data->originalQueryValue;
		newQueryResult->rm3100QueryValue = data->rm3100QueryValue;
		newQueryResult->rm310QueryValue = data->rm310QueryValue;

		vector< std::pair<string,double> > *result = &data->result;
		for(unsigned int j=0 ; j<result->size() ; ++j)
		{
			newQueryResult->result.push_back(std::make_pair( SSTR( (j + 1) ).c_str() , (*result)[j].second ));
		}
		evalInFile.insert(std::make_pair<string,QueryResult*> (evalName, newQueryResult));

	}

	void addAndSum(string evalName , QueryResult* data)
	{
		std::map<string,QueryResult*>::const_iterator  pos = evalInFile.find(evalName);
		if( pos == evalInFile.end()) // not found
		{
			add( evalName ,  data);
		}
		else
		{
			QueryResult* queryResultFromMap = pos->second;
			queryResultFromMap->addToSum(data);
		}
	}

	void addAndSum(EvaluateResultFile *evaluateResultFile)
	{
		typedef std::map<string,QueryResult*>::iterator it_type;
		for(it_type iterator = evaluateResultFile->evalInFile.begin(); iterator != evaluateResultFile->evalInFile.end(); iterator++) 
		{
			string evalName = iterator->first;
			QueryResult* evalValues = iterator->second;
			addAndSum(evalName,evalValues);
		}
	}

	QueryResult* getEvaluateValuesInFile(string evalName)
	{
		std::map< string , QueryResult* >::const_iterator  pos = evalInFile.find(evalName);
		if( pos == evalInFile.end()) // not found
		{
			printf("Evaluate not found in map : %s" , evalName.c_str());
			return NULL;
		}
		else
		{
			return pos->second;
		}

	}

	double getEvaluateValuesInFile(string evalName,string queryName, bool *isOk)
	{
		*isOk = false;
		std::map<string,vector<EvaluatResultFileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
		if( pos == evalMapFile.end()) // not found
		{
			printf("Evaluate not found in map : %s" , evalName.c_str());

			return -1;
		}
		else
		{
			vector<EvaluatResultFileLine*>* vec = pos->second;
			int len = vec->size();
			for(int i =0 ; i<len ; i++)
			{
				EvaluatResultFileLine* line = (*vec)[i];
				if( line->queryName == queryName)
				{
					*isOk = true;
					return line->evalValue;
				}
			}
			// printf("Query not found in map : %s" , queryName.c_str());
			return -1;
		}

	}

	void normalizationAll(int numberOfFile)
	{
		typedef std::map<string,QueryResult*>::iterator it_type;
		for(it_type iterator = evalInFile.begin(); iterator != evalInFile.end(); iterator++) 
		{
			QueryResult* queryResultFromMap = iterator->second;
			queryResultFromMap->normalization(numberOfFile);
		}
	}

	void setQueryResult()
	{
		typedef std::map<string,vector<EvaluatResultFileLine*>*>::iterator it_type;
		for(it_type iterator = evalMapFile.begin(); iterator != evalMapFile.end(); iterator++) 
		{
			vector<EvaluatResultFileLine*> *resultFileLine = iterator->second;
			EvaluatResultFileLine* min = getMin(resultFileLine);
			string queryName = min->queryName;
			QueryResult *evalRes = new QueryResult();;
			evalRes->init(resultFileLine,  queryName); 
			evalInFile.insert( std::make_pair<string,QueryResult*>(iterator->first,evalRes));
		}
	}

	EvaluatResultFileLine* getMin(vector<EvaluatResultFileLine*> *resultFileLine)
	{
		vector<EvaluatResultFileLine*>::iterator it;
		EvaluatResultFileLine* min = (*resultFileLine)[0];
		for(it = resultFileLine->begin(); it != resultFileLine->end(); it++) 
		{
			if(min->queryName.size() > (*it)->queryName.size())
			{
				min = *it;
			}
		}
		return min;
	}

	string getOriginalQuery(std::set<string> *queries)
	{
		string min = *(queries->begin());
		std::set<string>::iterator it;
		for(it = queries->begin(); it != queries->end(); it++) 
		{
			if(min.size() > it->size())
			{
				min = *it;
			}
		}
		return min;
	}

	std::set<string>* getQueriesName()
	{
		return &queriesNames;
	}

	std::set<string>* getEvaluationNames()
	{
		return &evaluationNames;
	}

	vector<EvaluatResultFileLine*>* getEvaluatResultFileLine()
	{
		return &lines;
	}

}EvaluateResultFile;

typedef struct EvaluateResultFileForOneEvaluationParamter
{
	vector<EvaluatResultFileLine*> lines; // vector of all line in the file
	std::map<string,vector<EvaluatResultFileLine*>*> evalMapFile; // key= evaluation name , value= all the line with the same evaluation name;
	std::map<string, QueryResult*> evalInFile; // // key= evaluation name , value= all the data that connect to the evaluation is structure order;
	std::set<string> queriesNames;
	std::set<string> evaluationNames;

	void init(string fileName, string evalName)
	{
		ReadEvaluateFileToVector(fileName,evalName);
		mapEval();
		setQueryResult();
	}

	void Clear()
	{
		std::map<string,vector<EvaluatResultFileLine*>*>::iterator it;
		for(it = evalMapFile.begin() ; it != evalMapFile.end() ; it ++)
		{
			it->second->clear();
			delete it->second;
		}
		evalMapFile.clear();

		for (unsigned int i = 0; i < lines.size(); i++)
		{
			delete lines[i];
		}
		lines.clear();
		std::map<string,QueryResult*>::iterator it2;
		for(it2 = evalInFile.begin() ; it2 != evalInFile.end() ; it2 ++)
		{
			delete it2->second;
		}
		evalInFile.clear();

		evaluationNames.clear();
		queriesNames.clear();
	}

	// read evaluate file (file that generate in trac_eval)
	void ReadEvaluateFileToVector(string fileName,string evalName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return ;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			EvaluatResultFileLine* evalline = new EvaluatResultFileLine();

			std::string qcopy( line);
			std::istringstream oss (qcopy);

			std::string eval;
			oss >> eval;
			if ( eval.empty()) {
				return ;    
			}

			if(eval != evalName)
			{
				continue;
			}

			std::string queryName;
			oss >> queryName;
			if ( queryName.empty()) {
				return ;    
			}

			std::string evalValueStr;
			oss >> evalValueStr;
			if ( evalValueStr.empty()) {
				return ;    
			}

			double evalValue = atof(evalValueStr.c_str());

			evalline->evalName = eval;
			evalline->queryName = queryName;
			evalline->evalValue = evalValue;

			lines.push_back(evalline);

			queriesNames.insert(queryName);
		}

		finput.close();
	}

	// return for each evaluation parameter (map, P@10..) vector that contain all queries
	// evalFile - vector that contain whole file - contain all queries
	void mapEval()
	{
		int len = lines.size();
		for(int i=0 ; i < len ; ++i)
		{
			EvaluatResultFileLine* fileLine = lines[i];
			if(fileLine->queryName == "all")
			{
				continue;
			}

			string evalName = fileLine->evalName;
			evaluationNames.insert(evalName);

			std::map<string,vector<EvaluatResultFileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
			if( pos == evalMapFile.end()) // not found
			{
				vector<EvaluatResultFileLine*>* newVector = new vector<EvaluatResultFileLine*>();
				newVector->push_back(fileLine);
				evalMapFile.insert(std::make_pair<string,vector<EvaluatResultFileLine*>*> (evalName, newVector));
			}
			else
			{
				vector<EvaluatResultFileLine*>* vec = pos->second;
				vec->push_back(fileLine);
			}
		}
	}

	void add(string evalName , QueryResult* data)
	{
		evaluationNames.insert(evalName);

		QueryResult* newQueryResult= new QueryResult();
		newQueryResult->queryName = evalName;
		newQueryResult->originalQueryValue = data->originalQueryValue;
		newQueryResult->rm3100QueryValue = data->rm3100QueryValue;
		newQueryResult->rm310QueryValue = data->rm310QueryValue;

		vector< std::pair<string,double> > *result = &data->result;
		for(unsigned int j=0 ; j<result->size() ; ++j)
		{
			newQueryResult->result.push_back(std::make_pair( SSTR( (j + 1) ).c_str() , (*result)[j].second ));
		}
		evalInFile.insert(std::make_pair<string,QueryResult*> (evalName, newQueryResult));

	}

	void addAndSum(string evalName , QueryResult* data)
	{
		std::map<string,QueryResult*>::const_iterator  pos = evalInFile.find(evalName);
		if( pos == evalInFile.end()) // not found
		{
			add( evalName ,  data);
		}
		else
		{
			QueryResult* queryResultFromMap = pos->second;
			queryResultFromMap->addToSum(data);
		}
	}

	void addAndSum(EvaluateResultFile *evaluateResultFile)
	{
		typedef std::map<string,QueryResult*>::iterator it_type;
		for(it_type iterator = evaluateResultFile->evalInFile.begin(); iterator != evaluateResultFile->evalInFile.end(); iterator++) 
		{
			string evalName = iterator->first;
			QueryResult* evalValues = iterator->second;
			addAndSum(evalName,evalValues);
		}
	}

	QueryResult* getEvaluateValuesInFile(string evalName)
	{
		std::map< string , QueryResult* >::const_iterator  pos = evalInFile.find(evalName);
		if( pos == evalInFile.end()) // not found
		{
			printf("Evaluate not found in map : %s" , evalName.c_str());
			return NULL;
		}
		else
		{
			return pos->second;
		}

	}

	double getEvaluateValuesInFile(string evalName,string queryName, bool *isOk)
	{
		*isOk = false;
		std::map<string,vector<EvaluatResultFileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
		if( pos == evalMapFile.end()) // not found
		{
			printf("Evaluate not found in map : %s" , evalName.c_str());

			return -1;
		}
		else
		{
			vector<EvaluatResultFileLine*>* vec = pos->second;
			int len = vec->size();
			for(int i =0 ; i<len ; i++)
			{
				EvaluatResultFileLine* line = (*vec)[i];
				if( line->queryName == queryName)
				{
					*isOk = true;
					return line->evalValue;
				}
			}
			// printf("Query not found in map : %s" , queryName.c_str());
			return -1;
		}

	}

	void normalizationAll(int numberOfFile)
	{
		typedef std::map<string,QueryResult*>::iterator it_type;
		for(it_type iterator = evalInFile.begin(); iterator != evalInFile.end(); iterator++) 
		{
			QueryResult* queryResultFromMap = iterator->second;
			queryResultFromMap->normalization(numberOfFile);
		}
	}

	void setQueryResult()
	{
		typedef std::map<string,vector<EvaluatResultFileLine*>*>::iterator it_type;
		for(it_type iterator = evalMapFile.begin(); iterator != evalMapFile.end(); iterator++) 
		{
			vector<EvaluatResultFileLine*> *resultFileLine = iterator->second;
			EvaluatResultFileLine* min = getMin(resultFileLine);
			string queryName = min->queryName;
			QueryResult *evalRes = new QueryResult();;
			evalRes->init(resultFileLine,  queryName); 
			evalInFile.insert( std::make_pair<string,QueryResult*>(iterator->first,evalRes));
		}
	}

	EvaluatResultFileLine* getMin(vector<EvaluatResultFileLine*> *resultFileLine)
	{
		vector<EvaluatResultFileLine*>::iterator it;
		EvaluatResultFileLine* min = (*resultFileLine)[0];
		for(it = resultFileLine->begin(); it != resultFileLine->end(); it++) 
		{
			if(min->queryName.size() > (*it)->queryName.size())
			{
				min = *it;
			}
		}
		return min;
	}

	std::set<string>* getQueriesName()
	{
		return &queriesNames;
	}

	std::set<string>* getEvaluationNames()
	{
		return &evaluationNames;
	}

	vector<EvaluatResultFileLine*>* getEvaluatResultFileLine()
	{
		return &lines;
	}

}EvaluateResultFileForOneEvaluationParamter;

typedef struct AvgEvaluateResultFiles
{
	std::set<string> queriesNames;
	std::set<string> evaluationNames;

	vector<EvaluateResultFile*> EvaluateResultFiles;
	EvaluateResultFile evaluateResultFile;

	bool isInitialized;

	void init()
	{
		isInitialized = false;
	}

#pragma region Add Path to files

	void AvgFilesToAvg(vector<string> *fileNameList)
	{
		try
		{
			int numberOfFile = fileNameList->size();
			for(int i=0; i<numberOfFile ; ++i)
			{
				cout << '\r' << "	Add file to Avg-Evaluate-Result-Files : Progress : " << i <<  '/' <<  fileNameList->size() <<
					".     Progress Percent " <<  (int)(((double)i/(double)fileNameList->size()*100.0))  <<  "%" << std::flush ;
				string fileName = (*fileNameList)[i];
				AddFileToAvg(fileName);
			}
			normalizationAll(numberOfFile);
		}
		catch (std::exception& e) 
		{ 
			FILE_LOG(logERROR) << "Receive exception when adding files to Avg-Evaluate-Result-Files. caught " <<  e.what(); 
		}
		
	}

	void AddFileToAvg(string fileName)
	{
		if(isInitialized == false)
		{
			evaluateResultFile.init(fileName);
			addData(&evaluateResultFile);
			EvaluateResultFiles.push_back(&evaluateResultFile);
			isInitialized = true;
		}
		else
		{
			EvaluateResultFile *localEvaluateResultFile = new EvaluateResultFile();
			localEvaluateResultFile->init(fileName);
			evaluateResultFile.addAndSum(localEvaluateResultFile);
			addData(localEvaluateResultFile);
			EvaluateResultFiles.push_back(localEvaluateResultFile);
		}
	}

	void addData(EvaluateResultFile *evaluateResultFiles)
	{
		std::set<string> *queriesNamesFormFile = evaluateResultFiles->getQueriesName();
		std::set<string>::iterator it;
		for (it = queriesNamesFormFile->begin(); it != queriesNamesFormFile->end(); ++it)
		{
			string q = *it; // Note the "*" here
			queriesNames.insert(q);
		}

		std::set<string> *evaluationNamesFormFile = evaluateResultFiles->getEvaluationNames();
		for (it = evaluationNamesFormFile->begin(); it != evaluationNamesFormFile->end(); ++it)
		{
			string q = *it; // Note the "*" here
			evaluationNames.insert(q);
		}
	}

#pragma endregion 


#pragma region Add AvgEvaluateResultFiles to files

	void addAvgEvaluateResultFiles(std::vector<AvgEvaluateResultFiles*> *avgEvaluateResultFilesList)
	{
		int numberOfElem = avgEvaluateResultFilesList->size();
		for(int i=0; i<numberOfElem ; ++i)
		{
			addEvaluationAndQueriesNamesAvg((*avgEvaluateResultFilesList)[i]);
			if(isInitialized == false)
			{
				isInitialized = true;
			}
			evaluateResultFile.addAndSum(&(*avgEvaluateResultFilesList)[i]->evaluateResultFile );
		}
		normalizationAll(numberOfElem);
	}

	void addEvaluationAndQueriesNamesAvg(AvgEvaluateResultFiles *avgEvaluateResultFiles)
	{
		std::set<string> *queriesNamesFormFile = avgEvaluateResultFiles->getQueriesName();
		std::set<string>::iterator it;
		for (it = queriesNamesFormFile->begin(); it != queriesNamesFormFile->end(); ++it)
		{
			string q = *it; // Note the "*" here
			queriesNames.insert(q);
		}

		std::set<string> *evaluationNamesFormFile = avgEvaluateResultFiles->getEvaluationNames();
		for (it = evaluationNamesFormFile->begin(); it != evaluationNamesFormFile->end(); ++it)
		{
			string q = *it; // Note the "*" here
			evaluationNames.insert(q);
		}
		vector<EvaluateResultFile*> *evaluateResultFiles = &avgEvaluateResultFiles->EvaluateResultFiles;
		int length = evaluateResultFiles->size();
		for (int i = 0; i < length; i++)
		{
			EvaluateResultFiles.push_back((*evaluateResultFiles)[i]);
		}
	}
#pragma endregion 



	void clear()
	{
		evaluateResultFile.Clear();
		int length = EvaluateResultFiles.size();
		for (int i = 0; i < length; i++)
		{
			EvaluateResultFiles[i]->Clear();
		}
		EvaluateResultFiles.clear();
		queriesNames.clear();
		evaluationNames.clear();
	}

	void normalizationAll(int numberOfFile)
	{
		evaluateResultFile.normalizationAll(numberOfFile);
	}

	QueryResult* getEvaluateValuesInFile(string evalName)
	{
		return evaluateResultFile.getEvaluateValuesInFile(evalName);
	}

	double getEvaluateValuesInFile(string evalName , string queryName)
	{
		int length = EvaluateResultFiles.size();
		for (int i = 0; i < length; i++)
		{
			bool isOk;
			double value = EvaluateResultFiles[i]->getEvaluateValuesInFile(evalName,queryName,&isOk);
			if(isOk)
			{
				return value;
			}
		}
		//printf("Query not found in map : %s" , queryName.c_str());
		return -1;
	}

	std::set<string> *getQueriesName()
	{
		return &(queriesNames);
	}

	std::set<string> *getEvaluationNames()
	{
		return &(evaluationNames);
	}



}AvgEvaluateResultFiles;


typedef struct AvgEvaluateResult
{
	std::set<string> evaluationNames;

	EvaluateResultFile evaluateResultFile;

	bool isInitialized;

	void init()
	{
		isInitialized = false;
	}

#pragma region Add Path to files

	void AvgFilesToAvg(vector<string> *fileNameList)
	{
		try
		{
			int numberOfFile = fileNameList->size();
			for(int i=0; i<numberOfFile ; ++i)
			{
				string fileName = (*fileNameList)[i];

				FILE_LOG(logINFO) << "Add file to Avg-Evaluate-Result-Files : Progress : " << i <<  '/' <<  numberOfFile <<	". Progress Percent " <<  (int)(((double)i/(double)numberOfFile*100.0))  <<  "%.  file Name = " << fileName;

				AddFileToAvg(fileName);
			}
			FILE_LOG(logDEBUG) << "AvgFilesToAvg.AddFileToAvg  - Normalization";
			normalizationAll(numberOfFile);
		}
		catch (std::exception& e) 
		{ 
			FILE_LOG(logERROR) << "Receive exception when adding files to Avg-Evaluate-Result-Files. caught " <<  e.what(); 
		}
		
	}

	void AddFileToAvg(string fileName)
	{
		FILE_LOG(logDEBUG) << "AvgEvaluateResult.AddFileToAvg  - Start";
		if(isInitialized == false)
		{
			FILE_LOG(logDEBUG) << "AvgEvaluateResult.AddFileToAvg  - Create first file to the Avg Files";
			evaluateResultFile.init(fileName);
			addData(&evaluateResultFile);
			isInitialized = true;
		}
		else
		{
			EvaluateResultFile localEvaluateResultFile;
			
			FILE_LOG(logDEBUG) << "AvgEvaluateResult.AddFileToAvg  - Init file to the Avg Files";
			localEvaluateResultFile.init(fileName);
			
			FILE_LOG(logDEBUG) << "AvgEvaluateResult.AddFileToAvg  - Add  evaluation name form file";
			addData(&localEvaluateResultFile);

			FILE_LOG(logDEBUG) << "AvgEvaluateResult.AddFileToAvg  - Add file to the Avg Files";
			evaluateResultFile.addAndSum(&localEvaluateResultFile);
			
			FILE_LOG(logDEBUG) << "AvgEvaluateResult.AddFileToAvg  - Clear file form memory";
			localEvaluateResultFile.Clear();
		}
		FILE_LOG(logDEBUG) << "AvgEvaluateResult.AddFileToAvg  - Finish";
	}

	void addData(EvaluateResultFile *evaluateResultFiles)
	{
		std::set<string>::iterator it;


		std::set<string> *evaluationNamesFormFile = evaluateResultFiles->getEvaluationNames();
		if(evaluationNamesFormFile == NULL)
		{
			FILE_LOG(logERROR) << "AvgEvaluateResult.addData  - Receive NULL reference for evaluation list : " ;
			return;
		}

		FILE_LOG(logDEBUG) << "AvgEvaluateResult.addData  - Add queries : " << evaluationNamesFormFile->size() << " Size of current queries  is " << evaluationNames.size();
		for (it = evaluationNamesFormFile->begin(); it != evaluationNamesFormFile->end(); ++it)
		{
			string q = *it; // Note the "*" here

			std::set<string>::iterator evaluationNamesIt = evaluationNames.find(q);
			if(evaluationNamesIt == evaluationNames.end())
			{
				evaluationNames.insert(q);
			}

		}
	}

#pragma endregion 

	void clear()
	{
		evaluateResultFile.Clear();
		evaluationNames.clear();
	}

	void normalizationAll(int numberOfFile)
	{
		evaluateResultFile.normalizationAll(numberOfFile);
	}

	QueryResult* getEvaluateValuesInFile(string evalName)
	{
		return evaluateResultFile.getEvaluateValuesInFile(evalName);
	}


	std::set<string> *getEvaluationNames()
	{
		return &(evaluationNames);
	}



}AvgEvaluateResult;




typedef struct ResultToPrint
{
	std::map< string,double > listOfSingleValue;
	std::map< string,vector<double> *> listOfMultipleValue;

	void writeFileForPlot( string path)
	{
		ofstream of;
		std::cout << "Create file " << std::endl;
		of.open ( path.c_str(), std::ofstream::out ); 

		// Single Values + names
		std::map<string,double>::iterator listOfSingleValue_it ;
		string singleValueNems = "";
		string singleValue = "";
		string columnheader = "\"index\" ";

		std::cout << "Start write Single Values header " << std::endl;
		for(listOfSingleValue_it = listOfSingleValue.begin() ; listOfSingleValue_it != listOfSingleValue.end() ; listOfSingleValue_it++  )
		{
			singleValueNems +=  listOfSingleValue_it->first + " ,"   ;
			columnheader +=  "\"" + listOfSingleValue_it->first + "\" ";

			std::ostringstream strs;
			strs << listOfSingleValue_it->second;
			std::string str = strs.str();
			singleValue += str + " " ;
		}
		singleValueNems = trimEnd(singleValueNems,",");


		// Multiple Value names
		std::cout << "Start write Multiple Values header " << std::endl;
		std::map<string,vector<double>*>::iterator multipleValue_it ;
		string multipleValueNems = "";
		for(multipleValue_it = listOfMultipleValue.begin() ; multipleValue_it != listOfMultipleValue.end() ; multipleValue_it++  )
		{
			multipleValueNems += multipleValue_it->first + ", ";
			columnheader +=  "\"" + multipleValue_it->first + "\" ";
		}
		multipleValueNems = trimEnd(multipleValueNems,",");

		// write the heading
		of << "# index, " << singleValueNems << " , " << multipleValueNems  <<  std::endl;
		of << columnheader <<  std::endl;


		int length = listOfMultipleValue.begin()->second->size();
		int i;
		string multipleValue = "";
		std::cout << "Start write to file. length =  " << length << std::endl;
		for ( i = 0; i < length; i++)
		{
			multipleValue = "";
			for(multipleValue_it = listOfMultipleValue.begin() ; multipleValue_it != listOfMultipleValue.end() ; multipleValue_it++  )
			{				

				vector<double>* values = multipleValue_it->second;

				std::ostringstream strs;
				strs << (*values)[i] ;
				std::string str = strs.str();
				multipleValue += str + " " ;
			}

			of << i  << " "  << singleValue << " " << multipleValue << std::endl;
		}

		// print the last line again - for plot 
		of << i  << " "  << singleValue << " " << multipleValue << std::endl;
		std::cout << "end write to file " << std::endl;
		of.close();
	}

	void writeOnlyMultipleValue(string path)
	{
		ofstream of;
		of.open ( path.c_str(), std::ofstream::out ); 
		// Multiple Value names
		std::map<string,vector<double>*>::iterator multipleValue_it ;
		string multipleValueNems = "";
		for(multipleValue_it = listOfMultipleValue.begin() ; multipleValue_it != listOfMultipleValue.end() ; multipleValue_it++  )
		{
			multipleValueNems += multipleValue_it->first + ", ";
		}
		multipleValueNems = trimEnd(multipleValueNems,",");

		// write the heading
		of << "# index, " << multipleValueNems <<  std::endl;

		int length = listOfMultipleValue.begin()->second->size();
		int i;
		string multipleValue = "";
		for ( i = 0; i < length; i++)
		{
			multipleValue = "";
			for(multipleValue_it = listOfMultipleValue.begin() ; multipleValue_it != listOfMultipleValue.end() ; multipleValue_it++  )
			{
				vector<double>* values = multipleValue_it->second;

				std::ostringstream strs;
				strs << (*values)[i] ;
				std::string str = strs.str();
				multipleValue += str + " " ;
			}

			of << i  << " "  << multipleValue << std::endl;
		}

		// print the last line again - for plot 
		of << i  << " "  << multipleValue << std::endl;
		of.close();
	}

	void writeOnlySingelValue(string path)
	{
		ofstream of;
		std::cout << "Create file " << std::endl;
		of.open ( path.c_str(), std::ofstream::out ); 

		// Single Values + names
		std::map<string,double>::iterator listOfSingleValue_it ;
		string singleValueNems = "";
		string singleValue = "";
		string columnheader = "\"index\" ";

		std::cout << "Start write Single Values header " << std::endl;
		for(listOfSingleValue_it = listOfSingleValue.begin() ; listOfSingleValue_it != listOfSingleValue.end() ; listOfSingleValue_it++  )
		{
			singleValueNems +=  listOfSingleValue_it->first + " ,"   ;
			columnheader +=  "\"" + listOfSingleValue_it->first + "\" ";

			std::ostringstream strs;
			strs << listOfSingleValue_it->second;
			std::string str = strs.str();
			singleValue += str + " " ;
		}
		singleValueNems = trimEnd(singleValueNems,",");

		// write the heading
		of << "# index, " << singleValueNems  <<  std::endl;
		of << columnheader <<  std::endl;

		std::cout << "Start write to file. " << std::endl;
		for (int i = 0; i < 101; i++)
		{
			of << i  << " "  << singleValue  << std::endl;
		}

		std::cout << "end write to file " << std::endl;
		of.close();
	}

	string toString()
	{
		return "ResultToPrint = listOfSingleValue size is : " + SSTR(listOfSingleValue.size()) + " listOfSingleValue size is : " +  SSTR(listOfMultipleValue.size());
	}


	void writeCSVFile( string path)
	{
		ofstream of;
		path = path + ".csv";
		std::cout << "Create file " << std::endl;
		of.open ( path.c_str(), std::ofstream::out ); 

		// Single Values + names
		std::map<string,double>::iterator listOfSingleValue_it ;
		string singleValueNems = "";
		string singleValue = "";

		std::cout << "Start write Single Values header " << std::endl;
		for(listOfSingleValue_it = listOfSingleValue.begin() ; listOfSingleValue_it != listOfSingleValue.end() ; listOfSingleValue_it++  )
		{
			singleValueNems +=  listOfSingleValue_it->first + " ,"   ;

			std::ostringstream strs;
			strs << std::fixed  << listOfSingleValue_it->second;
			std::string str = strs.str();
			singleValue += str + " , " ;
		}
		singleValueNems = trimEnd(singleValueNems,",");
		singleValue = trimEnd(singleValue,",");

		// Multiple Value names
		std::cout << "Start write Multiple Values header " << std::endl;
		std::map<string,vector<double>*>::iterator multipleValue_it ;
		string multipleValueNems = "";
		for(multipleValue_it = listOfMultipleValue.begin() ; multipleValue_it != listOfMultipleValue.end() ; multipleValue_it++  )
		{
			multipleValueNems += multipleValue_it->first + ", ";
		}
		multipleValueNems = trimEnd(multipleValueNems,",");

		// write the heading
		of << "# index , " << singleValueNems << " , " << multipleValueNems  <<  std::endl;


		int length = listOfMultipleValue.begin()->second->size();
		int i;
		string multipleValue = "";
		std::cout << "Start write to file. length =  " << length << std::endl;
		for ( i = 0; i < length; i++)
		{
			multipleValue = "";
			for(multipleValue_it = listOfMultipleValue.begin() ; multipleValue_it != listOfMultipleValue.end() ; multipleValue_it++  )
			{				

				vector<double>* values = multipleValue_it->second;

				std::ostringstream strs;
				strs << std::fixed  << (*values)[i] ;
				std::string str = strs.str();
				multipleValue += str + " , " ;


			}
			multipleValue = trimEnd(multipleValue,",");
			of << i  << " , "  << singleValue << " , " << multipleValue << std::endl;
		}

		// print the last line again - for plot 
		of << i  << " , "  << singleValue << " , " << multipleValue << std::endl;
		std::cout << "end write to file " << std::endl;
		of.close();
	}

	void Clear()
	{
		std::map<string,vector<double>*>::iterator multipleValue_it ;
		for(multipleValue_it = listOfMultipleValue.begin() ; multipleValue_it != listOfMultipleValue.end() ; multipleValue_it++  )
		{
			delete multipleValue_it->second;
		}
		listOfMultipleValue.clear();
		listOfSingleValue.clear();
	}

}ResultToPrint;

typedef struct EvaluateFile
{
	vector<EvaluatResultFileLine*> lines; // vector of all line in the file
	std::map<string,vector<EvaluatResultFileLine*>*> evalMapFile; // key= evaluation name , value= all the line with the same evaluation name;

	std::set<string> queriesNames;
	std::set<string> evaluationNames;

	void init(string fileName)
	{
		ReadEvaluateFileToVector(fileName);
		mapEval();
	}

	void Clear()
	{
		std::map<string,vector<EvaluatResultFileLine*>*>::iterator it;
		for(it = evalMapFile.begin() ; it != evalMapFile.end() ; it ++)
		{
			it->second->clear();
			delete it->second;
		}
		evalMapFile.clear();

		for (unsigned int i = 0; i < lines.size(); i++)
		{
			delete lines[i];
		}
		lines.clear();

		evaluationNames.clear();
		queriesNames.clear();
	}

	void ReadEvaluateFileToVector(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return ;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			EvaluatResultFileLine* evalline = new EvaluatResultFileLine();

			std::string qcopy( line);
			std::istringstream oss (qcopy);

			std::string eval;
			oss >> eval;
			if ( eval.empty()) {
				return ;    
			}

			std::string queryName;
			oss >> queryName;
			if ( queryName.empty()) {
				return ;    
			}

			std::string evalValueStr;
			oss >> evalValueStr;
			if ( evalValueStr.empty()) {
				return ;    
			}

			double evalValue = atof(evalValueStr.c_str());

			evalline->evalName = eval;
			evalline->queryName = queryName;
			evalline->evalValue = evalValue;

			lines.push_back(evalline);

			queriesNames.insert(queryName);
		}

		finput.close();
	}

	void mapEval()
	{
		int len = lines.size();
		for(int i=0 ; i < len ; ++i)
		{
			EvaluatResultFileLine* fileLine = lines[i];
			if(fileLine->queryName == "all")
			{
				continue;
			}

			string evalName = fileLine->evalName;
			evaluationNames.insert(evalName);

			std::map<string,vector<EvaluatResultFileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
			if( pos == evalMapFile.end()) // not found
			{
				vector<EvaluatResultFileLine*>* newVector = new vector<EvaluatResultFileLine*>();
				newVector->push_back(fileLine);
				evalMapFile.insert(std::make_pair<string,vector<EvaluatResultFileLine*>*> (evalName, newVector));
			}
			else
			{
				vector<EvaluatResultFileLine*>* vec = pos->second;
				vec->push_back(fileLine);
			}
		}
	}

	double getEvaluateValuesInFile(string evalName,string queryName, bool *isOk)
	{
		*isOk = false;
		std::map<string,vector<EvaluatResultFileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
		if( pos == evalMapFile.end()) // not found
		{
			printf("Evaluate not found in map : %s" , evalName.c_str());
			return -1;
		}
		else
		{
			vector<EvaluatResultFileLine*>* vec = pos->second;
			int len = vec->size();
			for(int i =0 ; i<len ; i++)
			{
				EvaluatResultFileLine* line = (*vec)[i];
				if( line->queryName == queryName)
				{
					*isOk = true;
					return line->evalValue;
				}
			}
			return -1;
		}

	}

	vector<EvaluatResultFileLine*>* getEvaluateValues(string evalName)
	{
		std::map<string,vector<EvaluatResultFileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
		if( pos == evalMapFile.end()) // not found
		{
			printf("Evaluate not found in map : %s" , evalName.c_str());
			return NULL;
		}
		else
		{
			vector<EvaluatResultFileLine*>* vec = pos->second;
			return vec;
		}
	}

	EvaluatResultFileLine* getMin(vector<EvaluatResultFileLine*> *resultFileLine)
	{
		vector<EvaluatResultFileLine*>::iterator it;
		EvaluatResultFileLine* min = (*resultFileLine)[0];
		for(it = resultFileLine->begin(); it != resultFileLine->end(); it++) 
		{
			if(min->queryName.size() > (*it)->queryName.size())
			{
				min = *it;
			}
		}
		return min;
	}

	string getOriginalQueryName()
	{
		return getMin(&lines)->queryName;
	}

	string getOriginalQuery(std::set<string> *queries)
	{
		string min = *(queries->begin());
		std::set<string>::iterator it;
		for(it = queries->begin(); it != queries->end(); it++) 
		{
			if(min.size() > it->size())
			{
				min = *it;
			}
		}
		return min;
	}

	std::set<string>* getQueriesName()
	{
		return &queriesNames;
	}

	std::set<string>* getEvaluationNames()
	{
		return &evaluationNames;
	}

	vector<EvaluatResultFileLine*>* getEvaluatResultFileLine()
	{
		return &lines;
	}

}EvaluateFile;


typedef struct Rm3CVFileLine
{
	string evalName;
	double rm310CVValue;
	double rm3100CVValue;
}Rm3CVFileLine;

typedef struct Rm3CVFile
{
	std::map<string,Rm3CVFileLine> lines;
	void add(Rm3CVFileLine line)
	{
		lines.insert(std::make_pair<string,Rm3CVFileLine> (line.evalName, line));
	}
	Rm3CVFileLine getRm3CVFileLine(string evalName)
	{
		std::map< string , Rm3CVFileLine >::const_iterator  pos = lines.find(evalName);
		return pos->second;
	}
	void init(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			Rm3CVFileLine cvline ;

			std::string qcopy( line);
			std::istringstream oss (qcopy);

			std::string eval;
			oss >> eval;
			if ( eval.empty()) {
				return;    
			}

			std::string rm310CVValueStr;
			oss >> rm310CVValueStr;
			if ( rm310CVValueStr.empty()) {
				return;    
			}

			std::string erm3100CVValueStr;
			oss >> erm3100CVValueStr;
			if ( erm3100CVValueStr.empty()) {
				return;    
			}

			double rm310CVValue = atof(rm310CVValueStr.c_str());
			double rm3100CVValue = atof(erm3100CVValueStr.c_str());

			cvline.evalName = eval;
			cvline.rm310CVValue = rm310CVValue;
			cvline.rm3100CVValue = rm3100CVValue;

			add(cvline);
		}

		finput.close();
	}

	static void CreateFilesForGnuplot_ForRM3CrossValidation(string crossValidationfile, string pathToDir)
	{
		std::cout << "Start - read Cross Validation file." << std::endl;
		Rm3CVFile rm3CvFile;
		rm3CvFile.init(crossValidationfile);

		std::map<string,ResultToPrint*> evaluationPerfDocsResult;

		std::cout << "Start - built a evaluation parameter per configuration (before write the file)" << std::endl;	
		std::map<string,Rm3CVFileLine>::iterator it;
		for(it = rm3CvFile.lines.begin() ; it != rm3CvFile.lines.end() ; ++it)
		{
			ResultToPrint* evalfDocsResult = new ResultToPrint();
			string evalName = it->first;
			Rm3CVFileLine rm3 = it->second;
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Cross Validation Rm3-10 Terms",rm3.rm310CVValue));
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Cross Validation Rm3-100 Terms",rm3.rm3100CVValue));
			evaluationPerfDocsResult.insert(std::make_pair<string,ResultToPrint*>(evalName,evalfDocsResult));
		}

		std::cout << "Start - write files" << std::endl;
		std::map<string,ResultToPrint*>::iterator  evaluationPerfDocsResultIterator;
		for(evaluationPerfDocsResultIterator = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIterator != evaluationPerfDocsResult.end(); evaluationPerfDocsResultIterator++) 
		{
			string evalName = evaluationPerfDocsResultIterator->first;
			ResultToPrint* fDocsResultToPlot = evaluationPerfDocsResultIterator->second;
			string path = pathToDir + "/" + evalName + ".dat";
			std::cout << "     write files for : " << evalName + ".dat   ,   " << fDocsResultToPlot->toString()<< std::endl;
			fDocsResultToPlot->writeOnlySingelValue(path); 
		}


		std::map<string,ResultToPrint*>::iterator evaluationPerfDocsResultIt;
		for (evaluationPerfDocsResultIt = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIt != evaluationPerfDocsResult.end() ; evaluationPerfDocsResultIt++)
		{
			evaluationPerfDocsResultIt->second->Clear();
			delete evaluationPerfDocsResultIt->second;
		}
		evaluationPerfDocsResult.clear();



	}
}Rm3CVFile;



class EvaluateFileOperation
{
public:

	static void CreateClusterPerformanceAverage(vector<string> clustresPerformanceFiles, string pathToDir)
	{
		try
		{
			FILE_LOG(logDEBUG) << "Create Cluster Performance Average DAT files"; 

			if(clustresPerformanceFiles.size() == 0)
			{
				FILE_LOG(logERROR) << "Failed to Create Cluster Performance Average files - Number of received files are 0"; 
				return;
			}

			if( pathToDir.size() < 3)
			{
				FILE_LOG(logERROR) << "Failed to Create Cluster Performance Average files - Bad output dir name"; 
				return;
			}

			AvgEvaluateResult avgEvaluateResultFiles;
			avgEvaluateResultFiles.init();
			FILE_LOG(logDEBUG) << "Start calculate Cluster Performance Average for all received files, Number of files are = " << clustresPerformanceFiles.size(); 
			avgEvaluateResultFiles.AvgFilesToAvg(&clustresPerformanceFiles);
			FILE_LOG(logDEBUG) << "Create Cluster Performance Average files"  ; 

			std::map<string,ResultToPrint*> evaluationPerfDocsResult;
			std::set<string> *evals = avgEvaluateResultFiles.getEvaluationNames();
			for( std::set<string>::iterator it = evals->begin() ; it != evals->end() ; ++it)
			{
				string evalName = *it;
				ResultToPrint* evalfDocsResult = new ResultToPrint();
				QueryResult* qrAll = avgEvaluateResultFiles.evaluateResultFile.getEvaluateValuesInFile(evalName);
				vector<double> *data = qrAll->getClustresValues();
				evalfDocsResult->listOfSingleValue.insert(std::make_pair("Average Over All Clusters",qrAll->avg));
				evalfDocsResult->listOfMultipleValue.insert(std::make_pair("Cluster Performance Average Over All Files",data));
				evaluationPerfDocsResult.insert(std::make_pair(evalName,evalfDocsResult));
			}

			FILE_LOG(logDEBUG) <<  "Start to write dat files : " ;
			std::map<string,ResultToPrint*>::iterator  evaluationPerfDocsResultIterator;
			for(evaluationPerfDocsResultIterator = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIterator != evaluationPerfDocsResult.end(); evaluationPerfDocsResultIterator++) 
			{
				string evalName = evaluationPerfDocsResultIterator->first;
				ResultToPrint* fDocsResultToPlot = evaluationPerfDocsResultIterator->second;
				string path = pathToDir + "/" + evalName + ".dat";
				FILE_LOG(logDEBUG) << "     write file for : " << evalName + ".dat   ,   " << fDocsResultToPlot->toString() ;
				fDocsResultToPlot->writeFileForPlot(path); 
			}

			FILE_LOG(logDEBUG) <<  "Finished to create Cluster Performance Average DAT files. clear all data from memory." ;
			avgEvaluateResultFiles.clear();

			std::map<string,ResultToPrint*>::iterator evaluationPerfDocsResultIt;
			for (evaluationPerfDocsResultIt = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIt != evaluationPerfDocsResult.end() ; evaluationPerfDocsResultIt++)
			{
				evaluationPerfDocsResultIt->second->Clear();
				delete evaluationPerfDocsResultIt->second;
			}
			evaluationPerfDocsResult.clear();
		}
		catch(const std::exception& e)
		{
			FILE_LOG(logERROR) << "Received exception when try to create Cluster Performance Average DAT files. exception = " << e.what();
		}
	}


private:

};

#endif