#ifndef GeneralModelFunctions_H
#define GeneralModelFunctions_H

#include "Evaluate.h"
#include "ClustersFeature.h"


typedef struct GeneralModel
{
	typedef struct QueryPerformance
	{
		string clusterName;
		double clusterPerformance;
		map< string , double > clusterFeatures;
	}QueryPerformance;


	typedef struct QueryPerformanceList
	{
		vector<QueryPerformance> QueryPerformances;
		std::set<string> ClusterFeaturesNames;
		
		void Sort()
		{
			std::sort(QueryPerformances.begin(), QueryPerformances.end(), descending_order_compare_QueryPerformance_by_Performance());
		}

		void Add(QueryPerformanceList* queryPerformanceList)
		{
			queryPerformanceList->Sort();

			if(QueryPerformances.size() == 0 )
			{
				QueryPerformances = (*queryPerformanceList).QueryPerformances;
				ClusterFeaturesNames = (*queryPerformanceList).ClusterFeaturesNames;
				return;
			}

			for (int i = 0; i < QueryPerformances.size(); i++)
			{
				QueryPerformances[i].clusterPerformance += (*queryPerformanceList).QueryPerformances[i].clusterPerformance;
				QueryPerformances[i].clusterPerformance = QueryPerformances[i].clusterPerformance / 2.0;

				map< string , double >::iterator clusterFeatures_it;
				for( clusterFeatures_it = QueryPerformances[i].clusterFeatures.begin() ; clusterFeatures_it != QueryPerformances[i].clusterFeatures.end() ; clusterFeatures_it++  )
				{				
					clusterFeatures_it->second += (*queryPerformanceList).QueryPerformances[i].clusterFeatures.find(clusterFeatures_it->first)->second;
					clusterFeatures_it->second = clusterFeatures_it->second / 2.0;
				}
			}
		}

		void writeFileForPlot( string path)
		{
			ofstream of;
			std::cout << "Create file " << std::endl;
			of.open ( path.c_str(), std::ofstream::out ); 

			string singleValueNems = "Cluster Performance ,";
			string columnheader = "\"index\"  \"Cluster Performance\" ";

			std::cout << "Start write header : " << std::endl;
			std::set<string>::iterator ClusterFeaturesNames_it;
			for( ClusterFeaturesNames_it = ClusterFeaturesNames.begin() ; ClusterFeaturesNames_it != ClusterFeaturesNames.end(); ++ClusterFeaturesNames_it)
			{
				singleValueNems +=  *ClusterFeaturesNames_it + " ,"   ;
				columnheader +=  "\"" + *ClusterFeaturesNames_it + "\" ";
			}
			singleValueNems = trimEnd(singleValueNems,",");

			// write the heading
			of << "# index, " << singleValueNems  <<  std::endl;
			of << columnheader <<  std::endl;
			
			string line;
			int i;
			for ( i = 0; i < QueryPerformances.size() ; i++)
			{
				line = "";

				std::ostringstream strs;
				strs << QueryPerformances[i].clusterPerformance ;
				std::string str = strs.str();
				line += str + " " ;

				map< string , double >::iterator clusterFeatures_it;
				for( clusterFeatures_it = QueryPerformances[i].clusterFeatures.begin() ; clusterFeatures_it != QueryPerformances[i].clusterFeatures.end() ; clusterFeatures_it++  )
				{				
					std::ostringstream strs;
					strs << clusterFeatures_it->second ;
					std::string str = strs.str();
					line += str + " " ;
				}
				of << i  << " "  << line  << std::endl;
			}
			of << i  << " "  << line  << std::endl;

			std::cout << "end write to file " << std::endl;
			of.close();
		}


		void CalcCorrelation(map< string , double > *correlationList)
		{
			int length = QueryPerformances.size();
			vector<double> Performance;
			map< string , vector<double> > clusterFeatures;

			std::set<string>::iterator ClusterFeaturesNamesIterator ;
			for (ClusterFeaturesNamesIterator = ClusterFeaturesNames.begin() ; ClusterFeaturesNamesIterator != ClusterFeaturesNames.end(); ClusterFeaturesNamesIterator++)
			{
				vector<double> values;
				clusterFeatures.insert(std::make_pair(*ClusterFeaturesNamesIterator , values));
			}

			for (int i = 0; i < length; i++)
			{
				Performance.push_back(QueryPerformances[i].clusterPerformance);
				map< string , double >::iterator it; // clusterFeatures
				for( it = QueryPerformances[i].clusterFeatures.begin() ;
					it != QueryPerformances[i].clusterFeatures.end();
					++it)
				{
					clusterFeatures.find(it->first)->second.push_back(it->second) ;
				}
			}
			map< string , vector<double> >::iterator clusterFeaturesIt ;
			for( clusterFeaturesIt = clusterFeatures.begin() ;
				clusterFeaturesIt != clusterFeatures.end();
				++clusterFeaturesIt)
			{
				double corl = my_correlation(&Performance , &clusterFeaturesIt->second);
				correlationList->insert(std::make_pair(clusterFeaturesIt->first , corl));
			}
		}

	}QueryPerformanceList;

	struct descending_order_compare_QueryPerformance_by_Performance
	{
		inline bool operator()(const QueryPerformance& p1, const QueryPerformance& p2)
		{
			if ( p1.clusterPerformance > p2.clusterPerformance ) return true;
			return false;
		}   
	};


	typedef struct CalcCorrelationData
	{
		string Key;
		double Min , Max , Avg;

		void Init(string key)
		{
			Key = key;
			Min = Max = Avg = 0.0;
		}

		void Add(double value)
		{
			Avg = ( Avg + value) / 2.0;
			Min = Min > value ? value : Min;
			Max = Max < value ? value : Max;
		}

		string ToString()
		{
			return Key + " " + SSTR_4(Max) + " " + SSTR_4(Min) + " " + SSTR_4(Avg) ;
		}

	}CalcCorrelationData;

	typedef struct CalcCorrelationHelper
	{
		map< string , CalcCorrelationData > CorrelationList;

		void Add(map< string , double > *correlationList)
		{
			map< string , double >::iterator correlationListIt ;
			for( correlationListIt = correlationList->begin() ;
				correlationListIt != correlationList->end();
				++correlationListIt)
			{
				map< string , CalcCorrelationData >::iterator it = CorrelationList.find(correlationListIt->first);
				if(it != CorrelationList.end())
				{
					it->second.Add(correlationListIt->second);
				}
				else
				{
					CalcCorrelationData calcCorrelationData;
					calcCorrelationData.Init(correlationListIt->first);
					calcCorrelationData.Add(correlationListIt->second);
					CorrelationList.insert( std::make_pair(correlationListIt->first ,calcCorrelationData ) );
				}
			}
		}


		void write(string file)
		{
			ofstream of;
			std::cout << "Create file " << std::endl;
			of.open ( file.c_str(), std::ofstream::out ); 

			string columnheader = "Feature , Max , Min , Avg";
			//of << columnheader <<  std::endl;
			

			map< string , CalcCorrelationData >::iterator it;
			for( it = CorrelationList.begin() ;
				it != CorrelationList.end();
				++it)
			{
				of << it->second.ToString()  << std::endl;
			}

			std::cout << "end write to file " << std::endl;
			of.close();
		}

	}CalcCorrelationHelper;

	void PrintClusterPerformanceByFeature(vector<string> clusterEvaluationFile , vector<string> clusterseature , string pathToDir)
	{
		vector<QueriesClustersFeature> ClustersFeatures;
		for (int i = 0; i < clusterseature.size() ; i++)
		{
			QueriesClustersFeature feature;
			feature.Init(clusterseature[i] , 0.0);
			ClustersFeatures.push_back(feature);
		}
		
		map< string , QueryPerformanceList > evaluationClustersPerformance;
		
		EvaluateFile evaluateFile;
		evaluateFile.init(clusterEvaluationFile[0]);
		std::set<string>* evaluationNames = evaluateFile.getEvaluationNames();
		std::set<string>::iterator it;
		for(it = evaluationNames->begin(); it != evaluationNames->end(); it++) 
		{
			string evalName = *it;
			
			QueryPerformanceList queryPerformance;

			for (int i = 0; i < clusterEvaluationFile.size() ; i++)
			{
				QueryPerformanceList* queryDaya = CreateClusterPerformanceByFeature(evalName , clusterEvaluationFile[i] , &ClustersFeatures);
				queryPerformance.Add(queryDaya);
				delete queryDaya;
			}

			string path = pathToDir + "/" + evalName + ".dat";
			queryPerformance.writeFileForPlot(path);
		}
		

		
	}


	void CalcCorrlationClusterPerformanceByFeature(vector<string> clusterEvaluationFile , vector<string> clusterseature , string pathToDir)
	{
		vector<QueriesClustersFeature> ClustersFeatures;
		for (int i = 0; i < clusterseature.size() ; i++)
		{
			QueriesClustersFeature feature;
			feature.Init(clusterseature[i] , 0.0);
			ClustersFeatures.push_back(feature);
		}
		
		CalcCorrelationHelper calcCorrelationHelper;
		
		EvaluateFile evaluateFile;
		evaluateFile.init(clusterEvaluationFile[0]);
		std::set<string>* evaluationNames = evaluateFile.getEvaluationNames();
		std::set<string>::iterator it;
		for(it = evaluationNames->begin(); it != evaluationNames->end(); it++) 
		{
			string evalName = *it;
			
			QueryPerformanceList queryPerformance;

			for (int i = 0; i < clusterEvaluationFile.size() ; i++)
			{
				QueryPerformanceList* queryDaya = CreateClusterPerformanceByFeature(evalName , clusterEvaluationFile[i] , &ClustersFeatures);
				map< string , double > correlationList;
				queryDaya->CalcCorrelation(&correlationList);
				calcCorrelationHelper.Add(&correlationList);
				delete queryDaya;
			}

			string path = pathToDir + "/" + evalName + ".txt";
			calcCorrelationHelper.write(path);
		}
		

		
	}

	QueryPerformanceList* CreateClusterPerformanceByFeature(string evalName , string clusterEvaluationFile , vector<QueriesClustersFeature>* clusterseature)
	{
		EvaluateFile evaluateFile;
		evaluateFile.init(clusterEvaluationFile);
		string queryName = getFileName_Query(clusterEvaluationFile);

		QueryPerformanceList *queryPerformance = new QueryPerformanceList();

		vector<EvaluatResultFileLine*>* evaluatResultFileLineList = evaluateFile.getEvaluateValues( evalName);
		for (int i = 0; i < evaluatResultFileLineList->size(); i++)
		{
			EvaluatResultFileLine* evaluatResultFileLine = (*evaluatResultFileLineList)[i]; 
			QueryPerformance performance;
			performance.clusterName = evaluatResultFileLine->queryName;
			performance.clusterPerformance = evaluatResultFileLine->evalValue;
			string clusterName =  getClusterNameFormQueryClsuter(performance.clusterName);

			for (int i = 0; i < clusterseature->size() ; i++)
			{
				ClustersFeatureInQuery* clustersFeatureInQuery = (*clusterseature)[i].GetClustersFeatureForQuery(queryName);
					
				double value = clustersFeatureInQuery->GetClusterProbability(clusterName);
				string name = (*clusterseature)[i].FeatureName;
				performance.clusterFeatures.insert( std::make_pair(name , value) );

				queryPerformance->ClusterFeaturesNames.insert(name);
			}
			queryPerformance->QueryPerformances.push_back(performance);
		}
		queryPerformance->Sort();
		return queryPerformance;
	}


	void CreateClusterPerformanceByFeature(string clusterEvaluationFile , vector<QueriesClustersFeature>* clusterseature)
	{
		EvaluateFile evaluateFile;
		evaluateFile.init(clusterEvaluationFile);
		string queryName = getFileName_Query(clusterEvaluationFile);

		std::set<string>* evaluationNames = evaluateFile.getEvaluationNames();
		std::set<string>::iterator it;
		
		map< string , QueryPerformanceList > evaluationClustersPerformance;

		for(it = evaluationNames->begin(); it != evaluationNames->end(); it++) 
		{
			string evalName = *it;
			QueryPerformanceList queryPerformance;

			vector<EvaluatResultFileLine*>* evaluatResultFileLineList = evaluateFile.getEvaluateValues( evalName);
			for (int i = 0; i < evaluatResultFileLineList->size(); i++)
			{
				EvaluatResultFileLine* evaluatResultFileLine = (*evaluatResultFileLineList)[i]; 
				QueryPerformance performance;
				performance.clusterName = evaluatResultFileLine->queryName;
				performance.clusterPerformance = evaluatResultFileLine->evalValue;
				string clusterName =  getClusterNameFormQueryClsuter(performance.clusterName);

				for (int i = 0; i < clusterseature->size() ; i++)
				{
					ClustersFeatureInQuery* clustersFeatureInQuery = (*clusterseature)[i].GetClustersFeatureForQuery(queryName);
					
					double value = clustersFeatureInQuery->GetClusterProbability(clusterName);
					string name = (*clusterseature)[i].FeatureName;
					performance.clusterFeatures.insert( std::make_pair(name , value) );

					queryPerformance.ClusterFeaturesNames.insert(name);
				}
				queryPerformance.QueryPerformances.push_back(performance);
			}
			queryPerformance.Sort();

			
			evaluationClustersPerformance.insert(std::make_pair(evalName , queryPerformance) );	
		}

	}



	void CreateReducedExtensionQueriesFile(string queriesFileName , int numberOfTerms , string outputQueriesFileName)
	{
		QueriesFile queriesFile;
		queriesFile.readFile(queriesFileName);

		
		ofstream of;
		of.open ( outputQueriesFileName.c_str(), std::ofstream::out ); 
		of << "<parameters>\n";

		map<string,string >::iterator it;
		for(it = queriesFile.Queries.begin() ; it != queriesFile.Queries.end();  ++it)
		{
			string queryName = it->first;
			string queryText = it->second;

			QueryFromXmlFile queryFromXmlFile;
			queryFromXmlFile.parse_query_xml_file_line(queryText);
			queryFromXmlFile.DownloadExpensionTermsTo(numberOfTerms);
			queryText = queryFromXmlFile.parse_query_to_xml_file_line();
			queryFromXmlFile.Clear();

			of << "<query>\n";
			of << "<number>" + queryName + "</number>\n";
			of << "<text>" + queryText + "</text>\n";
			of << "</query>\n";
		}

		of << "</parameters>";
		of.close(); 	


	}

}GeneralModel;


#endif
