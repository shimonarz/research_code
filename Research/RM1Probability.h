#ifndef RM1Probability_H
#define RM1Probability_H

#include "research.h"

// P(w|RM1)
typedef struct RM1Probability
{
	map< string , vector<Term*> > RM1TermsPerQuery; // key = query name , value = list of terms

	void CreateRM1ProbabilityFile(string firstQueryExpensionFile)
	{
		std::cout << "Read First Query Expansion File " << std::endl;
		std::vector<FirstQueryExpension*> *rm =  FirstQueryExpension::readExpensQueryFile(firstQueryExpensionFile);
		for (unsigned int j = 0 ; j < rm->size() ; ++j)
		{  
			FirstQueryExpension *firstQueryExpension = (*rm)[j];
			std::cout << "\r" << "	Extend Query " <<  firstQueryExpension->QueryId << std::flush;
			std::vector<Term*> TermFromDiscriminativeModel = firstQueryExpension->ExpensionTerms;
			vector<Term*> newVec;
			for (int i = 0; i < TermFromDiscriminativeModel.size(); i++)
			{
				newVec.push_back( TermFromDiscriminativeModel[i]->Copy() );
			}
			RM1TermsPerQuery.insert(std::make_pair(firstQueryExpension->QueryId, newVec));
		}

		for (unsigned int j = 0 ; j < rm->size() ; ++j)
		{  
			FirstQueryExpension *firstQueryExpension = (*rm)[j];
			firstQueryExpension->Clear();
			delete firstQueryExpension;
		}
		rm->clear();
		delete rm;
	}

	void WriteToFile(string fileName)
	{
		ofstream of;
		of.open ( fileName.c_str(), std::ofstream::out ); 
		int j=1;

		map< string , vector<Term*> >::iterator main_it;
		for(main_it = RM1TermsPerQuery.begin() ; main_it != RM1TermsPerQuery.end() ; ++main_it )
		{
			cout << '\r' << "		Progress : " << j <<  '/' <<  RM1TermsPerQuery.size() << 
				".     Progress Percent " <<  ((double)j/(double)RM1TermsPerQuery.size())*100.0  <<  "%" << std::flush;

			string QueryName = main_it->first;
			vector<Term*> *QueryTerms = &main_it->second;
			string fullQueryLine = QueryName + ":";
			for (int i = 0; i < QueryTerms->size(); i++)
			{
				string termName = (*QueryTerms)[i]->value;
				double termWeight = (*QueryTerms)[i]->weight;
				fullQueryLine += termName + " " + SSTR(termWeight) + "," ;
			}
			fullQueryLine = trimEnd(fullQueryLine,",");
			of << fullQueryLine << "\n" ;	
			j++;

		}
		cout << '\n';
		of.close(); 
	}

	void ReadFromFile(string fileName)
	{
		FILE_LOG(logDEBUG) << "Read RM1Probability File - " << fileName ;

		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			FILE_LOG(logERROR) << "Can't open RM1Probability file: " << fileName;
			return ;
		}
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			vector<string> splited = split(line, ':');
			string QueryName = splited[0];
			string terms = splited[1];

			vector<Term*> queryTerms;
			vector<string> termsList = split(terms, ',');
			for (int i = 0; i < termsList.size(); i++)
			{
				vector<string> termData = split(termsList[i], ' ');
				string termName = termData[0];
				string termWeight = termData[1];
				Term *t = new Term(termName,termWeight);
				queryTerms.push_back(t);
			}	
			RM1TermsPerQuery.insert(std::make_pair(QueryName,queryTerms));	
		}
		finput.close();

		FILE_LOG(logDEBUG) << "Finish Read RM1Probability File - " << fileName ;
	}

	vector<Term*> GetRM1Terms(string queryName)
	{
		return RM1TermsPerQuery.find(queryName)->second;
	}

	void Clear()
	{
		map< string , vector<Term*> >::iterator main_it;
		for(main_it = RM1TermsPerQuery.begin() ; main_it != RM1TermsPerQuery.end() ; ++main_it )
		{
			string QueryName = main_it->first;
			vector<Term*> *QueryTerms = &main_it->second;

			for (int i = 0; i < QueryTerms->size(); i++)
			{
				delete (*QueryTerms)[i];
			}
		}
	}

}RM1Probability;


#endif
