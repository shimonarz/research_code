
#include "research.h"

#include "BCBRM3.h"
#include "CB.h"
#include "CBRM3.h"

#include "ClustersFeature.h"
#include "RM1Probability.h"
#include "WordInClusterProbability.h"
#include "ClustersFeature.h"
#include "ModelConfiguration.h"

#include "DataStructer.h"
#include "WordToVec.h"
#include "DiscriminativeModel.h"
#include "RM3.h"
#include "GeneralModelFunctions.h"
#include <sstream>

using namespace std;

/* Definitions */
typedef  bool (*CheckInputDelegate)(int, char**);
typedef  void (*ExecuteOperationDelegate)();

typedef struct Operation 
{
	CheckInputDelegate checkInput;
	ExecuteOperationDelegate execute;
	char **argv;
	int argc;
}Operation;

/* Global Variables */
Operation operation;


/* Functions */
bool SetTheCurrentOperation(int argc, char *argv[]);



int main(int argc, char *argv[])
{
	//cout << (("ATTACHED VERSION: "+ string(__DATE__) + " " + string(__TIME__)).c_str()) << std::endl;
	if (argc <3 ) 
	{
		cout << "Usage (1) Create Query Expansion Using KNN Clustering - Words Come From Discriminative Model :  'CreateFirstExpansionFromDiscriminativeModel' positiveWordsNumber negativeWordsNumber output-FileName queries-FileName queryModel-Files CollectionFeatures-Files" << std::endl;
		return EXIT_FAILURE;
	}
	cout << "numbers of arg is : " << argc << endl;

	

	if ( SetTheCurrentOperation(argc,argv)  == false)
	{
		return EXIT_FAILURE;
	}

	if(operation.checkInput(argc,argv) == false)
	{
		std::cerr << "Try again with the right format of parameters." << std::endl;
		return EXIT_FAILURE;
	}

	operation.execute();

	return EXIT_SUCCESS;
}

bool checkInput_ReturnTrue(int argc , char **argv)
{
	return true;
}


#pragma region CBRM3

#pragma region Create Query Expansion For CBRM3

bool checkQueryExpansionForCBRM3(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateQueryExpansionForCBRM3 command \n " <<
			"[2] = model Configuration File   \n " ;
	std::string s = ss.str();

	if(argc != 3) 
	{
		std::cout << "The number of parameters to command 'CreateQueryExpansionForCBRM3' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void QueryExpansionForCBRM3()
{
	FILE_LOG(logINFO) << "Create Query Expansion For CBRM3 Model" ;

	string modelConfigurationFile = operation.argv[2];;
	
	CBRM3Model CBRM3_Model;
	CBRM3_ModelConfiguration modelConfiguration;
	ModelConfiguration configuration;

	configuration.Init(modelConfigurationFile);
	modelConfiguration.Init(&configuration);

	CBRM3_Model.Init(modelConfiguration);
	CBRM3_Model.BuildModel();
	CBRM3_Model.Clear();

}
 
#pragma endregion


#pragma region Create Query Expansion For CBRM3

bool checkCreateBestClusterQueryExpansionForCBRM3(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateBestClusterQueryExpansionForCBRM3 command \n " <<
			"[2] = model Configuration File   \n " ;
	std::string s = ss.str();

	if(argc != 3) 
	{
		std::cout << "The number of parameters to command 'CreateBestClusterQueryExpansionForCBRM3' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateBestClusterQueryExpansionForCBRM3()
{
	FILE_LOG(logINFO) << "Create Query Expansion For CBRM3 Model" ;

	string modelConfigurationFile = operation.argv[2];;
	
	CBRM3Model CBRM3_Model;
	CBRM3_ModelConfiguration modelConfiguration;
	ModelConfiguration configuration;

	configuration.Init(modelConfigurationFile);
	modelConfiguration.Init(&configuration);

	CBRM3_Model.Init(modelConfiguration);
	CBRM3_Model.BuildBestClusterModel(&configuration.clustersFeatureFileList);
	CBRM3_Model.Clear();

}
 
#pragma endregion

#pragma endregion

#pragma region CB

#pragma region Create Query Expansion For CB

bool checkQueryExpansionForCB(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateQueryExpansionForCB command \n " <<
			"[2] = model Configuration File   \n " ;
	std::string s = ss.str();

	if(argc != 3) 
	{
		std::cout << "The number of parameters to command 'CreateQueryExpansionForCB' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void QueryExpansionForCB()
{
	std::cout << "Create Query Expansion For CB Model" << std::endl;

	string modelConfigurationFile = operation.argv[2];
	
	CBModel CB_Model;
	CB_ModelConfiguration modelConfiguration;
	ModelConfiguration configuration;

	configuration.Init(modelConfigurationFile);
	modelConfiguration.Init(&configuration);

	CB_Model.Init(modelConfiguration);
	CB_Model.BuildModel();
	CB_Model.Clear();

}
 
#pragma endregion


#pragma region Create Best Cluster Query Expansion For CB

bool checkBestClusterQueryExpansionForCB(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateBestClusterQueryExpansionForCB command \n " <<
			"[2] = model Configuration File   \n " ;
	std::string s = ss.str();

	if(argc != 3) 
	{
		std::cout << "The number of parameters to command 'CreateBestClusterQueryExpansionForCB' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateBestClusterQueryExpansionForCB()
{
	std::cout << "Create Query Expansion For CB Model" << std::endl;

	string modelConfigurationFile = operation.argv[2];
	
	CBModel CB_Model;
	CB_ModelConfiguration modelConfiguration;
	ModelConfiguration configuration;

	configuration.Init(modelConfigurationFile);
	modelConfiguration.Init(&configuration);

	
	CB_Model.Init(modelConfiguration);
	CB_Model.BuildBestClusterModel(&configuration.clustersFeatureFileList);
	CB_Model.Clear();

}
 
#pragma endregion


#pragma endregion

#pragma region CBDM

#pragma region create First Time Query Expansion - Words Come From Discriminative Model

bool checkKnnRm1TermsInput(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateFirstExpansionFromDiscriminativeModel command \n " <<
			"[2] = positiveWordsNumber  \n " <<
			"[3] = negativeWordsNumber   \n " <<
			"[4] = output-FileName  \n " <<
			"[5] = queries-FileName   \n " <<
			"[6...n] = queryModel-Files \n" <<
			"[n...m] = collectionFeatures-Files";
	std::string s = ss.str();
	if(argc < 7) 
	{
		std::cout << "The number of parameters to command 'CreateFirstExpansionFromDiscriminativeModel' is wrong : " << argc - 1 << "\n The format is :" << s << std::endl;
		return false; 
	}

	return true;
}

void CreateFirstExpansionFromDiscriminativeModel()
{
	std::cout << "Start Create a new first file Expansion - word comes From Discriminative model " << std::endl;

	vector<string> queryModelFileNames;
	vector<string> queryCollectionFeaturesFileNames;
	

	istringstream ss_positiveWordsNumber(operation.argv[2]);
	int positiveWordsNumber(0);
	ss_positiveWordsNumber >> positiveWordsNumber;
	
	istringstream sss_negativeWordsNumber(operation.argv[3]);
	int negativeWordsNumber(0);
	sss_negativeWordsNumber >> negativeWordsNumber;

	string outputFile = operation.argv[4];

	string originalQueriesFileName = operation.argv[5];

	string modleFile = "model";
	string collectionFile = "collection";
	
	for (int i = 6; i < operation.argc; i++)
	{
		string fileName = operation.argv[i];
		if (fileName.find(modleFile) != std::string::npos) {
			queryModelFileNames.push_back(fileName);
		}
		else if (fileName.find(collectionFile) != std::string::npos) {
			queryCollectionFeaturesFileNames.push_back(fileName);
		}
	}

	
	std::cout << "	Collection Features files number = " << queryCollectionFeaturesFileNames.size() <<  ", Query Model files number = " << queryModelFileNames.size() << std::endl;

	CBDM::createFirstTimeQueryExpansion_WordsComeFromDiscriminativeModel( originalQueriesFileName ,  queryModelFileNames ,   queryCollectionFeaturesFileNames ,  positiveWordsNumber ,  negativeWordsNumber, outputFile);
}



#pragma endregion

#pragma region Create Query Expansion Using Knn Clustering Words Come From Discriminative Model

bool checkCreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModel(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModel command \n " <<
			"[2] = wordToVecModel-FileName  \n " <<
			"[3] = firstQueryExpension-FileName   \n " <<
			"[4] = qrels-FileName  \n " <<
			"[5] = output-DirName   \n " <<
			"[6] = k-neigb \n" <<
			"[7] = originalQueryWeight";
	std::string s = ss.str();

	if(argc != 8) 
	{
		std::cout << "The number of parameters to command 'CreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModel' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void createQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModel()
{
	std::cout << "Start Create Expansion Using Knn Clustering  - word comes From Discriminative model " << std::endl;

	string wordToVecModelFileName = operation.argv[2];
	string firstQueryExpensionFile = operation.argv[3];
	string qrelsFileName = operation.argv[4];
	string outputDir = operation.argv[5];
	

	istringstream ss_k(operation.argv[6]);
	int k(0);
	ss_k >> k;
	
	istringstream ss_originalQueryWeigh(operation.argv[7]);
	double originalQueryWeight(0);
	ss_originalQueryWeigh >> originalQueryWeight;

	CBDM::createQueryExpansionUsingKnnClustering_WordsComeFromDiscriminativeModel(firstQueryExpensionFile,qrelsFileName,outputDir,wordToVecModelFileName,k,originalQueryWeight);

}

#pragma endregion

#pragma region Create Query Expansion Using Knn Clustering Words Come From Discriminative Model

bool checkCreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModelDiffrentW2V(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModelDiffrentW2V command \n " <<
			"[2] = firstQueryExpension-FileName   \n " <<
			"[3] = qrels-FileName  \n " <<
			"[4] = output-DirName   \n " <<
			"[5] = k-neigb \n" <<
			"[6] = originalQueryWeight \n " <<
			"[7] = wordToVecModel-FileName";
	std::string s = ss.str();

	if(argc < 8) 
	{
		std::cout << "The number of parameters to command 'CreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModelDiffrentW2V' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModelDiffrentW2V()
{
	std::cout << "Start Create Expansion Using Knn Clustering  - word comes From Discriminative model " << std::endl;

	string firstQueryExpensionFile = operation.argv[2];
	string qrelsFileName = operation.argv[3];
	string outputDir = operation.argv[4];
	

	istringstream ss_k(operation.argv[5]);
	int k(0);
	ss_k >> k;
	
	istringstream ss_originalQueryWeigh(operation.argv[6]);
	double originalQueryWeight(0);
	ss_originalQueryWeigh >> originalQueryWeight;
	vector<string> wordToVecModelFileNameVec;
	for (int i = 7; i < operation.argc; i++)
	{
		wordToVecModelFileNameVec.push_back(operation.argv[i]);
	}

	CBDM::createQueryExpansionUsingKnnClusteringDiffrentW2V_WordsComeFromDiscriminativeModel(firstQueryExpensionFile,qrelsFileName,outputDir,wordToVecModelFileNameVec,k,originalQueryWeight);

}



#pragma endregion

#pragma region Create Clusters Score Files Using Knn Clustering - Words Come From Discriminative Model

bool checkCreateClustersScoreFilesUsingKnnClusteringWordsComeFromDiscriminativeModel(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateClustersScoreFiles command \n " <<
			"[2] = first Query Expansion - FileName  \n " <<
			"[3] = wordToVecModel-FileName  \n " <<
			"[4] = k-neigb \n" <<
			"[5] = output-DirName ";
	std::string s = ss.str();

	if(argc != 6) 
	{
		std::cout << "The number of parameters to command 'CreateClustersScoreFiles' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void createClustersScoreFilesUsingKnnClusteringWordsComeFromDiscriminativeModel()
{
	std::cout << "Start Create Clusters Score Files." << std::endl;

	string firstQueryExpensionFile = operation.argv[2];
	string wordToVecModelFileName = operation.argv[3];
	
	istringstream ss_k(operation.argv[4]);
	int k(0);
	ss_k >> k;

	string outputDir = operation.argv[5];

	CBDM::createClustersScoreFilesUsingKnnClustering_WordsComeFromDiscriminativeModel(firstQueryExpensionFile,outputDir,wordToVecModelFileName, k);
}

#pragma endregion

#pragma endregion

#pragma region RM3


#pragma region Create Cross validation for RM3

bool checkCreateCrossValidationForRM3(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateCrossValidationForRM3 command \n " <<
			"[2] = output-DirName  \n " 
			"[3] = print step (1 print, else not print) \n" <<
			"[4..n] = evaluation files  \n" ;
	std::string s = ss.str();

	if(argc < 4 ) 
	{
		std::cout << "The number of parameters to command 'CreateCrossValidationForRM3' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateCrossValidationForRM3()
{
	

	string outputDir = operation.argv[2];
	string printStep = operation.argv[3];

	istringstream ss_k(printStep);
	int k(0);
	ss_k >> k;

	FILE_LOG(logINFO) << "Create Cross validation for RM3 - printStep = " << printStep << " k = " << k;

	int n = operation.argc;
	vector<string> evaluationFiles ;
	for (int i = 4; i < n; i++)
	{
		evaluationFiles.push_back(operation.argv[i]);
	}
	
	Rm3 rm3;
	rm3.Init(&evaluationFiles);
	if( k == 1 )
	{
		FILE_LOG(logINFO) << "Create Cross validation for RM3 With Prints." ;
		rm3.CalculateCrossValidationWithPrints(outputDir);
	}
	else
	{
		FILE_LOG(logINFO) << "Create Cross validation for RM3 no prints." ;
		rm3.CalculateCrossValidation(outputDir);
	}
	

	FILE_LOG(logINFO) << "Finish Create Cross validation for RM3." ;

}

#pragma endregion

#pragma region Create BestoneAvg for RM3

bool checkCreateBestoneAvgForRM3(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateBestoneAvgForRM3 command \n " <<
			"[2] = output-DirName  \n " <<
			"[3] = print step (1 print, else not print) \n" <<
			"[4..n] = evaluation files  \n " ;
	std::string s = ss.str();

	if(argc < 4 ) 
	{
		std::cout << "The number of parameters to command 'CreateBestoneAvgForRM3' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateBestoneAvgForRM3()
{
	FILE_LOG(logINFO) << "Create BestoneAvg for RM3." ;

	string outputDir = operation.argv[2];
	string printStep = operation.argv[3];

	istringstream ss_k(printStep);
	int k(0);
	ss_k >> k;

	FILE_LOG(logINFO) << "Create BestoneAvg for RM3 - printStep = " << printStep << " k = " << k;

	vector<string> evaluationFiles ;
	for (int i = 4; i < operation.argc; i++)
	{
		evaluationFiles.push_back(operation.argv[i]);
	}
	
	Rm3 rm3;
	rm3.Init(&evaluationFiles);

	if( k == 1 )
	{
		FILE_LOG(logINFO) << "Create BestoneAvg for RM3 With Prints." ;
		rm3.CalculateBestoneAvgWithPrints(outputDir);
	}
	else
	{
		FILE_LOG(logINFO) << "Create BestoneAvg for RM3 no prints." ;
		rm3.CalculateBestoneAvg(outputDir);
	}

	

	FILE_LOG(logINFO) << "Finish BestoneAvg validation for RM3." ;

}

#pragma endregion

#pragma endregion

#pragma region BCBRM3

#pragma region Create Query Expansion For CBRM3

bool checkCreateQueryExpansionForBCBRM3(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateQueryExpansionForBCBRM3 command \n " <<
			"[2] = model Configuration File   \n " ;
	std::string s = ss.str();

	if(argc != 3) 
	{
		std::cout << "The number of parameters to command 'CreateQueryExpansionForBCBRM3' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateQueryExpansionForBCBRM3()
{
	FILE_LOG(logINFO) << "Create Query Expansion For CBRM3 Model" ;

	string modelConfigurationFile = operation.argv[2];;
	
	BCBRM3Model model;
	BCBRM3_ModelConfiguration modelConfiguration;
	ModelConfiguration configuration;

	configuration.Init(modelConfigurationFile);
	modelConfiguration.Init(&configuration);

	model.Init(modelConfiguration);
	model.BuildModel();
	model.Clear();

}
 
#pragma endregion

#pragma endregion

#pragma region General Model Functions


#pragma region Create Cluster Performance By Feature Files For Gnuplot

bool checkCreateClusterPerformanceByFeatureFilesForGnuplot(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateClusterPerformanceByFeatureFilesForGnuplot command \n " <<
			"[2] = output-DirName  \n " <<
			"[3] = Algo name \n " <<
			"[4...n] = evaluate files " << 
			"[n+1 .. m] = Feature files" ;
	std::string s = ss.str();

	if(argc < 6) 
	{
		std::cout << "The number of parameters to command 'CreateClusterPerformanceByFeatureFilesForGnuplot' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateClusterPerformanceByFeatureFilesForGnuplot()
{
	std::cout << "Start Create Cluster Performance By Feature Files For Gnuplot " << std::endl;

	string pathToDir = operation.argv[2];
	string algName = operation.argv[3];

	vector<string> evaluatefileList;
	vector<string> clustresFeatureFiles;
	int i;
	for( i=4; i< operation.argc ; ++i)
	{
		string fileName = operation.argv[i];
		if( fileName.find("evaluate") != std::string::npos )
		{
			evaluatefileList.push_back(fileName);
		}
		else
		{
			clustresFeatureFiles.push_back(fileName);;
		}
	}

	std::cout << "	Number of Clusters Feature files is :  " << clustresFeatureFiles.size() << "	Number of Clusters files is :  " << evaluatefileList.size()<< std::endl;
	
	
	GeneralModel generalModel;
	
	if( algName == "PrintClusterPerformanceByFeature" )
	{
		generalModel.PrintClusterPerformanceByFeature(evaluatefileList , clustresFeatureFiles , pathToDir);
	}
	else if(algName == "CalcCorrlationClusterPerformanceByFeature" )
	{
		generalModel.CalcCorrlationClusterPerformanceByFeature(evaluatefileList , clustresFeatureFiles , pathToDir);
	}
	else
	{
		std::cout << "Bead config" ; 
	}
	
}
 
#pragma endregion

#pragma region Create Reduced Extension Queries File

bool checkCreateReducedExtensionQueriesFile(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateReducedExtensionQueriesFile command \n " <<
			"[2] = queries File Name output-DirName  \n " <<
			"[3] = number Of Terms \n " <<
			"[4] = output Queries File Name ";
	std::string s = ss.str();

	if(argc != 5) 
	{
		std::cout << "The number of parameters to command 'CreateReducedExtensionQueriesFile' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateReducedExtensionQueriesFile()
{
	std::cout << "Create Reduced Extension Queries File " << std::endl;

	string queriesFileName = operation.argv[2];
	int numberOfTerms = convertToInt(operation.argv[3]);
	string outputQueriesFileName = operation.argv[4];

	std::cout << "Queries File Name = " << queriesFileName <<  std::endl;
	std::cout << "Number Of Terms = " << numberOfTerms <<  std::endl;
	std::cout << "Output Queries File Name = " << outputQueriesFileName <<  std::endl;

	GeneralModel generalModel;
	generalModel.CreateReducedExtensionQueriesFile(queriesFileName , numberOfTerms , outputQueriesFileName);
	
}
 
#pragma endregion

#pragma endregion

#pragma region Create Word In Cluster Probability

bool checkCreateWordInClusterProbability(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateWordInClusterProbability command \n " <<
			"[2] = Type Of Distribution : Uniform, UniformForAllTerms ,  ProportionalToRM1,  ProportionalToRM1ForAllTerms , W2VSimilarityToClusterCentroid, W2VSimilarityToClusterCentroidForAllTerms \n " <<
			"[3] = output-DirName   \n " <<
			"[4..n] = Cluster Files - for each query \n"  <<
			"[(n+1)..m] = wordToVecModel-FileNames / rm1HighestTermsFiles / None \n" ;
	std::string s = ss.str();

	if(argc < 5) 
	{
		std::cout << "The number of parameters to command 'CreateWordInClusterProbability' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateWordInClusterProbability()
{
	std::cout << "Start Create Word In Cluster Probability " << std::endl;
	string TypeOfDistribution = operation.argv[2];
	string outputDir = operation.argv[3];

	int i = 4;
	vector<string> ClusterFileList;
	for ( ; i < operation.argc; i++)
	{
		string fileName = operation.argv[i];
		if (fileName.find("QueryClusters") != std::string::npos) {
			ClusterFileList.push_back(fileName);
		}
		else
		{
			break;
		}
	}
	std::cout << "Find  " << ClusterFileList.size() << " of Clusters files " <<  std::endl;

	WordInClusterProbabilityCreator wordInClusterProbabilityCreator;
	wordInClusterProbabilityCreator.init(ClusterFileList);

	if(TypeOfDistribution == "Uniform")
	{
		wordInClusterProbabilityCreator.CreateUniformDistribution(outputDir);
	}
	if(TypeOfDistribution == "UniformForAllTerms")
	{
		wordInClusterProbabilityCreator.CreateUniformDistributionForAllTerms(outputDir);
	}
	else if(TypeOfDistribution == "ProportionalToRM1")
	{
		string rm1HighestTermsFiles = operation.argv[i];
		wordInClusterProbabilityCreator.CreateProportionalToRM1Distribution(outputDir,rm1HighestTermsFiles);
	}
	else if(TypeOfDistribution == "ProportionalToRM1ForAllTerms")
	{
		string rm1HighestTermsFiles = operation.argv[i];
		wordInClusterProbabilityCreator.CreateProportionalToRM1DistributionForAllTerms(outputDir,rm1HighestTermsFiles);
	}
	else if(TypeOfDistribution == "W2VSimilarityToClusterCentroid")
	{
		vector<string> wordToVecModelFileNameVec;
		for (; i < operation.argc; i++)
		{
			wordToVecModelFileNameVec.push_back(operation.argv[i]);
		}
		wordInClusterProbabilityCreator.CreateW2VSimilarityToClusterCentroidDistribution(outputDir,wordToVecModelFileNameVec);
	}
	else if(TypeOfDistribution == "W2VSimilarityToClusterCentroidForAllTerms")
	{
		vector<string> wordToVecModelFileNameVec;
		for (; i < operation.argc; i++)
		{
			wordToVecModelFileNameVec.push_back(operation.argv[i]);
		}
		std::cout << "Find  " << wordToVecModelFileNameVec.size() << " of W2V files " <<  std::endl;
		wordInClusterProbabilityCreator.CreateW2VSimilarityToClusterCentroidDistributionForAllTerms(outputDir,wordToVecModelFileNameVec);
	}

	wordInClusterProbabilityCreator.Clear();

}
 
#pragma endregion

#pragma region Create ClusterPerQuery Probability

bool checkCreateClusterPerQueryProbability(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateClusterPerQueryProbability command \n " <<
			"[2] = Type Of Distribution : Uniform, SingleLink, CompleteLink, AverageLink, W2VQueryClusterSimilarity, IDF, ICF \n " <<
			"[3] = output-DirName   \n " <<
			"[4..n] = Cluster Files - for each query \n"  <<
			"[(n+1)..m] = wordToVecModel-FileNames / rm1HighestTermsFiles / queries Stemmed FileName / IDF Scores Files / None \n" ;
	std::string s = ss.str();

	if(argc < 5) 
	{
		FILE_LOG(logINFO) << "The number of parameters to command 'CreateClusterPerQueryProbability' is wrong : " << argc - 1 <<  "\n The format is :" << s  ;
		return false; 
	}

	return true;
}

void CreateClusterPerQueryProbability()
{
	FILE_LOG(logINFO) << "Start Create Cluster Per Query Probability " ;
	string TypeOfDistribution = operation.argv[2];
	string outputDir = operation.argv[3];

	int i = 4;
	vector<string> ClusterFileList;
	for ( ; i < operation.argc; i++)
	{
		string fileName = operation.argv[i];
		if (fileName.find("QueryClusters") != std::string::npos) {
			ClusterFileList.push_back(fileName);
		}
		else
		{
			break;
		}
	}
	FILE_LOG(logINFO) << "Find  " << ClusterFileList.size() << " of Clusters files ";

	ClustersFeatureCreator clustersFeatureCreator;
	clustersFeatureCreator.Init(&ClusterFileList);

	if(TypeOfDistribution == "Uniform")
	{
		clustersFeatureCreator.CreateUniformDistribution(outputDir);
	}
	if(TypeOfDistribution == "ICF")
	{
		clustersFeatureCreator.CreateICFDistribution(outputDir);
	}
	else if(TypeOfDistribution == "W2VQueryClusterSimilarity")
	{
		string queriesStemmedFileName = operation.argv[i];
		i++;
		vector<string> wordToVecModelFileNameVec;
		for (; i < operation.argc; i++)
		{
			wordToVecModelFileNameVec.push_back(operation.argv[i]);
		}
		FILE_LOG(logINFO) << "Find  " << wordToVecModelFileNameVec.size() << " of word-To-Vec-Model files ";
		clustersFeatureCreator.CreateW2VQueryClusterSimilarityDistribution(outputDir,wordToVecModelFileNameVec,queriesStemmedFileName);
	}
	else if(TypeOfDistribution == "SingleLink")
	{
		string queriesStemmedFileName = operation.argv[i];
		i++;
		vector<string> wordToVecModelFileNameVec;
		for (; i < operation.argc; i++)
		{
			wordToVecModelFileNameVec.push_back(operation.argv[i]);
		}
		FILE_LOG(logINFO) << "Find  " << wordToVecModelFileNameVec.size() << " of word-To-Vec-Model files ";
		clustersFeatureCreator.CreateSingleLinkSimilarityDistribution(outputDir,wordToVecModelFileNameVec,queriesStemmedFileName);
	}
	else if(TypeOfDistribution == "CompleteLink")
	{
		string queriesStemmedFileName = operation.argv[i];
		i++;
		vector<string> wordToVecModelFileNameVec;
		for (; i < operation.argc; i++)
		{
			wordToVecModelFileNameVec.push_back(operation.argv[i]);
		}
		FILE_LOG(logINFO) << "Find  " << wordToVecModelFileNameVec.size() << " of word-To-Vec-Model files ";
		clustersFeatureCreator.CreateCompleteLinkSimilarityDistribution(outputDir,wordToVecModelFileNameVec,queriesStemmedFileName);
	}
	else if(TypeOfDistribution == "AverageLink")
	{
		string queriesStemmedFileName = operation.argv[i];
		i++;
		vector<string> wordToVecModelFileNameVec;
		for (; i < operation.argc; i++)
		{
			wordToVecModelFileNameVec.push_back(operation.argv[i]);
		}
		FILE_LOG(logINFO) << "Find  " << wordToVecModelFileNameVec.size() << " of word-To-Vec-Model files ";
		clustersFeatureCreator.CreateAverageLinkSimilarityDistribution(outputDir,wordToVecModelFileNameVec,queriesStemmedFileName);
	}
	else if(TypeOfDistribution == "IDF")
	{
		vector<string> idfScoreFile;
		for (; i < operation.argc; i++)
		{
			idfScoreFile.push_back(operation.argv[i]);
		}
		FILE_LOG(logINFO) << "Find  " << idfScoreFile.size() << " of idf-Score files ";
		clustersFeatureCreator.CreateIDFDistribution(outputDir,idfScoreFile);
	}









	clustersFeatureCreator.Clear();

}
 
#pragma endregion




#pragma region Create Files for gnuplot (dat files) - CB (Cluster base) model -  Words Come From Discriminative Model

bool checkCreateFilesForGnuplotWordsComeFromDiscriminativeModel(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateFilesForGnuplotWordsComeFromDiscriminativeModel command \n " <<
			"[2] = output-DirName  \n " <<
			"[3...n] = evaluate files ";
	std::string s = ss.str();

	if(argc < 4) 
	{
		std::cout << "The number of parameters to command 'CreateFilesForGnuplotWordsComeFromDiscriminativeModel' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateFilesForGnuplotWordsComeFromDiscriminativeModel()
{
	std::cout << "Start Create Files For Gnuplot - Words Come Fro mDiscriminative Model " << std::endl;

	string pathToDir = operation.argv[2];
	vector<string> fileNameList;
	for(int i=3; i< operation.argc ; ++i)
	{
		fileNameList.push_back(operation.argv[i]);
	}

	CBDM::CreateFilesForGnuplot_WordsComeFromDiscriminativeModel(pathToDir,&fileNameList);
}

#pragma endregion

#pragma region Create Files for gnuplot (dat files) - CB (Cluster base) model with scores

bool checkCreateFilesForGnuplotForClusterScoreAndPerformance(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateFilesForGnuplotForClusterScoreAndPerformance command \n " <<
			"[2] = output-DirName  \n " <<
			"[3...n] = score files " << 
			"[n+1...m] = evaluate files ";
	std::string s = ss.str();

	if(argc < 5) 
	{
		std::cout << "The number of parameters to command 'CreateFilesForGnuplotForClusterScoreAndPerformance' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateFilesForGnuplotForClusterScoreAndPerformance()
{
	std::cout << "Start Create Files For Gnuplot - For Cluster Score And Performance " << std::endl;

	string pathToDir = operation.argv[2];
	vector<string> fileNameList;
	vector<string> clustresScoreFiles;
	for(int i=3; i< operation.argc ; ++i)
	{
		string fileName = operation.argv[i];
		if( fileName.find("clusterGrade") != std::string::npos )
		{
			clustresScoreFiles.push_back(fileName);
		}
		else
		{
			fileNameList.push_back(fileName);
		}
	}
	std::cout << "	Number of Clusters score files is :  " << clustresScoreFiles.size() << "	Number of Clusters files is :  " << fileNameList.size()<< std::endl;
	CBDM::CreateFilesForGnuplot_ForClusterScoreAndPerformance(pathToDir,&fileNameList,&clustresScoreFiles );
}
 
#pragma endregion

#pragma region Compare between CB (word from RM1) to CBRM3 models

bool checkCompareBetweenCBToCBRM3ModelsWordComeFromRM1(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CompareBetweenCBToCBRM3ModelsWordComeFromRM1 command \n " <<
			"[2] = Cross validation - FileName  \n " <<
			"[3] = output-DirName  \n " <<
			"[4...n] = CB evaluate files \n" <<
			"[(n+1)...m] = CBRM3 evaluate files ";
	std::string s = ss.str();

	if(argc < 5) 
	{
		std::cout << "The number of parameters to command 'CompareBetweenCBToCBRM3ModelsWordComeFromRM1' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CompareBetweenCBToCBRM3ModelsWordComeFromRM1()
{
	std::cout << "Start Create Files For Gnuplot - Compare Between CB To CBRM3 Models - Word Come From RM1 " << std::endl;

	string cvFile = operation.argv[2];
	string pathToDir = operation.argv[3];
	vector<string> fileNameList;
	for(int i=4; i< operation.argc ; ++i)
	{
		fileNameList.push_back(operation.argv[i]);
	}

	CompareBetweenCBToCBRM3Models_WordComeFromRM1(pathToDir,cvFile,&fileNameList);
}

#pragma endregion

#pragma region create from the RM1 First Time Query Expansion other First Time Query Expansion file that include Discriminative Model score

bool checkCreateFromRM1FirstExpansionFirstOtherExpansionFromDiscriminativeModel(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateFromRm1FirstExpansionOtherFirstExpansionFromDiscriminativeModel command \n " <<
			"[2] = First Expansion from rm1 file \n " <<
			"[3] = output-FileName  \n " <<
			"[4...n] = queryModel-Files \n" <<
			"[n...m] = collectionFeatures-Files";
	std::string s = ss.str();
	if(argc < 6) 
	{
		std::cout << "The number of parameters to command 'CreateFromRm1FirstExpansionOtherFirstExpansionFromDiscriminativeModel' is wrong : " << argc - 1 << "\n The format is :" << s << std::endl;
		return false; 
	}

	return true;
}

void CreateFromRM1FirstExpansionFirstOtherExpansionFromDiscriminativeModel()
{
	std::cout << "Start Create from the RM1 First Time Query Expansion other First Time Query Expansion file that include Discriminative Model score " << std::endl;

	vector<string> queryModelFileNames;
	vector<string> queryCollectionFeaturesFileNames;
	
	string firstExpansionFromRM1File = operation.argv[2];
	string outputFile = operation.argv[3];

	string modleFile = "model";
	string collectionFile = "collection";
	
	for (int i = 4; i < operation.argc; i++)
	{
		string fileName = operation.argv[i];
		if (fileName.find(modleFile) != std::string::npos) {
			queryModelFileNames.push_back(fileName);
		}
		else if (fileName.find(collectionFile) != std::string::npos) {
			queryCollectionFeaturesFileNames.push_back(fileName);
		}
	}

	std::cout << "	Collection Features files number = " << queryCollectionFeaturesFileNames.size() <<  ", Query Model files number = " << queryModelFileNames.size() << std::endl;
	CBDM::CreateFromRM1FirstExpansionFirst_OtherExpansionFromDiscriminativeModel( firstExpansionFromRM1File ,  queryModelFileNames ,   queryCollectionFeaturesFileNames , outputFile);
}

#pragma endregion

#pragma region Create Files for gnuplot (dat files) - RM3 Cross Validation

bool checkCreateFilesForGnuplotForRM3CrossValidation(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateFilesForGnuplotForRM3CrossValidation command \n " <<
			"[2] = RM3 Cross Validation - file name  \n " <<
			"[3] = output-DirName " ;
	std::string s = ss.str();

	if(argc != 4) 
	{
		std::cout << "The number of parameters to command 'CreateFilesForGnuplotForRM3CrossValidation' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateFilesForGnuplotForRM3CrossValidation()
{
	std::cout << "Start Create Files For Gnuplot - For RM3 Cross Validation " << std::endl;

	string crossValidationfile = operation.argv[2];
	string pathToDir = operation.argv[3];
	
	Rm3CVFile::CreateFilesForGnuplot_ForRM3CrossValidation(crossValidationfile,pathToDir);
}
 
#pragma endregion

#pragma region Cross Validation - On cluster Grade

bool checkCrossValidationOnClusterScore(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CrossValidationOnClusterScore command \n " <<
			"[2] = output-DirName \n " <<
			"[3...n] = queryModel-Files  " ;
	std::string s = ss.str();

	if(argc < 4) 
	{
		std::cout << "The number of parameters to command 'CrossValidationOnClusterScore' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CrossValidationOnClusterScore()
{
	std::cout << "Start Create Cross Validation - On Cluster Score " << std::endl;

	string output = operation.argv[2];
	
	vector<string> clusterScoreFile ;
	vector<string> clusterPerformance;

	for (int i = 3; i < operation.argc; i++)
	{
		string fileName = operation.argv[i];
		if (fileName.find("clusterGrade") != std::string::npos) {
			clusterScoreFile.push_back(fileName);
		}
		else  {
			clusterPerformance.push_back(fileName);
		}
	}
	CBDM::crossValidationOnClusterScore(&clusterScoreFile,&clusterPerformance,1,output);
}
 
#pragma endregion

#pragma region beston Average On Cluster Score

bool checkBestonAverageOnClusterScore(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = BestonAverageOnClusterScore command \n " <<
			"[2] = output-DirName \n " <<
			"[3...n] = queryModel-Files  " ;
	std::string s = ss.str();

	if(argc < 4) 
	{
		std::cout << "The number of parameters to command 'BestonAverageOnClusterScore' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void BestonAverageOnClusterScore()
{
	std::cout << "Start Create Beston Average - On Cluster Score " << std::endl;

	string output = operation.argv[2];
	
	vector<string> clusterScoreFile ;
	vector<string> clusterPerformance;

	for (int i = 3; i < operation.argc; i++)
	{
		string fileName = operation.argv[i];
		if (fileName.find("clusterGrade") != std::string::npos) {
			clusterScoreFile.push_back(fileName);
		}
		else  {
			clusterPerformance.push_back(fileName);
		}
	}
	CBDM::bestonAverageOnClusterScore(&clusterScoreFile,&clusterPerformance,1,output);
}
 
#pragma endregion

#pragma region Create Clusters File For All Queries

bool checkCreateClustersFileForAllQueries(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateClustersFileForAllQueries command \n " <<
			"[2] = firstQueryExpensionFile - file Name \n " <<
			"[3] = output-DirName   \n " <<
			"[4] = k-neigb \n" <<
			"[5..n] = wordToVecModel-FileName " ;
	std::string s = ss.str();

	if(argc < 5) 
	{
		std::cout << "The number of parameters to command 'CreateClustersFileForAllQueries' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateClustersFileForAllQueries()
{
	FILE_LOG(logINFO) << "Start Create Clusters Files For All Queries " ;

	string firstQueryExpensionFile = operation.argv[2];
	string outputDir = operation.argv[3];
	
	istringstream ss_k(operation.argv[4]);
	int k(0);
	ss_k >> k;

	vector<string> wordToVecModelFileNameVec;
	for (int i = 5; i < operation.argc; i++)
	{
		wordToVecModelFileNameVec.push_back(operation.argv[i]);
	}

	FILE_LOG(logINFO) << "firstQueryExpensionFile = " << firstQueryExpensionFile ;
	FILE_LOG(logINFO) << "outputDir = " << outputDir ;
	FILE_LOG(logINFO) << "k = " << k ;
	FILE_LOG(logINFO) << "wordToVecModelFileNameVec size = " << wordToVecModelFileNameVec.size() ;
	CreateClusterFileForAllQueries( firstQueryExpensionFile, wordToVecModelFileNameVec, k, outputDir );
}
 
#pragma endregion

#pragma region Create Qural File For All Query Clusers

bool checkCreateQrelFileForAllQueryClusers(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateQrelFileForAllQueryClusers command \n " <<
			"[2] = qrels File Name \n " <<
			"[3] = output-DirName   \n " <<
			"[4..n] = Cluster Files - for each query \n" ;
	std::string s = ss.str();

	if(argc < 5) 
	{
		std::cout << "The number of parameters to command 'CreateQrelFileForAllQueryClusers' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateQrelFileForAllQueryClusers()
{
	std::cout << "Start Create Qural File For All Query Clusters " << std::endl;

	string qrelsFileName = operation.argv[2];
	string outputDir = operation.argv[3];
	vector<string> clustersfilesNames; 
	for (int i = 4; i < operation.argc; i++)
	{
		clustersfilesNames.push_back(operation.argv[i]);
	}
	CreateQrelFileForAllQueryClusers(clustersfilesNames ,  qrelsFileName ,  outputDir);
}
 
#pragma endregion

#pragma region Create RM1Probability File

bool checkCreateRM1ProbabilityFile(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateRM1ProbabilityFile command \n " <<
			"[2] = firstQueryExpensionFile \n " <<
			"[3] = output-File Name   \n " ;
	std::string s = ss.str();

	if(argc != 4) 
	{
		std::cout << "The number of parameters to command 'CreateRM1ProbabilityFile' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateRM1ProbabilityFile()
{
	std::cout << "Start Create Qural File For All Query Clusters " << std::endl;

	string firstQueryExpensionFile = operation.argv[2];
	string outputFile = operation.argv[3];
	
	RM1Probability rm1Probability;
	rm1Probability.CreateRM1ProbabilityFile(firstQueryExpensionFile);
	rm1Probability.WriteToFile(outputFile);

	rm1Probability.Clear();
}
 
#pragma endregion

#pragma region Create Queries File Wit hWorkingSetDocno

bool checkCreateQueriesFileWithWorkingSetDocno(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = CreateQueriesFileWithWorkingSetDocno command \n " <<
			"[2] = reRankingQueries File   \n " ;
			"[3] = outputDir   \n " ;
			"[4..n] = clusters Query File List   \n " ;
	std::string s = ss.str();

	if(argc < 5) 
	{
		std::cout << "The number of parameters to command 'CreateQueriesFileWithWorkingSetDocno' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void CreateQueriesFileWithWorkingSetDocno()
{
	std::cout << "Create Query Expansion For CB Model" << std::endl;

	string reRankingQueries = operation.argv[2];
	string outputDir = operation.argv[3];

	if( operation.argc == 5)
	{
		string queries = operation.argv[4];
		CreateQueriesFileWithWorkingSetDocno(reRankingQueries , outputDir, queries);
	}
	else
	{
		vector<string> clustersQueryFileList;
		for (int i = 4 ; i < operation.argc; i++)
		{
			clustersQueryFileList.push_back(operation.argv[i]);
		}

		CreateQueriesFileWithWorkingSetDocno(reRankingQueries , outputDir, &clustersQueryFileList);
	}
}


 
#pragma endregion

#pragma region MargeConfigurationToFile

bool checkMargeConfigurationToFile(int argc , char **argv)
{
	std::stringstream ss;
	ss <<   "[0] = program name \n" <<
			"[1] = MargeConfigurationToFile command \n " <<
			"[2] = output-DirName \n " <<
			"[3...n] = clusters Performance Files  \n" ;
	std::string s = ss.str();

	if(argc < 4) 
	{
		std::cout << "The number of parameters to command 'MargeConfigurationToFile' is wrong : " << argc - 1 <<  "\n The format is :" << s  << std::endl;
		return false; 
	}

	return true;
}

void MargeConfigurationToFile()
{
	FILE_LOG(logINFO) << "Marge Configuration To File - For One Configuration" ;
	string output = operation.argv[2];
	vector<string> clusterPerformance;
	for (int i = 3; i < operation.argc; i++)
	{
		string fileName = operation.argv[i];
		clusterPerformance.push_back(fileName);
	}
	EvaluateFileOperation::CreateClusterPerformanceAverage(clusterPerformance,output);
}
 
#pragma endregion


bool SetTheCurrentOperation(int argc, char *argv[])
{
	
	

	string command = argv[1];
	operation.argv = argv;
	operation.argc = argc;

	FILELog::ReportingLevel() = logDEBUG3;
	string logFileName = command + "_" + createLogFileNameByTime();
	FILE* log_fd = fopen( logFileName.c_str(), "w" );
	Output2FILE::Stream() = log_fd;

	FILE_LOG(logINFO) << "Numbers of parameters = " << argc ;
	FILE_LOG(logINFO) << "Receive Command = " << command ;

	if (command == "CreateFirstExpansionFromDiscriminativeModel") 
	{ 
		operation.checkInput = checkKnnRm1TermsInput;
		operation.execute = CreateFirstExpansionFromDiscriminativeModel;
	}
	else if (command == "CreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModel") 
	{ 
		operation.checkInput = checkCreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModel;
		operation.execute = createQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModel;
	}
	else if (command == "CreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModelDiffrentW2V") 
	{ 
		operation.checkInput = checkCreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModelDiffrentW2V;
		operation.execute = CreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModelDiffrentW2V;
	}
	else if (command == "CreateFilesForGnuplotWordsComeFromDiscriminativeModel") 
	{ 
		operation.checkInput = checkCreateFilesForGnuplotWordsComeFromDiscriminativeModel;
		operation.execute = CreateFilesForGnuplotWordsComeFromDiscriminativeModel;
	}
	else if (command == "CompareBetweenCBToCBRM3ModelsWordComeFromRM1") 
	{ 
		operation.checkInput = checkCompareBetweenCBToCBRM3ModelsWordComeFromRM1;
		operation.execute = CompareBetweenCBToCBRM3ModelsWordComeFromRM1;
	}
	else if (command == "CreateClustersScoreFiles") 
	{ 
		operation.checkInput = checkCreateClustersScoreFilesUsingKnnClusteringWordsComeFromDiscriminativeModel;
		operation.execute = createClustersScoreFilesUsingKnnClusteringWordsComeFromDiscriminativeModel;
	}
	else if (command == "CreateFromRm1FirstExpansionOtherFirstExpansionFromDiscriminativeModel") 
	{ 
		operation.checkInput = checkCreateFromRM1FirstExpansionFirstOtherExpansionFromDiscriminativeModel;
		operation.execute = CreateFromRM1FirstExpansionFirstOtherExpansionFromDiscriminativeModel;
	}
	else if (command == "CreateFilesForGnuplotForClusterScoreAndPerformance") 
	{ 
		operation.checkInput = checkCreateFilesForGnuplotForClusterScoreAndPerformance;
		operation.execute = CreateFilesForGnuplotForClusterScoreAndPerformance;
	}
	else if (command == "CreateFilesForGnuplotForRM3CrossValidation") 
	{ 
		operation.checkInput = checkCreateFilesForGnuplotForRM3CrossValidation;
		operation.execute = CreateFilesForGnuplotForRM3CrossValidation;
	}
	else if( command == "CrossValidationOnClusterScore")
	{
		operation.checkInput = checkCrossValidationOnClusterScore;
		operation.execute = CrossValidationOnClusterScore;
	}
	else if( command == "BestonAverageOnClusterScore")
	{
		operation.checkInput = checkBestonAverageOnClusterScore;
		operation.execute = BestonAverageOnClusterScore;
	}
	else if( command == "CreateClustersFileForAllQueries")
	{
		operation.checkInput = checkCreateClustersFileForAllQueries;
		operation.execute = CreateClustersFileForAllQueries;
	}
	else if( command == "CreateWordInClusterProbability")
	{
		operation.checkInput = checkCreateWordInClusterProbability;
		operation.execute = CreateWordInClusterProbability;
	}
	else if( command == "CreateClusterPerQueryProbability")
	{
		operation.checkInput = checkCreateClusterPerQueryProbability;
		operation.execute = CreateClusterPerQueryProbability;
	}
	else if( command == "CreateQrelFileForAllQueryClusers")
	{
		operation.checkInput = checkCreateQrelFileForAllQueryClusers;
		operation.execute = CreateQrelFileForAllQueryClusers;
	}
	else if( command == "CreateRM1ProbabilityFile")
	{
		operation.checkInput = checkCreateRM1ProbabilityFile;
		operation.execute = CreateRM1ProbabilityFile;
	}
	else if( command == "CreateQueriesFileWithWorkingSetDocno")
	{
		operation.checkInput = checkCreateQueriesFileWithWorkingSetDocno;
		operation.execute = CreateQueriesFileWithWorkingSetDocno;
	}
	else if( command == "CreateQueryExpansionForCBRM3")
	{
		operation.checkInput = checkQueryExpansionForCBRM3;
		operation.execute = QueryExpansionForCBRM3;
	}
	else if( command == "CreateQueryExpansionForCB")
	{
		operation.checkInput = checkQueryExpansionForCB;
		operation.execute = QueryExpansionForCB;
	}
	else if( command == "CreateQueryExpansionForBCBRM3")
	{
		operation.checkInput = checkCreateQueryExpansionForBCBRM3;
		operation.execute = CreateQueryExpansionForBCBRM3;
	}
	else if( command == "CreateBestClusterQueryExpansionForCBRM3")
	{
		operation.checkInput = checkCreateBestClusterQueryExpansionForCBRM3;
		operation.execute = CreateBestClusterQueryExpansionForCBRM3;
	}
	else if( command == "CreateBestClusterQueryExpansionForCB")
	{
		operation.checkInput = checkBestClusterQueryExpansionForCB;
		operation.execute = CreateBestClusterQueryExpansionForCB;
	}
	else if( command == "MargeConfigurationToFile")
	{
		operation.checkInput = checkMargeConfigurationToFile;
		operation.execute = MargeConfigurationToFile;
	}
	else if( command == "CreateCrossValidationForRM3")
	{
		operation.checkInput = checkCreateCrossValidationForRM3;
		operation.execute = CreateCrossValidationForRM3;
	}
	else if( command == "CreateBestoneAvgForRM3")
	{
		operation.checkInput = checkCreateBestoneAvgForRM3;
		operation.execute = CreateBestoneAvgForRM3;
	}
	else if( command == "CreateClusterPerformanceByFeatureFilesForGnuplot")
	{
		operation.checkInput = checkCreateClusterPerformanceByFeatureFilesForGnuplot;
		operation.execute = CreateClusterPerformanceByFeatureFilesForGnuplot;
	}
	else if( command == "CreateReducedExtensionQueriesFile")
	{
		operation.checkInput = checkCreateReducedExtensionQueriesFile;
		operation.execute = CreateReducedExtensionQueriesFile;
	}
	
	else
	{
		FILE_LOG(logERROR) << "Cannot Find any command = " << command ;
		return false;
	}

	return true;
}