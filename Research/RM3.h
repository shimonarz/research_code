
#ifndef _RM3_H
#define _RM3_H

#include "GeneralFunctions.h"
#include "Evaluate.h"

#include "Statistics.h"

#include <limits> 

/*
typedef struct Config
{
	string configName;
	double Sum;
	double Avg;
	std::map<string ,double> listOfQuery; // the key is query name , value is query value 

	void init(string name)
	{
		configName = name;
		Sum = 0;
		Avg = 0;
	}

	void clear()
	{
		listOfQuery.clear();
	}
	
	void add(string queryName, double value)
	{
		Sum += value;
		listOfQuery.insert(std::make_pair(queryName, value));
	}

	void calcAvg()
	{
		Avg = Sum / listOfQuery.size();
	}

	double getAvgNotIncludeQuery(string queryName)
	{
		double queryRes = getQueryResult(queryName)	;
		return (Sum - queryRes) / (listOfQuery.size() - 1);
	}
	
	double getQueryResult(string queryName)
	{
		return listOfQuery.find(queryName)->second;
	}

	vector<string> getQueriesNames( )
	{
		vector<string> vec;
		typedef std::map<string ,double>::iterator it_type;
		for(it_type iterator = listOfQuery.begin(); 
					iterator != listOfQuery.end(); 
					iterator++) 
		{
			vec.push_back(iterator->first);
		}
		return vec;
	}

}Config;

typedef struct ConfigMap
{
	std::map<string,Config*> listOfConfig; // the key is Configuration name , value is Configuration values

	void add(string configName, string queryName , double value )
	{
		std::map< string , Config* >::const_iterator  configIt = listOfConfig.find(configName);
		if( configIt == listOfConfig.end() ) // config not found
		{
			Config* newConfig= new Config();
			newConfig->init(configName);
			newConfig->add(queryName, value);
			listOfConfig.insert(std::make_pair<string,Config*> (configName, newConfig));
		}
		else
		{
			Config* configFromMap = configIt->second ;
			configFromMap->add(queryName, value);
		}
	}

	void calcAvg()
	{
		typedef std::map<string,Config*>::iterator it_type;
		for(it_type iterator = listOfConfig.begin(); iterator != listOfConfig.end(); iterator++) {
			iterator->second->calcAvg();
		}
	}

	string getMaxAvgConfigNotIncludeQuery(string queryName)
	{
		std::map<string,Config*>::iterator listOfConfigIterator = listOfConfig.begin();

		double maxValue = listOfConfigIterator->second->getAvgNotIncludeQuery(queryName);
		string maxConfig = listOfConfigIterator->first;

		for(listOfConfigIterator = listOfConfig.begin(); listOfConfigIterator != listOfConfig.end(); listOfConfigIterator++) 
		{
			double currentValue = listOfConfigIterator->second->getAvgNotIncludeQuery(queryName);
			if(currentValue > maxValue )
			{
				maxValue = currentValue;
				maxConfig = listOfConfigIterator->first;
			}
		}
		return maxConfig;
	}

	double getQueryResultPerConfiguration(string maxConfigForRm310, string query)
	{
		std::map< string , Config* >::const_iterator  configIt = listOfConfig.find(maxConfigForRm310);
		return configIt->second->getQueryResult(query);
	}

	double getMaxQueryResult(string query)
	{
		std::map<string,Config*>::iterator listOfConfigIterator = listOfConfig.begin();

		double maxValue = listOfConfigIterator->second->getQueryResult(query);

		for(listOfConfigIterator = listOfConfig.begin(); listOfConfigIterator != listOfConfig.end(); listOfConfigIterator++) 
		{
			double currentValue = listOfConfigIterator->second->getQueryResult(query);
			if(currentValue > maxValue )
			{
				maxValue = currentValue;
			}
		}
		return maxValue;
	}

	vector<string> getQueriesNames( )
	{
		return listOfConfig.begin()->second->getQueriesNames() ;
	}

}ConfigMap;

typedef struct ConfigPerEvaluation
{
	std::map<string, ConfigMap* > configPerEvaluation; // key is evaluation , 
	
	void add(string configName, string evalName, string queryName , double value)
	{
		std::map< string , ConfigMap* >::const_iterator  pos = configPerEvaluation.find(evalName);
		if( pos == configPerEvaluation.end()) // evaluation not found
		{ 
			ConfigMap* configMap = new ConfigMap();
			configMap->add(configName, queryName , value);
			configPerEvaluation.insert(std::make_pair<string, ConfigMap* > (evalName, configMap));
		}
		else
		{
			ConfigMap* configMap = pos->second;
			configMap->add(configName, queryName , value);
		}

	}

	void calcAvg()
	{
		typedef std::map<string,ConfigMap*>::iterator it_type;
		for(it_type iterator = configPerEvaluation.begin(); iterator != configPerEvaluation.end(); iterator++) {
			iterator->second->calcAvg();
		}
	}

	vector<string> getEvaluationsNames( )
	{
		vector<string> vec;
		typedef std::map<string,ConfigMap*>::iterator it_type;
		for(it_type iterator = configPerEvaluation.begin(); 
					iterator != configPerEvaluation.end(); 
					iterator++) 
		{
			vec.push_back(iterator->first);
		}
		return vec;
	}

	vector<string> getQueriesNames( )
	{
		return configPerEvaluation.begin()->second->getQueriesNames() ;
	}

	ConfigMap* getConfigMap(string eval)
	{
		return configPerEvaluation.find(eval)->second;
	}

}ConfigPerEvaluation;

class Rm3
{
	ConfigPerEvaluation configPerEvaluation;
public:
	bool Init(vector<string> *files)
	{

		if(files == NULL)
		{
			FILE_LOG(logERROR) << "Failed to init rm3 data structure - receive null reference for file list "  ;
			return false;
		}

		int numberOfFiles = files->size();

		if(numberOfFiles == 0)
		{
			FILE_LOG(logERROR) << "Failed to init rm3 data structure - receive empty file list "  ;
			return false;
		}

		FILE_LOG(logINFO) << "Start to read all files to create RM3 data structure, number of files is :  " << numberOfFiles ;
		
		for(int i = 0 ; i < numberOfFiles ; ++i)
		{
			string fileName = (*files)[i];
			std::vector<std::string> splitedFile = split(fileName, '/'); // the file path is split by "/" note.
			string weightConfiguration = splitedFile[splitedFile.size()-2];
			string configuration = splitedFile[splitedFile.size()-3];

			string key = configuration + "_" + weightConfiguration;

			EvaluateFile* file = new EvaluateFile();
			file->init(fileName);

			int linesNumber = file->lines.size();
			for (int j = 0; j < linesNumber; j++)
			{
				EvaluatResultFileLine *evaluatResultFileLine = file->lines[j];
				string evalName = evaluatResultFileLine->evalName;
				string queryName = evaluatResultFileLine->queryName;
				double value = evaluatResultFileLine->evalValue;
				if(queryName == "all")
				{
					continue;
				}
				configPerEvaluation.add( key , evalName , queryName , value);
			}
			file->Clear();
			delete file;
			
		}
		configPerEvaluation.calcAvg();
		return true;
	}
	

	void CalculateCrossValidation(string dirPath)
	{
		FILE_LOG(logINFO) << "Start Calculate Cross validation for RM3." ;

		vector<string> evaluationNames = configPerEvaluation.getEvaluationsNames();
		vector<string> originalQueryNames = configPerEvaluation.getQueriesNames();
		std::map<string, double> cv;
		int evaluationlength = evaluationNames.size();
		for (int i = 0; i < evaluationlength; i++)
		{
			ResultToPrint resultToPrint;
			string evalName = evaluationNames[i];
			double cvPerEval = CalculateCrossValidationPerEval(evalName);
			
			cv.insert(std::make_pair(evalName,cvPerEval));
			
			resultToPrint.listOfSingleValue.insert(std::make_pair<string,double>("CV Value",cvPerEval));
			string datFile = dirPath + "/" + evalName + ".dat";
			resultToPrint.writeOnlySingelValue(datFile);
			resultToPrint.Clear();
		}
		
		string datFile = dirPath + "/cv.txt";
		FILE_LOG(logINFO) << "Write Cross validation :" << datFile ;
		writeCVFile(datFile, &cv);
		return ;
	}


	void CalculateBestoneAvg(string dirPath)
	{
		vector<string> evaluationNames = configPerEvaluation.getEvaluationsNames();
		vector<string> originalQueryNames = configPerEvaluation.getQueriesNames();
		std::map<string, double> bestoneAvgList;
		int evaluationlength = evaluationNames.size();
		for (int i = 0; i < evaluationlength; i++)
		{
			ResultToPrint resultToPrint;
			string evalName = evaluationNames[i];
			double bestoneAvg = CalculateBestoneAvgPerEval(evalName);
			
			bestoneAvgList.insert(std::make_pair(evalName,bestoneAvg));
			
			resultToPrint.listOfSingleValue.insert(std::make_pair<string,double>("bestoneAvg Value",bestoneAvg));
			string datFile = dirPath + "/" + evalName + ".dat";
			resultToPrint.writeOnlySingelValue(datFile);
			resultToPrint.Clear();
		}

		string datFile = dirPath + "/bestoneAvg.txt";
		writeCVFile(datFile, &bestoneAvgList);
		return ;
	}

private:
	double CalculateCrossValidationPerEval(string evalName)
	{
		vector<string> originalQueryNames = configPerEvaluation.getQueriesNames();
		ConfigMap *configMap = configPerEvaluation.getConfigMap(evalName);
		double sum = 0;

		// Do cross validation for each query in evaluation
		int originalQuerylength = originalQueryNames.size();
		for (int j = 0; j < originalQuerylength; j++)
		{
			string queryName = originalQueryNames[j];
			
			// Train - calculate the maximum value of all configuration, the current query not included
			string maxConfig = configMap->getMaxAvgConfigNotIncludeQuery(queryName);

			// Test  - get the value for the query in the chosen configuration
			double queryResConfig = configMap->getQueryResultPerConfiguration(maxConfig,queryName);

			// sum the query result
			sum += queryResConfig;
		}

		double cvPerEval = sum / originalQuerylength;
		return cvPerEval;
	}


	double CalculateBestoneAvgPerEval(string evalName)
	{
		vector<string> originalQueryNames = configPerEvaluation.getQueriesNames();
		ConfigMap *configMap = configPerEvaluation.getConfigMap(evalName);
		double sum = 0;

		// Do cross validation for each query in evaluation
		int originalQuerylength = originalQueryNames.size();
		for (int j = 0; j < originalQuerylength; j++)
		{
			string queryName = originalQueryNames[j];
			
			// calculate the maximum value of all configuration
			double queryResConfig = configMap->getMaxQueryResult(queryName);

			// sum the query result
			sum += queryResConfig;
		}

		double bestoneAvg = sum / originalQuerylength;
		return bestoneAvg;
	}


	void writeCVFile(string path_to_file, std::map<string, double>* cv)
	{
		ofstream of;
		of.open ( path_to_file.c_str(), std::ofstream::out ); 
		of << "# evaluation , cv value" << std::endl;

		std::map<string, double>::iterator cvIterator;
		for (cvIterator = cv->begin(); cvIterator != cv->end(); ++cvIterator)
		{
			string eval = cvIterator->first;
			double value = cvIterator->second;
			of << eval << " " <<  value  << " "  << std::endl;
		}

	}
};

*/














typedef struct StatisticsCalculatorPerEvaluation
{
	std::map<string, StatisticsData* > configPerEvaluation; // key is evaluation , 
	
	void add(string configName, string evalName, string queryName , double value)
	{
		std::map< string , StatisticsData* >::const_iterator  pos = configPerEvaluation.find(evalName);
		if( pos == configPerEvaluation.end()) // evaluation not found
		{ 
			StatisticsData* configMap = new StatisticsData();
			configMap->add(configName, queryName , value);
			configPerEvaluation.insert(std::make_pair<string, StatisticsData* > (evalName, configMap));
		}
		else
		{
			StatisticsData* configMap = pos->second;
			configMap->add(configName, queryName , value);
		}

	}

	void calcAvg()
	{
		typedef std::map<string,StatisticsData*>::iterator it_type;
		for(it_type iterator = configPerEvaluation.begin(); iterator != configPerEvaluation.end(); iterator++) {
			iterator->second->calcAvg();
		}
	}

	vector<string> getEvaluationsNames( )
	{
		vector<string> vec;
		typedef std::map<string,StatisticsData*>::iterator it_type;
		for(it_type iterator = configPerEvaluation.begin(); 
					iterator != configPerEvaluation.end(); 
					iterator++) 
		{
			vec.push_back(iterator->first);
		}
		return vec;
	}

	vector<string> getQueriesNames( )
	{
		return configPerEvaluation.begin()->second->getKeys() ;
	}

	StatisticsData* getConfigMap(string eval)
	{
		return configPerEvaluation.find(eval)->second;
	}

}StatisticsCalculatorPerEvaluation;


class Rm3
{
	StatisticsCalculatorPerEvaluation configPerEvaluation;
public:
	bool Init(vector<string> *files)
	{

		if(files == NULL)
		{
			FILE_LOG(logERROR) << "Failed to init rm3 data structure - receive null reference for file list "  ;
			return false;
		}

		int numberOfFiles = files->size();

		if(numberOfFiles == 0)
		{
			FILE_LOG(logERROR) << "Failed to init rm3 data structure - receive empty file list "  ;
			return false;
		}

		FILE_LOG(logINFO) << "Start to read all files to create RM3 data structure, number of files is :  " << numberOfFiles ;
		
		for(int i = 0 ; i < numberOfFiles ; ++i)
		{
			string fileName = (*files)[i];
			std::vector<std::string> splitedFile = split(fileName, '/'); // the file path is split by "/" note.
			
			string key = getPathToFile_UnderScoreFormat(fileName);
			
			/*string weightConfiguration = splitedFile[splitedFile.size()-2];
			string configuration = splitedFile[splitedFile.size()-3];

			string key = configuration + "_" + weightConfiguration;*/

			EvaluateFile* file = new EvaluateFile();
			file->init(fileName);

			int linesNumber = file->lines.size();
			for (int j = 0; j < linesNumber; j++)
			{
				EvaluatResultFileLine *evaluatResultFileLine = file->lines[j];
				string evalName = evaluatResultFileLine->evalName;
				string queryName = evaluatResultFileLine->queryName;
				double value = evaluatResultFileLine->evalValue;
				if(queryName == "all")
				{
					continue;
				}
				configPerEvaluation.add( key , evalName , queryName , value);
			}
			file->Clear();
			delete file;
			
		}
		configPerEvaluation.calcAvg();
		return true;
	}
	

	void CalculateCrossValidation(string dirPath)
	{
		FILE_LOG(logINFO) << "Start Calculate Cross validation for RM3." ;

		vector<string> evaluationNames = configPerEvaluation.getEvaluationsNames();
		vector<string> originalQueryNames = configPerEvaluation.getQueriesNames();
		std::map<string, double> cv;
		int evaluationlength = evaluationNames.size();
		for (int i = 0; i < evaluationlength; i++)
		{
			ResultToPrint resultToPrint;
			string evalName = evaluationNames[i];
			StatisticsCalculator statisticsCalculator;
			statisticsCalculator._statisticsData = *configPerEvaluation.getConfigMap(evalName);
			
			double cvPerEval = statisticsCalculator.CalculateCrossValidation();
			
			cv.insert(std::make_pair(evalName,cvPerEval));
			
			resultToPrint.listOfSingleValue.insert(std::make_pair<string,double>("CV Value",cvPerEval));
			string datFile = dirPath + "/" + evalName + ".dat";
			resultToPrint.writeOnlySingelValue(datFile);
			resultToPrint.Clear();
		}
		
		string datFile = dirPath + "/cv.txt";
		FILE_LOG(logINFO) << "Write Cross validation :" << datFile ;
		writeCVFile(datFile, &cv);
		return ;
	}

	void CalculateCrossValidationWithPrints(string dirPath)
	{
		FILE_LOG(logINFO) << "Start Calculate Cross validation for RM3." ;

		vector<string> evaluationNames = configPerEvaluation.getEvaluationsNames();
		vector<string> originalQueryNames = configPerEvaluation.getQueriesNames();
		std::map<string, double> cv;
		int evaluationlength = evaluationNames.size();
		for (int i = 0; i < evaluationlength; i++)
		{
			ResultToPrint resultToPrint;
			string evalName = evaluationNames[i];
			StatisticsCalculator statisticsCalculator;
			statisticsCalculator._statisticsData = *configPerEvaluation.getConfigMap(evalName);
			string cvFile = dirPath + "/" + evalName + "_cv_steps.txt";
			double cvPerEval = statisticsCalculator.CalculateCrossValidationWithPrints(cvFile);
			
			cv.insert(std::make_pair(evalName,cvPerEval));
			
			resultToPrint.listOfSingleValue.insert(std::make_pair<string,double>("CV Value",cvPerEval));
			string datFile = dirPath + "/" + evalName + ".dat";
			resultToPrint.writeOnlySingelValue(datFile);
			resultToPrint.Clear();
		}
		
		string datFile = dirPath + "/cv.txt";
		FILE_LOG(logINFO) << "Write Cross validation :" << datFile ;
		writeCVFile(datFile, &cv);
		return ;
	}


	void CalculateBestoneAvg(string dirPath)
	{
		vector<string> evaluationNames = configPerEvaluation.getEvaluationsNames();
		vector<string> originalQueryNames = configPerEvaluation.getQueriesNames();
		std::map<string, double> bestoneAvgList;
		int evaluationlength = evaluationNames.size();
		for (int i = 0; i < evaluationlength; i++)
		{
			ResultToPrint resultToPrint;
			string evalName = evaluationNames[i];
			StatisticsCalculator statisticsCalculator;
			statisticsCalculator._statisticsData = *configPerEvaluation.getConfigMap(evalName);
			double bestoneAvg = statisticsCalculator.CalculateBestoneAvg();
			
			bestoneAvgList.insert(std::make_pair(evalName,bestoneAvg));
			
			resultToPrint.listOfSingleValue.insert(std::make_pair<string,double>("bestoneAvg Value",bestoneAvg));
			string datFile = dirPath + "/" + evalName + ".dat";
			resultToPrint.writeOnlySingelValue(datFile);
			resultToPrint.Clear();
		}

		string datFile = dirPath + "/bestoneAvg.txt";
		writeCVFile(datFile, &bestoneAvgList);
		return ;
	}


		void CalculateBestoneAvgWithPrints(string dirPath)
	{
		vector<string> evaluationNames = configPerEvaluation.getEvaluationsNames();
		vector<string> originalQueryNames = configPerEvaluation.getQueriesNames();
		std::map<string, double> bestoneAvgList;
		int evaluationlength = evaluationNames.size();
		for (int i = 0; i < evaluationlength; i++)
		{
			ResultToPrint resultToPrint;
			string evalName = evaluationNames[i];
			StatisticsCalculator statisticsCalculator;
			statisticsCalculator._statisticsData = *configPerEvaluation.getConfigMap(evalName);
			string bestoneAvgFile = dirPath + "/" + evalName + "_bestoneAvg_steps.txt";
			double bestoneAvg = statisticsCalculator.CalculateBestoneAvgWithPrints(bestoneAvgFile);
			
			bestoneAvgList.insert(std::make_pair(evalName,bestoneAvg));
			
			resultToPrint.listOfSingleValue.insert(std::make_pair<string,double>("bestoneAvg Value",bestoneAvg));
			string datFile = dirPath + "/" + evalName + ".dat";
			resultToPrint.writeOnlySingelValue(datFile);
			resultToPrint.Clear();
		}

		string datFile = dirPath + "/bestoneAvg.txt";
		writeCVFile(datFile, &bestoneAvgList);
		return ;
	}

private:

	void writeCVFile(string path_to_file, std::map<string, double>* cv)
	{
		ofstream of;
		of.open ( path_to_file.c_str(), std::ofstream::out ); 
		of << "# evaluation , cv value" << std::endl;

		std::map<string, double>::iterator cvIterator;
		for (cvIterator = cv->begin(); cvIterator != cv->end(); ++cvIterator)
		{
			string eval = cvIterator->first;
			double value = cvIterator->second;
			of << eval << " " <<  value  << " "  << std::endl;
		}

	}
};


#endif