﻿#ifndef BC_H
#define BC_H

#include "GeneralFunctions.h"
#include "ModelConfiguration.h"
#include "DataStructer.h"

class CB_ModelConfiguration : public Base_ModelConfiguration
{
public:
	vector<string> queriesClustersFileList;
	vector<string> queriesTermsProbabilityInClustersFileList;
	double termsProbabilityInClustersWeight;
	string clustersFile;

	void Init(string fileName)
	{
		ModelConfiguration modelConfiguration;
		modelConfiguration.Init(fileName);
		Init(&modelConfiguration);
	}

	void virtual Init(ModelConfiguration *modelConfiguration)
	{
		Base_ModelConfiguration::Init(modelConfiguration);
		queriesClustersFileList = modelConfiguration->queriesClustersFileList;
		queriesTermsProbabilityInClustersFileList = modelConfiguration->queriesTermsProbabilityInClustersFileList;
		termsProbabilityInClustersWeight = modelConfiguration->termsProbabilityInClustersWeight;
		clustersFile = modelConfiguration->clustersFile;
	}

};

class CBQuery 
{
public:
	std::string QueryId;

	double OriginalQueryWeight;
	std::vector<Term*> OriginalQuery;
	
	double ClustresTermsWeight;
	std::vector<Term*> ClustresTerms;

	void init()
	{
		QueryId = "";
		OriginalQueryWeight = ClustresTermsWeight = 0.0;
	}

	
	void InitOriginalQuery(string fullQuery)
	{
		// #combine( international organized crime )
		std::string qcopy( fullQuery);
		for ( size_t i=0; i<qcopy.size(); i++ )
		{
			if ( (qcopy[i]=='(')||(qcopy[i]==')')||(qcopy[i]==',')||(qcopy[i]=='"')||(qcopy[i]==':')) 
			{
				qcopy[i]=' ';
			}
		}

		vector<string> queryTerms = split(qcopy , ' ' );

		for (int i = 1; i < queryTerms.size(); i++)
		{
			Term* term = new Term(queryTerms[i]);
			OriginalQuery.push_back(term);
		}
	}


	void Clear()
	{
		int length = OriginalQuery.size();
		for (int i = 0; i < length; i++)
		{
			delete OriginalQuery[i];
		}
		OriginalQuery.clear();

		length = ClustresTerms.size();
		for (int i = 0; i < length; i++)
		{
			delete ClustresTerms[i];
		}
		ClustresTerms.clear();
	}

	void ClearClustresExtended()
	{
		int length = ClustresTerms.size();
		for (int i = 0; i < length; i++)
		{
			delete ClustresTerms[i];
		}
		ClustresTerms.clear();
	}
};

typedef struct CBModel
{
	QueriesFile Queries; // Queries 
	WordInClusterProbabilityForAllQuery WordInClusterProbability; //	WordInClusterProbabilityForAllQuery WordInClusterProbability; // P(w|C)
	CB_ModelConfiguration ModelConfiguration; // ���� �� ������������ �� �����
	QueriesClusters ClustersTermsForAllQuery; // ���� �� �� �������� ������� ����

	void Init(CB_ModelConfiguration configuration)
	{
		FILE_LOG(logINFO) << "Init CB model " ;

		ModelConfiguration = configuration;
		Queries.readFile(ModelConfiguration.queries);
		WordInClusterProbability.init( &ModelConfiguration.queriesTermsProbabilityInClustersFileList );
		ClustersTermsForAllQuery.init(&ModelConfiguration.queriesClustersFileList);

		FILE_LOG(logINFO) << "Finish Init CB model " ;
	}

	void BuildModel()
	{
		int k = 1;
		cout << "Start Build retrieval files \n";

		string outputDir = ModelConfiguration.outputDir;
		map< string,string >::iterator query_it; // Queries
		for(query_it = Queries.Queries.begin() ; 
			query_it != Queries.Queries.end() ;
			++query_it)
		{
			string queryName = query_it->first;
			string queryOutputFile = ModelConfiguration.outputDir + "/" + queryName + ".clusters";

			cout << '\r' << "	Extend Query " <<  queryName  << " - Progress : " << k <<  '/' <<  Queries.Queries.size() <<
				".     Progress Percent " <<  (int)(((double)k/(double)Queries.Queries.size())*100.0)  <<  "%" << std::flush;
		
			ofstream of;
			of.open ( queryOutputFile.c_str(), std::ofstream::out ); 
			of << "<parameters>\n";

			CBQuery extendedQuery;

			extendedQuery.InitOriginalQuery(query_it->second);
			extendedQuery.OriginalQueryWeight = ModelConfiguration.queriesWeight;

			extendedQuery.ClustresTermsWeight = ModelConfiguration.termsProbabilityInClustersWeight;
			
			ClustersInQuery* clusters = ClustersTermsForAllQuery.GetClustersInQuery(queryName);
			vector<string> clustersNames;
			clusters->GetClusterNames(&clustersNames);
			//cout << "Get-Cluster-Names " << clustersNames.size() << " \n";
			for (int i = 0; i < clustersNames.size(); i++)
			{
				string clusterName = clustersNames[i];
				vector<string> clusterTermsNames;
				clusters->GetClusterTerms(clusterName, &clusterTermsNames);
				//cout << "Get-Cluster-Terms for cluster  " << clusterName << " , size = " << clusterTermsNames.size() << " \n";
				vector<Term*> ClustresTerms;
				for (int j = 0; j < clusterTermsNames.size() ; j++)
				{
					string term = clusterTermsNames[j];
					double probability = WordInClusterProbability.GetWordInClusterProbability(queryName ,  clusterName , term);
					Term* t = new Term(term , probability);
					ClustresTerms.push_back(t);
				}
				extendedQuery.ClustresTerms = ClustresTerms;
				
				string clusterHeader = GetClusterQueryFullName(queryName , clusterName );
				writeExtendedQuery(&of,clusterHeader,&extendedQuery);
				extendedQuery.ClearClustresExtended();
			}

			of << "</parameters>";
			of.close(); 
			k++;
		}
		cout << '\n';

	}


	void BuildBestClusterModel(vector< pair<string,double> > *clustersFeatureFileList)
	{
		ClustersFeature ClusterInQueryProbability;
		ClusterInQueryProbability.init(clustersFeatureFileList);
	
		int k = 1;
		FILE_LOG(logINFO)  << "Start Build Best Cluster Model retrieval files";

		string queryOutputFile = ModelConfiguration.outputDir + "//queries.xml";

		ofstream of;
		of.open ( queryOutputFile.c_str(), std::ofstream::out ); 
		of << "<parameters>\n";

		string outputDir = ModelConfiguration.outputDir;
		map< string,string >::iterator query_it; // Queries
		for(query_it = Queries.Queries.begin() ; 
			query_it != Queries.Queries.end() ;
			++query_it)
		{
			string queryName = query_it->first;
		
			cout << '\r' << "	Extend Query " <<  queryName  << " - Progress : " << k <<  '/' <<  Queries.Queries.size() <<
				".     Progress Percent " <<  (int)(((double)k/(double)Queries.Queries.size())*100.0)  <<  "%" << std::flush;
		

			CBQuery extendedQuery;

			extendedQuery.InitOriginalQuery(query_it->second);
			extendedQuery.OriginalQueryWeight = ModelConfiguration.queriesWeight;

			extendedQuery.ClustresTermsWeight = ModelConfiguration.termsProbabilityInClustersWeight;
			
			string maxClustreName = ClusterInQueryProbability.getMaxCluster(queryName);

			ClustersInQuery* clusters = ClustersTermsForAllQuery.GetClustersInQuery(queryName);
			vector<string> clustersNames;
			clusters->GetClusterTerms(maxClustreName, &clustersNames);

			FILE_LOG(logINFO) << "Extend Query " <<  queryName << " Max cluster is - " << maxClustreName;
			vector<Term*> ClustresTerms;
			for (int j = 0; j < clustersNames.size() ; j++)
			{
				string term = clustersNames[j];
				double probability = WordInClusterProbability.GetWordInClusterProbability(queryName ,  maxClustreName , term);
				Term* t = new Term(term , probability);
				ClustresTerms.push_back(t);
			}
			extendedQuery.ClustresTerms = ClustresTerms;
			writeExtendedQuery(&of,queryName,&extendedQuery);
			extendedQuery.ClearClustresExtended();
			k++;
		
		}
		cout << '\n';

		of << "</parameters>";
		of.close(); 
		
		FILE_LOG(logINFO) << "Finish Start Build retrieval files.";
		
	}



	void writeExtendedQuery(ofstream *of,string queryNumber,CBQuery *expansion)
	{
		std::string query = buildQuery(expansion);

		*of << "<query>\n";
		*of << "<number>" + queryNumber + "</number>\n";
		*of << "<text>" + query + "</text>\n";
		*of << "</query>\n";
	}

	 std::string buildQuery(CBQuery *terms)
	{
		std::string result;

		char originalQueryWeightStr[40];
		sprintf(originalQueryWeightStr,"%.32f",terms->OriginalQueryWeight);
		std::ostringstream originalQueryWeight;
		originalQueryWeight << originalQueryWeightStr;

		char termsWeightStr[40];
		sprintf(termsWeightStr,"%.32f",terms->ClustresTermsWeight);
		std::ostringstream termsWeight;
		termsWeight << termsWeightStr;

		if(terms->OriginalQueryWeight != 0 && terms->ClustresTermsWeight == 0)
		{
			// no expansion 
			result = "#combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + " )" ;
		}
		else
		{
			result = "#weight( " + 
			originalQueryWeight.str() + 
			" #combine( #combine( "  + 
			Term::convertTermToString(terms->OriginalQuery) + ") ) " + 
			termsWeight.str() + 
			" #weight(  " + 
			Term::convertTermToString(terms->ClustresTerms,true,true) + " ) )";
		}

		return result;
	}

	void Clear()
	{
		WordInClusterProbability.Clear();
		ClustersTermsForAllQuery.Clear();

	}

}CBModel;

#endif