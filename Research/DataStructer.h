
#ifndef DataStructer_H
#define DataStructer_H


#include "GeneralFunctions.h"

// TODO - rename to : XmlQueriesFile
typedef struct QueriesFile
{
	map<string,string > Queries; // key = query id , value = query text

	void readFile(string queriesFileName)
	{
		FILE_LOG(logDEBUG) << "Read Queries XML File - " << queriesFileName;

		ifstream finput;
		finput.open ( queriesFileName.c_str() ); 
		if (!finput.good())
		{
			FILE_LOG(logERROR) << "Can't open queries file: " << queriesFileName ;
			return;
		}

		string numberMark = "<number>";
		string queryStartMark = "<query>";
		string queryEndMark = "</query>";
		string textMark = "<text>";

		string number;
		string text;

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			line.erase(std::remove(line.begin(), line.end(), '\t'), line.end());

			if (line.find(numberMark) != std::string::npos) 
			{
				vector<string> splitLine = split(line, '>');
				number = splitLine[1];
				splitLine = split(number, '<');
				number = splitLine[0];
			}
			else if(line.find(textMark) != std::string::npos)
			{
				vector<string> splitLine = split(line, '>');
				text = splitLine[1];
				splitLine = split(text, '<');
				text = splitLine[0];
			}
			else if(line.find(queryEndMark) != std::string::npos)
			{
				Queries.insert(std::make_pair<string,string>(number,text));
			}
		}
		finput.close();

		FILE_LOG(logDEBUG) << "Finish Read Queries XML File - " << queriesFileName;
	}

	void WriteFile(string outputFile)
	{
		ofstream of;
		of.open ( outputFile.c_str(), std::ofstream::out ); 
		of << "<parameters>\n";

		map<string,string >::iterator it;
		for(it = Queries.begin() ; it != Queries.end();  ++it)
		{
			string queryName = it->first;
			string queryText = it->second;

			of << "<query>\n";
			of << "<number>" + queryName + "</number>\n";
			of << "<text>" + queryText + "</text>\n";
			of << "</query>\n";
		}

		of << "</parameters>";
		of.close(); 	
	}


}QueriesFile;

// TODO - rename to : XmlQueriesFileWithWorkingSetDocno
typedef struct QueriesFileWithWorkingSetDocno
{
	map<string,string > Queries; // key = query id , value = query text
	map<string, vector<string> > WorkingSetDocnoPerQuery; // key = query id , value = query text

	void readFileWitoutWorkingSetDocno(string queriesFileName)
	{
		QueriesFile queriesFile;
		queriesFile.readFile(queriesFileName);
		Queries = queriesFile.Queries;

		map<string,string >::iterator it;
		for(it = Queries.begin() ; it != Queries.end();  ++it)
		{
			vector<string> workingSetDocnoList;
			WorkingSetDocnoPerQuery.insert(std::make_pair(it->first,workingSetDocnoList));
		}

	}

	void AddWorkingSetDocnoListForAllQuery(vector<string> *workingSetDocnoList)
	{
		map<string,vector<string> >::iterator it;
		for(it = WorkingSetDocnoPerQuery.begin() ; it != WorkingSetDocnoPerQuery.end();  ++it)
		{
			it->second = *workingSetDocnoList;
		}
	}

	void AddWorkingSetDocnoListToQuery(string query, vector<string> *workingSetDocnoList)
	{
		map<string,vector<string> >::iterator it = WorkingSetDocnoPerQuery.find(query);
		if( it != WorkingSetDocnoPerQuery.end())
		{
			it->second = *workingSetDocnoList;
		}
		else
		{
			FILE_LOG(logINFO)  << "AddWorkingSetDocnoListToQuery :: Cannot find query " << query;
		}
	}

	void WriteFile(string outputFile)
	{
		ofstream of;
		of.open ( outputFile.c_str(), std::ofstream::out ); 
		of << "<parameters>\n";

		map<string,string >::iterator it;
		for(it = Queries.begin() ; it != Queries.end();  ++it)
		{
			string queryName = it->first;
			string queryText = it->second;
			vector<string> *workingSetDocnoList = &WorkingSetDocnoPerQuery.find(queryName)->second;

			of << "<query>\n";
			of << "<number>" + queryName + "</number>\n";
			of << "<text>" + queryText + "</text>\n";
			for (int i = 0; i < workingSetDocnoList->size(); i++)
			{
				string doc = (*workingSetDocnoList)[i];
				of << "<workingSetDocno>" + doc + "</workingSetDocno>\n";
			}
			of << "</query>\n";
		}

		of << "</parameters>";
		of.close(); 	
	}

	void readFile(string queriesFileName)
	{
		cout << "Read Queries XML File" << std::endl;

		ifstream finput;
		finput.open ( queriesFileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open queries file: " << queriesFileName << std::endl;
			return;
		}

		string numberMark = "<number>";
		string queryStartMark = "<query>";
		string workingSetDocnoMark = "<workingSetDocno>";
		string queryEndMark = "</query>";
		string textMark = "<text>";
		string number;
		string text;

		vector<string> workingSetDocnoList;
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			line.erase(std::remove(line.begin(), line.end(), '\t'), line.end());

			if (line.find(numberMark) != std::string::npos) 
			{
				vector<string> splitLine = split(line, '>');
				number = splitLine[1];
				splitLine = split(number, '<');
				number = splitLine[0];
			}
			else if(line.find(textMark) != std::string::npos)
			{
				vector<string> splitLine = split(line, '>');
				text = splitLine[1];
				splitLine = split(text, '<');
				text = splitLine[0];
			}
			else if(line.find(workingSetDocnoMark) != std::string::npos)
			{
				vector<string> splitLine = split(line, '>');
				text = splitLine[1];
				splitLine = split(text, '<');
				string workingSetDocno = splitLine[0];
				workingSetDocnoList.push_back(workingSetDocno);
			}
			else if(line.find(queryEndMark) != std::string::npos)
			{
				Queries.insert(std::make_pair(number,text));
				WorkingSetDocnoPerQuery.insert(std::make_pair(number,workingSetDocnoList));
				workingSetDocnoList.clear();
			}
		}
		finput.close();
	}

	void getQueriesNames(vector<string> *names)
	{
		map<string,string >::iterator it;
		for( it = Queries.begin() ; it != Queries.end() ; ++it)
		{
			names->push_back(it->first);
		}
	}

}QueriesFileWithWorkingSetDocno;

// TODO - remove the "vec" from the struct
typedef struct Term
{
	std::string value;
	float* vec;
	double weight;

	bool operator () ( const Term& m ) const
	{
		return m.value == value;
	}

	Term()
	{
		value = "";
		vec = NULL;
		weight = 0.0;
	}

	Term(string _value)
	{
		value = _value;
		vec = NULL;
		weight = 0.0;
	}

	Term(string _value , string _weight)
	{
		value = _value;
		vec = NULL;
		weight = convertToDouble(_weight);
	}

	Term(string _value , double _weight)
	{
		value = _value;
		vec = NULL;
		weight = _weight;
	}

	bool operator==(const Term& m  ) const
	{
		return m.value == value;
	} 

	Term* Copy()
	{
		Term* t = new Term();
		t->value = value;
		t->weight = weight;
		t->vec = vec;
		return t;
	}

	static std::string convertTermToString(std::vector<Term*> terms , bool withWeight = false, bool withQuotationMark  = false )
	{
		std::string result;
		char str[40];
		for(unsigned int i=0 ; i<terms.size() ; ++i)
		{
			Term *t = terms[i];
			if( withWeight )
			{
				sprintf(str,"%.32f",t->weight);
				std::ostringstream wight;
				wight << str;
				result += wight.str() + " ";
			}
			if(withQuotationMark)
			{
				result += "\"" +  t->value + "\"" + " ";
			}
			else
			{
				result += t->value + " ";
			}
		}
		return result;
	}

	static void Normalize( vector<Term*> *vec )
	{
		double sum = 0;
		for (int i = 0; i < vec->size(); i++)
		{
			sum += (*vec)[i]->weight;
		}
		for (int i = 0; i < vec->size(); i++)
		{
			double weight = (*vec)[i]->weight;
			weight = weight / sum;
			(*vec)[i]->weight = weight;
		}
	}

	struct increasing_order_compare_Term
	{
		inline bool operator()(const Term& p1, const Term& p2)
		{
			if ( p1.weight < p2.weight ) return true;
			return false;
		}   
	};

	
	
}Term;


typedef struct QueryFromXmlFile
{
	double OriginalQueryWeight;
	std::vector<Term*> OriginalQuery;

	double ExpensionTermsWeight;
	std::vector<Term*> ExpensionTerms;

	void parse_query_xml_file_line( const std::string& query )
	{
		std::vector<std::string> qset;
		qset.clear();
		std::string qcopy( query);
		for ( size_t i=0; i<qcopy.size(); i++ )
		{
			if ( (qcopy[i]=='(')||(qcopy[i]==')')||(qcopy[i]==',')||(qcopy[i]=='"')||(qcopy[i]==':')) 
			{
				qcopy[i]=' ';
			}
		}
		std::istringstream oss (qcopy);
		std::string temp;

		while (oss.good())
		{
			std::string temp;
			oss >> temp;
			if ( temp.empty()) continue;    
			qset.push_back( temp );
		}

		unsigned int j ;
		if(qset[0].compare("#combine") == 0)
		{
			OriginalQueryWeight = 0.0;
			ExpensionTermsWeight = 0.0;
			j = 1;
			while(j < qset.size() )
			{
				Term* term = new Term();
				term->value = qset[j];
				term->vec = NULL;
				term->weight = 0;
				OriginalQuery.push_back(term);
				j++;
			}
		}
		else
		{
			OriginalQueryWeight = atof (qset[1].c_str()); 
			j = 4;
			while(qset[j + 1].compare("#weight") != 0)
			{
				Term* term = new Term();
				term->value = qset[j];
				term->vec = NULL;
				term->weight = 0;
				OriginalQuery.push_back(term);
				j++;
			}
			ExpensionTermsWeight = atof (qset[j].c_str());
			j+=2;

			while(j < qset.size() )
			{
				Term* term = new Term();
				term->value = qset[j+1];
				term->vec = NULL;
				term->weight = atof (qset[j].c_str());  
				ExpensionTerms.push_back(term);
				j+=2;
			}

		}
	}

	string parse_query_to_xml_file_line()
	{
		std::string result;

		if(ExpensionTermsWeight == 0.0)
		{
			// no expansion 
			result = "#combine( "  + 	Term::convertTermToString(OriginalQuery) + " )" ;
		}
		else 
		{
			char originalQueryWeightStr[40];
			sprintf(originalQueryWeightStr,"%.32f",OriginalQueryWeight);
			std::ostringstream originalQueryWeightStream;
			originalQueryWeightStream << originalQueryWeightStr ;
		

			char expansionQueryWeightStr[40];
			sprintf(expansionQueryWeightStr,"%.32f",ExpensionTermsWeight);
			std::ostringstream expansionQueryWeightStream;
			expansionQueryWeightStream << expansionQueryWeightStr;

			// only Rm1 expansion
			result = "#weight( " + 
				originalQueryWeightStream.str() + " #combine( #combine( "  + 	Term::convertTermToString(OriginalQuery) + ") ) " + 
				expansionQueryWeightStream.str()  + " #weight(  " + Term::convertTermToString(ExpensionTerms,true,true) + " ) " + " )";
		}

		return result;
	}


	static bool descending_order_compare_Term( Term* p1,  Term* p2)
	{
		if ( p1->weight > p2->weight ) return true;
			return false;
	}

	void DownloadExpensionTermsTo(int numberOfTerms)
	{
		if(ExpensionTerms.size() < numberOfTerms)
		{
			return;
		}
		std::sort(ExpensionTerms.begin(), ExpensionTerms.end(), descending_order_compare_Term);
		std::vector<Term*> ExpensionTermsNew;
		for (int i = 0; i < numberOfTerms; i++)
		{
			ExpensionTermsNew.push_back(ExpensionTerms[i]->Copy());
		}
		Term::Normalize(& ExpensionTermsNew);

		int length = ExpensionTerms.size();
		for (int i = 0; i < length; i++)
		{
			delete ExpensionTerms[i];
		}
		ExpensionTerms.clear();

		ExpensionTerms = ExpensionTermsNew;
	}
	
	virtual void Clear()
	{
		int length = OriginalQuery.size();
		for (int i = 0; i < length; i++)
		{
			delete OriginalQuery[i];
		}
		OriginalQuery.clear();

		length = ExpensionTerms.size();
		for (int i = 0; i < length; i++)
		{
			delete ExpensionTerms[i];
		}
		ExpensionTerms.clear();

	}

}QueryFromXmlFile;

// TODO - change name to  - QueryExpension ( is the file that i create from indri - line is query with rm1 words)
class FirstQueryExpension
{
public:
	std::string QueryId;

	double OriginalQueryWeight;
	std::vector<Term*> OriginalQuery;

	double ExpensionTermsWeight;
	std::vector<Term*> ExpensionTerms;

	void init()
	{
		QueryId = "";
		OriginalQueryWeight = 0.0;
		ExpensionTermsWeight = 0.0;
	}

	void parse_query( const std::string& query )
	{
		std::vector<std::string> qset;
		qset.clear();
		std::string qcopy( query);
		for ( size_t i=0; i<qcopy.size(); i++ )
		{
			if ( (qcopy[i]=='(')||(qcopy[i]==')')||(qcopy[i]==',')||(qcopy[i]=='"')||(qcopy[i]==':')) 
			{
				qcopy[i]=' ';
			}
		}
		std::istringstream oss (qcopy);
		std::string temp;

		bool isWeight = false;
		bool isOriginalQuery = true;
		//oss << qcopy;
		while (oss.good())
		{
			std::string temp;
			oss >> temp;
			if ( temp.empty()) continue;    
			qset.push_back( temp );
		}

		QueryId = qset[0];
		OriginalQueryWeight = atof (qset[2].c_str()); 

		unsigned int j = 5;

		while(qset[j + 1].compare("#weight") != 0)
		{
			Term* term = new Term();
			term->value = qset[j];
			term->vec = NULL;
			term->weight = 0;
			OriginalQuery.push_back(term);
			j++;
		}

		ExpensionTermsWeight = atof (qset[j].c_str());
		j+=2;

		while(j < qset.size() )
		{
			Term* term = new Term();
			term->value = qset[j+1];
			term->vec = NULL;
			term->weight = atof (qset[j].c_str());  
			ExpensionTerms.push_back(term);
			j+=2;
		}
	}

	Term* FindInExpensionTerms(string value)
	{
		int length = ExpensionTerms.size();
		for (int i = 0; i < length; i++)
		{
			if( ExpensionTerms[i]->value == value)
			{
				return ExpensionTerms[i];
			}
		}
		return NULL;
	}

	// TODO - create class for the first Expension file
	static std::vector<FirstQueryExpension*>* readExpensQueryFile( string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return NULL;
		}
		vector<FirstQueryExpension*>* rm1Terms = new vector<FirstQueryExpension*>();
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;
			FirstQueryExpension *firstQueryExpension = new FirstQueryExpension();
			firstQueryExpension->parse_query(line);
			rm1Terms->push_back(firstQueryExpension);		
		}
		finput.close();
		return rm1Terms;
	}




	virtual void Clear()
	{
		int length = OriginalQuery.size();
		for (int i = 0; i < length; i++)
		{
			delete OriginalQuery[i];
		}
		OriginalQuery.clear();

		length = ExpensionTerms.size();
		for (int i = 0; i < length; i++)
		{
			delete ExpensionTerms[i];
		}
		ExpensionTerms.clear();

	}

};

typedef struct QrelsLine
{
	std::string QueryId;
	std::string Zero;
	std::string DocId;
	std::string Score;

	void init(string qrelsLine)
	{
		std::string qcopy( qrelsLine);
		std::istringstream oss (qcopy);
		std::string temp;

		if ( ! oss.good() ) {
			return ; 
		}

		std::string queryId;
		oss >> queryId;
		if ( queryId.empty()) {
			return ;    
		}

		std::string zero;
		oss >> zero;
		if ( zero.empty()) {
			return ;    
		}

		std::string docId;
		oss >> docId;
		if ( docId.empty()) {
			return;    
		}

		std::string docScore;
		oss >> docScore;
		if ( docScore.empty()) {
			return ;    
		}

		QueryId = queryId;
		Zero = zero;
		DocId = docId;
		Score = docScore;
	}

	void SetScoreToOneOrZero()
	{
		double score = convertToDouble(Score);
		if(score != 0.0)
			Score = "1";
	}

}QrelsLine;

typedef struct QrelsFile
{
	std::vector<QrelsLine*> Lines; 

	void init(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 

		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			QrelsLine* qrelsLine = new QrelsLine();
			qrelsLine->init(line);
			Lines.push_back(qrelsLine);
		}

		finput.close();
	}

	
	void SetScoreToOneOrZero()
	{
		int length = Lines.size();
		for (int i = 0; i < length; i++)
		{
			Lines[i]->SetScoreToOneOrZero();
		}
	}

	void Clear()
	{
		int length = Lines.size();
		for (int i = 0; i < length; i++)
		{
			delete Lines[i];
		}
	}


}QrelsFile;

typedef struct ClustersInQuery
{
	map< string , vector<Term> > ClusterInQueryList;  // key = cluster name , value = list of cluster words

	void init(string file)
	{
		ifstream finput;
		finput.open ( file.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << file << std::endl;
			return ;
		}
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			vector<string> splited = split(line, ':');
			string clusterName = splited[0];
			string clusterWords = splited[1];

			vector<string> splitedClusterWords = split(clusterWords, ',');
			vector<Term> list;
			for (int i = 0; i < splitedClusterWords.size(); i++)
			{
				Term t(splitedClusterWords[i]);
				list.push_back(t);
			}
			ClusterInQueryList.insert(std::make_pair<string , vector<Term> >(clusterName,list));	
		}
		finput.close();

	}

	void initWithScore(string file)
	{
		ifstream finput;
		finput.open ( file.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << file << std::endl;
			return ;
		}
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			vector<string> splited = split(line, ':');
			string clusterName = splited[0];
			string clusterWords = splited[1];

			vector<string> splitedClusterWords = split(clusterWords, ',');
			vector<Term> list;
			for (int i = 0; i < splitedClusterWords.size(); i++)
			{
				vector<string> splitedClusterWord = split(splitedClusterWords[i], ' ');
				Term t;
				t.value = splitedClusterWord[0];
				t.weight = convertToDouble(splitedClusterWord[1]);

				list.push_back(t);
			}
			ClusterInQueryList.insert(std::make_pair<string , vector<Term> >(clusterName,list));	
		}
		finput.close();

	}

	void writeFile(string fileName)
	{
		ofstream of;
		of.open ( fileName.c_str(), std::ofstream::out ); 

		map< string , vector<Term> >::iterator it;
		for(it = ClusterInQueryList.begin() ; it != ClusterInQueryList.end() ; ++it)
		{
			string clusterMainTerm = it->first;
			vector<Term> clusterWords = it->second;

			string fullCluster = clusterMainTerm + ":";
			for (int i = 0; i < clusterWords.size(); i++)
			{
				fullCluster += clusterWords[i].value + "," ;
			}
			fullCluster = trimEnd(fullCluster,",");
			of << fullCluster << "\n" ;	
		}
		of.close(); 
	}

	void writeFileWithScore(string fileName)
	{
		ofstream of;
		of.open ( fileName.c_str(), std::ofstream::out ); 

		map< string , vector<Term> >::iterator it;
		for(it = ClusterInQueryList.begin() ; it != ClusterInQueryList.end() ; ++it)
		{
			string clusterMainTerm = it->first;
			vector<Term> clusterWords = it->second;

			string fullCluster = clusterMainTerm + ":";
			for (int i = 0; i < clusterWords.size(); i++)
			{
				fullCluster += clusterWords[i].value + " " + SSTR(clusterWords[i].weight) + "," ;
			}
			fullCluster = trimEnd(fullCluster,",");
			of << fullCluster << "\n" ;	
		}
		of.close(); 
	}

	bool IsTermInCluster(string cluster, string term , double *value)
	{
		map< string , vector<Term> >::iterator it = ClusterInQueryList.find(cluster);
		if(it == ClusterInQueryList.end() ) // cluster don't found
		{
			return false;
		}
		vector<Term> *clusterWords = &it->second;
		for (int i = 0; i < clusterWords->size(); i++)
		{
			if ( (*clusterWords)[i].value == term )
			{
				*value = (*clusterWords)[i].weight;
				return true;
			}
		}
		false;
	}

	bool IsTermInCluster(string cluster, string term)
	{
		map< string , vector<Term> >::iterator it = ClusterInQueryList.find(cluster);
		if(it == ClusterInQueryList.end() ) // cluster don't found
		{
			return false;
		}
		vector<Term> *clusterWords = &it->second;
		for (int i = 0; i < clusterWords->size(); i++)
		{
			if ( (*clusterWords)[i].value == term )
			{
				return true;
			}
		}
		return false;
	}

	void GetClusterNames(vector<string> *names)
	{
		map< string , vector<Term> >::iterator it;
		for(it = ClusterInQueryList.begin() ;
			it != ClusterInQueryList.end() ;
			++it)
		{
			string clusterName = it->first;
			names->push_back(clusterName);
		}
	}

	void GetAllClusterTerms(vector<string> *names)
	{
		std::set<string> terms;
		map< string , vector<Term> >::iterator it;
		for(it = ClusterInQueryList.begin() ;
			it != ClusterInQueryList.end() ;
			++it)
		{
			vector<Term> *termList = &it->second;
			for (int i = 0; i < termList->size(); i++)
			{
				terms.insert( (*termList)[i].value );
			}
		}
		std::vector<string> output(terms.size());
		std::copy(terms.begin(), terms.end(), output.begin());
		*names = output;
	}

	void GetClusterTerms(string cluster , vector<string> *names)
	{
		map< string , vector<Term> >::iterator it = ClusterInQueryList.find(cluster);
		if(	it != ClusterInQueryList.end() )
		{
			vector<Term> *termList = &it->second;
			for (int i = 0; i < termList->size(); i++)
			{
				names->push_back( (*termList)[i].value );
			}
		}
	} 

}ClustersInQuery;

typedef struct QueriesStemmedFile
{
	map< string , vector<string> > Queries; // key = query name , value = query terms

	void Init(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			vector<string> data = split(line , ' ' );
			std::string queryName = data[0];
			vector<string> queryTerms;
			for (int i = 1; i < data.size(); i++)
			{
				queryTerms.push_back(data[i]);
			}
			Queries.insert(std::make_pair(queryName,queryTerms) );
		}

		finput.close();
	}


}QueriesStemmedFile;

typedef struct QueriesClusters
{
	map< string , ClustersInQuery > ClusterListPerQuery; // key = query name , value = list of clusters

	void init(vector<string> *filesName)
	{
		init(filesName, false);
	}

	void initWithScore(vector<string> *filesName)
	{
		init(filesName, true);
	}

	void writeFile(string outputDir)
	{
		map< string , ClustersInQuery >::iterator it;
		for(it = ClusterListPerQuery.begin() ; it != ClusterListPerQuery.end() ; ++it)
		{
			string queryName = it->first;
			string fileName = outputDir + "/" + queryName + ".QueryClusters" ;
			it->second.writeFile(fileName);
		}
	}

	void writeFileWithScore(string outputDir)
	{
		map< string , ClustersInQuery >::iterator it;
		int j=1;
		cout << "Write Query Clusters Distribution Files \n" ; 
		for(it = ClusterListPerQuery.begin() ; it != ClusterListPerQuery.end() ; ++it)
		{
			cout << '\r' << "		Progress : " << j <<  '/' <<  ClusterListPerQuery.size() << ".     Progress Percent " <<  ((double)j/(double)ClusterListPerQuery.size())*100.0  <<  "%" << std::flush;
			string queryName = it->first;
			string fileName = outputDir + "/" + queryName + ".QueryClustersDistribution" ;
			it->second.writeFileWithScore(fileName);
			j++;
		}
		cout << '\n';
	}

	ClustersInQuery* GetClustersInQuery(string queryName)
	{
		map< string , ClustersInQuery >::iterator it = ClusterListPerQuery.find(queryName);
		if( it != ClusterListPerQuery.end())
		{
			return &(it->second);
		}
		cout << "Cannot find query " << queryName << "in QueriesClusters data structure \n";
		return NULL;
	}

	void GetAllClustersNames(vector<string> *names)
	{
		std::set<string> all_clustes_names;
		map< string , ClustersInQuery >::iterator it;
		for(it = ClusterListPerQuery.begin() ; it != ClusterListPerQuery.end() ; ++it)
		{
			vector<string> clusters_names;
			it->second.GetClusterNames(&clusters_names);
			for (int i = 0; i < clusters_names.size(); i++)
			{
				all_clustes_names.insert(clusters_names[i]);
			}
		}
		std::vector<string> output(all_clustes_names.size());
		std::copy(all_clustes_names.begin(), all_clustes_names.end(), output.begin());
		*names = output;
	}

	void GetAllClustersTerms(vector<string> *names)
	{
		std::set<string> all_clustes_Terms;
		map< string , ClustersInQuery >::iterator it;
		for(it = ClusterListPerQuery.begin() ; it != ClusterListPerQuery.end() ; ++it)
		{
			vector<string> clusters_Terms;
			it->second.GetAllClusterTerms(&clusters_Terms);
			for (int i = 0; i < clusters_Terms.size(); i++)
			{
				all_clustes_Terms.insert(clusters_Terms[i]);
			}
		}
		std::vector<string> output(all_clustes_Terms.size());
		std::copy(all_clustes_Terms.begin(), all_clustes_Terms.end(), output.begin());
		*names = output;
	}

	void Clear()
	{
		ClusterListPerQuery.clear();
	}

private:
	void init(vector<string> *filesName, bool WithScore)
	{
		FILE_LOG(logDEBUG) << "Read cluster Probability Files" ;
		for (int i = 0; i < filesName->size() ; i++)
		{
			string fullFileName = (*filesName)[i];
			ClustersInQuery clustersInQuery;
			if(WithScore)
			{
				clustersInQuery.initWithScore(fullFileName);
			}
			else
			{
				clustersInQuery.init(fullFileName);
			}


			string clusterFileName = getFileName( fullFileName);
			vector<string> splited = split(clusterFileName, '.');
			string clusterName = splited[0];
			ClusterListPerQuery.insert(std::make_pair< string , ClustersInQuery > (clusterName,clustersInQuery));
			cout << '\r' << "		Progress : " << i+1 <<  '/' <<  filesName->size() << ".     Progress Percent " << (int)(((double)(1+i)/(double)filesName->size())*100.0)  <<  "%" << std::flush;
		}
		cout << '\n';
		FILE_LOG(logDEBUG) << "Finish Read cluster Probability Files" ;
	}

}QueriesClusters;




typedef struct QueriesClustersFile
{
	map< string , vector<string> > ClusterListPerQuery; // key = query name , value = list of clusters

	void Init(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return ;
		}
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			vector<string> splited = split(line, ':');
			string QueryName = splited[0];
			string clusters = splited[1];
			vector<string> clustersList = split(clusters, ',');
			ClusterListPerQuery.insert(std::make_pair(QueryName,clustersList));	
		}
		finput.close();
	}

	void WriteFile(string fileName)
	{
		ofstream of;
		of.open ( fileName.c_str(), std::ofstream::out ); 

		map< string ,  vector<string> >::iterator it;
		for(it = ClusterListPerQuery.begin() ;
			it != ClusterListPerQuery.end() ;
			++it)
		{
			string QueryName = it->first;
			vector<string> listofclusters = it->second;
			string fullQueryLine = QueryName + ":" ;

			for (int i = 0; i < listofclusters.size(); i++)
			{
				string clusterName = listofclusters[i];
				fullQueryLine += clusterName + "," ;
			}
			fullQueryLine = trimEnd(fullQueryLine,",");
			of << fullQueryLine << "\n" ;	
		}
		of.close(); 
	}

	vector<string> GetQueryClusters(string queryName)
	{
		return ClusterListPerQuery.find(queryName)->second;
	}

	void Init(vector<string> *clustersFiles){

		std::cout << "Read First Query clusters Files " << std::endl;
		QueriesClusters queriesClusters;
		queriesClusters.init(clustersFiles);

		std::cout << "Build File : " << std::endl;
		int i = 0;
		// key = query name , value = list of clusters
		map< string , ClustersInQuery >::iterator ClusterListPerQuery_it;
		for(ClusterListPerQuery_it = queriesClusters.ClusterListPerQuery.begin() ; 
			ClusterListPerQuery_it != queriesClusters.ClusterListPerQuery.end() ;
			++ClusterListPerQuery_it)
		{
			string queryName = ClusterListPerQuery_it->first;
		
			vector<string> listofclusters;
			// key = cluster name , value = list of cluster words
			map< string , vector<Term> >::iterator ClusterInQueryList_it;
			for(ClusterInQueryList_it = ClusterListPerQuery_it->second.ClusterInQueryList.begin() ; 
				ClusterInQueryList_it != ClusterListPerQuery_it->second.ClusterInQueryList.end() ; 
				++ClusterInQueryList_it)
			{
				string clusterName = ClusterInQueryList_it->first;
				listofclusters.push_back(clusterName);
			}
			ClusterListPerQuery.insert(std::make_pair(queryName,listofclusters));
		}
	}

}QueriesClustersFile;


#pragma region General Data Structures



class ExtendedQuery : public FirstQueryExpension
{
public:
    void ClearExtended()
	{
		int length = ExpensionTerms.size();
		for (int i = 0; i < length; i++)
		{
			delete ExpensionTerms[i];
		}
		ExpensionTerms.clear();
	}
};

class ExtendedQueryForCBRM3 : public FirstQueryExpension
{
public:
	double ClustresExpensionTermsWeight;
	std::vector<Term*> ClustresExpensionTerms;

	void Clear()
	{
		FirstQueryExpension::Clear();
		int length = ClustresExpensionTerms.size();
		for (int i = 0; i < length; i++)
		{
			delete ClustresExpensionTerms[i];
		}
		ClustresExpensionTerms.clear();
	}

	void ClearClustresExtended()
	{
		int length = ClustresExpensionTerms.size();
		for (int i = 0; i < length; i++)
		{
			delete ClustresExpensionTerms[i];
		}
		ClustresExpensionTerms.clear();
	}
};

class WriteQueryExpensionHelper
{
public:
	static void writeOriginalQuery(ofstream *of,string queryNumber,vector<Term*> *OriginalQuery)
	{
		*of << "<query>\n";
		*of << "<number>" + queryNumber + "</number>\n";
		*of << "<text> #combine( "  + Term::convertTermToString(*OriginalQuery) + ") </text>\n";
		*of << "</query>\n";
	}

	static void writeExtendedQuery(ofstream *of,string queryNumber,ExtendedQuery *expansion)
	{
		std::string query = buildQuery(expansion);

		*of << "<query>\n";
		*of << "<number>" + queryNumber + "</number>\n";
		*of << "<text>" + query + "</text>\n";
		*of << "</query>\n";
	}

	static void writeExtendedQuery(ofstream *of,string queryNumber,ExtendedQueryForCBRM3 *expansion)
	{
		std::string query = buildQuery(expansion);

		*of << "<query>\n";
		*of << "<number>" + queryNumber + "</number>\n";
		*of << "<text>" + query + "</text>\n";
		*of << "</query>\n";
	}
	 
	static std::string buildQuery(FirstQueryExpension *terms)
	{
		std::string result;

		char originalQueryWeightStr[40];
		sprintf(originalQueryWeightStr,"%.32f",terms->OriginalQueryWeight);
		std::ostringstream originalQueryWeight;
		originalQueryWeight << originalQueryWeightStr;

		char termsWeightStr[40];
		sprintf(termsWeightStr,"%.32f",terms->ExpensionTermsWeight);
		std::ostringstream termsWeight;
		termsWeight << termsWeightStr;

		if(terms->OriginalQueryWeight != 0 && terms->ExpensionTermsWeight == 0)
		{
			// no expansion 
			result = "#combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + " )" ;
		}
		else
		{
			result = "#weight( " + 
			originalQueryWeight.str() + 
			" #combine( #combine( "  + 
			Term::convertTermToString(terms->OriginalQuery) + ") ) " + 
			termsWeight.str() + 
			" #weight(  " + 
			Term::convertTermToString(terms->ExpensionTerms,true,true) + " ) )";
		}

		return result;
	}

	static std::string buildQuery(ExtendedQueryForCBRM3 *terms)
	{
		std::string result;

		double originalQueryWeight = terms->OriginalQueryWeight;
		double expansionQueryWeight = terms->ClustresExpensionTermsWeight + terms->ExpensionTermsWeight;

		char originalQueryWeightStr[40];
		sprintf(originalQueryWeightStr,"%.32f",originalQueryWeight);
		std::ostringstream originalQueryWeightStream;
		originalQueryWeightStream << originalQueryWeightStr;


		char expansionQueryWeightStr[40];
		sprintf(expansionQueryWeightStr,"%.32f",expansionQueryWeight);
		std::ostringstream expansionQueryWeightStream;
		expansionQueryWeightStream << expansionQueryWeightStr;

		if(terms->ClustresExpensionTermsWeight == 0 && terms->ExpensionTermsWeight == 0)
		{
			// no expansion 
			result = "#combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + " )" ;
		}
		else if(terms->ClustresExpensionTermsWeight == 0 && terms->ExpensionTermsWeight != 0)
		{
			// only Rm1 expansion
			result = "#weight( " + 
				originalQueryWeightStream.str() + " #combine( #combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + ") ) " + 
				expansionQueryWeightStream.str()  + " #weight(  " + Term::convertTermToString(terms->ExpensionTerms,true,true) + " ) " + " )";
		}
		else if( terms->ClustresExpensionTermsWeight != 0 && terms->ExpensionTermsWeight == 0)
		{
			// only Cluster expansion
			result = "#weight( " + 
				originalQueryWeightStream.str() + " #combine( #combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + ") ) " + 
				expansionQueryWeightStream.str()  + " #weight(  " + Term::convertTermToString(terms->ClustresExpensionTerms,true,true) + " ) " + " )";

		}
		else
		{
			// Rm1 and Cluster expansion
			std::vector<Term*> margeTerms;
			// rm1 values
			int length = terms->ExpensionTerms.size();
			for (int i = 0; i < length; i++)
			{
				Term* newTerm = terms->ExpensionTerms[i]->Copy();
				newTerm->weight = ( terms->ExpensionTermsWeight / expansionQueryWeight) * newTerm->weight;
				margeTerms.push_back(newTerm);
			}

			length = terms->ClustresExpensionTerms.size();
			for (int i = 0; i < length; i++)
			{
				Term* t = terms->ClustresExpensionTerms[i];
				
				// Search if the Term is already exist
				Term* foundT = NULL;
				std::vector<Term*>::iterator it =  margeTerms.begin();
				for (it =  margeTerms.begin() ; it != margeTerms.end(); it++)
				{
					if((*it)->value == t->value)
					{
						foundT = *it;
						break;
					}
				}

				// if Term is already exist
				if(foundT != NULL)
				{
					foundT->weight += ( terms->ClustresExpensionTermsWeight / expansionQueryWeight) * t->weight;
				}
				else
				{
					Term* newTerm = t->Copy();
					newTerm->weight = ( terms->ClustresExpensionTermsWeight / expansionQueryWeight) * newTerm->weight;
					margeTerms.push_back(newTerm);
				}

			}

			result = "#weight( " + 
				originalQueryWeightStream.str() + " #combine( #combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + ") ) " + 
				expansionQueryWeightStream.str()  + " #weight(  " + Term::convertTermToString(margeTerms,true,true) + " ) " + " )";

			// delete all new Terms that we create
			length = margeTerms.size();
			for (int i = 0; i < length; i++)
			{
				delete margeTerms[i];
			}
		}



		return result;
	}
	
};


#pragma endregion


#endif







/*


class FileLine
{
public:
virtual FileLine* Copy() = 0;
};

class EvaluatFileLine : public FileLine
{
public:
string evalName;
string queryName;
double evalValue;

EvaluatFileLine* Copy()
{
EvaluatFileLine *newLine = new EvaluatFileLine();
newLine->evalName = evalName;
newLine->queryName = queryName;
newLine->evalValue = evalValue;
return newLine;
}
};

template <class T_FileLine , class T_QueryMergeResult >
class QueryMergeResult
{
public:
string queryName;
double originalQueryValue;

virtual void init(string name)
{
queryName = name;
originalQueryValue = 0;
}

virtual void init(vector<T_FileLine*>* resultFileLineVec,  string queryNme) = 0;
virtual void addToSum(T_QueryMergeResult* value) = 0;
virtual void normalization(int numberOfFile) = 0;
virtual T_QueryMergeResult* Copy() = 0;
virtual string toString() = 0;
};

class QueryResultRM1 : public QueryMergeResult<EvaluatFileLine,QueryResultRM1>
{
public:
double rm310QueryValue;
double rm3100QueryValue;
vector< std::pair<string,double> > result;

void init(string name = NULL)
{
queryName = name;
originalQueryValue = 0;
rm3100QueryValue = 0;
rm310QueryValue = 0;
}

void init(vector<EvaluatFileLine*> * resultFileLineVec,  string queryNme)
{
init(queryNme);
for( unsigned int i = 0 ; i< resultFileLineVec->size(); ++i)
{
EvaluatFileLine* resultFileLine = (EvaluatFileLine*)(*resultFileLineVec)[i];

if(resultFileLine->queryName.compare("all") == 0)
{
continue;
}

if(resultFileLine->queryName.compare(queryNme) == 0)
{
queryName = resultFileLine->queryName;
originalQueryValue = resultFileLine->evalValue;
continue;
}

if(resultFileLine->queryName.compare(queryNme+ "_rm3_10") == 0)
{
rm310QueryValue = resultFileLine->evalValue;
continue;
}

if(resultFileLine->queryName.compare(queryNme+ "_rm3_100") == 0)
{
rm3100QueryValue = resultFileLine->evalValue;
continue;
}

result.push_back(std::make_pair(resultFileLine->queryName, resultFileLine->evalValue ) );	
}

std::sort(result.begin(), result.end(), sort_string_double);
}

void addToSum(QueryResultRM1* queryMergeResultValue)
{
QueryResultRM1 *value = (QueryResultRM1*)queryMergeResultValue;
originalQueryValue += value->originalQueryValue;
rm3100QueryValue += value->rm3100QueryValue;
rm310QueryValue += value->rm310QueryValue;

vector< std::pair<string,double> > *vec = &value->result;
for(unsigned int j=0 ; j<vec->size() ; ++j)
{
result[j].second += (*vec)[j].second ;		
}
}

void normalization(int numberOfFile)
{
originalQueryValue /= numberOfFile;
rm3100QueryValue /= numberOfFile;
rm310QueryValue /= numberOfFile;

for(unsigned int j=0 ; j<result.size() ; ++j)
{
result[j].second /= numberOfFile;
}
}

vector<double>* getClustresValues()
{
vector<double> *data = new vector<double>();
for (unsigned int i = 0; i < result.size(); i++)
{
data->push_back(result[i].second);
}
return data;
}

QueryResultRM1* Copy()
{
QueryResultRM1* value = new QueryResultRM1();
value->queryName = queryName;
value->originalQueryValue = originalQueryValue;
value->rm310QueryValue = rm310QueryValue;
value->rm3100QueryValue = rm3100QueryValue;

for (unsigned int i = 0; i < result.size(); i++)
{
value->result.push_back(std::make_pair(result[i].first, result[i].second ) );
}

return value;
}

string toString()
{
return "Query-Result for Query - " + queryName + 
" : OriginalQueryValue=" + SSTR(originalQueryValue) + 
", rm310QueryValue=" + SSTR(rm310QueryValue) + 
", rm3100QueryValue=" + SSTR(rm3100QueryValue) +
", number of clusters =" + SSTR(result.size()) 
;
}

};

template <class T_FileLine , class T_QueryMergeResult >
class EvaluateFile
{
public:
vector<T_FileLine*> lines; // vector of all line in the file
std::map<string,vector<T_FileLine*>*> evalMapFile; // key= evaluation name , value= all the line with the same evaluation name;
std::map<string, T_QueryMergeResult*> evalInFile; // // key= evaluation name , value= all the data that connect to the evaluation is structure order;
std::set<string> queriesNames;
std::set<string> evaluationNames;

void init(string fileName)
{
ReadEvaluateFileToVector(fileName);
mapEval();
setQueryResult();
}

void Clear()
{
std::map<string,vector<EvaluatFileLine*>*>::iterator it;
for(it = evalMapFile.begin() ; it != evalMapFile.end() ; it ++)
{
it->second->clear();
delete it->second;
}
evalMapFile.clear();

for (unsigned int i = 0; i < lines.size(); i++)
{
delete lines[i];
}
lines.clear();
std::map<string,QueryResultRM1*>::iterator it2;
for(it2 = evalInFile.begin() ; it2 != evalInFile.end() ; it2 ++)
{
delete it2->second;
}
evalInFile.clear();

evaluationNames.clear();
queriesNames.clear();
}

// read evaluate file (file that generate in trac_eval)
void ReadEvaluateFileToVector(string fileName)
{
ifstream finput;
finput.open ( fileName.c_str() ); 
if (!finput.good())
{
std::cerr << "Can't open file: " << fileName << std::endl;
return ;
}

while  ( finput.good() )
{
string line;
getline (finput, line);
if(line.size() == 0 ) continue;

T_FileLine* evalline = new T_FileLine();

std::string qcopy( line);
std::istringstream oss (qcopy);

std::string eval;
oss >> eval;
if ( eval.empty()) {
return ;    
}

std::string queryName;
oss >> queryName;
if ( queryName.empty()) {
return ;    
}

std::string evalValueStr;
oss >> evalValueStr;
if ( evalValueStr.empty()) {
return ;    
}

double evalValue = atof(evalValueStr.c_str());

evalline->evalName = eval;
evalline->queryName = queryName;
evalline->evalValue = evalValue;

lines.push_back(evalline);

queriesNames.insert(queryName);
}

finput.close();
}

// return for each evaluation parameter (map, P@10..) vector that contain all queries
// evalFile - vector that contain whole file - contain all queries
void mapEval()
{
int len = lines.size();
for(int i=0 ; i < len ; ++i)
{
T_FileLine* fileLine = lines[i];
if(fileLine->queryName == "all")
{
continue;
}

string evalName = fileLine->evalName;
evaluationNames.insert(evalName);

std::map<string,vector<T_FileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
if( pos == evalMapFile.end()) // not found
{
vector<T_FileLine*>* newVector = new vector<T_FileLine*>();
newVector->push_back(fileLine);
evalMapFile.insert(std::make_pair<string,vector<T_FileLine*>*> (evalName, newVector));
}
else
{
vector<T_FileLine*>* vec = pos->second;
vec->push_back(fileLine);
}
}
}


void add(string evalName , QueryResultRM1* data)
{
evaluationNames.insert(evalName);

QueryResultRM1* newQueryResult= new QueryResultRM1();
newQueryResult->queryName = evalName;
newQueryResult->originalQueryValue = data->originalQueryValue;
newQueryResult->rm3100QueryValue = data->rm3100QueryValue;
newQueryResult->rm310QueryValue = data->rm310QueryValue;

vector< std::pair<string,double> > *result = &data->result;
for(unsigned int j=0 ; j<result->size() ; ++j)
{
newQueryResult->result.push_back(std::make_pair( SSTR( (j + 1) ).c_str() , (*result)[j].second ));
}
evalInFile.insert(std::make_pair<string,QueryResultRM1*> (evalName, newQueryResult));

}

void addAndSum(string evalName , QueryResultRM1* data)
{
std::map<string,QueryResultRM1*>::const_iterator  pos = evalInFile.find(evalName);
if( pos == evalInFile.end()) // not found
{
add( evalName ,  data);
}
else
{
QueryResultRM1* queryResultFromMap = pos->second;
queryResultFromMap->addToSum(data);
}
}

void addAndSum(EvaluateFile *evaluateResultFile)
{
typedef std::map<string,QueryResultRM1*>::iterator it_type;
for(it_type iterator = evaluateResultFile->evalInFile.begin(); iterator != evaluateResultFile->evalInFile.end(); iterator++) 
{
string evalName = iterator->first;
QueryResultRM1* evalValues = iterator->second;
addAndSum(evalName,evalValues);
}
}

QueryResultRM1* getEvaluateValuesInFile(string evalName)
{
std::map< string , QueryResultRM1* >::const_iterator  pos = evalInFile.find(evalName);
if( pos == evalInFile.end()) // not found
{
printf("Evaluate not found in map : %s" , evalName.c_str());
return NULL;
}
else
{
return pos->second;
}

}

double getEvaluateValuesInFile(string evalName,string queryName, bool *isOk)
{
*isOk = false;
std::map<string,vector<EvaluatFileLine*>*>::const_iterator  pos = evalMapFile.find(evalName);
if( pos == evalMapFile.end()) // not found
{
printf("Evaluate not found in map : %s" , evalName.c_str());

return -1;
}
else
{
vector<EvaluatFileLine*>* vec = pos->second;
int len = vec->size();
for(int i =0 ; i<len ; i++)
{
EvaluatFileLine* line = (*vec)[i];
if( line->queryName == queryName)
{
*isOk = true;
return line->evalValue;
}
}
// printf("Query not found in map : %s" , queryName.c_str());
return -1;
}

}

void normalizationAll(int numberOfFile)
{
typedef std::map<string,QueryResultRM1*>::iterator it_type;
for(it_type iterator = evalInFile.begin(); iterator != evalInFile.end(); iterator++) 
{
QueryResultRM1* queryResultFromMap = iterator->second;
queryResultFromMap->normalization(numberOfFile);
}
}

void setQueryResult()
{
typedef std::map<string,vector<T_FileLine*>*>::iterator it_type;
for(it_type iterator = evalMapFile.begin(); iterator != evalMapFile.end(); iterator++) 
{
vector<T_FileLine*> *resultFileLine = iterator->second;
T_FileLine* min = getMin(resultFileLine);
string queryName = min->queryName;
T_QueryMergeResult *evalRes = new T_QueryMergeResult();;
evalRes->init(resultFileLine,  queryName); 
evalInFile.insert( std::make_pair<string,T_QueryMergeResult*>(iterator->first,evalRes));
}
}

T_FileLine* getMin(vector<T_FileLine*> *resultFileLine)
{
vector<T_FileLine*>::iterator it;
T_FileLine* min = (*resultFileLine)[0];
for(it = resultFileLine->begin(); it != resultFileLine->end(); it++) 
{
if(min->queryName.size() > (*it)->queryName.size())
{
min = *it;
}
}
return min;
}

string getOriginalQuery(std::set<string> *queries)
{
string min = *(queries->begin());
std::set<string>::iterator it;
for(it = queries->begin(); it != queries->end(); it++) 
{
if(min.size() > it->size())
{
min = *it;
}
}
return min;
}

std::set<string>* getQueriesName()
{
return &queriesNames;
}

std::set<string>* getEvaluationNames()
{
return &evaluationNames;
}

vector<T_FileLine*>* getEvaluatResultFileLine()
{
return &lines;
}

std::set<string>* GetQueryNames(string fileName)
{
ifstream finput;
finput.open ( fileName.c_str() ); 
if (!finput.good())
{
std::cerr << "Can't open file: " << fileName << std::endl;
return NULL;
}

while  ( finput.good() )
{
string line;
getline (finput, line);
if(line.size() == 0 ) continue;

EvaluatFileLine* evalline = new EvaluatFileLine();

std::string qcopy( line);
std::istringstream oss (qcopy);

std::string eval;
oss >> eval;
if ( eval.empty()) {
return NULL;    
}

std::string queryName;
oss >> queryName;
if ( queryName.empty()) {
return NULL;    
}
evalline->queryName = queryName;
queriesNames.insert(queryName);
}
finput.close();
return &queriesNames;
}

};

class EvaluateFileRM1
{
public:

};*/