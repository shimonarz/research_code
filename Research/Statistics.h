
#ifndef __Statistics_H
#define __Statistics_H

#include "GeneralFunctions.h"

class StatisticsPairList
{
public:

	void init()
	{
		Sum = 0;
		Avg = 0;
	}

	void clear()
	{
		_statisticsPairList.clear();
		Sum = Avg = 0;
	}

	void add(string key, double value)
	{
		Sum += value;
		_statisticsPairList.insert(std::make_pair(key, value));
	}

	void calcAvg()
	{
		Avg = Sum / _statisticsPairList.size();
	}


	double getAvgNotIncludeKey(string key)
	{
		double value = getKeyValue(key);
		FILE_LOG(logDEBUG) << "Avg Not Include Key - " << key << ", value = " << value << " Sum = " <<  Sum << " , new Value = " <<  (Sum - value) / (_statisticsPairList.size() - 1);
		return (Sum - value) / (_statisticsPairList.size() - 1);
	}
	
	double getAvg()
	{
		return Avg;
	}

	double getKeyValue(string key)
	{
		return _statisticsPairList.find(key)->second;
	}

	vector<string> getKeyNames( )
	{
		vector<string> vec;
		typedef std::map<string ,double>::iterator it_type;
		for(it_type iterator = _statisticsPairList.begin(); 
					iterator != _statisticsPairList.end(); 
					iterator++) 
		{
			vec.push_back(iterator->first);
		}
		return vec;
	}

private:
	std::map<string ,double> _statisticsPairList;
	double Sum;
	double Avg;

};

class StatisticsData
{
public:

	void add(string groupName, string key , double value )
	{
		std::map< string , StatisticsPairList >::iterator  configIt = _statisticsData.find(groupName);
		if( configIt == _statisticsData.end() ) // config not found
		{
			StatisticsPairList statisticsPairList;
			statisticsPairList.init();
			statisticsPairList.add(key, value);
			_statisticsData.insert(std::make_pair<string,StatisticsPairList> (groupName, statisticsPairList));
		}
		else
		{
			StatisticsPairList* statisticsPairList = (&(configIt->second)) ;
			statisticsPairList->add(key, value);
		}
	}

	void calcAvg()
	{
		typedef std::map<string,StatisticsPairList>::iterator it_type;
		for(it_type iterator = _statisticsData.begin(); iterator != _statisticsData.end(); iterator++) {
			iterator->second.calcAvg();
		}
	}

	string getMaxAvgGroupNotIncludeKey(string key)
	{
		std::map<string,StatisticsPairList>::iterator statisticsDataIterator = _statisticsData.begin();

		double maxValue = statisticsDataIterator->second.getAvgNotIncludeKey(key);
		string maxGroup = statisticsDataIterator->first;

		for(statisticsDataIterator = _statisticsData.begin(); statisticsDataIterator != _statisticsData.end(); statisticsDataIterator++) 
		{
			double currentValue = statisticsDataIterator->second.getAvgNotIncludeKey(key);
			FILE_LOG(logDEBUG) << key << " " << statisticsDataIterator->first << " " <<  currentValue ;
			if(currentValue > maxValue )
			{
				maxValue = currentValue;
				maxGroup = statisticsDataIterator->first;
			}
		}
		FILE_LOG(logDEBUG) << " * * * Max " << key << " " << maxGroup << " " <<  maxValue ;
		return maxGroup;
	}

	double getKeyValuePerGroup(string maxGroup, string key)
	{
		std::map< string , StatisticsPairList >::iterator  statisticsDataIterator = _statisticsData.find(maxGroup);
		return statisticsDataIterator->second.getKeyValue(key);
	}

	double getMaxKeyValue(string key)
	{
		std::map< string , StatisticsPairList >::iterator  statisticsDataIterator = _statisticsData.begin();

		double maxValue = statisticsDataIterator->second.getKeyValue(key);
		string maxGroup = statisticsDataIterator->first;

		for(statisticsDataIterator = _statisticsData.begin(); statisticsDataIterator != _statisticsData.end(); statisticsDataIterator++) 
		{
			double currentValue = statisticsDataIterator->second.getKeyValue(key);
			FILE_LOG(logDEBUG) << key << " " << statisticsDataIterator->first << " " <<  currentValue ;
			if(currentValue > maxValue )
			{
				maxValue = currentValue;
				maxGroup = statisticsDataIterator->first;
			}
		}
		FILE_LOG(logDEBUG) << " * * * Max " << key << " " << maxGroup << " " <<  maxValue ;
		return maxValue;
	}

	vector<string> getKeys()
	{
		return _statisticsData.begin()->second.getKeyNames();
	}

	void getGroupMaxValue(double *maxValue , string *maxGroupName)
	{
		std::map< string , StatisticsPairList >::iterator  statisticsDataIterator = _statisticsData.begin();

		*maxValue = statisticsDataIterator->second.getAvg();
		*maxGroupName = statisticsDataIterator->first;

		for(statisticsDataIterator = _statisticsData.begin(); statisticsDataIterator != _statisticsData.end(); statisticsDataIterator++) 
		{
			double currentValue = statisticsDataIterator->second.getAvg();
			if(currentValue > *maxValue )
			{
				*maxValue = currentValue;
				*maxGroupName = statisticsDataIterator->first;
			}
		}
	}

private:
	map<string , StatisticsPairList> _statisticsData;

};

class StatisticsCalculator
{
public:

	StatisticsData _statisticsData;

	double CalculateCrossValidation()
	{
		vector<string> keys = _statisticsData.getKeys();
		double sum = 0;

		// Do cross validation for each key 
		int keyslength = keys.size();
		for (int j = 0; j < keyslength; j++)
		{
			string key = keys[j];
			
			// Train - calculate the maximum value of all Groups, the current key not included
			string maxGroup = _statisticsData.getMaxAvgGroupNotIncludeKey(key);

			// Test  - get the value for the key in the chosen Group
			double queryResConfig = _statisticsData.getKeyValuePerGroup(maxGroup , key);

			// sum the keys result
			sum += queryResConfig;
		}

		double cv = sum / keyslength;
		return cv;
	}

	double CalculateCrossValidationWithPrints(string fileName)
	{
		ofstream of;
		std::cout << "Create file " << fileName << std::endl;
		of.open ( fileName.c_str(), std::ofstream::out ); 


		vector<string> keys = _statisticsData.getKeys();
		double sum = 0;

		// Do cross validation for each key 
		int keyslength = keys.size();
		for (int j = 0; j < keyslength; j++)
		{
			string key = keys[j];
			
			// Train - calculate the maximum value of all Groups, the current key not included
			string maxGroup = _statisticsData.getMaxAvgGroupNotIncludeKey(key);
			FILE_LOG(logDEBUG) << " * * *Train -  maxGroup " << key << " " << maxGroup  ;

			// Test  - get the value for the key in the chosen Group
			double queryResConfig = _statisticsData.getKeyValuePerGroup(maxGroup , key);
			FILE_LOG(logDEBUG) << " * * *Test - " << key << " " << queryResConfig  ;

			of << key << " " << maxGroup  << " " << queryResConfig << std::endl;

			// sum the keys result
			sum += queryResConfig;
		}
		
		of.close();

		double cv = sum / keyslength;
		return cv;
	}

	double CalculateBestoneAvg()
	{
		/*vector<string> keys = _statisticsData.getKeys();
		double sum = 0;

		// Do bestoneAvg for each key
		int keyslength = keys.size();
		for (int j = 0; j < keyslength; j++)
		{
			string key = keys[j];
			
			// calculate the maximum value of all configuration for "Key"
			double maxKeyValue = _statisticsData.getMaxKeyValue(key);

			// sum the query result
			sum += maxKeyValue;
		}

		double bestoneAvg = sum / keyslength;
		return bestoneAvg;*/

		double bestoneAvg;
		string maxGroupName;
		_statisticsData.getGroupMaxValue(&bestoneAvg , &maxGroupName);
		return bestoneAvg;
	}
	
	double CalculateBestoneAvgWithPrints(string fileName)
	{
		ofstream of;
		std::cout << "Create file " << fileName << std::endl;
		of.open ( fileName.c_str(), std::ofstream::out ); 


		double bestoneAvg;
		string maxGroupName;
		_statisticsData.getGroupMaxValue(&bestoneAvg , &maxGroupName);

		of << maxGroupName << " " << bestoneAvg  <<  std::endl;

		of.close();

		return bestoneAvg;
	}

};

#endif
