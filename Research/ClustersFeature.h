#ifndef ClustersFeature_H
#define ClustersFeature_H

#include "research.h"
#include <math.h>

typedef struct IdfHelper
{

	map< string , double > ClusterScore; // key = cluster name , value = idf score for all cluster terms

	double GetClusterScore(string cluster)
	{
		return ClusterScore.find(cluster)->second;
	}

	void init(ClustersInQuery* ClustersTerms)
	{
		vector<string> clustersNames;
		ClustersTerms->GetClusterNames(&clustersNames);

		vector<string> termsNames;
		ClustersTerms->GetAllClusterTerms(&termsNames);

		map< string , double > TermsIdf;
		map< string , double > TermsOkapiIdf;
		double N = clustersNames.size();


		// insert all term to map and set is idf score to 0.
		for (int i = 0; i < termsNames.size(); i++)
		{
			TermsIdf.insert( std::make_pair(termsNames[i] , 0) );
		}

		// calculate idf for all terms
		for (int i = 0; i < clustersNames.size(); i++)
		{
			for (int j = 0; j < termsNames.size(); j++)
			{
				string cluster = clustersNames[i];
				string term = termsNames[j];
				if( ClustersTerms->IsTermInCluster(cluster, term ) == true)
				{
					TermsIdf.find(term)->second ++ ;
				}
			}
		}

		for (int i = 0; i < termsNames.size(); i++)
		{
			string term = termsNames[i];
			double n_w = TermsIdf.find( term )->second;
			double okapiIdf = log (  (N - n_w + 0.5) / (n_w + 0.5) );
			TermsOkapiIdf.insert( std::make_pair( term , okapiIdf) );
		}


		for (int i = 0; i < clustersNames.size(); i++)
		{
			string cluster = clustersNames[i];
			vector<string> clusterTerms;
			ClustersTerms->GetClusterTerms(cluster , &clusterTerms);
			double idfSum = 0;
			for (int j = 0; j < clusterTerms.size(); j++)
			{
				string term = clusterTerms[j];
				idfSum += TermsOkapiIdf.find(term)->second;
			}
			ClusterScore.insert( std::make_pair(cluster , idfSum) );
		}

		// normalize the idf to be distribution
		double sum = 0;
		map< string , double >::iterator it;
		for( it = ClusterScore.begin() ; it != ClusterScore.end() ; ++it)
		{
			sum += it->second;
		}
		for( it = ClusterScore.begin() ; it != ClusterScore.end() ; ++it)
		{
			it->second /= sum;
		}

	}

	void init(string clustersFile)
	{
		ClustersInQuery clustersInQuery;
		clustersInQuery.init(clustersFile);

		map< string , vector<Term> >::iterator it;
		for(it = clustersInQuery.ClusterInQueryList.begin() ;
			it != clustersInQuery.ClusterInQueryList.end() ;
			++it)
		{
			double idfSum = 0;
			string cluster = it->first;
			//std::cout << "Clusters : " << clusterName << std::endl;
			vector<Term> *termList = &it->second;
			for (int i = 0; i < termList->size(); i++)
			{
				Term* t = &(*termList)[i];
				double idf = t->weight;
				idfSum += idf;
			}
			ClusterScore.insert( std::make_pair(cluster , idfSum) );
		}

		// normalize the idf to be distribution
		double sum = 0;
		map< string , double >::iterator ClusterScore_it;
		for( ClusterScore_it = ClusterScore.begin() ; ClusterScore_it != ClusterScore.end() ; ++ClusterScore_it)
		{
			sum += ClusterScore_it->second;
		}
		for( ClusterScore_it = ClusterScore.begin() ; ClusterScore_it != ClusterScore.end() ; ++ClusterScore_it)
		{
			ClusterScore_it->second /= sum;
		}

	}

}IdfHelper;

// line in the Clusters Feature file
typedef struct ClustersFeatureInQuery
{
	
	map< string , double > ClusterListPerQuery; // key = Cluster name , value = p(c | q) 

	void Init(string lineFormFeaturefile)
	{
		
		vector<string> splitedClusterWords = split(lineFormFeaturefile, ',');
		vector<Term> list;
		for (int i = 0; i < splitedClusterWords.size(); i++)
		{
			vector<string> splitedClusterWord = split(splitedClusterWords[i], ' ');
			string clusterName =  splitedClusterWord[0];
			double Probability = convertToDouble(splitedClusterWord[1]);
			ClusterListPerQuery.insert(std::make_pair<string , double >(clusterName,Probability));	
		}
	}

	string GetLineToWiteInFile()
	{
		string line = "";
		map< string , double >::iterator ClusterListPerQuery_it;
		for(ClusterListPerQuery_it = ClusterListPerQuery.begin() ;
			ClusterListPerQuery_it != ClusterListPerQuery.end() ;
			++ClusterListPerQuery_it)
		{
			string clusterName = ClusterListPerQuery_it->first;
			double Probability = ClusterListPerQuery_it->second;
			line += clusterName + " " + SSTR(Probability) + "," ;
		}
		line = trimEnd(line,",");
		return line;
	}

	void Softmax_normalize()
	{
		map< string , double >::iterator it;  // key = Cluster name , value = p(c | q) 

		// Probability = e ^ x
		for(it = ClusterListPerQuery.begin() ; 
			it != ClusterListPerQuery.end() ;
			++it)
		{
			it->second = exp(it->second ) ;
		}
	
		// sum all e ^ x
		double sum = 0.0;
		for(it = ClusterListPerQuery.begin() ; 
			it != ClusterListPerQuery.end() ;
			++it)
		{
			sum += it->second;
		}
		// e ^ x / Sum(e ^ x)
		for(it = ClusterListPerQuery.begin() ; 
			it != ClusterListPerQuery.end() ;
			++it)
		{
			it->second = it->second / sum ;
		}
	}

	void normalize()
	{
		double sum = 0.0;
		map< string , double >::iterator it;  // key = Cluster name , value = p(c | q) 
		for(it = ClusterListPerQuery.begin() ; 
			it != ClusterListPerQuery.end() ;
			++it)
		{
			sum += it->second;
		}

		for(it = ClusterListPerQuery.begin() ; 
			it != ClusterListPerQuery.end() ;
			++it)
		{
			it->second = it->second / sum ;
		}
				
	}

	double GetClusterProbability(string clusterName)
	{
		map< string , double >::iterator it = ClusterListPerQuery.find(clusterName);
		if(it != ClusterListPerQuery.end())
		{
			return it->second ;
		}
		FILE_LOG(logERROR) << "ClustersFeatureInQuery:GetClusterProbability - Can find the cluster : " << clusterName  ;
		return  0 ;
	}

	void GetClusterNames(vector<string> *names)
	{
		map< string , double >::iterator it;
		for(it = ClusterListPerQuery.begin() ;
			it != ClusterListPerQuery.end() ;
			++it)
		{
			string ClusterName = it->first;
			names->push_back(ClusterName);
		}
	}

}ClustersFeatureInQuery;
//Clusters Feature file 
typedef struct QueriesClustersFeature
{
	double FeatureWeight;
	map< string , ClustersFeatureInQuery* > ClusterFeaturePerQuery; // key = Query name , value = Clusters Feature
	string FeatureName;

	QueriesClustersFeature()
	{
		FeatureWeight = 0.0;
	}

	void Init(string featureFileName , double featureWeight )
	{
		FeatureWeight = featureWeight;
		FeatureName = getFileName_Query(featureFileName);

		ifstream finput;
		finput.open ( featureFileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << featureFileName << std::endl;
			return ;
		}
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			vector<string> splited = split(line, ':');
			string QueryName = splited[0];
			string ClustersFeature = splited[1];
			ClustersFeatureInQuery* clustersFeatureInQuery = new ClustersFeatureInQuery();
			clustersFeatureInQuery->Init(ClustersFeature);		
			ClusterFeaturePerQuery.insert(std::make_pair<string , ClustersFeatureInQuery* >(QueryName,clustersFeatureInQuery));	
		}
		finput.close();
	}

	void WriteClustersFeatureToFile(string outputFeatureFileName)
	{
		FILE_LOG(logINFO) << "Write Clusters Feature To File : " << outputFeatureFileName; 
		ofstream of;
		of.open ( outputFeatureFileName.c_str(), std::ofstream::out ); 
		int j=1;
		map< string , ClustersFeatureInQuery* >::iterator ClusterFeaturePerQuery_it;
		for(ClusterFeaturePerQuery_it = ClusterFeaturePerQuery.begin() ;
			ClusterFeaturePerQuery_it != ClusterFeaturePerQuery.end() ;
			++ClusterFeaturePerQuery_it)
		{
			string QueryName = ClusterFeaturePerQuery_it->first;
			ClustersFeatureInQuery* clustersFeatureInQuery = ClusterFeaturePerQuery_it->second;
			string fullQueryLine = QueryName + ":" + clustersFeatureInQuery->GetLineToWiteInFile();
			of << fullQueryLine << "\n" ;	
			cout << '\r' << "		Progress : " << j <<  '/' <<  ClusterFeaturePerQuery.size() << ".     Progress Percent " <<  ((double)j/(double)ClusterFeaturePerQuery.size())*100.0  <<  "%" << std::flush;
			j++;
		}
		cout << '\n';
		of.close(); 
	}

	void GetQueryNames(vector<string> *names)
	{
		map< string , ClustersFeatureInQuery* >::iterator ClusterFeaturePerQuery_it;
		for(ClusterFeaturePerQuery_it = ClusterFeaturePerQuery.begin() ;
			ClusterFeaturePerQuery_it != ClusterFeaturePerQuery.end() ;
			++ClusterFeaturePerQuery_it)
		{
			string QueryName = ClusterFeaturePerQuery_it->first;
			names->push_back(QueryName);
		}
	}

	ClustersFeatureInQuery* GetClustersFeatureForQuery(string queryName)
	{
		return ClusterFeaturePerQuery.find(queryName)->second;
	}

	void Clear()
	{
		map< string , ClustersFeatureInQuery* >::iterator ClusterFeaturePerQuery_it;
		for(ClusterFeaturePerQuery_it = ClusterFeaturePerQuery.begin() ;
			ClusterFeaturePerQuery_it != ClusterFeaturePerQuery.end() ;
			++ClusterFeaturePerQuery_it)
		{
			delete ClusterFeaturePerQuery_it->second;
		}
		ClusterFeaturePerQuery.clear();
	}

}QueriesClustersFeature;

typedef struct QueriesClustersFeatures
{
	vector<QueriesClustersFeature> QueriesClusterFeatureList; 

	map< string , ClustersFeatureInQuery* > ClusterFeaturePerQuery; // key = Query name , value = Clusters Feature

	map< string , vector<string> > ClustersList;

	void AddFeature(string featureFileName , double featureWeight)
	{
		FILE_LOG(logINFO) << "QueriesClustersFeatures:AddFeature - Add Feature : " << featureFileName ;
		QueriesClustersFeature queriesClustersFeature;
		queriesClustersFeature.Init(featureFileName , featureWeight);
		QueriesClusterFeatureList.push_back(queriesClustersFeature);
	}

	void SumAllClustersFeatures()
	{
		FILE_LOG(logINFO) << "QueriesClustersFeatures:SumAllClustersFeatures - Sum All Clusters Features : " ;

		int clusterFeatureFilesNumber = QueriesClusterFeatureList.size();
		if(clusterFeatureFilesNumber == 0)
		{
			return;
		}
		vector<string> queryNames;
		QueriesClusterFeatureList[0].GetQueryNames(&queryNames);

		// loop on each query
		int queriesNumber = queryNames.size();
		for (int i = 0; i < queriesNumber; i++)
		{
			string queryName = queryNames[i];
			ClustersFeatureInQuery *QueryFinalFeature = new ClustersFeatureInQuery();
			
			vector<string> clusterNames;
			ClustersFeatureInQuery*  clustersFeatureInQuery = QueriesClusterFeatureList[0].GetClustersFeatureForQuery( queryName);
			clustersFeatureInQuery->GetClusterNames(&clusterNames);
			int clustersNumber = clusterNames.size();

			ClustersList.insert( std::make_pair(queryName , clusterNames) );

			// loop on each cluster Feature
			map< string , double > *ClusterListPerQuery = &QueryFinalFeature->ClusterListPerQuery; // key = Cluster name , value = p(c | q) 
			for (int j = 0; j < clusterFeatureFilesNumber; j++)
			{
				// loop on each cluster in query
				ClustersFeatureInQuery*  clustersFeatureInQuery = QueriesClusterFeatureList[j].GetClustersFeatureForQuery( queryName);
				double featureWeight = QueriesClusterFeatureList[j].FeatureWeight ;
				for (int k = 0; k < clustersNumber; k++)
				{
					string clusterName = clusterNames[k];
					double probability = featureWeight * clustersFeatureInQuery->GetClusterProbability(clusterName);
					map< string , double >::iterator it = ClusterListPerQuery->find(clusterName);
					if(it != ClusterListPerQuery->end() ){
						it->second += probability;
					}
					else{
						ClusterListPerQuery->insert( std::make_pair(clusterName , probability) );
					}
				}
			}
			QueryFinalFeature->normalize();
			ClusterFeaturePerQuery.insert( std::make_pair(queryName , QueryFinalFeature) );
			cout << '\r' << "		Progress : " << i+1 <<  '/' <<  queriesNumber << ".     Progress Percent " <<  (int)(((double)(i+1)/(double)queriesNumber)*100.0)  <<  "%" << std::flush;
		}
		cout << '\n';
	}

	double GetClusterProbability(string cluster , string query)
	{
		map< string , ClustersFeatureInQuery* >::iterator it = ClusterFeaturePerQuery.find(query);
		if( it != ClusterFeaturePerQuery.end() )
		{
			ClustersFeatureInQuery *feature = it->second;
			return feature->GetClusterProbability(cluster);
		}
		FILE_LOG(logERROR) << "QueriesClustersFeatures:GetClusterProbability - Can find the query : " << query  ;
		return -1;
	}

	

	
	void Clear()
	{
		map< string , ClustersFeatureInQuery* >::iterator it;
		for(it = ClusterFeaturePerQuery.begin() ; 
			it != ClusterFeaturePerQuery.end();
			++it )
		{
			delete it->second;
		}
		ClusterFeaturePerQuery.clear();
		ClustersList.clear();
	}

}QueriesClustersFeatures;

typedef struct ClustersFeatureCreator
{
	
	QueriesClusters queriesClusters;
	QueriesClustersFeature queriesClustersFeature;

	void Init(vector<string> *clustersFiles){

		FILE_LOG(logINFO) << "ClustersFeatureCreator:Init - Read First Query clusters Files " ;
		queriesClusters.init(clustersFiles);

		FILE_LOG(logINFO) << "ClustersFeatureCreator:Init - Build First Queries Clusters Feature Data Structure " ;
		int i = 0;
		// key = query name , value = list of clusters
		map< string , ClustersInQuery >::iterator ClusterListPerQuery_it;
		for(ClusterListPerQuery_it = queriesClusters.ClusterListPerQuery.begin() ; 
			ClusterListPerQuery_it != queriesClusters.ClusterListPerQuery.end() ;
			++ClusterListPerQuery_it)
		{
			string queryName = ClusterListPerQuery_it->first;
			ClustersFeatureInQuery* clustersFeatureInQuery = new ClustersFeatureInQuery();

			// key = cluster name , value = list of cluster words
			map< string , vector<Term> >::iterator ClusterInQueryList_it;
			for(ClusterInQueryList_it = ClusterListPerQuery_it->second.ClusterInQueryList.begin() ; 
				ClusterInQueryList_it != ClusterListPerQuery_it->second.ClusterInQueryList.end() ; 
				++ClusterInQueryList_it)
			{
				string clusterName = ClusterInQueryList_it->first;
				double Probability = 0.0;
				clustersFeatureInQuery->ClusterListPerQuery.insert(std::make_pair<string , double >(clusterName,Probability));	
			}

			queriesClustersFeature.ClusterFeaturePerQuery.insert(std::make_pair<string , ClustersFeatureInQuery* >(queryName,clustersFeatureInQuery));
			cout << '\r' << "		Progress : " << i+1 <<  '/' <<  clustersFiles->size() << ".     Progress Percent " <<  int(((double)(1+i)/(double)clustersFiles->size())*100.0)  <<  "%" << std::flush;
			i++;
		}
		cout << '\n';
	}



	void CreateUniformDistribution(string outputDir)
	{
		FILE_LOG(logINFO) << "ClustersFeatureCreator:CreateUniformDistribution - Create Clusters Feature - Uniform Distribution " ;
		map< string , ClustersFeatureInQuery* >::iterator ClusterFeaturePerQuery_it;
		for(ClusterFeaturePerQuery_it = queriesClustersFeature.ClusterFeaturePerQuery.begin() ;
			ClusterFeaturePerQuery_it != queriesClustersFeature.ClusterFeaturePerQuery.end() ;
			++ClusterFeaturePerQuery_it)
		{
			string QueryName = ClusterFeaturePerQuery_it->first;
			ClustersFeatureInQuery* clustersFeatureInQuery = ClusterFeaturePerQuery_it->second;
				
			double size = clustersFeatureInQuery->ClusterListPerQuery.size();
			map< string , double >::iterator ClusterListPerQuery_it;
			for(ClusterListPerQuery_it = clustersFeatureInQuery->ClusterListPerQuery.begin() ;
				ClusterListPerQuery_it != clustersFeatureInQuery->ClusterListPerQuery.end() ;
				++ClusterListPerQuery_it)
			{
				string clusterName = ClusterListPerQuery_it->first;
				double Probability = 1.0 /  size ;
				ClusterListPerQuery_it->second = Probability;
			}
		}
		string fileName = outputDir + "/UniformClustersFeature.txt" ;
		queriesClustersFeature.WriteClustersFeatureToFile(fileName);
		
	}

	void CreateW2VQueryClusterSimilarityDistribution(string outputDir, vector<string> w2vModels , string queriesStemmedFileName)
	{
		FILE_LOG(logINFO) << "Create Clusters Feature - W2VQueryClusterSimilarity Distribution " ;
		FILE_LOG(logINFO) << "Read Queries File " ;
		QueriesStemmedFile queriesStemmedFile ;
		queriesStemmedFile.Init(queriesStemmedFileName);

		WordToVecModel wordToVecModel;

		#pragma region if there is one w2v model for all queries
		bool isOnlyOneW2vModel = (w2vModels.size() == 1);
		if(isOnlyOneW2vModel)
		{
			FILE_LOG(logINFO) << "Read Word-To-Vec Model File " ;
			if( ! wordToVecModel.LoadWordToVecModel(w2vModels[0]))
			{
				FILE_LOG(logERROR) << "Fail to load word 2 vec model :  " << w2vModels[0] ;
				return;
			}
		}
		#pragma endregion

		#pragma region loop on each query
		map< string , vector<string> >::iterator  Queries_it; // key = query name , value = query terms
		for(Queries_it = queriesStemmedFile.Queries.begin() ;
			Queries_it != queriesStemmedFile.Queries.end();
			++Queries_it			)
		{ 
			string queryName = Queries_it->first; //queryName == "361"
			vector<string> *OriginalQuery = &Queries_it->second;
			FILE_LOG(logINFO) << "calculate similarity for query :  " << queryName  ;
			// read word to vec model
			#pragma region Read Word-To-Vec Model File
			if(! isOnlyOneW2vModel)
			{
				string w2vFileName = getW2vModelName(&w2vModels, queryName );
				FILE_LOG(logINFO) << "Read Word-To-Vec Model File " ;
				if( w2vFileName != "" )
				{
					if( ! wordToVecModel.LoadWordToVecModel(w2vFileName))
					{
						FILE_LOG(logERROR) << "Fail to load word 2 vec model :  " << w2vFileName ;
						continue;
					}
				}
				else
				{
					FILE_LOG(logERROR) << "Can find w2v model for query " << queryName ;
					continue;
				}
			}
			#pragma endregion

			#pragma region calculate query Centroid 
			vector<string> queryTerms;
			int queryTermsSize = OriginalQuery->size();
			for (int i = 0; i < queryTermsSize ; i++)
			{
				string queryTerm = (*OriginalQuery)[i];
				queryTerms.push_back(queryTerm);
			}
			float *queryCentroid =  wordToVecModel.GetCentroid(queryTerms);

			#pragma region loop on each cluster
			// Get query Clusters Feature 
			map< string , ClustersFeatureInQuery* >::iterator ClusterPerQuery_it = queriesClustersFeature.ClusterFeaturePerQuery.find(queryName);
			if(ClusterPerQuery_it == queriesClustersFeature.ClusterFeaturePerQuery.end())
			{
				FILE_LOG(logERROR) << "Cannot find query " << queryName << " in Queries Clusters Feature files" ;
				delete queryCentroid;
				continue;
			}

			// Get query Clusters Terms
			map< string , ClustersInQuery >::iterator ClusterInQueryList_it = queriesClusters.ClusterListPerQuery.find(queryName);
			if(ClusterInQueryList_it == queriesClusters.ClusterListPerQuery.end())
			{
				FILE_LOG(logERROR) << "Cannot find query " << queryName << " in Queries Clusters files" ;
				delete queryCentroid;
				continue;
			}
			
			// loop on each cluster
			ClustersInQuery *clustersInQuery = &ClusterInQueryList_it->second;
			map< string , vector<Term> >::iterator ClusterListPerQuery_it;
			for(ClusterListPerQuery_it = clustersInQuery->ClusterInQueryList.begin() ;
				ClusterListPerQuery_it != clustersInQuery->ClusterInQueryList.end() ;
				++ClusterListPerQuery_it)
			{
				// key = cluster name , value = list of cluster words
				string clusterName = ClusterListPerQuery_it->first; 
				vector<Term> *clusterWords = &ClusterListPerQuery_it->second;
				int clusterSize = clusterWords->size();
				// calculate cluster Centroid 
				vector<string> clusterTerms;
				for (int i = 0; i < clusterSize ; i++)
				{
					string term = (*clusterWords)[i].value;
					clusterTerms.push_back(term);
				}
				float *clusterCentroid =  wordToVecModel.GetCentroid(clusterTerms);
				// calculate Query Cluster Similarity -> p( C |q)
				double qeryClusterSimilarity = wordToVecModel.CalculateSimilarity(queryCentroid,clusterCentroid);

				delete clusterCentroid;

				map< string , double >::iterator it;  // key = Cluster name , value = p(c | q) 
				it = ClusterPerQuery_it->second->ClusterListPerQuery.find(clusterName);
				if(it !=  ClusterPerQuery_it->second->ClusterListPerQuery.end())
				{
					//FILE_LOG(logINFO) << "Centroid Similarity for query :  " << queryName  << " and cluster " << clusterName <<  " is : " << qeryClusterSimilarity;
					it->second = qeryClusterSimilarity;
				}
				else
				{
					FILE_LOG(logERROR) << "Cannot find Cluster " << clusterName << " in Queries Clusters files" ;
				}
			}

			delete queryCentroid;
			// normalize
			ClusterPerQuery_it->second->Softmax_normalize();

			#pragma endregion

			#pragma endregion

			#pragma region free Word-To-Vec Model File
			if(! isOnlyOneW2vModel)
			{
				wordToVecModel.Clear();
			}
			#pragma endregion


		}
		#pragma endregion	

		#pragma region free Word-To-Vec Model File
		if(isOnlyOneW2vModel)
		{
			wordToVecModel.Clear();
		}
		#pragma endregion

		string fileName = outputDir + "/W2VQueryClusterSimilarityClustersFeature.txt" ;
		queriesClustersFeature.WriteClustersFeatureToFile(fileName);

		
	}

	void CreateSingleLinkSimilarityDistribution(string outputDir, vector<string> w2vModels , string queriesStemmedFileName)
	{
		FILE_LOG(logINFO) << "Create Clusters Feature - Single Link Similarity Distribution ";
		FILE_LOG(logINFO) << "Read Queries File ";
		QueriesStemmedFile queriesStemmedFile ;
		queriesStemmedFile.Init(queriesStemmedFileName);

		WordToVecModel wordToVecModel;

		#pragma region if there is one w2v model for all queries
		bool isOnlyOneW2vModel = (w2vModels.size() == 1);
		if(isOnlyOneW2vModel)
		{
			FILE_LOG(logINFO) << "Read Word-To-Vec Model File " ;
			if( ! wordToVecModel.LoadWordToVecModel(w2vModels[0]))
			{
				FILE_LOG(logERROR) << "Fail to load word 2 vec model :  " << w2vModels[0] ;
				return;
			}
		}
		#pragma endregion

		#pragma region loop on each query
		map< string , vector<string> >::iterator  Queries_it; // key = query name , value = query terms
		for(Queries_it = queriesStemmedFile.Queries.begin() ;
			Queries_it != queriesStemmedFile.Queries.end();
			++Queries_it			)
		{ 
			string queryName = Queries_it->first; //queryName == "361"
			vector<string> *OriginalQuery = &Queries_it->second;

			FILE_LOG(logINFO) << "calculate similarity for query :  " << queryName  ;

			// read word to vec model
			#pragma region Read Word-To-Vec Model File
			if(! isOnlyOneW2vModel)
			{
				string w2vFileName = getW2vModelName(&w2vModels, queryName );
				FILE_LOG(logINFO) << "Read Word-To-Vec Model File " ;
				if( w2vFileName != "" )
				{
					if( ! wordToVecModel.LoadWordToVecModel(w2vFileName))
					{
						FILE_LOG(logERROR) << "Fail to load word 2 vec model :  " << w2vFileName ;
						continue;
					}
				}
				else
				{
					FILE_LOG(logERROR) << "Can find w2v model for query " << queryName ;
					continue;
				}
			}
			#pragma endregion

			#pragma region calculate query Centroid 
			vector<string> queryTerms;
			int queryTermsSize = OriginalQuery->size();
			for (int i = 0; i < queryTermsSize ; i++)
			{
				string queryTerm = (*OriginalQuery)[i];
				queryTerms.push_back(queryTerm);
			}
			
			#pragma region loop on each cluster
			// Get query Clusters Feature 
			map< string , ClustersFeatureInQuery* >::iterator ClusterPerQuery_it = queriesClustersFeature.ClusterFeaturePerQuery.find(queryName);
			if(ClusterPerQuery_it == queriesClustersFeature.ClusterFeaturePerQuery.end())
			{
				FILE_LOG(logERROR) << "Cannot find query " << queryName << " in Queries Clusters Feature files" ;
				continue;
			}

			// Get query Clusters Terms
			map< string , ClustersInQuery >::iterator ClusterInQueryList_it = queriesClusters.ClusterListPerQuery.find(queryName);
			if(ClusterInQueryList_it == queriesClusters.ClusterListPerQuery.end())
			{
				FILE_LOG(logERROR) << "Cannot find query " << queryName << " in Queries Clusters files" ;
				continue;
			}
			
			// loop on each cluster
			ClustersInQuery *clustersInQuery = &ClusterInQueryList_it->second;
			map< string , vector<Term> >::iterator ClusterListPerQuery_it;
			for(ClusterListPerQuery_it = clustersInQuery->ClusterInQueryList.begin() ;
				ClusterListPerQuery_it != clustersInQuery->ClusterInQueryList.end() ;
				++ClusterListPerQuery_it)
			{
				// key = cluster name , value = list of cluster words
				string clusterName = ClusterListPerQuery_it->first; 
				vector<Term> *clusterWords = &ClusterListPerQuery_it->second;
				int clusterSize = clusterWords->size();

				double maxSimilarity = -1.0;
				// calculate similarity between all cluster term and all  query term
				for (int i = 0; i < queryTerms.size() ; i++)
				{
					string queryTerm = queryTerms[i];
					for (int i = 0; i < clusterSize ; i++)
					{
						string clusterTerm = (*clusterWords)[i].value;
						double qeryClusterSimilarity = wordToVecModel.CalculateSimilarity(queryTerm,clusterTerm);
						if(maxSimilarity < qeryClusterSimilarity )
						{
							maxSimilarity = qeryClusterSimilarity;
						}
					}
				}
				
				map< string , double >::iterator it;  // key = Cluster name , value = p(c | q) 
				it = ClusterPerQuery_it->second->ClusterListPerQuery.find(clusterName);
				if(it !=  ClusterPerQuery_it->second->ClusterListPerQuery.end())
				{
					//FILE_LOG(logINFO) << "Max Similarity similarity for query :  " << queryName  << " and cluster " << clusterName <<  " is : " << maxSimilarity;
					it->second = maxSimilarity;
				}
				else
				{
					FILE_LOG(logERROR) << "Cannot find Cluster " << clusterName << " in Queries Clusters files" ;
				}
			}

			// normalize
			ClusterPerQuery_it->second->Softmax_normalize();

			#pragma endregion

			#pragma endregion

			
			#pragma region free Word-To-Vec Model File
			if(! isOnlyOneW2vModel)
			{
				wordToVecModel.Clear();
			}
			#pragma endregion

		}
		#pragma endregion	

		#pragma region free Word-To-Vec Model File
		if(isOnlyOneW2vModel)
		{
			wordToVecModel.Clear();
		}

		string fileName = outputDir + "/SingleLinkSimilarityDistribution.txt" ;
		queriesClustersFeature.WriteClustersFeatureToFile(fileName);
	}

	void CreateCompleteLinkSimilarityDistribution(string outputDir, vector<string> w2vModels , string queriesStemmedFileName)
	{
		FILE_LOG(logINFO) << "Create Clusters Feature - Complete Link Similarity Distribution ";
		FILE_LOG(logINFO) << "Read Queries File ";
		QueriesStemmedFile queriesStemmedFile ;
		queriesStemmedFile.Init(queriesStemmedFileName);

		WordToVecModel wordToVecModel;

		#pragma region if there is one w2v model for all queries
		bool isOnlyOneW2vModel = (w2vModels.size() == 1);
		if(isOnlyOneW2vModel)
		{
			FILE_LOG(logINFO) << "Read Word-To-Vec Model File " ;
			if( ! wordToVecModel.LoadWordToVecModel(w2vModels[0]))
			{
				FILE_LOG(logERROR) << "Fail to load word 2 vec model :  " << w2vModels[0] ;
				return;
			}
		}
		#pragma endregion

		#pragma region loop on each query
		map< string , vector<string> >::iterator  Queries_it; // key = query name , value = query terms
		for(Queries_it = queriesStemmedFile.Queries.begin() ;
			Queries_it != queriesStemmedFile.Queries.end();
			++Queries_it			)
		{ 
			string queryName = Queries_it->first; //queryName == "361"
			vector<string> *OriginalQuery = &Queries_it->second;

			FILE_LOG(logINFO) << "calculate similarity for query :  " << queryName  ;

			// read word to vec model
			#pragma region Read Word-To-Vec Model File
			if(! isOnlyOneW2vModel)
			{
				string w2vFileName = getW2vModelName(&w2vModels, queryName );
				FILE_LOG(logINFO) << "Read Word-To-Vec Model File " ;
				if( w2vFileName != "" )
				{
					if( ! wordToVecModel.LoadWordToVecModel(w2vFileName))
					{
						FILE_LOG(logERROR) << "Fail to load word 2 vec model :  " << w2vFileName ;
						continue;
					}
				}
				else
				{
					FILE_LOG(logERROR) << "Can find w2v model for query " << queryName ;
					continue;
				}
			}
			#pragma endregion

			#pragma region calculate query Centroid 
			vector<string> queryTerms;
			int queryTermsSize = OriginalQuery->size();
			for (int i = 0; i < queryTermsSize ; i++)
			{
				string queryTerm = (*OriginalQuery)[i];
				queryTerms.push_back(queryTerm);
			}
			
			#pragma region loop on each cluster
			// Get query Clusters Feature 
			map< string , ClustersFeatureInQuery* >::iterator ClusterPerQuery_it = queriesClustersFeature.ClusterFeaturePerQuery.find(queryName);
			if(ClusterPerQuery_it == queriesClustersFeature.ClusterFeaturePerQuery.end())
			{
				FILE_LOG(logERROR) << "Cannot find query " << queryName << " in Queries Clusters Feature files" ;
				continue;
			}

			// Get query Clusters Terms
			map< string , ClustersInQuery >::iterator ClusterInQueryList_it = queriesClusters.ClusterListPerQuery.find(queryName);
			if(ClusterInQueryList_it == queriesClusters.ClusterListPerQuery.end())
			{
				FILE_LOG(logERROR) << "Cannot find query " << queryName << " in Queries Clusters files" ;
				continue;
			}
			
			// loop on each cluster
			ClustersInQuery *clustersInQuery = &ClusterInQueryList_it->second;
			map< string , vector<Term> >::iterator ClusterListPerQuery_it;
			for(ClusterListPerQuery_it = clustersInQuery->ClusterInQueryList.begin() ;
				ClusterListPerQuery_it != clustersInQuery->ClusterInQueryList.end() ;
				++ClusterListPerQuery_it)
			{
				// key = cluster name , value = list of cluster words
				string clusterName = ClusterListPerQuery_it->first; 
				vector<Term> *clusterWords = &ClusterListPerQuery_it->second;
				int clusterSize = clusterWords->size();

				double minSimilarity = 1.0;
				// calculate similarity between all cluster term and all  query term
				for (int i = 0; i < queryTerms.size() ; i++)
				{
					string queryTerm = queryTerms[i];
					for (int i = 0; i < clusterSize ; i++)
					{
						string clusterTerm = (*clusterWords)[i].value;
						double qeryClusterSimilarity = wordToVecModel.CalculateSimilarity(queryTerm,clusterTerm);
						if(minSimilarity > qeryClusterSimilarity )
						{
							minSimilarity = qeryClusterSimilarity;
						}
					}
				}
				
				map< string , double >::iterator it;  // key = Cluster name , value = p(c | q) 
				it = ClusterPerQuery_it->second->ClusterListPerQuery.find(clusterName);
				if(it !=  ClusterPerQuery_it->second->ClusterListPerQuery.end())
				{
					//FILE_LOG(logINFO) << "Min Similarity similarity for query :  " << queryName  << " and cluster " << clusterName <<  " is : " << minSimilarity;
					it->second = minSimilarity;
				}
				else
				{
					FILE_LOG(logERROR) << "Cannot find Cluster " << clusterName << " in Queries Clusters files" ;
				}
			}

			// normalize
			ClusterPerQuery_it->second->Softmax_normalize();

			#pragma endregion

			#pragma endregion

			
			#pragma region free Word-To-Vec Model File
			if(! isOnlyOneW2vModel)
			{
				wordToVecModel.Clear();
			}
			#pragma endregion

		}
		#pragma endregion	

		#pragma region free Word-To-Vec Model File
		if(isOnlyOneW2vModel)
		{
			wordToVecModel.Clear();
		}

		string fileName = outputDir + "/CompleteLinkSimilarityDistribution.txt" ;
		queriesClustersFeature.WriteClustersFeatureToFile(fileName);

	}

	void CreateAverageLinkSimilarityDistribution(string outputDir, vector<string> w2vModels , string queriesStemmedFileName)
	{
		FILE_LOG(logINFO) << "Create Clusters Feature - Average Link Similarity Distribution ";
		FILE_LOG(logINFO) << "Read Queries File ";
		QueriesStemmedFile queriesStemmedFile ;
		queriesStemmedFile.Init(queriesStemmedFileName);

		WordToVecModel wordToVecModel;

		#pragma region if there is one w2v model for all queries
		bool isOnlyOneW2vModel = (w2vModels.size() == 1);
		if(isOnlyOneW2vModel)
		{
			FILE_LOG(logINFO) << "Read Word-To-Vec Model File " ;
			if( ! wordToVecModel.LoadWordToVecModel(w2vModels[0]))
			{
				FILE_LOG(logERROR) << "Fail to load word 2 vec model :  " << w2vModels[0] ;
				return;
			}
		}
		#pragma endregion

		#pragma region loop on each query
		map< string , vector<string> >::iterator  Queries_it; // key = query name , value = query terms
		for(Queries_it = queriesStemmedFile.Queries.begin() ;
			Queries_it != queriesStemmedFile.Queries.end();
			++Queries_it			)
		{ 
			string queryName = Queries_it->first; //queryName == "361"
			vector<string> *OriginalQuery = &Queries_it->second;

			FILE_LOG(logINFO) << "calculate similarity for query :  " << queryName  ;

			// read word to vec model
			#pragma region Read Word-To-Vec Model File
			if(! isOnlyOneW2vModel)
			{
				string w2vFileName = getW2vModelName(&w2vModels, queryName );
				FILE_LOG(logINFO) << "Read Word-To-Vec Model File " ;
				if( w2vFileName != "" )
				{
					if( ! wordToVecModel.LoadWordToVecModel(w2vFileName))
					{
						FILE_LOG(logERROR) << "Fail to load word 2 vec model :  " << w2vFileName ;
						continue;
					}
				}
				else
				{
					FILE_LOG(logERROR) << "Can find w2v model for query " << queryName ;
					continue;
				}
			}
			#pragma endregion

			#pragma region calculate query Centroid 
			vector<string> queryTerms;
			int queryTermsSize = OriginalQuery->size();
			for (int i = 0; i < queryTermsSize ; i++)
			{
				string queryTerm = (*OriginalQuery)[i];
				queryTerms.push_back(queryTerm);
			}
			
			#pragma region loop on each cluster
			// Get query Clusters Feature 
			map< string , ClustersFeatureInQuery* >::iterator ClusterPerQuery_it = queriesClustersFeature.ClusterFeaturePerQuery.find(queryName);
			if(ClusterPerQuery_it == queriesClustersFeature.ClusterFeaturePerQuery.end())
			{
				FILE_LOG(logERROR) << "Cannot find query " << queryName << " in Queries Clusters Feature files" ;
				continue;
			}

			// Get query Clusters Terms
			map< string , ClustersInQuery >::iterator ClusterInQueryList_it = queriesClusters.ClusterListPerQuery.find(queryName);
			if(ClusterInQueryList_it == queriesClusters.ClusterListPerQuery.end())
			{
				FILE_LOG(logERROR) << "Cannot find query " << queryName << " in Queries Clusters files" ;
				continue;
			}
			
			// loop on each cluster
			ClustersInQuery *clustersInQuery = &ClusterInQueryList_it->second;
			map< string , vector<Term> >::iterator ClusterListPerQuery_it;
			for(ClusterListPerQuery_it = clustersInQuery->ClusterInQueryList.begin() ;
				ClusterListPerQuery_it != clustersInQuery->ClusterInQueryList.end() ;
				++ClusterListPerQuery_it)
			{
				// key = cluster name , value = list of cluster words
				string clusterName = ClusterListPerQuery_it->first; 
				vector<Term> *clusterWords = &ClusterListPerQuery_it->second;
				int clusterSize = clusterWords->size();

				double sumSimilarity = 0;
				// calculate similarity between all cluster term and all  query term
				int queryTermsSize = queryTerms.size();
				for (int i = 0; i < queryTermsSize ; i++)
				{
					string queryTerm = queryTerms[i];
					for (int i = 0; i < clusterSize ; i++)
					{
						string clusterTerm = (*clusterWords)[i].value;
						double qeryClusterSimilarity = wordToVecModel.CalculateSimilarity(queryTerm,clusterTerm);
						sumSimilarity += qeryClusterSimilarity;
					}
				}
				
				double similarity = (sumSimilarity) / (clusterSize * queryTermsSize) ;
				map< string , double >::iterator it;  // key = Cluster name , value = p(c | q) 
				it = ClusterPerQuery_it->second->ClusterListPerQuery.find(clusterName);
				if(it !=  ClusterPerQuery_it->second->ClusterListPerQuery.end())
				{
				//	FILE_LOG(logINFO) << "Average Similarity for query :  " << queryName  << " and cluster " << clusterName <<  " is : " << similarity;
					it->second = similarity;
				}
				else
				{
					FILE_LOG(logERROR) << "Cannot find Cluster " << clusterName << " in Queries Clusters files" ;
				}
			}

			// normalize
			ClusterPerQuery_it->second->Softmax_normalize();

			#pragma endregion

			#pragma endregion

			
			#pragma region free Word-To-Vec Model File
			if(! isOnlyOneW2vModel)
			{
				wordToVecModel.Clear();
			}
			#pragma endregion

		}
		#pragma endregion	

		#pragma region free Word-To-Vec Model File
		if(isOnlyOneW2vModel)
		{
			wordToVecModel.Clear();
		}

		string fileName = outputDir + "/AverageLinkSimilarityDistribution.txt" ;
		queriesClustersFeature.WriteClustersFeatureToFile(fileName);
	}

	void CreateICFDistribution(string outputDir)
	{
		FILE_LOG(logINFO) << "Create Clusters Feature - ICF Distribution " ;
		int i = 0;
		map< string , ClustersFeatureInQuery* >::iterator ClusterFeaturePerQuery_it;
		for(ClusterFeaturePerQuery_it = queriesClustersFeature.ClusterFeaturePerQuery.begin() ;
			ClusterFeaturePerQuery_it != queriesClustersFeature.ClusterFeaturePerQuery.end() ;
			++ClusterFeaturePerQuery_it)
		{
			string QueryName = ClusterFeaturePerQuery_it->first;

			cout << '\r' << "Progress Query : " << QueryName << " - " << i+1 <<  '/' <<  queriesClustersFeature.ClusterFeaturePerQuery.size() << ".     Progress Percent " <<  int(((double)(1+i)/(double)queriesClustersFeature.ClusterFeaturePerQuery.size())*100.0)  <<  "%" << std::flush;
			i++;

			IdfHelper idfHelper;
			ClustersInQuery* clustersInQuery = queriesClusters.GetClustersInQuery(QueryName);
			idfHelper.init(clustersInQuery);

			ClustersFeatureInQuery* clustersFeatureInQuery = ClusterFeaturePerQuery_it->second;
			double size = clustersFeatureInQuery->ClusterListPerQuery.size();
			map< string , double >::iterator ClusterListPerQuery_it;
			for(ClusterListPerQuery_it = clustersFeatureInQuery->ClusterListPerQuery.begin() ;
				ClusterListPerQuery_it != clustersFeatureInQuery->ClusterListPerQuery.end() ;
				++ClusterListPerQuery_it)
			{
				string clusterName = ClusterListPerQuery_it->first;
				double Probability = idfHelper.GetClusterScore(clusterName);
				ClusterListPerQuery_it->second = Probability;
			}
		}
		string fileName = outputDir + "/ICFClustersFeature.txt" ;
		queriesClustersFeature.WriteClustersFeatureToFile(fileName);
		
	}

	void CreateIDFDistribution(string outputDir , vector<string> idfScoreFile)
	{
		FILE_LOG(logINFO) << "Create Clusters Feature - IDF Distribution " ;

		std::map<string,string> fileByQuery;
		FILE_LOG(logINFO) << "ClustersFeatureCreator:CreateIDFDistribution - Order files by query name, there is : " << idfScoreFile.size() << " files";
		orderFileByConfig( &idfScoreFile, &fileByQuery );

		int i = 0;
		map< string , ClustersFeatureInQuery* >::iterator ClusterFeaturePerQuery_it;
		for(ClusterFeaturePerQuery_it = queriesClustersFeature.ClusterFeaturePerQuery.begin() ;
			ClusterFeaturePerQuery_it != queriesClustersFeature.ClusterFeaturePerQuery.end() ;
			++ClusterFeaturePerQuery_it)
		{
			string QueryName = ClusterFeaturePerQuery_it->first;

			std::map<string,string>::iterator it = fileByQuery.find(QueryName);
			if(it == fileByQuery.end())
			{
				FILE_LOG(logERROR) << "ClustersFeatureCreator:CreateIDFDistribution - Cannot find query " << QueryName << " in file list ";
				return;
			}
	
			cout << '\r' << "Progress Query : " << QueryName << " - " << i+1 <<  '/' <<  queriesClustersFeature.ClusterFeaturePerQuery.size() << ".     Progress Percent " <<  int(((double)(1+i)/(double)queriesClustersFeature.ClusterFeaturePerQuery.size())*100.0)  <<  "%" << std::flush;
			i++;

			IdfHelper idfHelper;
			ClustersInQuery* clustersInQuery = queriesClusters.GetClustersInQuery(QueryName);
			idfHelper.init(clustersInQuery);

			ClustersFeatureInQuery* clustersFeatureInQuery = ClusterFeaturePerQuery_it->second;
			double size = clustersFeatureInQuery->ClusterListPerQuery.size();
			map< string , double >::iterator ClusterListPerQuery_it;
			for(ClusterListPerQuery_it = clustersFeatureInQuery->ClusterListPerQuery.begin() ;
				ClusterListPerQuery_it != clustersFeatureInQuery->ClusterListPerQuery.end() ;
				++ClusterListPerQuery_it)
			{
				string clusterName = ClusterListPerQuery_it->first;
				double Probability = idfHelper.GetClusterScore(clusterName);
				ClusterListPerQuery_it->second = Probability;
			}
		}
		string fileName = outputDir + "/IDFClustersFeature.txt" ;
		queriesClustersFeature.WriteClustersFeatureToFile(fileName);
		
	}




	void Clear()
	{
		queriesClustersFeature.Clear();
	}

private:

	inline void orderFileByConfig(vector<string> *fileNameList, std::map<string,string> *fileByQuery )
	{
		int numberOfFiles = fileNameList->size();
		for(int i = 0 ; i < numberOfFiles ; ++i)
		{
			string fileName = (*fileNameList)[i];
			// extract from the file name the configuration name and the query name
			std::vector<std::string> splitedFile = split(fileName, '/'); // the file path is split by "/" note.
			string queryFullName = splitedFile[splitedFile.size()-1]; 
			std::vector<std::string> splitedQuery = split(queryFullName, '.');
			string query = splitedQuery[0];
			fileByQuery->insert(std::make_pair (query, fileName));
		}
	}


	// 
	void CreateW2VQueryClusterSimilarityDistributionOld(string outputDir, vector<string> w2vModels , string rm1HighestTermsFiles)
	{
		std::cout << "Read First Query Expansion File " << std::endl;
		std::vector<FirstQueryExpension*> *rm =  FirstQueryExpension::readExpensQueryFile(rm1HighestTermsFiles);

		WordToVecModel wordToVecModel;

		#pragma region if there is one w2v model for all queries
		bool isOnlyOneW2vModel = (w2vModels.size() == 1);
		if(isOnlyOneW2vModel)
		{
			std::cout << "Read Word-To-Vec Model File " << std::endl;
			if( ! wordToVecModel.LoadWordToVecModel(w2vModels[0]))
			{
				return;
			}
		}
		#pragma endregion

		#pragma region loop on each query
		for (unsigned int j = 0 ; j < rm->size() ; ++j)
		{  
			FirstQueryExpension *firstQueryExpension = (*rm)[j];
			string queryName = firstQueryExpension->QueryId; //queryName == "361"
			vector<Term*>* OriginalQuery = &firstQueryExpension->OriginalQuery;

			if(queryName == "361")
			{
				cout << "Can find w2v model for query " << queryName << std::endl;
			}

			// read word to vec model
			#pragma region Read Word-To-Vec Model File
			if(! isOnlyOneW2vModel)
			{
				string w2vFileName = getW2vModelName(&w2vModels, queryName );
				std::cout << "Read Word-To-Vec Model File " << std::endl;
				if( w2vFileName != "" )
				{
					if( ! wordToVecModel.LoadWordToVecModel(w2vModels[0]))
					{
						continue;
					}
				}
				else
				{
					cout << "Can find w2v model for query " << queryName << std::endl;
					continue;
				}
			}
			#pragma endregion

			#pragma region calculate query Centroid 
			vector<string> queryTerms;
			int queryTermsSize = OriginalQuery->size();
			for (int i = 0; i < queryTermsSize ; i++)
			{
				string queryTerm = (*OriginalQuery)[i]->value;
				queryTerms.push_back(queryTerm);
			}
			float *queryCentroid =  wordToVecModel.GetCentroid(queryTerms);

			#pragma region loop on each cluster
			// Get query Clusters Feature 
			map< string , ClustersFeatureInQuery* >::iterator ClusterPerQuery_it = queriesClustersFeature.ClusterFeaturePerQuery.find(queryName);
			if(ClusterPerQuery_it == queriesClustersFeature.ClusterFeaturePerQuery.end())
			{
				cout << "Cannot find query " << queryName << " in Queries Clusters Feature files" <<std::endl;
				delete queryCentroid;
				continue;
			}

			// Get query Clusters Terms
			map< string , ClustersInQuery >::iterator ClusterInQueryList_it = queriesClusters.ClusterListPerQuery.find(queryName);
			if(ClusterInQueryList_it == queriesClusters.ClusterListPerQuery.end())
			{
				cout << "Cannot find query " << queryName << " in Queries Clusters files" <<std::endl;
				delete queryCentroid;
				continue;
			}
			
			// loop on each cluster
			ClustersInQuery *clustersInQuery = &ClusterInQueryList_it->second;
			map< string , vector<Term> >::iterator ClusterListPerQuery_it;
			for(ClusterListPerQuery_it = clustersInQuery->ClusterInQueryList.begin() ;
				ClusterListPerQuery_it != clustersInQuery->ClusterInQueryList.end() ;
				++ClusterListPerQuery_it)
			{
				// key = cluster name , value = list of cluster words
				string clusterName = ClusterListPerQuery_it->first; 
				vector<Term> *clusterWords = &ClusterListPerQuery_it->second;
				int clusterSize = clusterWords->size();
				// calculate cluster Centroid 
				vector<string> clusterTerms;
				for (int i = 0; i < clusterSize ; i++)
				{
					string term = (*clusterWords)[i].value;
					clusterTerms.push_back(term);
				}
				float *clusterCentroid =  wordToVecModel.GetCentroid(clusterTerms);
				// calculate Query Cluster Similarity -> p( C |q)
				double qeryClusterSimilarity = wordToVecModel.CalculateSimilarity(queryCentroid,clusterCentroid);

				delete clusterCentroid;

				map< string , double >::iterator it;  // key = Cluster name , value = p(c | q) 
				it = ClusterPerQuery_it->second->ClusterListPerQuery.find(clusterName);
				if(it !=  ClusterPerQuery_it->second->ClusterListPerQuery.end())
				{
					it->second = qeryClusterSimilarity;
				}
				else
				{
					cout << "Cannot find Cluster " << clusterName << " in Queries Clusters files" <<std::endl;
				}
			}

			delete queryCentroid;
			// normalize
			ClusterPerQuery_it->second->normalize();

			#pragma endregion

			#pragma endregion
		}
		#pragma endregion	

		string fileName = outputDir + "/W2VQueryClusterSimilarityClustersFeature.txt" ;
		queriesClustersFeature.WriteClustersFeatureToFile(fileName);

		for (unsigned int j = 0 ; j < rm->size() ; ++j)
		{  
			FirstQueryExpension *firstQueryExpension = (*rm)[j];
			firstQueryExpension->Clear();
			delete firstQueryExpension;
		}
		rm->clear();
		delete rm;
	}

}ClustersFeatureCreator;

typedef struct ClustersFeature
{
	QueriesClustersFeatures ClusterInQueryProbability; // P(c|q)

	void init( vector< pair<string,double> > *clustersFeatureFileList )
	{
		InitQueriesClustersFeatures(clustersFeatureFileList);
	}


	string getMaxCluster(string query)
	{
		map< string , vector<string> > *ClustersList = GetClustersList();
		map< string , vector<string> >::iterator it = ClustersList->find(query);
		
		if( it != ClustersList->end() )
		{
			double maxClusterProb = -1;
			string maxClusterName = "";
			vector<string> *clusters = &it->second;
			for (int i = 0; i < clusters->size(); i++)
			{
				string cluster = (*clusters)[i];
				double featuresProbability = ClusterInQueryProbability.GetClusterProbability(cluster,query) ; //P(c|q)
				if( maxClusterProb <  featuresProbability)
				{
					maxClusterProb = featuresProbability;
					maxClusterName = cluster;
				}
			}
			return maxClusterName;
		}
		return "";
		
	}



private :
	map< string , vector<string> > *GetClustersList()
	{
		return &ClusterInQueryProbability.ClustersList;
	}
	void InitQueriesClustersFeatures(vector< pair<string,double> > *clustersFeatureFileList)
	{
		for (int i = 0; i < clustersFeatureFileList->size(); i++)
		{
			string featureFileName = (*clustersFeatureFileList)[i].first;
			double featureWeight = (*clustersFeatureFileList)[i].second;
			ClusterInQueryProbability.AddFeature(featureFileName , featureWeight);
		}
		ClusterInQueryProbability.SumAllClustersFeatures();
	}
}ClustersFeature;



#endif