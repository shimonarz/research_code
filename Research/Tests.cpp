

#include <iostream>
#include <cstdlib>
#include <windows.h>

#include "research.h"
#include "BCBRM3.h"
#include "CB.h"
#include "CBRM3.h"

#include "ClustersFeature.h"
#include "RM1Probability.h"
#include "WordInClusterProbability.h"
#include "ClustersFeature.h"
#include "ModelConfiguration.h"

#include "DataStructer.h"
#include "WordToVec.h"
#include "GeneralFunctions.h"
#include "DiscriminativeModel.h"
#include "Evaluate.h"
#include "RM3.h"

#include "GeneralModelFunctions.h"

using namespace std;



void getAllFileFromDirectory(string dirFullPath ,vector<string> *listOfFiles )
{
	WIN32_FIND_DATA file_data;
	HANDLE hFindFileNameEvaluation = FindFirstFile((dirFullPath+ "/*").c_str() , &file_data);
	if (hFindFileNameEvaluation != INVALID_HANDLE_VALUE) 
	{
		do 
		{
			string fileName = file_data.cFileName;
			if(fileName != "." && fileName != ".." )
			{
				if( file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
				{
					//std::cout << "DIRECTORY Name = " << fileName << std::endl ;
					string fullDirName = dirFullPath + "/" + fileName;
					getAllFileFromDirectory(fullDirName,listOfFiles);
				}
				else
				{
					//std::cout << "file Name = " << fileName << std::endl ;
					string fullFileName = dirFullPath + "/" + fileName;
					listOfFiles->push_back(fullFileName);
				}
				
			}
		}
		while (FindNextFile(hFindFileNameEvaluation, &file_data));
		FindClose(hFindFileNameEvaluation);
	}
}

void getAllFileFromDirectory_temp(string dirFullPath , vector<string> *listOfFiles )
{
	WIN32_FIND_DATA file_data;
	HANDLE hFindFileNameEvaluation = FindFirstFile((dirFullPath+ "/*").c_str() , &file_data);
	if (hFindFileNameEvaluation != INVALID_HANDLE_VALUE) 
	{
		do 
		{
			string fileName = file_data.cFileName;
			if(fileName != "." && fileName != ".." )
			{
				if( file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
				{
					std::cout << "DIRECTORY Name = " << fileName << std::endl ;
					string fullDirName = dirFullPath + "/" + fileName;
					getAllFileFromDirectory_temp(fullDirName, listOfFiles);
				}
				else
				{
					if( fileName.find("305.clusters") != std::string::npos ||
						fileName.find("306.clusters") != std::string::npos ||
						fileName.find("307.clusters") != std::string::npos )
					{
						std::cout << "file Name = " << fileName << std::endl ;
						string fullFileName = dirFullPath + "/" + fileName;
						listOfFiles->push_back(fullFileName);
					}
				}
				
			}
		}
		while (FindNextFile(hFindFileNameEvaluation, &file_data));
		FindClose(hFindFileNameEvaluation);
	}
}

void test_createQueryExpansionUsingKnnClustering_WordsComeFromDiscriminativeModel()
{
	
	string originalQueriesFileName = "D:/Research/DataSet/queries/queriesROBUST.xml";
	string queryModelDir = "D:/Research/DataFromServer/Discriminative/ROBUST 50/model_50";
	string queryCollectionFeaturesDir = "D:/Research/DataFromServer/Discriminative/ROBUST 50/train_50";

	vector<string> queryModelFileNames;
	vector<string> queryCollectionFeaturesFileNames;
	int positiveWordsNumber = 50;
	int negativeWordsNumber = 50;

	getAllFileFromDirectory( queryModelDir ,&queryModelFileNames );
	getAllFileFromDirectory( queryCollectionFeaturesDir ,&queryCollectionFeaturesFileNames );

	string outputFile = "D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_WordsComeFromDiscriminativeModel/out_new2.txt";
	CBDM::createFirstTimeQueryExpansion_WordsComeFromDiscriminativeModel( originalQueriesFileName ,  queryModelFileNames ,   queryCollectionFeaturesFileNames ,  positiveWordsNumber ,  negativeWordsNumber, outputFile);
}

void test_createQueryExpansionUsingKnnClusteringForQuery_WordsComeFromDiscriminativeModel()
{
	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_WordsComeFromDiscriminativeModel/ROBUST_FirstExpansion.txt";
	string qrelsFileName = "D:/Research/DataSet/qrels/qrelsROBUST";
	string outputDir = "D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_WordsComeFromDiscriminativeModel";
	string wordToVecModelFileName ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_WordsComeFromDiscriminativeModel/ROBUST.w2v.dim.300.win.16.neg.10.bin";
	int k =10;
	double originalQueryWeight = 0.3;

	CBDM::createQueryExpansionUsingKnnClustering_WordsComeFromDiscriminativeModel(firstQueryExpensionFile,qrelsFileName, outputDir,wordToVecModelFileName, k, originalQueryWeight);
}
	
void test_CreateFilesForGnuplot_WordsComeFromDiscriminativeModel()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateFilesForGnuplot_WordsComeFromDiscriminativeModel";
	vector<string> listOfConfig ;
	
	/*
	getAllFileFromDirectory( "D:/Research/TestData/CBDM.copy" ,&listOfConfig );
	vector<string> listOfConfigModelDocs_25 ;
	vector<string> listOfConfigModelDocs_50;
	for (int i = 0; i < listOfConfig.size(); i++)
	{
		if( listOfConfig[i].find("ModelDocs_25") != string::npos)
		{
			listOfConfigModelDocs_25.push_back(listOfConfig[i]);
		}
		else
		{
			listOfConfigModelDocs_50.push_back(listOfConfig[i]);
		}
	}*/
	
	getAllFileFromDirectory( "D:/Research/TestData/CBDM.copy/ROBUST_PSWR/ROBUST.w2v.dim.300.win.16.neg.10/ModelDocs_25_Positive_50_Negative_50_OriginalQueryWeight_1.0" ,&listOfConfig );

	// 
	CBDM::CreateFilesForGnuplot_WordsComeFromDiscriminativeModel( pathToDir,&listOfConfig);

}

void test_CreateFilesForGnuplot_WordsComeFromDiscriminativeModel_1()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateFilesForGnuplot_WordsComeFromDiscriminativeModel";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( "D:/Research/DataFromServer/Discriminative/ROBUST_PSWR/ROBUST.w2v.dim.300.win.16.neg.10/ModelDocs_25_Positive_50_Negative_50_OriginalQueryWeight_0.0/evaluateFolder" ,&listOfConfig );
	getAllFileFromDirectory( "D:/Research/DataFromServer/Discriminative/ROBUST_PSWR/ROBUST.w2v.dim.300.win.16.neg.10/ModelDocs_25_Positive_50_Negative_50_OriginalQueryWeight_0.2/evaluateFolder" ,&listOfConfig );

	CBDM::CreateFilesForGnuplot_WordsComeFromDiscriminativeModel( pathToDir,&listOfConfig);

}

void test_CompareBetweenCBToCBRM3Models_WordComeFromRM1()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CompareBetweenCBToCBRM3Models_WordComeFromRM1";
	string cvPath = "D:/Research/DataFromServer/OutputTests/test_CompareBetweenCBToCBRM3Models_WordComeFromRM1/cv.txt";
	vector<string> listOfFiles ;
	vector<string> listOfConfig ;
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/CB/AP/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.8/evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/CB/AP/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.5/evaluateFolder");
	//listOfConfig.push_back("D:/Research/DataFromServer/FIles/CB/AP/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.5/evaluateFolder");
	//listOfConfig.push_back("D:/Research/DataFromServer/FIles/CB/AP/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.8/evaluateFolder");
	//listOfConfig.push_back("D:/Research/DataFromServer/FIles/CB/AP/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_1.0/evaluateFolder");

	listOfConfig.push_back("D:/Research/DataFromServer/FIles/CBRM3/AP/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.8/evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/FIles/CBRM3/AP/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.5/evaluateFolder");
	//listOfConfig.push_back("D:/Research/DataFromServer/FIles/CBRM3/AP/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.5/evaluateFolder");
	//listOfConfig.push_back("D:/Research/DataFromServer/FIles/CBRM3/AP/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_0.8/evaluateFolder");
	//listOfConfig.push_back("D:/Research/DataFromServer/FIles/CBRM3/AP/AP.w2v.dim.100.win.16.neg.10.bin/fbDocs_10_fbTerms_100_fbMu_0_muRule_1000_fbOrigWeight_1.0/evaluateFolder");

	int n = listOfConfig.size();
	for (int i = 0; i < n; i++)
	{
		string path = listOfConfig[i];
		getAllFileFromDirectory( path ,&listOfFiles );
	}

	CompareBetweenCBToCBRM3Models_WordComeFromRM1( pathToDir, cvPath , &listOfFiles);
}

void test_DiscriminativeModelQuery()
{
	vector<string> listOfFiles ;
	vector<string> listOfConfig ;

	listOfConfig.push_back("D:/Research/DataFromServer/Discriminative/ROBUST_PSWR/ROBUST.w2v.dim.300.win.16.neg.10/ModelDocs_25_Positive_100_Negative_0_OriginalQueryWeight_0.8/uniform_evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/Discriminative/ROBUST_PSWR/ROBUST.w2v.dim.300.win.16.neg.10/ModelDocs_25_Positive_100_Negative_0_OriginalQueryWeight_0.8/uniform_clustersScore");
	listOfConfig.push_back("D:/Research/DataFromServer/Discriminative/ROBUST_PSWR/ROBUST.w2v.dim.300.win.16.neg.10/ModelDocs_25_Positive_100_Negative_0_OriginalQueryWeight_0.5/uniform_evaluateFolder");
	listOfConfig.push_back("D:/Research/DataFromServer/Discriminative/ROBUST_PSWR/ROBUST.w2v.dim.300.win.16.neg.10/ModelDocs_25_Positive_100_Negative_0_OriginalQueryWeight_0.5/uniform_clustersScore");
	
	int n = listOfConfig.size();
	for (int i = 0; i < n; i++)
	{
		string path = listOfConfig[i];
		getAllFileFromDirectory( path ,&listOfFiles );
	}

	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateFilesForGnuplot_WordsComeFromDiscriminativeModel";
	//test_xxx(pathToDir,&listOfFiles);
}

void test_CreateFromRM1FirstExpansionFirstOtherExpansionFromDiscriminativeModel()
{
		std::cout << "Start Create from the RM1 First Time Query Expansion other First Time Query Expansion file that include Discriminative Model score " << std::endl;

	vector<string> queryModelFileNames;
	vector<string> queryCollectionFeaturesFileNames;
	
	string firstExpansionFromRM1File ="D:/Research/DataFromServer/OutputTests/test_CreateFromRM1FirstExpansionFirstOtherExpansionFromDiscriminativeModel/relativeToRm1_queryContainRM1Trems";
	string outputFile = "D:/Research/DataFromServer/OutputTests/test_CreateFromRM1FirstExpansionFirstOtherExpansionFromDiscriminativeModel/outputFile";

	string modleFile = "model";
	string collectionFile = "collection";
	
	string queryModelDir = "D:/Research/DataFromServer/Discriminative/ROBUST 50/model_50";
	string queryCollectionFeaturesDir = "D:/Research/DataFromServer/Discriminative/ROBUST 50/train_50";

	vector<string> listOfFiles ;
	getAllFileFromDirectory(queryModelDir , &listOfFiles);
	getAllFileFromDirectory(queryCollectionFeaturesDir , &listOfFiles);

	for (int i = 0; i < listOfFiles.size(); i++)
	{
		string fileName = listOfFiles[i];
		if (fileName.find(modleFile) != std::string::npos) {
			queryModelFileNames.push_back(fileName);
		}
		else if (fileName.find(collectionFile) != std::string::npos) {
			queryCollectionFeaturesFileNames.push_back(fileName);
		}
	}

	std::cout << "	Collection Features files number = " << queryCollectionFeaturesFileNames.size() <<  ", Query Model files number = " << queryModelFileNames.size() << std::endl;
	CBDM::CreateFromRM1FirstExpansionFirst_OtherExpansionFromDiscriminativeModel( firstExpansionFromRM1File ,  queryModelFileNames ,   queryCollectionFeaturesFileNames , outputFile);

}

void test_crossValidationOnClusterScore()
{
	vector<string> listOfFiles ;
	getAllFileFromDirectory_temp("D:/Research/TestData/CBDM.copy/ROBUST_PSWR/ROBUST.w2v.dim.300.win.16.neg.10" , &listOfFiles);

	vector<string> clusterScoreFile ;
	vector<string> clusterPerformance;

	for (int i = 0; i < listOfFiles.size(); i++)
	{
		string fileName = listOfFiles[i];
		if (fileName.find("clusterGrade") != std::string::npos) {
			clusterScoreFile.push_back(fileName);
		}
		else  {
			clusterPerformance.push_back(fileName);
		}
	}

	string outputFile = "D:/Research/DataFromServer/OutputTests/test_crossValidationOnClusterScore/out.txt";

	CBDM::crossValidationOnClusterScore( &clusterScoreFile ,  &clusterPerformance , 1,   outputFile);
}

string getMainConfieTest(string fullConfigName)
{
	const string end = "_Orig";
	return trimFormStrToEnd(fullConfigName,end);
}

void trimEndTestFunctio()
{
	string str = "ModelDocs_25_Positive_100_Negative_0_OriginalQueryWeight_0.0";
	string res = getMainConfieTest(str);
}

/*
void test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model()
{
	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST_FirstExpansion.txt";
	string qrelsFileName = "D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/qrelsROBUST";
	string outputDir = "D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model";
	int k =10;
	double originalQueryWeight = 0.3 , clusterTermsWeight = 0.2;
	TermsDistributionType type = TermsDistributionTypeConvertor(1);
	vector<string> wordToVecModelFileNameVec;
	string wordToVecModelFileName1 ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST.w2v.dim.300.win.16.neg.10.bin";
	string wordToVecModelFileName2 ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/301.txt.w2v.dim.300.win.16.neg.10.bin";
	
	wordToVecModelFileNameVec.push_back(wordToVecModelFileName1);
	wordToVecModelFileNameVec.push_back(wordToVecModelFileName2);
	for (int i = 302; i <701; i++)
	{
		string wordToVecModelFileName ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/" + SSTR(i) + ".txt.w2v.dim.300.win.16.neg.10.bin";
		wordToVecModelFileNameVec.push_back(wordToVecModelFileName);
	}

	createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model(firstQueryExpensionFile,qrelsFileName, outputDir,wordToVecModelFileNameVec, k, originalQueryWeight , clusterTermsWeight ,type );
}*/

void test_CreateClusterFileForAllQueries()
{
	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST_FirstExpansion.txt";
	string outputDir = "D:/Research/DataFromServer/OutputTests/test_CreateClusterFileForAllQueries";
	int k =10;
	vector<string> wordToVecModelFileNameVec;
	string wordToVecModelFileName1 ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST.w2v.dim.300.win.16.neg.10.bin";
	string wordToVecModelFileName2 ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/301.txt.w2v.dim.300.win.16.neg.10.bin";
	
	wordToVecModelFileNameVec.push_back(wordToVecModelFileName1);
	/*wordToVecModelFileNameVec.push_back(wordToVecModelFileName2);
	for (int i = 302; i <701; i++)
	{
		string wordToVecModelFileName ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/" + SSTR(i) + ".txt.w2v.dim.300.win.16.neg.10.bin";
		wordToVecModelFileNameVec.push_back(wordToVecModelFileName);
	}*/

	CreateClusterFileForAllQueries( firstQueryExpensionFile, wordToVecModelFileNameVec, k, outputDir );
}

void test_ReadClusterFileForAllQueries()
{
	QueriesClusters queriesClusters;
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateClusterFileForAllQueries";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( pathToDir ,&listOfConfig );
	queriesClusters.init(&listOfConfig);

}

void test_ReadClusterFileForAllQueriesWithScor()
{
	QueriesClusters queriesClusters;
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateUniformDistribution";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( pathToDir ,&listOfConfig );
	queriesClusters.initWithScore(&listOfConfig);

}


/*
void test_CreateProportionalToRM1Distribution()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateClusterFileForAllQueries";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( pathToDir ,&listOfConfig );

	WordInClustersDistribution wordInClustersDistribution;
	wordInClustersDistribution.InitRM1HighestTermsFiles(listOfConfig);
	
	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST_FirstExpansion.txt";
	pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateProportionalToRM1Distribution";
	wordInClustersDistribution.CreateProportionalToRM1Distribution(pathToDir,firstQueryExpensionFile);
}

void test_CreateW2VSimilaritydistributionDistribution()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateClusterFileForAllQueries";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( pathToDir ,&listOfConfig );

	WordInClustersDistribution wordInClustersDistribution;
	wordInClustersDistribution.InitRM1HighestTermsFiles(listOfConfig);
	
	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST_FirstExpansion.txt";
	vector<string> wordToVecModelFileNameVec;
	string wordToVecModelFileName1 ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST.w2v.dim.300.win.16.neg.10.bin";
	wordToVecModelFileNameVec.push_back(wordToVecModelFileName1);
	pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateW2VSimilaritydistributionDistribution";
	wordInClustersDistribution.CreateW2VSimilaritydistributionDistribution(pathToDir,wordToVecModelFileNameVec);
}



*/


void test_ClustersFeatureCreator_CreateUniformDistribution()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateClusterFileForAllQueries";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( pathToDir ,&listOfConfig );

	ClustersFeatureCreator clustersFeatureCreator;
	clustersFeatureCreator.Init(&listOfConfig);
	
	pathToDir = "D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateUniformDistribution";
	clustersFeatureCreator.CreateUniformDistribution(pathToDir);
}

void test_ClustersFeatureCreator_CreateIDFDistribution()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateClusterFileForAllQueries";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( pathToDir ,&listOfConfig );

	ClustersFeatureCreator clustersFeatureCreator;
	clustersFeatureCreator.Init(&listOfConfig);
	
	pathToDir = "D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateIDFDistribution";
	clustersFeatureCreator.CreateICFDistribution(pathToDir);
}


void test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistributionOld()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateClusterFileForAllQueries";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( pathToDir ,&listOfConfig );

	ClustersFeatureCreator clustersFeatureCreator;
	clustersFeatureCreator.Init(&listOfConfig);
	
	vector<string> wordToVecModelFileNameVec;
	string wordToVecModelFileName1 ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST.w2v.dim.300.win.16.neg.10.bin";
	wordToVecModelFileNameVec.push_back(wordToVecModelFileName1);

	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST_FirstExpansion.txt";

	pathToDir = "D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistribution";
	clustersFeatureCreator.CreateW2VQueryClusterSimilarityDistribution(pathToDir,wordToVecModelFileNameVec,firstQueryExpensionFile);
}

void test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistribution()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateClusterFileForAllQueries";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( pathToDir ,&listOfConfig );

	ClustersFeatureCreator clustersFeatureCreator;
	clustersFeatureCreator.Init(&listOfConfig);
	
	vector<string> wordToVecModelFileNameVec;
	string wordToVecModelFileName1 ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST.w2v.dim.300.win.16.neg.10.bin";
	wordToVecModelFileNameVec.push_back(wordToVecModelFileName1);

	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistribution/queriesStemmedROBUST.txt";

	pathToDir = "D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistribution";
	clustersFeatureCreator.CreateW2VQueryClusterSimilarityDistribution(pathToDir,wordToVecModelFileNameVec,firstQueryExpensionFile);
}


void test_ClustersFeatureCreator_CreateSingleLinkSimilarityDistribution()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateClusterFileForAllQueries";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( pathToDir ,&listOfConfig );

	ClustersFeatureCreator clustersFeatureCreator;
	clustersFeatureCreator.Init(&listOfConfig);
	
	vector<string> wordToVecModelFileNameVec;
	string wordToVecModelFileName1 ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST.w2v.dim.300.win.16.neg.10.bin";
	wordToVecModelFileNameVec.push_back(wordToVecModelFileName1);

	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistribution/queriesStemmedROBUST.txt";

	pathToDir = "D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistribution";
	clustersFeatureCreator.CreateSingleLinkSimilarityDistribution(pathToDir,wordToVecModelFileNameVec,firstQueryExpensionFile);
}

void test_ClustersFeatureCreator_CreateCompleteLinkSimilarityDistribution()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateClusterFileForAllQueries";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( pathToDir ,&listOfConfig );

	ClustersFeatureCreator clustersFeatureCreator;
	clustersFeatureCreator.Init(&listOfConfig);
	
	vector<string> wordToVecModelFileNameVec;
	string wordToVecModelFileName1 ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST.w2v.dim.300.win.16.neg.10.bin";
	wordToVecModelFileNameVec.push_back(wordToVecModelFileName1);

	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistribution/queriesStemmedROBUST.txt";

	pathToDir = "D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistribution";
	clustersFeatureCreator.CreateCompleteLinkSimilarityDistribution(pathToDir,wordToVecModelFileNameVec,firstQueryExpensionFile);
}

void test_ClustersFeatureCreator_CreateAverageLinkSimilarityDistribution()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_CreateClusterFileForAllQueries";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( pathToDir ,&listOfConfig );

	ClustersFeatureCreator clustersFeatureCreator;
	clustersFeatureCreator.Init(&listOfConfig);
	
	vector<string> wordToVecModelFileNameVec;
	string wordToVecModelFileName1 ="D:/Research/DataFromServer/OutputTests/test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model/DataSet/ROBUST.w2v.dim.300.win.16.neg.10.bin";
	wordToVecModelFileNameVec.push_back(wordToVecModelFileName1);

	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistribution/queriesStemmedROBUST.txt";

	pathToDir = "D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistribution";
	clustersFeatureCreator.CreateAverageLinkSimilarityDistribution(pathToDir,wordToVecModelFileNameVec,firstQueryExpensionFile);
}


void test_QueriesClustersFeatures()
{
	QueriesClustersFeatures queriesClustersFeatures;
	queriesClustersFeatures.AddFeature("D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistribution/W2VQueryClusterSimilarityClustersFeature.txt", 0.6);
	queriesClustersFeatures.AddFeature("D:/Research/DataFromServer/OutputTests/test_ClustersFeatureCreator_CreateUniformDistribution/UniformClustersFeature.txt", 0.4);
	queriesClustersFeatures.SumAllClustersFeatures();
}



void test_QueriesClustersFeatures_AP()
{
	
	//// CreateClusterFileForAllQueries
	//string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_ap/AP_FirstExpansion.txt";
	string ClusterFileDir = "D:/Research/DataFromServer/OutputTests/test_ap/ClusterFile";
	//int k =10;
	vector<string> wordToVecModelFileNameVec;
	string wordToVecModelFileName ="D:/Research/DataFromServer/w2vModels/AP/AP.w2v.dim.300.win.16.neg.10.bin";
	wordToVecModelFileNameVec.push_back(wordToVecModelFileName);
	//CreateClusterFileForAllQueries( firstQueryExpensionFile, wordToVecModelFileNameVec, k, ClusterFileDir );

	////CreateUniformDistribution
	vector<string> listOfConfig ;
	getAllFileFromDirectory( ClusterFileDir ,&listOfConfig );
	//WordInClustersDistribution wordInClustersDistribution;
	//wordInClustersDistribution.InitRM1HighestTermsFiles(listOfConfig);

	//string UniformDistributionDir = "D:/Research/DataFromServer/OutputTests/test_ap/UniformDistribution";
	//wordInClustersDistribution.CreateUniformDistribution(UniformDistributionDir);

	////CreateProportionalToRM1Distribution()
	//WordInClustersDistribution wordInClustersDistribution2;
	//wordInClustersDistribution2.InitRM1HighestTermsFiles(listOfConfig);
	//string ProportionalToRM1DistributionDir = "D:/Research/DataFromServer/OutputTests/test_ap/ProportionalToRM1Distribution";
	//wordInClustersDistribution2.CreateProportionalToRM1Distribution(ProportionalToRM1DistributionDir,firstQueryExpensionFile);

	//// CreateW2VSimilarityDistribution()
	//WordInClustersDistribution wordInClustersDistribution3;
	//wordInClustersDistribution3.InitRM1HighestTermsFiles(listOfConfig);
	//string W2VSimilarityDistributionDir = "D:/Research/DataFromServer/OutputTests/test_ap/W2VSimilarityDistribution";
	//wordInClustersDistribution.CreateW2VSimilaritydistributionDistribution(W2VSimilarityDistributionDir,wordToVecModelFileNameVec);

	//// ClustersFeatureCreator
	////CreateUniformDistribution
	//ClustersFeatureCreator clustersFeatureCreator;
	//clustersFeatureCreator.InitRM1HighestTermsFiles(listOfConfig);
	string ClustersFeatureDir = "D:/Research/DataFromServer/OutputTests/test_ap/ClustersFeature";
	//clustersFeatureCreator.CreateUniformDistribution(ClustersFeatureDir);
	
	//CreateW2VQueryClusterSimilarityDistributionO()
	ClustersFeatureCreator clustersFeatureCreator2;
	clustersFeatureCreator2.Init(&listOfConfig);
	string queriesFile = "D:/Research/DataFromServer/OutputTests/test_ap/queriesAP.txt";
	clustersFeatureCreator2.CreateW2VQueryClusterSimilarityDistribution(ClustersFeatureDir,wordToVecModelFileNameVec,queriesFile);
}

void test_ModelConfiguration()
{
	string pathToDir = "D:/Research/DataFromServer/OutputTests/test_ModelConfiguration";

	ModelConfiguration modelConfiguration;
	modelConfiguration.queries = "D:/Research/queries.txt";
	modelConfiguration.queriesWeight = 0.2;
	modelConfiguration.qrels = "D:/Research/qrels.txt";
	modelConfiguration.rm1ProbabilityFile = "D:/Research/rm1ProbabilityFile.txt";
	modelConfiguration.rm1Weight = 0.3;

	modelConfiguration.queriesClustersFileList.push_back("D:/Research/queriesClustersFile_1.txt"); 
	modelConfiguration.queriesClustersFileList.push_back("D:/Research/queriesClustersFile_2.txt"); 
	modelConfiguration.queriesClustersFileList.push_back("D:/Research/queriesClustersFile_3.txt"); 

	modelConfiguration.queriesTermsProbabilityInClustersFileList.push_back("D:/Research/queriesTermsProbabilityInClustersFile_1.txt"); 
	modelConfiguration.queriesTermsProbabilityInClustersFileList.push_back("D:/Research/queriesTermsProbabilityInClustersFile_2.txt"); 
	modelConfiguration.queriesTermsProbabilityInClustersFileList.push_back("D:/Research/queriesTermsProbabilityInClustersFile_3.txt"); 

	modelConfiguration.clustersFeatureFileList.push_back(std::make_pair<string,double>("D:/Research/clustersFeatureFile_1.txt",0.2)); 
	modelConfiguration.clustersFeatureFileList.push_back(std::make_pair<string,double>("D:/Research/clustersFeatureFile_2.txt",0.3)); 
	modelConfiguration.clustersFeatureFileList.push_back(std::make_pair<string,double>("D:/Research/clustersFeatureFile_3.txt",0.5)); 

	modelConfiguration.termsProbabilityInClustersWeight = 0.5;
	modelConfiguration.outputDir = "D:/Research/outputDir/";

	string fileName = pathToDir + "/config.txt" ;
	modelConfiguration.WriteModelConfigurationToFile(fileName);

	ModelConfiguration modelConfiguration_fromFile;
	modelConfiguration_fromFile.Init(fileName);
	
	modelConfiguration_fromFile.ValidateModelConfiguration();
}

void Test_WordInClusterProbabilityCreator_UniformDistribution()
{
	WordInClusterProbabilityCreator wordInClusterProbabilityCreator;

	string ClusterFileDir = "D:/Research/DataFromServer/OutputTests/test_ap/ClusterFile";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( ClusterFileDir ,&listOfConfig );

	wordInClusterProbabilityCreator.init(listOfConfig);
	string dir = "D:/Research/DataFromServer/OutputTests/Test_WordInClusterProbabilityCreator/UniformDistribution";
	wordInClusterProbabilityCreator.CreateUniformDistribution(dir);
	wordInClusterProbabilityCreator.Clear();
}


void Test_WordInClusterProbabilityCreator_UniformDistributionForAllTerms()
{
	WordInClusterProbabilityCreator wordInClusterProbabilityCreator;

	string ClusterFileDir = "D:/Research/DataFromServer/OutputTests/test_ap/ClusterFile";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( ClusterFileDir ,&listOfConfig );

	wordInClusterProbabilityCreator.init(listOfConfig);
	string dir = "D:/Research/DataFromServer/OutputTests/Test_WordInClusterProbabilityCreator/UniformDistributionForAllTerms";
	wordInClusterProbabilityCreator.CreateUniformDistributionForAllTerms(dir);
	wordInClusterProbabilityCreator.Clear();
}

void Test_WordInClusterProbabilityCreator_ProportionalToRM1Distribution()
{
	WordInClusterProbabilityCreator wordInClusterProbabilityCreator;

	string ClusterFileDir = "D:/Research/DataFromServer/OutputTests/test_ap/ClusterFile";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( ClusterFileDir ,&listOfConfig );

	wordInClusterProbabilityCreator.init(listOfConfig);
	string dir = "D:/Research/DataFromServer/OutputTests/Test_WordInClusterProbabilityCreator/ProportionalToRM1Distribution";
	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_ap/AP_FirstExpansion.txt";
	wordInClusterProbabilityCreator.CreateProportionalToRM1Distribution(dir, firstQueryExpensionFile);
	wordInClusterProbabilityCreator.Clear();
}

void Test_WordInClusterProbabilityCreator_ProportionalToRM1DistributionForAllTerms()
{
	WordInClusterProbabilityCreator wordInClusterProbabilityCreator;

	string ClusterFileDir = "D:/Research/DataFromServer/OutputTests/test_ap/ClusterFile";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( ClusterFileDir ,&listOfConfig );

	wordInClusterProbabilityCreator.init(listOfConfig);
	string dir = "D:/Research/DataFromServer/OutputTests/Test_WordInClusterProbabilityCreator/ProportionalToRM1DistributionForAllTerms";
	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_ap/AP_FirstExpansion.txt";
	wordInClusterProbabilityCreator.CreateProportionalToRM1DistributionForAllTerms(dir, firstQueryExpensionFile);
	wordInClusterProbabilityCreator.Clear();
}

void Test_WordInClusterProbabilityCreator_W2VSimilarityDistribution()
{
	/*WordInClusterProbabilityCreator wordInClusterProbabilityCreator;

	string ClusterFileDir = "D:/Research/DataFromServer/OutputTests/test_ap/ClusterFile";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( ClusterFileDir ,&listOfConfig );

	wordInClusterProbabilityCreator.init(listOfConfig);
	string dir = "D:/Research/DataFromServer/OutputTests/Test_WordInClusterProbabilityCreator/W2VSimilarityDistribution";
	vector<string> wordToVecModelFileNameVec;
	string wordToVecModelFileName ="D:/Research/DataFromServer/w2vModels/AP/AP.w2v.dim.300.win.16.neg.10.bin";
	wordToVecModelFileNameVec.push_back(wordToVecModelFileName);
	wordInClusterProbabilityCreator.CreateW2VSimilarityToClusterCentroidDistribution(dir, wordToVecModelFileNameVec);
	wordInClusterProbabilityCreator.Clear();*/

	WordInClusterProbabilityCreator wordInClusterProbabilityCreator;

	vector<string> listOfConfig ;
	listOfConfig.push_back("D:/Research/DataFromServer/OutputTests/TestErrorInW2V/707.QueryClusters");
	listOfConfig.push_back("D:/Research/DataFromServer/OutputTests/TestErrorInW2V/715.QueryClusters");
	wordInClusterProbabilityCreator.init(listOfConfig);

	string dir = "D:/Research/DataFromServer/OutputTests/TestErrorInW2V";
	vector<string> wordToVecModelFileNameVec;
	wordToVecModelFileNameVec.push_back("D:/Research/DataFromServer/OutputTests/TestErrorInW2V/707.txt.dim.300.win.16.neg.10.bin");
	wordToVecModelFileNameVec.push_back("D:/Research/DataFromServer/OutputTests/TestErrorInW2V/715.txt.dim.300.win.16.neg.10.bin");
	wordInClusterProbabilityCreator.CreateW2VSimilarityToClusterCentroidDistribution(dir, wordToVecModelFileNameVec);
	wordInClusterProbabilityCreator.Clear();

}

void Test_WordInClusterProbabilityCreator_W2VSimilarityToClusterCentroidDistributionForAllTerms()
{
	WordInClusterProbabilityCreator wordInClusterProbabilityCreator;

	string ClusterFileDir = "D:/Research/DataFromServer/OutputTests/test_ap/ClusterFile";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( ClusterFileDir ,&listOfConfig );

	wordInClusterProbabilityCreator.init(listOfConfig);
	string dir = "D:/Research/DataFromServer/OutputTests/Test_WordInClusterProbabilityCreator/W2VSimilarityToClusterCentroidDistributionForAllTerms";
	vector<string> wordToVecModelFileNameVec;
	string wordToVecModelFileName ="D:/Research/DataFromServer/w2vModels/AP/AP.w2v.dim.300.win.16.neg.10.bin";
	wordToVecModelFileNameVec.push_back(wordToVecModelFileName);
	wordInClusterProbabilityCreator.CreateW2VSimilarityToClusterCentroidDistributionForAllTerms(dir, wordToVecModelFileNameVec);
	wordInClusterProbabilityCreator.Clear();
}

void Test_InitProbabilityMatrixFromFile_W2VSimilarityToClusterCentroidDistributionForAllTerms()
{
	WordInClusterProbabilityForQuery wordInClusterProbabilityForQuery;
	string dir = "D:/Research/DataFromServer/OutputTests/Test_WordInClusterProbabilityCreator/W2VSimilarityToClusterCentroidDistributionForAllTerms";
	vector<string> listOfConfig ;
	getAllFileFromDirectory( dir ,&listOfConfig );
	
	wordInClusterProbabilityForQuery.InitProbabilityMatrixFromFile(listOfConfig[0]);
	wordInClusterProbabilityForQuery.WriteWordInClusterProbabilityToFile(listOfConfig[0] + " - Copy");
	/*for (int i = 0; i < listOfConfig.size(); i++)
	{

	}*/
}

void Test_RM1Probability()
{
	RM1Probability writer;
	
	string dir = "D:/Research/DataFromServer/OutputTests/Test_RM1Probability/";
	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/test_ap/AP_FirstExpansion.txt";
	writer.CreateRM1ProbabilityFile(firstQueryExpensionFile);
	
	string file = dir + "/RM1Probability.txt" ;
	writer.WriteToFile(file);
	
	RM1Probability reader;
	reader.ReadFromFile(file);

	writer.Clear();
	reader.Clear();
}

void Test_QueriesFile()
{
	QueriesFile queriesFile;
	string queriesFilepath = "D:/Research/DataSet/queries/queriesROBUST.xml";
	queriesFile.readFile(queriesFilepath);
}


void Test_CBRM3Model()
{
	CBRM3Model _CBRM3Model;
	CBRM3_ModelConfiguration modelConfiguration;


	modelConfiguration.queries = "D:/Research/DataSet/queries/queriesROBUST.xml";
	modelConfiguration.queriesWeight = 0.2;
	modelConfiguration.qrels = "D:/Research/DataSet/qrels/qrelsROBUST";
	modelConfiguration.RM1ProbabilityFile = "D:/Research/DataFromServer/OutputTests/Test_CBRM3Model/RM1ProbabilityFile.txt";
	modelConfiguration.rm1Weight = 0.3;

	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_CBRM3Model/ClusterFiles" ,&modelConfiguration.queriesClustersFileList );
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_CBRM3Model/WordInClusterProbability/ProportionalToRM1Distribution" ,&modelConfiguration.queriesTermsProbabilityInClustersFileList );

	modelConfiguration.termsProbabilityInClustersWeight = 0.5;
	modelConfiguration.outputDir = "D:/Research/DataFromServer/OutputTests/Test_CBRM3Model/outputDir/ProportionalToRM1Clusters";
	
	modelConfiguration.loopOnAllClustersWords = "false";
	//modelConfiguration.

	_CBRM3Model.Init(modelConfiguration);
	_CBRM3Model.BuildModel();
	_CBRM3Model.Clear();

	CBRM3Model _CBRM3Model2;
	modelConfiguration.outputDir = "D:/Research/DataFromServer/OutputTests/Test_CBRM3Model/outputDir/UniformDistribution";
	modelConfiguration.queriesTermsProbabilityInClustersFileList.clear();
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_CBRM3Model/WordInClusterProbability/UniformDistribution" ,&modelConfiguration.queriesTermsProbabilityInClustersFileList );
	_CBRM3Model2.Init(modelConfiguration);
	_CBRM3Model2.BuildModel();
	_CBRM3Model2.Clear();
}

void Test_CBRM3Model_BuildBestClusterModel()
{
	CBRM3Model _CBRM3Model;
	CBRM3_ModelConfiguration modelConfiguration;


	modelConfiguration.queries = "D:/Research/DataSet/queries/queriesROBUST.xml";
	modelConfiguration.queriesWeight = 0.2;
	modelConfiguration.qrels = "D:/Research/DataSet/qrels/qrelsROBUST";
	modelConfiguration.RM1ProbabilityFile = "D:/Research/DataFromServer/OutputTests/Test_CBRM3Model/RM1ProbabilityFile.txt";
	modelConfiguration.rm1Weight = 0.3;

	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_CBRM3Model/ClusterFiles" ,&modelConfiguration.queriesClustersFileList );
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_CBRM3Model/WordInClusterProbability/ProportionalToRM1Distribution" ,&modelConfiguration.queriesTermsProbabilityInClustersFileList );

	modelConfiguration.termsProbabilityInClustersWeight = 0.5;
	modelConfiguration.outputDir = "D:/Research/DataFromServer/OutputTests/Test_CBRM3Model/outputDir/ProportionalToRM1Clusters";
	
	modelConfiguration.loopOnAllClustersWords = "false";
	//modelConfiguration.

	vector< pair<string,double> > clustersFeatureFileList;
	string featureFileName = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterPerQueryProbability/W2VQueryClusterSimilarityDistribution/W2VQueryClusterSimilarityClustersFeature.txt";
	double featureWeight = 1.0;
	clustersFeatureFileList.push_back( std::make_pair<string,double>(featureFileName,featureWeight) );

	_CBRM3Model.Init(modelConfiguration);
	_CBRM3Model.BuildBestClusterModel(&clustersFeatureFileList);
	_CBRM3Model.Clear();

}

void Test_CBRM3Model_2()
{
	string modelConfigurationFile = "D:/Research/DataFromServer/OutputTests/Test_CBRM3Model/W2VSimilarityToClusterCentroidDistribution_Configuration.txt";
	
	CBRM3Model CBRM3_Model;
	CBRM3_ModelConfiguration modelConfiguration;
	ModelConfiguration configuration;

	configuration.Init(modelConfigurationFile);
	modelConfiguration.Init(&configuration);

	CBRM3_Model.Init(modelConfiguration);
	CBRM3_Model.BuildModel();
	CBRM3_Model.Clear();
}

void Test_error()
{
	std::cout << "Start Create Clusters Files For All Queries " << std::endl;

	string firstQueryExpensionFile = "D:/Research/DataFromServer/OutputTests/TestErrorInW2V/Rm1TermExpension.txt";
	string outputDir = "D:/Research/DataFromServer/OutputTests/TestErrorInW2V/Out";

	int k(10);

	vector<string> wordToVecModelFileNameVec;
	wordToVecModelFileNameVec.push_back("D:/Research/DataFromServer/OutputTests/TestErrorInW2V/702.txt.dim.300.win.16.neg.10.bin");
	wordToVecModelFileNameVec.push_back("D:/Research/DataFromServer/OutputTests/TestErrorInW2V/704.txt.dim.300.win.16.neg.10.bin");

	CreateClusterFileForAllQueries( firstQueryExpensionFile, wordToVecModelFileNameVec, k, outputDir );
}

void Test_CreateQueriesFileWithWorkingSetDocno()
{
	string reRankingQueries = "D:/Research/DataFromServer/OutputTests/Test_CreateQueriesFileWithWorkingSetDocno/reRanking_queriesROBUST.xml";
	string outputDir = "D:/Research/DataFromServer/OutputTests/Test_CreateQueriesFileWithWorkingSetDocno/Out";
	vector<string> clustersQueryFileList ;
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_CreateQueriesFileWithWorkingSetDocno/expandedQuerys" ,&clustersQueryFileList );
	CreateQueriesFileWithWorkingSetDocno(reRankingQueries,outputDir, &clustersQueryFileList);
}

void Test_MargeConfigurationToFile_ForOneConfiguration()
{
	string outputDir = "D:/Research/DataFromServer/OutputTests/Test_MargeConfigurationToFile_ForOneConfiguration/Out";
	vector<string> clustersQueryFileList ;
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_MargeConfigurationToFile_ForOneConfiguration/evaluate_reRanking" ,&clustersQueryFileList );
	EvaluateFileOperation::CreateClusterPerformanceAverage(clustersQueryFileList,outputDir);
}

void Test_log()
{
	FILELog::ReportingLevel() = logDEBUG3;
	string time = NowTime();
	for (int i = 0; i < time.size(); i++)
	{
		if(time[i] == ':' || time[i] == '.')
		{
			time[i] = '_';
		}
	}
	string fileName = "D:/Research/DataFromServer/OutputTests/Test_log/" + time +".txt";
	FILE* log_fd = fopen( fileName.c_str() , "w" );
	Output2FILE::Stream() = log_fd;

	double expectedX = 1.2;
	double realX = 1.4;


	FILE_LOG(logWARNING) << "Ops, variable x should be " << expectedX << "; is " << realX;
	FILE_LOG(logERROR) << "Ops, variable x should be " << expectedX << "; is " << realX;
	FILE_LOG(logINFO) << "Ops, variable x should be " << expectedX << "; is " << realX;
	FILE_LOG(logDEBUG) << "Ops, variable x should be " << expectedX << "; is " << realX;
	FILE_LOG(logDEBUG1) << "Ops, variable x should be " << expectedX << "; is " << realX;
	FILE_LOG(logDEBUG2) << "Ops, variable x should be " << expectedX << "; is " << realX;
	FILE_LOG(logDEBUG3) << "Ops, variable x should be " << expectedX << "; is " << realX;
	FILE_LOG(logDEBUG4) << "Ops, variable x should be " << expectedX << "; is " << realX;


}

void TestRm3CV()
{
	string outputDir = "D:/Research/DataFromServer/OutputTests/TestRm3/CV/rm3";
	vector<string> fileList ;
	getAllFileFromDirectory( "D:/Research/DataSet/Models/Common/RM3/AP_PSWR" ,
		&fileList );;
	vector<string> filterdfileList ;
	for (int i = 0; i < fileList.size(); i++)
	{
		string file = fileList[i];
		vector<string> splited = split(file, '/');
		string fileName = splited[ splited.size() -1 ];
		if( fileName.find(".eval") != std::string::npos )
		{
			filterdfileList.push_back(file);
		}
	}

	Rm3 rm3;
	rm3.Init(&filterdfileList);
	rm3.CalculateCrossValidation(outputDir);
	
    outputDir = "D:/Research/DataFromServer/OutputTests/TestRm3/CV/rm3New";
	/*Rm3New rm3New;
	rm3New.Init(&filterdfileList);
	rm3New.CalculateCrossValidation(outputDir);*/
}

void TestbestoneAvg()
{
	string outputDir = "D:/Research/DataFromServer/OutputTests/TestRm3/bestonavg/rm3";
	vector<string> fileList ;
	getAllFileFromDirectory( "D:/Research/DataSet/Models/Common/RM3/AP_PSWR" ,
		&fileList );
	vector<string> filterdfileList ;
	for (int i = 0; i < fileList.size(); i++)
	{
		string file = fileList[i];
		vector<string> splited = split(file, '/');
		string fileName = splited[ splited.size() -1 ];
		if( fileName.find(".eval") != std::string::npos )
		{
			filterdfileList.push_back(file);
		}
	}

	Rm3 rm3;
	rm3.Init(&filterdfileList);
	rm3.CalculateBestoneAvg(outputDir);
	
	outputDir = "D:/Research/DataFromServer/OutputTests/TestRm3/bestonavg/rm3New";
	/*Rm3New rm3New;
	rm3New.Init(&filterdfileList);
	rm3New.CalculateBestoneAvg(outputDir);*/
}







void Test_BCBRM3Model_Proportional_Uniform()
{
	BCBRM3Model _model;
	BCBRM3_ModelConfiguration modelConfiguration;


	modelConfiguration.queries = "D:/Research/DataSet/queries/queriesROBUST.xml";
	modelConfiguration.queriesWeight = 0.2;
	modelConfiguration.qrels = "D:/Research/DataSet/qrels/qrelsROBUST";
	modelConfiguration.RM1ProbabilityFile = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/RM1ProbabilityFile.txt";
	modelConfiguration.rm1Weight = 0.3;
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterFiles" ,&modelConfiguration.queriesClustersFileList );
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/WordInClusterProbability/ProportionalToRM1Distribution" ,&modelConfiguration.queriesTermsProbabilityInClustersFileList );
	modelConfiguration.termsProbabilityInClustersWeight = 0.5;
	
	vector< pair<string,double> > clustersFeatureFileList;
	string featureFileName = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterPerQueryProbability/UniformDistribution/UniformClustersFeature.txt";
	double featureWeight = 1.0;
	clustersFeatureFileList.push_back( std::make_pair<string,double>(featureFileName,featureWeight) );
	/*featureFileName = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterPerQueryProbability/W2VQueryClusterSimilarityDistribution/W2VQueryClusterSimilarityClustersFeature.txt";
	featureWeight = 0.5;
	clustersFeatureFileList.push_back( std::make_pair<string,double>(featureFileName,featureWeight) );*/
	modelConfiguration.clustersFeatureFileList = clustersFeatureFileList;

	modelConfiguration.loopOnAllClustersWords = "false";

	modelConfiguration.outputDir = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/outputDir/ProportionalToRM1Clusters";

	_model.Init(modelConfiguration);
	_model.BuildModel();
	_model.Clear();
}

void Test_BCBRM3Model_W2VSimilarityToClusterCentroidForAllTermsDistribution_Uniform()
{
	BCBRM3Model _model;
	BCBRM3_ModelConfiguration modelConfiguration;


	modelConfiguration.queries = "D:/Research/DataSet/queries/queriesROBUST.xml";
	modelConfiguration.queriesWeight = 0.2;
	modelConfiguration.qrels = "D:/Research/DataSet/qrels/qrelsROBUST";
	modelConfiguration.RM1ProbabilityFile = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/RM1ProbabilityFile.txt";
	modelConfiguration.rm1Weight = 0.3;
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterFiles" ,&modelConfiguration.queriesClustersFileList );
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/WordInClusterProbability/W2VSimilarityToClusterCentroidForAllTermsDistribution" ,&modelConfiguration.queriesTermsProbabilityInClustersFileList );
	modelConfiguration.termsProbabilityInClustersWeight = 0.5;
	
	vector< pair<string,double> > clustersFeatureFileList;
	string featureFileName = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterPerQueryProbability/UniformDistribution/UniformClustersFeature.txt";
	double featureWeight = 1.0;
	clustersFeatureFileList.push_back( std::make_pair<string,double>(featureFileName,featureWeight) );
	/*featureFileName = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterPerQueryProbability/W2VQueryClusterSimilarityDistribution/W2VQueryClusterSimilarityClustersFeature.txt";
	featureWeight = 0.5;
	clustersFeatureFileList.push_back( std::make_pair<string,double>(featureFileName,featureWeight) );*/
	modelConfiguration.clustersFeatureFileList = clustersFeatureFileList;

	modelConfiguration.loopOnAllClustersWords = "false";

	modelConfiguration.outputDir = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/outputDir/ProportionalToRM1Clusters";

	_model.Init(modelConfiguration);
	_model.BuildModel();
	_model.Clear();
}

void Test_BCBRM3Model_W2VSimilarityToClusterCentroidDistribution_Uniform()
{
	BCBRM3Model _model;
	BCBRM3_ModelConfiguration modelConfiguration;


	modelConfiguration.queries = "D:/Research/DataSet/queries/queriesROBUST.xml";
	modelConfiguration.queriesWeight = 0.2;
	modelConfiguration.qrels = "D:/Research/DataSet/qrels/qrelsROBUST";
	modelConfiguration.RM1ProbabilityFile = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/RM1ProbabilityFile.txt";
	modelConfiguration.rm1Weight = 0.3;
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterFiles" ,&modelConfiguration.queriesClustersFileList );
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/WordInClusterProbability/UniformDistribution" ,&modelConfiguration.queriesTermsProbabilityInClustersFileList );
	modelConfiguration.termsProbabilityInClustersWeight = 0.5;
	
	vector< pair<string,double> > clustersFeatureFileList;
	string featureFileName = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterPerQueryProbability/UniformDistribution/UniformClustersFeature.txt";
	double featureWeight = 1.0;
	clustersFeatureFileList.push_back( std::make_pair<string,double>(featureFileName,featureWeight) );
	/*featureFileName = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterPerQueryProbability/W2VQueryClusterSimilarityDistribution/W2VQueryClusterSimilarityClustersFeature.txt";
	featureWeight = 0.5;
	clustersFeatureFileList.push_back( std::make_pair<string,double>(featureFileName,featureWeight) );*/
	modelConfiguration.clustersFeatureFileList = clustersFeatureFileList;

	modelConfiguration.loopOnAllClustersWords = "false";

	modelConfiguration.outputDir = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/outputDir/ProportionalToRM1Clusters";

	_model.Init(modelConfiguration);
	_model.BuildModel();
	_model.Clear();
}

void Test_QrelsFile()
{
	QrelsFile QrelsFile;
	std::cout << "Read Qrels File " << std::endl;
	string qrelsFileName = "D:/Research/DataSet/qrels/qrelsGOV2";
	QrelsFile.init(qrelsFileName);
	QrelsFile.SetScoreToOneOrZero();
}


void Test_CBModel_Uniform()
{
	CBModel _model;
	CB_ModelConfiguration modelConfiguration;


	modelConfiguration.queries = "D:/Research/DataSet/queries/queriesROBUST.xml";
	modelConfiguration.queriesWeight = 0.5;
	modelConfiguration.qrels = "D:/Research/DataSet/qrels/qrelsROBUST";
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterFiles" ,&modelConfiguration.queriesClustersFileList );
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/WordInClusterProbability/ProportionalToRM1Distribution" ,&modelConfiguration.queriesTermsProbabilityInClustersFileList );
	modelConfiguration.termsProbabilityInClustersWeight = 0.5;
	
	vector< pair<string,double> > clustersFeatureFileList;
	string featureFileName = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterPerQueryProbability/W2VQueryClusterSimilarityDistribution/W2VQueryClusterSimilarityClustersFeature.txt";
	double featureWeight = 1.0;
	clustersFeatureFileList.push_back( std::make_pair<string,double>(featureFileName,featureWeight) );
	/*featureFileName = "D:/Research/DataFromServer/OutputTests/Test_BCBRM3Model/ClusterPerQueryProbability/W2VQueryClusterSimilarityDistribution/W2VQueryClusterSimilarityClustersFeature.txt";
	featureWeight = 0.5;
	clustersFeatureFileList.push_back( std::make_pair<string,double>(featureFileName,featureWeight) );*/

	modelConfiguration.outputDir = "D:/Research/DataFromServer/OutputTests/Test_CBModel_Uniform/outputDir/";

	_model.Init(modelConfiguration);
	_model.BuildBestClusterModel(&clustersFeatureFileList);
	_model.Clear();
}

void test_GeneralModel_PrintClusterPerformanceByFeature()
{
	GeneralModel generalModel;

	string clusterEvaluationFile = "D:/Research/DataFromServer/OutputTests/PrintClusterPerformanceByFeature/Data/evaluate/051.evaluate";
	vector<string> clusterseature;
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/PrintClusterPerformanceByFeature/Data/ClusterPerQueryProbability" ,&clusterseature );

	vector<string> clusterEvaluationFileList;
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/PrintClusterPerformanceByFeature/Data/evaluate" ,&clusterEvaluationFileList );

	string pathToDir = "D:/Research/DataFromServer/OutputTests/PrintClusterPerformanceByFeature/Data/";
	generalModel.PrintClusterPerformanceByFeature(clusterEvaluationFileList , clusterseature , pathToDir);
}



void test_GeneralModel_CalcCorrlationClusterPerformanceByFeature()
{
	GeneralModel generalModel;

	vector<string> clusterseature;
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/PrintClusterPerformanceByFeature/Data/ClusterPerQueryProbability" ,&clusterseature );

	vector<string> clusterEvaluationFileList;
	getAllFileFromDirectory( "D:/Research/DataFromServer/OutputTests/PrintClusterPerformanceByFeature/Data/evaluate" ,&clusterEvaluationFileList );

	string pathToDir = "D:/Research/DataFromServer/OutputTests/PrintClusterPerformanceByFeature/Data/";
	generalModel.CalcCorrlationClusterPerformanceByFeature(clusterEvaluationFileList , clusterseature , pathToDir);
}

void test_GeneralModel_CreateReducedExtensionQueriesFile()
{
	GeneralModel generalModel;

	string queriesFileName = "D:/Research/DataFromServer/OutputTests/GeneralModel/CreateReducedExtensionQueriesFile/queries.xml";
	string pathToDir = "D:/Research/DataFromServer/OutputTests/GeneralModel/CreateReducedExtensionQueriesFile/";

	int numberOfTerms = 10;
	string outputQueriesFileName = pathToDir + "/10_queries.xml";
	generalModel.CreateReducedExtensionQueriesFile(queriesFileName , numberOfTerms , outputQueriesFileName);

	 numberOfTerms = 25;
	 outputQueriesFileName = pathToDir + "/25_queries.xml";
	generalModel.CreateReducedExtensionQueriesFile(queriesFileName , numberOfTerms , outputQueriesFileName);

	 numberOfTerms = 50;
	 outputQueriesFileName = pathToDir + "/50_queries.xml";
	generalModel.CreateReducedExtensionQueriesFile(queriesFileName , numberOfTerms , outputQueriesFileName);


	queriesFileName = "D:/Research/DataFromServer/OutputTests/GeneralModel/CreateReducedExtensionQueriesFile/queriesRegular.xml";
	 numberOfTerms = 10;
	 outputQueriesFileName = pathToDir + "/10_queriesRegular.xml";
	generalModel.CreateReducedExtensionQueriesFile(queriesFileName , numberOfTerms , outputQueriesFileName);

	 numberOfTerms = 25;
	 outputQueriesFileName = pathToDir + "/25_queriesRegular.xml";
	generalModel.CreateReducedExtensionQueriesFile(queriesFileName , numberOfTerms , outputQueriesFileName);

	 numberOfTerms = 52;
	 outputQueriesFileName = pathToDir + "/50_queriesRegular.xml";
	generalModel.CreateReducedExtensionQueriesFile(queriesFileName , numberOfTerms , outputQueriesFileName);

	
}

void main2()
{
	//Test_QrelsFile();
    // test_createQueryExpansionUsingKnnClustering_WordsComeFromDiscriminativeModel();
	// test_createQueryExpansionUsingKnnClusteringForQuery_WordsComeFromDiscriminativeModel();
	//test_CreateFilesForGnuplot_WordsComeFromDiscriminativeModel();
	// test_CompareBetweenCBToCBRM3Models_WordComeFromRM1();
	//test_DiscriminativeModelQuery();
	//trimEndTestFunctio();
	//test_CreateFromRM1FirstExpansionFirstOtherExpansionFromDiscriminativeModel();
	//test_crossValidationOnClusterScore();
	//test_createQueryExpansionUsingKnnClustering_NewRm3Model_WordsComeFromRM3Model();
	//test_CreateClusterFileForAllQueries();
	//test_ReadClusterFileForAllQueries();

	//test_CreateUniformDistribution();
	//test_CreateProportionalToRM1Distribution();
	//test_CreateClusterFileForAllQueries();
	//test_CreateW2VSimilaritydistributionDistribution();
	//test_ClustersFeatureCreator_CreateUniformDistribution();
	//test_ClustersFeatureCreator_CreateW2VQueryClusterSimilarityDistribution();
	//test_ClustersFeatureCreator_CreateSingleLinkSimilarityDistribution();
	//test_ClustersFeatureCreator_CreateCompleteLinkSimilarityDistribution();
	//test_ClustersFeatureCreator_CreateAverageLinkSimilarityDistribution();
	//test_ClustersFeatureCreator_CreateIDFDistribution();
	//test_QueriesClustersFeatures();
	//test_QueriesClustersFeatures_AP();
	//test_ModelConfiguration();
	
	//Test_WordInClusterProbabilityCreator_UniformDistribution();
	//Test_WordInClusterProbabilityCreator_UniformDistributionForAllTerms();
	//Test_WordInClusterProbabilityCreator_ProportionalToRM1Distribution();
	//Test_WordInClusterProbabilityCreator_ProportionalToRM1DistributionForAllTerms();
	//Test_WordInClusterProbabilityCreator_W2VSimilarityDistribution();
	//Test_WordInClusterProbabilityCreator_W2VSimilarityToClusterCentroidDistributionForAllTerms();
	//Test_InitProbabilityMatrixFromFile_W2VSimilarityToClusterCentroidDistributionForAllTerms();
	//Test_RM1Probability();
	//Test_QueriesFile();
	//Test_CBRM3Model();
	//Test_CBRM3Model_BuildBestClusterModel();
	//Test_CBRM3Model_2();
	//Test_error();
	//Test_CreateQueriesFileWithWorkingSetDocno();;
	//Test_MargeConfigurationToFile_ForOneConfiguration();
	//Test_log();
	//TestRm3CV();
	//TestbestoneAvg();

	//Test_BCBRM3Model_W2VSimilarityToClusterCentroidForAllTermsDistribution_Uniform();
	//Test_BCBRM3Model_W2VSimilarityToClusterCentroidDistribution_Uniform();


	//Test_CBModel_Uniform();

	//test_GeneralModel_PrintClusterPerformanceByFeature();
	//test_GeneralModel_CalcCorrlationClusterPerformanceByFeature();
	test_GeneralModel_CreateReducedExtensionQueriesFile();
}

