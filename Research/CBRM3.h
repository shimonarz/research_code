
/* 
 * File:   CBRM3.h
 * Author: Shimon Arzuan
 *
 * Created on March 31, 2017, 4:57 PM
 */

#ifndef CBRM3_H
#define CBRM3_H

#include "GeneralFunctions.h"
#include "ModelConfiguration.h"
#include "DataStructer.h"
#include "RM1Probability.h"
#include "WordInClusterProbability.h"
#include "ClustersFeature.h"

class CBRM3_ModelConfiguration : public  RM3_ModelConfiguration
{
public:

	vector<string> queriesClustersFileList;
	vector<string> queriesTermsProbabilityInClustersFileList;
	double termsProbabilityInClustersWeight;
	string clustersFile;
	string loopOnAllClustersWords;

	void virtual Init(ModelConfiguration *modelConfiguration) 
	{
		RM3_ModelConfiguration::Init(modelConfiguration);
		queriesClustersFileList = modelConfiguration->queriesClustersFileList;
		queriesTermsProbabilityInClustersFileList = modelConfiguration->queriesTermsProbabilityInClustersFileList;
		termsProbabilityInClustersWeight = modelConfiguration->termsProbabilityInClustersWeight;
		clustersFile = modelConfiguration->clustersFile;
		loopOnAllClustersWords = modelConfiguration->loopOnAllClustersWords;
	}
};

class CBRM3Query 
{
public:
	std::string QueryId;

	double OriginalQueryWeight;
	std::vector<Term*> OriginalQuery;
	
	double RM1TermsWeight;
	std::vector<Term*> RM1Terms;
	
	double ClustresTermsWeight;
	std::vector<Term*> ClustresTerms;

	void init()
	{
		QueryId = "";
		OriginalQueryWeight = RM1TermsWeight = ClustresTermsWeight = 0.0;
	}

	void InitOriginalQuery(string fullQuery)
	{
		// #combine( international organized crime )
		std::string qcopy( fullQuery);
		for ( size_t i=0; i<qcopy.size(); i++ )
		{
			if ( (qcopy[i]=='(')||(qcopy[i]==')')||(qcopy[i]==',')||(qcopy[i]=='"')||(qcopy[i]==':')) 
			{
				qcopy[i]=' ';
			}
		}

		vector<string> queryTerms = split(qcopy , ' ' );

		for (int i = 1; i < queryTerms.size(); i++)
		{
			Term* term = new Term(queryTerms[i]);
			OriginalQuery.push_back(term);
		}
	}

	void Clear()
	{
		int length = OriginalQuery.size();
		for (int i = 0; i < length; i++)
		{
			delete OriginalQuery[i];
		}
		OriginalQuery.clear();

		length = RM1Terms.size();
		for (int i = 0; i < length; i++)
		{
			delete RM1Terms[i];
		}
		RM1Terms.clear();

		length = ClustresTerms.size();
		for (int i = 0; i < length; i++)
		{
			delete ClustresTerms[i];
		}
		ClustresTerms.clear();
	}

	void ClearClustresExtended()
	{
		int length = ClustresTerms.size();
		for (int i = 0; i < length; i++)
		{
			delete ClustresTerms[i];
		}
		ClustresTerms.clear();
	}
};

typedef struct CBRM3Model
{
	QueriesFile Queries; // Queries 
	RM1Probability RM1_Probability; // P(w|RM1)
	WordInClusterProbabilityForAllQuery WordInClusterProbability; // P(w|c)
	CBRM3_ModelConfiguration ModelConfiguration; // ���� �� ������������ �� �����
	QueriesClusters ClustersTermsForAllQuery; // ���� �� �� �������� ������� ����

	void Init(CBRM3_ModelConfiguration configuration)
	{
		FILE_LOG(logINFO) << "Init CBRM3 model " ;
		ModelConfiguration = configuration;
		
		Queries.readFile(ModelConfiguration.queries);
		RM1_Probability.ReadFromFile(ModelConfiguration.RM1ProbabilityFile);
		WordInClusterProbability.init( &ModelConfiguration.queriesTermsProbabilityInClustersFileList );
		ClustersTermsForAllQuery.init(&ModelConfiguration.queriesClustersFileList);

		FILE_LOG(logINFO) << "Finish Init CBRM3 model " ;
	}

	void BuildModel()
	{
		int k = 1;
		FILE_LOG(logINFO) << "Start Build retrieval files.";

		string outputDir = ModelConfiguration.outputDir;
		map< string,string >::iterator query_it; // Queries
		for(query_it = Queries.Queries.begin() ; 
			query_it != Queries.Queries.end() ;
			++query_it)
		{
			string queryName = query_it->first;
			string queryOutputFile = ModelConfiguration.outputDir + "/" + queryName + ".clusters";

			cout << '\r' << "	Extend Query " <<  queryName  << " - Progress : " << k <<  '/' <<  Queries.Queries.size() <<
				".     Progress Percent " <<  (int)(((double)k/(double)Queries.Queries.size())*100.0)  <<  "%" << std::flush;
		
			FILE_LOG(logDEBUG) << "Extend Query " <<  queryName ;

			ofstream of;
			of.open ( queryOutputFile.c_str(), std::ofstream::out ); 
			of << "<parameters>\n";

			CBRM3Query extendedQuery;

			extendedQuery.InitOriginalQuery(query_it->second);
			extendedQuery.OriginalQueryWeight = ModelConfiguration.queriesWeight;

			extendedQuery.RM1Terms = RM1_Probability.GetRM1Terms(queryName);
			extendedQuery.RM1TermsWeight = ModelConfiguration.rm1Weight;

			extendedQuery.ClustresTermsWeight = ModelConfiguration.termsProbabilityInClustersWeight;
			
			ClustersInQuery* clusters = ClustersTermsForAllQuery.GetClustersInQuery(queryName);
			vector<string> clustersNames;
			clusters->GetClusterNames(&clustersNames);
			//cout << "Get-Cluster-Names " << clustersNames.size() << " \n";
			for (int i = 0; i < clustersNames.size(); i++)
			{
				string clusterName = clustersNames[i];
				vector<string> clusterTermsNames;
				clusters->GetClusterTerms(clusterName, &clusterTermsNames);
				//cout << "Get-Cluster-Terms for cluster  " << clusterName << " , size = " << clusterTermsNames.size() << " \n";
				vector<Term*> ClustresTerms;
				for (int j = 0; j < clusterTermsNames.size() ; j++)
				{
					string term = clusterTermsNames[j];
					double probability = WordInClusterProbability.GetWordInClusterProbability(queryName ,  clusterName , term);
					Term* t = new Term(term , probability);
					ClustresTerms.push_back(t);
				}
				extendedQuery.ClustresTerms = ClustresTerms;
				
				string clusterHeader = GetClusterQueryFullName(queryName , clusterName );
				writeExtendedQuery(&of,clusterHeader,&extendedQuery);
				extendedQuery.ClearClustresExtended();
			}

			of << "</parameters>";
			of.close(); 
			k++;
		}
		cout << '\n';
		FILE_LOG(logINFO) << "Finish Start Build retrieval files.";

	}
	
	void BuildBestClusterModel(vector< pair<string,double> > *clustersFeatureFileList)
	{
		ClustersFeature ClusterInQueryProbability;
		ClusterInQueryProbability.init(clustersFeatureFileList);
	
		int k = 1;
		FILE_LOG(logINFO) << "Start Build Best Cluster Model retrieval files.";

		
		string explLog = ModelConfiguration.outputDir + "//explLog.txt";
		ofstream ofexplLogOf;
		ofexplLogOf.open ( explLog.c_str(), std::ofstream::out ); 


		string queryOutputFile = ModelConfiguration.outputDir + "//queries.xml";
		ofstream of;
		of.open ( queryOutputFile.c_str(), std::ofstream::out ); 
		of << "<parameters>\n";

		string outputDir = ModelConfiguration.outputDir;
		map< string,string >::iterator query_it; // Queries
		for(query_it = Queries.Queries.begin() ; 
			query_it != Queries.Queries.end() ;
			++query_it)
		{
			string queryName = query_it->first;
			
			cout << '\r' << "	Extend Query " <<  queryName  << " - Progress : " << k <<  '/' <<  Queries.Queries.size() <<
				".     Progress Percent " <<  (int)(((double)k/(double)Queries.Queries.size())*100.0)  <<  "%" << std::flush;


			CBRM3Query extendedQuery;

			extendedQuery.InitOriginalQuery(query_it->second);
			extendedQuery.OriginalQueryWeight = ModelConfiguration.queriesWeight;

			extendedQuery.RM1Terms = RM1_Probability.GetRM1Terms(queryName);
			extendedQuery.RM1TermsWeight = ModelConfiguration.rm1Weight;

			extendedQuery.ClustresTermsWeight = ModelConfiguration.termsProbabilityInClustersWeight;
			
			string maxClustreName = ClusterInQueryProbability.getMaxCluster(queryName);

			ofexplLogOf << queryName << " , chosen cluster - " << maxClustreName << " : ";

			ClustersInQuery* clusters = ClustersTermsForAllQuery.GetClustersInQuery(queryName);
			vector<string> clustersNames;
			clusters->GetClusterTerms(maxClustreName, &clustersNames);

			vector<Term*> ClustresTerms;
			for (int j = 0; j < clustersNames.size() ; j++)
			{
				string term = clustersNames[j];
				double probability = WordInClusterProbability.GetWordInClusterProbability(queryName ,  maxClustreName , term);
				Term* t = new Term(term , probability);
				ClustresTerms.push_back(t);

				ofexplLogOf << "(" << term << "," << probability << ") ";
			}
			ofexplLogOf << "\n" ;

			extendedQuery.ClustresTerms = ClustresTerms;
			writeExtendedQuery(&of,queryName,&extendedQuery);
			extendedQuery.ClearClustresExtended();
			k++;
		}
		cout << '\n';

		of << "</parameters>";
		of.close(); 
		

		FILE_LOG(logINFO) << "Finish Start Build retrieval files.";
	}

    void writeExtendedQuery(ofstream *of,string queryNumber,CBRM3Query *expansion)
	{
		std::string query = buildQuery(expansion);

		*of << "<query>\n";
		*of << "<number>" + queryNumber + "</number>\n";
		*of << "<text>" + query + "</text>\n";
		*of << "</query>\n";
	}

	std::string buildQuery(CBRM3Query *terms)
	{
		std::string result;

		double originalQueryWeight = terms->OriginalQueryWeight;
		double expansionQueryWeight = terms->ClustresTermsWeight + terms->RM1TermsWeight;

		char originalQueryWeightStr[40];
		sprintf(originalQueryWeightStr,"%.32f",originalQueryWeight);
		std::ostringstream originalQueryWeightStream;
		//originalQueryWeightStream.precision(4);
		//originalQueryWeightStream<<fixed;    // for fixed point notation
		originalQueryWeightStream << originalQueryWeightStr ;
		

		char expansionQueryWeightStr[40];
		sprintf(expansionQueryWeightStr,"%.32f",expansionQueryWeight);
		std::ostringstream expansionQueryWeightStream;
		expansionQueryWeightStream << expansionQueryWeightStr;

		if(terms->ClustresTermsWeight == 0 && terms->RM1TermsWeight == 0)
		{
			// no expansion 
			result = "#combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + " )" ;
		}
		else if(terms->ClustresTermsWeight == 0 && terms->RM1TermsWeight != 0)
		{
			// only Rm1 expansion
			result = "#weight( " + 
				originalQueryWeightStream.str() + " #combine( #combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + ") ) " + 
				expansionQueryWeightStream.str()  + " #weight(  " + Term::convertTermToString(terms->RM1Terms,true,true) + " ) " + " )";
		}
		else if( terms->ClustresTermsWeight != 0 && terms->RM1TermsWeight == 0)
		{
			// only Cluster expansion
			result = "#weight( " + 
				originalQueryWeightStream.str() + " #combine( #combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + ") ) " + 
				expansionQueryWeightStream.str()  + " #weight(  " + Term::convertTermToString(terms->ClustresTerms,true,true) + " ) " + " )";

		}
		else
		{
			// Rm1 and Cluster expansion
			std::vector<Term*> margeTerms;
			// rm1 values
			int length = terms->RM1Terms.size();
			for (int i = 0; i < length; i++)
			{
				Term* newTerm = terms->RM1Terms[i]->Copy();
				newTerm->weight = ( terms->RM1TermsWeight / expansionQueryWeight) * newTerm->weight;
				margeTerms.push_back(newTerm);
			}

			length = terms->ClustresTerms.size();
			for (int i = 0; i < length; i++)
			{
				Term* t = terms->ClustresTerms[i];
				
				// Search if the Term is already exist
				Term* foundT = NULL;
				std::vector<Term*>::iterator it =  margeTerms.begin();
				for (it =  margeTerms.begin() ; it != margeTerms.end(); it++)
				{
					if((*it)->value == t->value)
					{
						foundT = *it;
						break;
					}
				}

				// if Term is already exist
				if(foundT != NULL)
				{
					foundT->weight += ( terms->ClustresTermsWeight / expansionQueryWeight) * t->weight;
				}
				else
				{
					Term* newTerm = t->Copy();
					newTerm->weight = ( terms->ClustresTermsWeight / expansionQueryWeight) * newTerm->weight;
					margeTerms.push_back(newTerm);
				}

			}

			double sum = 0;
			for (int i = 0; i < margeTerms.size(); i++)
			{
				sum += margeTerms[i]->weight;
			}
			for (int i = 0; i < margeTerms.size(); i++)
			{
				margeTerms[i]->weight /= sum;
			}

			result = "#weight( " + 
				originalQueryWeightStream.str() + " #combine( #combine( "  + 	Term::convertTermToString(terms->OriginalQuery) + ") ) " + 
				expansionQueryWeightStream.str()  + " #weight(  " + Term::convertTermToString(margeTerms,true,true) + " ) " + " )";

			// delete all new Terms that we create
			length = margeTerms.size();
			for (int i = 0; i < length; i++)
			{
				delete margeTerms[i];
			}
		}



		return result;
	}
	
	void Clear()
	{
		RM1_Probability.Clear();
		WordInClusterProbability.Clear();
		ClustersTermsForAllQuery.Clear();

	}

}CBRM3Model;


#endif
