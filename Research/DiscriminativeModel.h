#ifndef DiscriminativeModel_H
#define DiscriminativeModel_H

#include "GeneralFunctions.h"
#include "DataStructer.h"
#include "WordToVec.h"
#include "Evaluate.h"


enum TermsDistributionType
{
	Uniform,
	RelativeToRm1,
	W2VSimilarity
};

#define TermsDistributionTypeConvertor( x ) ( (x) == 1 ? Uniform : ( (x) == 2 ? RelativeToRm1 : W2VSimilarity) )


typedef struct ClusterScoreFileLine
{
	string clusterName;
	double clusterScore;

	ClusterScoreFileLine* Copy()
	{
		ClusterScoreFileLine *newLine = new ClusterScoreFileLine();
		newLine->clusterName = clusterName;
		newLine->clusterScore = clusterScore;
		return newLine;
	}

	bool operator <(const ClusterScoreFileLine& x)
	{
		return  clusterScore < x.clusterScore;
	}

}ClusterScoreFileLine;

typedef struct ClusterScoreFile
{
	map< string , ClusterScoreFileLine* > clusterScoreByName; // key is cluster name
	vector<ClusterScoreFileLine*> clusterScoreByNameVec;
	
	static bool sort_ClusterScoreFileLineByScore(const ClusterScoreFileLine* left, const ClusterScoreFileLine* right)
	{	
		return left->clusterScore > right->clusterScore  ;
	}

	string mainQuery;
	std::set<string> queriesNames;

	void init(string fileName)
	{
		ReadClusterScoreFileToVector(fileName);
	}

	ClusterScoreFileLine* getMaxIScoreForQuery(int i)
	{
		int len = clusterScoreByNameVec.size();
		if( i > len  || i <= 0)
		{
			return NULL;
		}
		return  clusterScoreByNameVec[i - 1];
	}

	void ReadClusterScoreFileToVector(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return ;
		}

		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			ClusterScoreFileLine* clusterScoreFileLine = new ClusterScoreFileLine();

			std::string qcopy( line);
			std::istringstream oss (qcopy);

			std::string clusterName;
			oss >> clusterName;
			if ( clusterName.empty()) {
				return ;    
			}

			std::string clusterScoreStr;
			oss >> clusterScoreStr;
			if ( clusterScoreStr.empty()) {
				return ;    
			}

			double clusterScore = atof(clusterScoreStr.c_str());

			clusterScoreFileLine->clusterName = clusterName;
			clusterScoreFileLine->clusterScore = clusterScore;

			clusterScoreByName.insert(std::make_pair<string, ClusterScoreFileLine*>(clusterName,clusterScoreFileLine));

			queriesNames.insert(clusterName);

			clusterScoreByNameVec.push_back(clusterScoreFileLine);

		}

		std::sort(clusterScoreByNameVec.begin(), clusterScoreByNameVec.end(),sort_ClusterScoreFileLineByScore);
		mainQuery = trimFormStrToEnd( *queriesNames.begin() , "_");

		finput.close();
	}

	std::set<string>* getClustersName()
	{
		return &queriesNames;
	}

	double getClustersScore(string clustreName)
	{
		return clusterScoreByName.find(clustreName)->second->clusterScore;
	}
	
	
	void Clear()
	{
		map< string , ClusterScoreFileLine* >::iterator it;
		for (it = clusterScoreByName.begin(); it != clusterScoreByName.end(); it++)
		{
			delete it->second;
		}
		clusterScoreByName.clear();
		queriesNames.clear();
	}
}ClusterScoreFile;

typedef struct ClusterScorePerConfig
{
	map< string , ClusterScoreFile* > clusterScoreByFile; // key = query name
	std::set<string> queriesNames;

	void init(vector<string>  *clusterScoreFile)
	{
		unsigned int length = clusterScoreFile->size();
		for (unsigned int i = 0; i < length; i++)
		{
			string fileName = (*clusterScoreFile)[i];
			ClusterScoreFile* clusterScoreFile = new ClusterScoreFile();
			clusterScoreFile->init( fileName );
			clusterScoreByFile.insert(std::make_pair<string , ClusterScoreFile*>(clusterScoreFile->mainQuery ,clusterScoreFile));
			queriesNames.insert(clusterScoreFile->mainQuery );
		}
	}

	ClusterScoreFileLine* getMaxIScoreForQuery(string query, int i)
	{
		map< string , ClusterScoreFile* >::iterator it = clusterScoreByFile.find(query);
		if( it != clusterScoreByFile.end() )
		{
			return it->second->getMaxIScoreForQuery(i);
		}
		else
		{
			return NULL;
		}
	}


	void Clear()
	{
		std::map<string,ClusterScoreFile*>::iterator it;
		for(it = clusterScoreByFile.begin() ; it != clusterScoreByFile.end(); it++ )
		{
			it->second->Clear();
			delete it->second;
		}
		clusterScoreByFile.clear();
		queriesNames.clear();
	}


}ClusterScorePerConfig;

typedef struct DiscriminativeModelCluster
{
	string clusterName;
	double clusterValue;
	double clusterScore;

	DiscriminativeModelCluster* Copy()
	{
		DiscriminativeModelCluster* value = new DiscriminativeModelCluster();
		value->clusterName = clusterName;
		value->clusterValue = clusterValue;
		value->clusterScore = clusterScore;
		return value;
	}

	friend bool operator<(const DiscriminativeModelCluster& l, const DiscriminativeModelCluster& r)
    {
		return l.clusterValue < r.clusterValue;
    }

	friend bool operator>(const DiscriminativeModelCluster& l, const DiscriminativeModelCluster& r)
    {
		return l.clusterValue > r.clusterValue;
    }

	

}DiscriminativeModelCluster;

typedef struct DiscriminativeModelQuery
{
	string queryName;
	double originalQueryValue;
	vector< DiscriminativeModelCluster > clustres;

	bool isSort;


	static bool sort_ClusterScoreFileLineByScore(const DiscriminativeModelCluster& left, const DiscriminativeModelCluster& right)
	{	
		return left.clusterScore > right.clusterScore  ;
	}

	void init(vector<EvaluatResultFileLine*> *evalLine, ClusterScoreFile *clusterScoreFile, string queryNme)
	{
		isSort = false;
		for( unsigned int i = 0 ; i< evalLine->size(); ++i)
		{
			EvaluatResultFileLine* resultFileLine = (*evalLine)[i];

			if(resultFileLine->queryName.compare("all") == 0)
			{
				continue;
			}

			if(resultFileLine->queryName.compare(queryNme) == 0)
			{
				queryName = resultFileLine->queryName;
				originalQueryValue = resultFileLine->evalValue;
				continue;
			}

			DiscriminativeModelCluster data;
			data.clusterName = resultFileLine->queryName;
			data.clusterValue = resultFileLine->evalValue;
			data.clusterScore = clusterScoreFile->getClustersScore(resultFileLine->queryName);
			clustres.push_back(data);	
		}

		
	}

	

	DiscriminativeModelCluster* getMaxIScoreForQuery(int i)
	{
		if(!isSort)
		{
			std::sort(clustres.begin(), clustres.end(),sort_ClusterScoreFileLineByScore);
			isSort = true;
		}
		int len = clustres.size();
		if( i > len  || i <= 0)
		{
			return NULL;
		}
		return  (clustres[i - 1]).Copy();
	}

	void Clear()
	{
		clustres.clear();
	}



}DiscriminativeModelQuery;

//evalMapFile
typedef struct DiscriminativeModelQueryByEval
{
	string queryName;
	map<string , DiscriminativeModelQuery* > discriminativeModelQueryByEval;

	EvaluateFile evaluateFile; // vector of all line in the file
	ClusterScoreFile clusterScoreFile; // vector of all line in the file

	void init(string evaluatFileName, string clusterScoreFileName)
	{
		evaluateFile.init(evaluatFileName);
		clusterScoreFile.init(clusterScoreFileName);
		string _queryNme = evaluateFile.getOriginalQueryName();
		queryName = _queryNme;
		std::map<string,vector<EvaluatResultFileLine*>*>::iterator it ;
		for(it =  evaluateFile.evalMapFile.begin(); it !=  evaluateFile.evalMapFile.end() ; it++ )
		{
			string evalName = it->first;
			DiscriminativeModelQuery* discriminativeModelQuery = new DiscriminativeModelQuery();
			vector<EvaluatResultFileLine*> *evalLine = it->second;
			discriminativeModelQuery->init(evalLine,&clusterScoreFile,queryName);
			discriminativeModelQueryByEval.insert(std::make_pair<string , DiscriminativeModelQuery*>(evalName,discriminativeModelQuery) );
		}
	}

	DiscriminativeModelQuery* getDiscriminativeModelQuery(string evalName)
	{
		return discriminativeModelQueryByEval.find(evalName)->second;
	}

	vector<string> getEvalNames()
	{
		vector<string> v;
		for(map<string , DiscriminativeModelQuery* >::iterator it = discriminativeModelQueryByEval.begin(); it != discriminativeModelQueryByEval.end(); ++it) 
		{
			v.push_back(it->first);
		}
		return v;
	}



	void Clear()
	{
		evaluateFile.Clear();
		clusterScoreFile.Clear();

		map< string , DiscriminativeModelQuery* >::iterator it;
		for (it = discriminativeModelQueryByEval.begin(); it != discriminativeModelQueryByEval.end(); it++)
		{
			it->second->Clear();
			delete it->second;
		}
		discriminativeModelQueryByEval.clear();
	}

}DiscriminativeModelQueryByEval;

typedef struct DiscriminativeModelQueryByConfig
{
	vector<DiscriminativeModelQueryByEval*>  DiscriminativeModelQueryByEvalList;
	
	void init(vector<string>* configFiles,vector<string>* clustresScoreFiles)
	{
		std::map<string,vector<string>*> orderFileQuery;
	    orderFileByConfig(configFiles, Query  , &orderFileQuery  );

		std::map<string,vector<string>*> orderClustresScoreFiles;
	    orderFileByConfig(clustresScoreFiles, Query  , &orderClustresScoreFiles  );

		std::map<string,vector<string>*>::iterator orderFileQueryit;
		for(orderFileQueryit = orderFileQuery.begin() ; orderFileQueryit != orderFileQuery.end(); orderFileQueryit++ )
		{
			vector<string>* queryFiles = orderFileQueryit->second;
			std::map<string,vector<string>*>::iterator it = orderClustresScoreFiles.find(orderFileQueryit->first);
			if( queryFiles->size() != 1 || it == orderClustresScoreFiles.end() )
			{
				cout << "Error - Receive wrong number of query files" << std::endl;
				continue;
			}

			DiscriminativeModelQueryByEval* discriminativeModelQuery = new DiscriminativeModelQueryByEval();;
			string evaluatFileName = (*queryFiles)[0] ;
			string clusterScoreFileName = (*it->second)[0];
			discriminativeModelQuery->init(evaluatFileName,clusterScoreFileName);
			DiscriminativeModelQueryByEvalList.push_back(discriminativeModelQuery);
		}

		for(std::map<string,vector<string>*>::iterator orderFilesIt = orderFileQuery.begin(); orderFilesIt != orderFileQuery.end(); orderFilesIt++) 
		{
			delete orderFilesIt->second;
		}
		orderFileQuery.clear();

		for(std::map<string,vector<string>*>::iterator orderFilesIt = orderClustresScoreFiles.begin(); orderFilesIt != orderClustresScoreFiles.end(); orderFilesIt++) 
		{
			delete orderFilesIt->second;
		}
		orderClustresScoreFiles.clear();
	}

	vector<string> getEvalNames()
	{
		return DiscriminativeModelQueryByEvalList[0]->getEvalNames();
	}

	void init(vector<string>* configFiles)
	{
		std::map<string,vector<string>*> orderFileQuery;
	    orderFileByConfig(configFiles, Query  , &orderFileQuery  );

		std::map<string,vector<string>*>::iterator orderFileQueryit;
		for(orderFileQueryit = orderFileQuery.begin() ; orderFileQueryit != orderFileQuery.end(); orderFileQueryit++ )
		{
			vector<string>* queryFiles = orderFileQueryit->second;
			if(queryFiles->size() != 2)
			{
				cout << "Error - Receive wrong number of query files" << std::endl;
				continue;
			}

			DiscriminativeModelQueryByEval* discriminativeModelQuery = new DiscriminativeModelQueryByEval();;
			string evaluatFileName;
			string clusterScoreFileName;

			if( (*queryFiles)[0].find("clusterGrade") != std::string::npos )
			{
				evaluatFileName = (*queryFiles)[1];
				clusterScoreFileName = (*queryFiles)[0];
			}
			else
			{
				evaluatFileName = (*queryFiles)[0];
				clusterScoreFileName = (*queryFiles)[1];
			}
			discriminativeModelQuery->init(evaluatFileName,clusterScoreFileName);

			DiscriminativeModelQueryByEvalList.push_back(discriminativeModelQuery);


		}
		for(std::map<string,vector<string>*>::iterator orderFilesIt = orderFileQuery.begin(); orderFilesIt != orderFileQuery.end(); orderFilesIt++) 
		{
			delete orderFilesIt->second;
		}
		orderFileQuery.clear();
	}

	/*static bool sort_DiscriminativeModelQueryByValue(const DiscriminativeModelCluster& left, const DiscriminativeModelCluster& right)
	{	
		return left.clusterValue > right.clusterValue  ||  left.clusterValue == right.clusterValue  && left.clusterScore > right.clusterScore;
	}
	static bool sort_DiscriminativeModelQueryByScore(const DiscriminativeModelCluster& left, const DiscriminativeModelCluster& right)
	{	
		return left.clusterScore > right.clusterScore  ||  left.clusterScore == right.clusterScore  && left.clusterValue > right.clusterValue;
	}*/

	static bool sort_DiscriminativeModelQueryByValue(const DiscriminativeModelCluster& left, const DiscriminativeModelCluster& right)
	{	
		return left.clusterValue > right.clusterValue  ;
	}
	static bool sort_DiscriminativeModelQueryByScore(const DiscriminativeModelCluster& left, const DiscriminativeModelCluster& right)
	{	
		return left.clusterScore > right.clusterScore  ;
	}

	void getClusterSortedByValue(string evalName , vector<double> *values, vector<double> *scores)
	{
		getClusterSortedBy(evalName, 1 , values , scores);
	}

	void getClusterSortedByScore(string evalName , vector<double> *values, vector<double> *scores)
	{
		getClusterSortedBy(evalName, 0 , values , scores);
	}

	void getClusterSortedBy(string evalName , int type , vector<double> *values, vector<double> *scores)
	{
		vector<DiscriminativeModelCluster> clustersAverageSortedBy;
		getClusterSortedBy( evalName, type , &clustersAverageSortedBy);
		unsigned int length = clustersAverageSortedBy.size();
		for (unsigned int i = 0; i < length; i++)
		{
			values->push_back(clustersAverageSortedBy[i].clusterValue);
			scores->push_back(clustersAverageSortedBy[i].clusterScore);
		}
	}

	void getClusterSortedBy(string evalName, int type , vector<DiscriminativeModelCluster> *clustersAverageSortedBy)
	{
		bool isFirstTime = true;
		unsigned int length = DiscriminativeModelQueryByEvalList.size();
		for (unsigned int i = 0; i < length; i++)
		{
			DiscriminativeModelQueryByEval * discriminativeModelQueryByEval = DiscriminativeModelQueryByEvalList[i];
			DiscriminativeModelQuery* discriminativeModelQuery = discriminativeModelQueryByEval->getDiscriminativeModelQuery(evalName);

			vector< DiscriminativeModelCluster > *clustres = &discriminativeModelQuery->clustres;
			if(type == 1) // sort by value
			{
				std::sort(clustres->begin(), clustres->end(),sort_DiscriminativeModelQueryByValue);
			}
			else // sort by scroe
			{
				std::sort(clustres->begin(), clustres->end(),sort_DiscriminativeModelQueryByScore);
			}
			if (isFirstTime)
			{
				for (int j = 0; j < clustres->size(); j++)
				{
					clustersAverageSortedBy->push_back((*clustres)[j]);
				}
				isFirstTime = false;
			}
			else
			{
				for (int j = 0; j < clustres->size(); j++)
				{
					(*clustersAverageSortedBy)[j].clusterScore +=  (*clustres)[j].clusterScore;
					(*clustersAverageSortedBy)[j].clusterValue += (*clustres)[j].clusterValue;
				}
			}
		}
		for (int j = 0; j < clustersAverageSortedBy->size(); j++)
		{
			(*clustersAverageSortedBy)[j].clusterScore /=  length;
			(*clustersAverageSortedBy)[j].clusterValue /= length;
		}
	}


	vector< DiscriminativeModelCluster > getListClusterSortedByValue(string evalName)
	{	
	    return getListClusterSortedByValue( evalName , 1);
	}

	vector< DiscriminativeModelCluster > getListClusterSortedByScore(string evalName)
	{
		 return getListClusterSortedByValue( evalName ,  0);
	}

	vector< DiscriminativeModelCluster > getListClusterSortedByValue(string evalName , int type)
	{	
	    bool isFirstTime = true;
		vector< DiscriminativeModelCluster > ClustersAverageSortedByValue;
		unsigned int length = DiscriminativeModelQueryByEvalList.size();
		for (unsigned int i = 0; i < length; i++)
		{
			DiscriminativeModelQueryByEval * discriminativeModelQueryByEval = DiscriminativeModelQueryByEvalList[i];
			DiscriminativeModelQuery* discriminativeModelQuery = discriminativeModelQueryByEval->getDiscriminativeModelQuery(evalName);

			vector< DiscriminativeModelCluster > *clustres = &discriminativeModelQuery->clustres;

			if(type == 1) // sort by value
			{
				std::sort(clustres->begin(), clustres->end(),sort_DiscriminativeModelQueryByValue);
			}
			else // sort by scroe
			{
				std::sort(clustres->begin(), clustres->end(),sort_DiscriminativeModelQueryByScore);
			}
			if (isFirstTime)
			{
				for (int j = 0; j < clustres->size(); j++)
				{
					ClustersAverageSortedByValue.push_back((*clustres)[j]);
				}
				isFirstTime = false;
			}
			else
			{
				for (int j = 0; j < clustres->size(); j++)
				{
					ClustersAverageSortedByValue[j].clusterScore +=  (*clustres)[j].clusterScore;
					ClustersAverageSortedByValue[j].clusterValue += (*clustres)[j].clusterValue;
				}
			}
		}
		for (int j = 0; j < ClustersAverageSortedByValue.size(); j++)
		{
			ClustersAverageSortedByValue[j].clusterScore /=  length;
			ClustersAverageSortedByValue[j].clusterValue /= length;
		}
		return ClustersAverageSortedByValue;
	}

	double getOriginalQueryValue(string evalName)
	{
		double originalQueryValue = 0;
		unsigned int length = DiscriminativeModelQueryByEvalList.size();
		for (unsigned int i = 0; i < length; i++)
		{
			DiscriminativeModelQueryByEval * discriminativeModelQueryByEval = DiscriminativeModelQueryByEvalList[i];
			DiscriminativeModelQuery* discriminativeModelQuery = discriminativeModelQueryByEval->getDiscriminativeModelQuery(evalName);
			originalQueryValue += discriminativeModelQuery->originalQueryValue;
		}
		return originalQueryValue / length;
	}

	void Clear()
	{
		for (unsigned int i = 0; i < DiscriminativeModelQueryByEvalList.size(); i++)
		{
			DiscriminativeModelQueryByEvalList[i]->Clear();
			delete DiscriminativeModelQueryByEvalList[i];
		}
		DiscriminativeModelQueryByEvalList.clear();
	}

}DiscriminativeModelQueryByConfig;

typedef struct DiscriminativeModelQueryByMultipleConfig
{
	vector<DiscriminativeModelQueryByConfig*>  DiscriminativeModelQueryByConfigList;

	void add(DiscriminativeModelQueryByConfig *discriminativeModelQueryByConfig)
	{
		DiscriminativeModelQueryByConfigList.push_back(discriminativeModelQueryByConfig);
	}

	void getClusterSortedByValue(string evalName , vector<double> *values, vector<double> *scores)
	{
		getClusterSortedBy(evalName, 1 , values , scores);
	}

	void getClusterSortedByScore(string evalName , vector<double> *values, vector<double> *scores)
	{
		getClusterSortedBy(evalName, 0 , values , scores);
	}

	void getClusterSortedBy(string evalName , int type , vector<double> *values, vector<double> *scores)
	{
		vector<DiscriminativeModelCluster> clustersAverageSortedBy;
		getClusterSortedBy( evalName, type , &clustersAverageSortedBy);
		unsigned int length = clustersAverageSortedBy.size();
		for (unsigned int i = 0; i < length; i++)
		{
			values->push_back(clustersAverageSortedBy[i].clusterValue);
			scores->push_back(clustersAverageSortedBy[i].clusterScore);
		}
	}

	void getClusterSortedBy(string evalName, int type , vector<DiscriminativeModelCluster> *clustersAverageSortedBy)
	{
		bool isFirstTime = true;
		unsigned int length = DiscriminativeModelQueryByConfigList.size();
		for (int i = 0; i < length; i++)
		{
			vector< DiscriminativeModelCluster > clustres;
			if(type == 1) // sort by value
			{
				clustres = DiscriminativeModelQueryByConfigList[i]->getListClusterSortedByValue(evalName);
			}
			else // sort by scroe
			{
				clustres = DiscriminativeModelQueryByConfigList[i]->getListClusterSortedByScore(evalName);
			}

			if (isFirstTime)
			{
				for (int j = 0; j < clustres.size(); j++)
				{
					clustersAverageSortedBy->push_back((clustres)[j]);
				}
				isFirstTime = false;
			}
			else
			{
				for (int j = 0; j < clustres.size(); j++)
				{
					(*clustersAverageSortedBy)[j].clusterScore +=  (clustres)[j].clusterScore;
					(*clustersAverageSortedBy)[j].clusterValue += (clustres)[j].clusterValue;
				}
			}
		}
		for (int j = 0; j < clustersAverageSortedBy->size(); j++)
		{
			(*clustersAverageSortedBy)[j].clusterScore /=  length;
			(*clustersAverageSortedBy)[j].clusterValue /= length;
		}
	}


	double getOriginalQueryValue(string evalName)
	{
		double originalQueryValue = 0;
		unsigned int length = DiscriminativeModelQueryByConfigList.size();
		for (int i = 0; i < length; i++)
		{
			originalQueryValue += DiscriminativeModelQueryByConfigList[i]->getOriginalQueryValue(evalName);
		}
		return originalQueryValue / length;
	}

	vector<string> getEvalNames()
	{
		return DiscriminativeModelQueryByConfigList[0]->getEvalNames();
	}

	void Clear()
	{
		DiscriminativeModelQueryByConfigList.clear();
	}

}DiscriminativeModelQueryByMultipleConfig;

typedef struct KeyValuePair
{
	string Key;
	string Value;

	void Set(string fileLine) // file line look like this: "<Key>:<Value>"
	{
		if(  (!fileLine.empty()) && fileLine.size() > 2)
		{
			vector<string> splitLine = split(fileLine,':');
			if(splitLine.size() != 2)
			{
				std::cout << "Line of KeyValuePair is not in the right format ( format is <Key>:<Value> , line is " << fileLine << ") " << std::endl;
				return;
			}
			Key = splitLine[0];
			Value = splitLine[1];
		}
		else
		{
			std::cout << "Line of KeyValuePair is not in the right format ( format is <Key>:<Value> , line is " << fileLine << ") " << std::endl;
		}
	}

	double getkey()
	{
		return atof(Key.c_str());
	}
	double getValue()
	{
		return atof(Value.c_str());
	}

}KeyValuePair;

typedef struct QueryCollectionFeatures
{
	
	std::map<double,string> CollectionFeatures;
	std::map<string,double> WordToFeatures;

	void Set(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open QueryCollectionFeatures file: " << fileName << std::endl;
			return;
		}

		KeyValuePair wordFeaturePair ;
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;
			wordFeaturePair.Set(line);
			CollectionFeatures.insert(std::make_pair<double,string>(wordFeaturePair.getkey(),wordFeaturePair.Value));
			WordToFeatures.insert(std::make_pair<string,double>(wordFeaturePair.Value , wordFeaturePair.getkey()));
		}
		finput.close();
	}

	double GetWordFeature(string word)
	{
		map<string,double>::iterator it = WordToFeatures.find(word);
		if(it == WordToFeatures.end())
		{
			std::cout << "Word not found" << std::endl;
			return -2;

		}
		else
		{
			return it->second;
		}
	}

	string GetQueryForFeature(double featuresId)
	{
		map<double,string>::iterator it = CollectionFeatures.find(featuresId);
		if(it == CollectionFeatures.end())
		{
			std::cout << "features not found" << std::endl;
			return "";

		}
		else
		{
			return it->second;
		}
	}

	void Clear()
	{
		CollectionFeatures.clear();
		WordToFeatures.clear();
	}


}QueryCollectionFeatures;

typedef struct SupportVector
{
	int SupportVectorNumber;
	double Weight;
	map<double,double> FeaturesProbabilities;

	void Set(int number , string line)
	{
		if(  line.empty() || line.size() <= 2)
		{
			std::cerr << "The line of model file not in the right format " << line << std::endl;
			return;
		}
		SupportVectorNumber = number;
		KeyValuePair featuresProbabilitiesPair ;
		vector<string> splitLine = split(line,' ');
		Weight = atof( splitLine[0].c_str() );

		int length = splitLine.size();
		for (int i = 1; i < length -1; i++)
		{
			featuresProbabilitiesPair.Set(splitLine[i]);
			FeaturesProbabilities.insert(std::make_pair<double,double>( featuresProbabilitiesPair.getkey(),featuresProbabilitiesPair.getValue()));
		}
	}
	
	double getFeaturesProbability(double featuresId)
	{
		map<double,double>::iterator it = FeaturesProbabilities.find(featuresId);
		if(it == FeaturesProbabilities.end())
		{
			std::cout << "features not found" << std::endl;
			return 0.0;
		}
		else
		{
			return Weight * (it->second);
		}
	}

	void getFeatures(std::set<double> *features)
	{
		for(map<double,double>::iterator it = FeaturesProbabilities.begin(); it != FeaturesProbabilities.end(); ++it) 
		{
		  features->insert(it->first);
		}
	}

}SupportVector;

typedef struct QueryModelFile
{
	int NumberOfSupportVectors;
	vector<SupportVector*> SupportVectorList;
	std::set<double> FeaturesId;

	void Set(string fileName)
	{
		ifstream finput;
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open QueryCollectionFeatures file: " << fileName << std::endl;
			return;
		}

		int currntNumberOfLine = 0;
		int supportVectorNumber = 1;
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			currntNumberOfLine++;
			if(line.size() == 0 ) continue;

			if(currntNumberOfLine == 10) // get number of support vector
			{
				vector<string> splitLine = split(line,' ');
				NumberOfSupportVectors =  atoi ( splitLine[splitLine.size() -1].c_str() );
			}
			else if(currntNumberOfLine > 11) // get support vector
			{
				SupportVector* sv = new SupportVector();
				sv->Set(supportVectorNumber,line);
				sv->getFeatures(&FeaturesId);
				SupportVectorList.push_back(sv);
				supportVectorNumber++;
			}
		}
		finput.close();
	}

	double getFeaturesProbability(double featuresId)
	{
		double featuresProbability = 0;
		int length = SupportVectorList.size();
		for (int i = 0; i < length; i++)
		{
			featuresProbability += SupportVectorList[i]->getFeaturesProbability(featuresId);
		}
		return featuresProbability;
	}

	// if the interpolation of the feature is grater than 0, is belong to the negative Model
	void getPositiveAndNegativeModelFeatures(vector< std::pair<double,double> > *positiveModelFeatures,vector< std::pair<double,double> > *negativeModelFeatures)
	{
		KeyValuePair pair;
		for(std::set<double>::iterator it = FeaturesId.begin(); it != FeaturesId.end(); ++it) 
		{
			double featuresId = *it;
			double featuresProbability = getFeaturesProbability(featuresId);

			if(featuresProbability <= 0 )
			{
				positiveModelFeatures->push_back(std::make_pair<double,double>(featuresId,-1 * featuresProbability));
			}
			else
			{
				negativeModelFeatures->push_back(std::make_pair<double,double>(featuresId, -1 * featuresProbability));
			}
		}
		std::sort(positiveModelFeatures->begin(), positiveModelFeatures->end(), descending_order_compare_DoublePair_by_second());
		std::sort(negativeModelFeatures->begin(), negativeModelFeatures->end(), increasing_order_compare_DoublePair_by_second());
	}


	void Clear()
	{
		int length = SupportVectorList.size();
		for (int i = 1; i < length; i++)
		{
			delete SupportVectorList[i];
		}
		SupportVectorList.clear();
	}

}QueryModelFile;

typedef struct QueryModel
{
	QueryModelFile queryModelFile;
	QueryCollectionFeatures queryCollectionFeatures;

	void Set(string queryModelFileName, string queryCollectionFeaturesFileName)
	{
		queryModelFile.Set(queryModelFileName);
		queryCollectionFeatures.Set(queryCollectionFeaturesFileName);
	}


	void getWordForExpansion(unsigned int positiveWordsNumber, unsigned int negativeWordsNumber , vector<Term*> *wordsWithProbability)
	{
		vector< std::pair<double,double> > positiveModelFeatures; // featuresId,abs(featuresProbability)
		vector< std::pair<double,double> > negativeModelFeatures; // featuresId,abs(featuresProbability)
		queryModelFile.getPositiveAndNegativeModelFeatures(&positiveModelFeatures,&negativeModelFeatures);

		if(positiveModelFeatures.size() < positiveWordsNumber )
		{
			std::cout << "There is not enough words in the positive Model ( the number of words in the model is : " << positiveModelFeatures.size()  << ", the requested words is  "<< positiveWordsNumber << ")" <<  std::endl;
			return;
		}
		if(negativeModelFeatures.size() < negativeWordsNumber )
		{
			std::cout << "There is not enough words in the negative Model ( the number of words in the model is : " << negativeModelFeatures.size()  << ", the requested words is  " << negativeWordsNumber << ")" <<  std::endl;
			return;
		}


		// Get all words from the positive model
		for (unsigned int i = 0; i < positiveWordsNumber; i++)
		{
			double featureId = positiveModelFeatures[i].first;;
			double featureProbability = positiveModelFeatures[i].second;
			string word = queryCollectionFeatures.GetQueryForFeature(featureId);
			
			Term *t = new Term();
			t->value = word;
			t->weight = featureProbability;
			wordsWithProbability->push_back(t);
		}

		// Get all words from the negative model
		for (unsigned int i = 0; i < negativeWordsNumber; i++)
		{
			double featureId = negativeModelFeatures[i].first;;
			double featureProbability = negativeModelFeatures[i].second;
			string word = queryCollectionFeatures.GetQueryForFeature(featureId);
			
			Term *t = new Term();
			t->value = word;
			t->weight = featureProbability;
			wordsWithProbability->push_back(t);
		}

		/*
		// normalize the Probabilities
		double sumOfAllProbabilities = 0;
		for (int i = 0; i < wordsWithProbability->size(); i++)
		{
			sumOfAllProbabilities += (*wordsWithProbability)[i]->weight;
		}
		for (int i = 0; i < wordsWithProbability->size(); i++)
		{
			double featureProbability = (*wordsWithProbability)[i]->weight;
			(*wordsWithProbability)[i]->weight = featureProbability /sumOfAllProbabilities;
		}*/
	}

	void SetScoreToTerms( std::vector<Term*> * expensionTerms)
	{
		int length = expensionTerms->size();
		for (int i = 0; i < length; i++)
		{
			Term* t = (*expensionTerms)[i];
			double featureId = queryCollectionFeatures.GetWordFeature(t->value);
			if(featureId == -2)
			{
				t->weight = -1 * 2;
			}
			else
			{
				double featureProbability = queryModelFile.getFeaturesProbability(featureId);
				t->weight = -1 * featureProbability;
			}
		}
	}

	void Clear()
	{
		queryModelFile.Clear();
		queryCollectionFeatures.Clear();
	}


}QueryModel;

typedef struct QueriesForCV
{
	map< string , DiscriminativeModelCluster* > clusterScoreByFile; // key = query name  ,value is max cluster
	
	double SumOfScore;
	double SumOfPerformance;
	int index;

	void init(int iIndex)
	{
		SumOfScore = 0;
		SumOfPerformance = 0;
		index = iIndex;
	}

	void AddQuery(DiscriminativeModelQuery* queryData)
	{
		string queryName = queryData->queryName;
		DiscriminativeModelCluster *discriminativeModelCluster = queryData->getMaxIScoreForQuery(index);

		map< string , DiscriminativeModelCluster* >::iterator it = clusterScoreByFile.find(queryName);
		if(it != clusterScoreByFile.end() )
		{
			DiscriminativeModelCluster *current = it->second;
			if(current->clusterScore < discriminativeModelCluster->clusterScore)
			{
				it->second = discriminativeModelCluster;

				SumOfPerformance -= current->clusterValue;
				SumOfScore -= current->clusterScore;

				SumOfPerformance += discriminativeModelCluster->clusterValue;
				SumOfScore += discriminativeModelCluster->clusterScore;
			}
			else if(current->clusterScore == discriminativeModelCluster->clusterScore && current->clusterValue < discriminativeModelCluster->clusterValue)
			{
				it->second = discriminativeModelCluster;

				SumOfPerformance -= current->clusterValue;

				SumOfPerformance += discriminativeModelCluster->clusterValue;

			}

		}
		else
		{
			clusterScoreByFile.insert(std::make_pair<string,DiscriminativeModelCluster*>(queryName,discriminativeModelCluster));
			SumOfPerformance += discriminativeModelCluster->clusterValue;
			SumOfScore += discriminativeModelCluster->clusterScore;
		}
		
	}

	double GetPerformanceNotIncludeQuery(string queryName)
	{
		map< string , DiscriminativeModelCluster* >::iterator it = clusterScoreByFile.find(queryName);
		if(it != clusterScoreByFile.end())
		{
			return (SumOfPerformance - it->second->clusterValue) / (clusterScoreByFile.size() - 1) ;
		}
		else
		{
			return -5;
		}
	}

	double getQueryPerformance(string queryName)
	{
		map< string , DiscriminativeModelCluster* >::iterator it = clusterScoreByFile.find(queryName);
		if(it != clusterScoreByFile.end())
		{
			return it->second->clusterValue ;
		}
		else
		{
			return -5;
		}
	}


	double getAverageScore()
	{
		double avg = 0;
		map< string , DiscriminativeModelCluster* >::iterator it;
		for(it = clusterScoreByFile.begin() ; it != clusterScoreByFile.end() ; ++it)
		{
			avg += it->second->clusterValue	;
		}
		avg = avg / clusterScoreByFile.size();
		return avg;
	}

	double getAveragePerformanc()
	{
		double avg = 0;
		map< string , DiscriminativeModelCluster* >::iterator it;
		for(it = clusterScoreByFile.begin() ; it != clusterScoreByFile.end() ; ++it)
		{
			avg += it->second->clusterScore	;
		}
		avg = avg / clusterScoreByFile.size();
		return avg;
	}


	void Clear()
	{
		map< string , DiscriminativeModelCluster* >::iterator it ;
		for(it = clusterScoreByFile.begin() ; it != clusterScoreByFile.end() ; it++)
		{
			delete it->second;
		}
		clusterScoreByFile.clear();
	}

}QueriesForCV;

typedef struct ConfigValuesPerEvalForCV
{
	string evalName;
	map< string , QueriesForCV* > clusterScoreByFile; // key = config name , value 
	int iIndex;
	std::set<string> QueryNameSet;

	void init(int i , string eval)
	{
		iIndex = i;
		evalName = eval;
	}

	void Add(string mainConfig , DiscriminativeModelQueryByEval* discriminativeModelQueryByEval)
	{
		map<string , DiscriminativeModelQuery* > *queryByEval = &discriminativeModelQueryByEval->discriminativeModelQueryByEval;
		map<string , DiscriminativeModelQuery* >::iterator it = queryByEval->find(evalName);

		if( it != queryByEval->end())
		{
			string evalName = it->first;
			DiscriminativeModelQuery *query = it->second;

			if(query == NULL)
			{
				cout << "Error - cannot find query" << std::endl;
			}
			else 
			{
				QueryNameSet.insert(query->queryName);
				map<string , QueriesForCV* >::iterator it2 = clusterScoreByFile.find(mainConfig);
				if(it2 != clusterScoreByFile.end())
				{
					QueriesForCV* cv = it2->second;
					cv->AddQuery(query);
				}
				else
				{
					QueriesForCV* cv = new QueriesForCV();
					cv->init(iIndex);
					cv->AddQuery(query);
					clusterScoreByFile.insert(std::make_pair<string,QueriesForCV*>(mainConfig,cv));
				}
			}
		}	
	}


	string getMaxConfigForNotIncludeQuery(string query)
	{
		string maxConfig = clusterScoreByFile.begin()->first;
		double maxValue = clusterScoreByFile.begin()->second->GetPerformanceNotIncludeQuery(query);
		map< string , QueriesForCV* >::iterator it;
		for( it = clusterScoreByFile.begin() ; it != clusterScoreByFile.end() ; ++it)
		{
			double value = it->second->GetPerformanceNotIncludeQuery(query);
			if(maxValue < value)
			{
				maxValue = value;
				maxConfig = it->first;
			}
		}
		return maxConfig;
	}

	double getQueryValuePerConfiguration(string query, string config )
	{
		map< string , QueriesForCV* >::iterator it = clusterScoreByFile.find(config);
		return it->second->getQueryPerformance(query);
	}


	double getBestClusterPerformance(string query)
	{
		double maxValue = clusterScoreByFile.begin()->second->getQueryPerformance(query);
		map< string , QueriesForCV* >::iterator it;
		for( it = clusterScoreByFile.begin() ; it != clusterScoreByFile.end() ; ++it)
		{
			double value = it->second->getQueryPerformance(query);
			if(maxValue < value)
			{
				maxValue = value;
			}
		}
		return maxValue;
	}


	void Clear()
	{
		map<string , QueriesForCV* >::iterator it;
		for(it = clusterScoreByFile.begin() ; it != clusterScoreByFile.end() ; ++it)
		{
			it->second->Clear();
			delete it->second;
		}
		clusterScoreByFile.clear();
	}

}ConfigValuesPerEvalForCV;

typedef struct ClusterScorePerAllConfigForCV
{
	map< string , ConfigValuesPerEvalForCV* > clusterScoreByFile; // key = eval-name , all config
	int index;
	vector<string> evalNames;

	void init(int i)
	{
		index = i;
	}

	void Add(string mainConfig , DiscriminativeModelQueryByConfig* discriminativeModelQueryByConfig)
	{
		evalNames = discriminativeModelQueryByConfig->getEvalNames();
		int length = evalNames.size();
		for (int i = 0; i < length; i++)
		{
			string eval = evalNames[i];
			map< string , ConfigValuesPerEvalForCV* >::iterator it =  clusterScoreByFile.find(eval);
			ConfigValuesPerEvalForCV* cv;
			if(it != clusterScoreByFile.end() ){
				cv = it->second;
			}
			else{
				cv = new ConfigValuesPerEvalForCV();
				cv->init(index,eval);
				clusterScoreByFile.insert(std::make_pair<string ,ConfigValuesPerEvalForCV*>(eval , cv) );
			}

			vector<DiscriminativeModelQueryByEval*> *vec = &discriminativeModelQueryByConfig->DiscriminativeModelQueryByEvalList;
			int len = vec->size();
			for (int j = 0; j < len; j++){
				DiscriminativeModelQueryByEval* discriminativeModelQueryByEval = (*vec)[j];
				cv->Add(mainConfig,discriminativeModelQueryByEval);
			}
		}
	}

	vector<string> GetQueryName()
	{
		std::set<string> *QueryNameSet = &clusterScoreByFile.begin()->second->QueryNameSet;
		std::vector<string> output(QueryNameSet->size());
		std::copy(QueryNameSet->begin(), QueryNameSet->end(), output.begin());
		return output;
	}


	double CalcBestonAvg(string eval)
	{
		map< string , ConfigValuesPerEvalForCV* >::iterator it =  clusterScoreByFile.find(eval);
		ConfigValuesPerEvalForCV* cv;
		if(it != clusterScoreByFile.end() )
		{
			cv = it->second;
			
			double avgValue = 0;
			vector<string> queryNames = GetQueryName();
			int length = queryNames.size();

			for (int i = 0; i < length; i++)
			{
				string query = queryNames[i];
				double queryValue = cv->getBestClusterPerformance(query);
				avgValue += queryValue;
			}

			avgValue = avgValue / length;
			return  avgValue;
		}
		else{
			cout << "Error - eval don't exists" << std::endl;
			return -5;
		}
	}


	double CalcCV(string eval)
	{
		map< string , ConfigValuesPerEvalForCV* >::iterator it =  clusterScoreByFile.find(eval);
		ConfigValuesPerEvalForCV* cv;
		if(it != clusterScoreByFile.end() )
		{
			cv = it->second;

			double avgValue = 0;

			vector<string> queryNames = GetQueryName();
			int length = queryNames.size();
			for (int i = 0; i < length; i++)
			{
				string query = queryNames[i];
				string maxConfig = cv->getMaxConfigForNotIncludeQuery(query);
				double queryValue = cv->getQueryValuePerConfiguration(query,maxConfig);
				avgValue += queryValue;
			}
			avgValue = avgValue / length;
			return  avgValue;
		}
		else{
			cout << "Error - eval don't exists" << std::endl;
			return -5;
		}


	}



	void Clear()
	{
		map<string , ConfigValuesPerEvalForCV* >::iterator it;
		for(it = clusterScoreByFile.begin() ; it != clusterScoreByFile.end() ; ++it)
		{
			it->second->Clear();
			delete it->second;
		}
		clusterScoreByFile.clear();
	}

}ClusterScorePerAllConfigForCV;


typedef struct CVPerEval
{
	string evalName;
	double CVValue;
}CVPerEval;

typedef struct CBDM
{

static void createFirstTimeQueryExpansion_WordsComeFromDiscriminativeModel(string originalQueriesFileName , 
																			 vector<string> queryModelFileNames , 
																			 vector<string> queryCollectionFeaturesFileNames ,
																			 int positiveWordsNumber , 
																			 int negativeWordsNumber,
																			 string outputFile)
{
	ofstream of;
	of.open ( outputFile.c_str(), std::ofstream::out ); 

	QueriesFile queriesFile;
	queriesFile.readFile(originalQueriesFileName);

	map<string,string > *Queries = &queriesFile.Queries;
	map<string,string >::iterator it ;
	for (it = Queries->begin() ; it != Queries->end() ; it ++)
	{
		string queryNumber = it->first;
		string originalQuery = it->second;

		string queryModelFileName = getFileNameForQuery(queryNumber, &queryModelFileNames );
		string queryCollectionFeaturesFileName = getFileNameForQuery(queryNumber, &queryCollectionFeaturesFileNames );

		string extendedQuery = buildExtendedQuery_WordsComeFromDiscriminativeModel(originalQuery, queryModelFileName , queryCollectionFeaturesFileName , positiveWordsNumber , negativeWordsNumber);

		of << queryNumber << " " <<  extendedQuery << "\n" ;
	}
	of.close();
}


static void createQueryExpansionUsingKnnClusteringForQuery_WordsComeFromDiscriminativeModel(
	            FirstQueryExpension *firstQueryExpension,
				WordToVecModel * wordToVecModel,
				string outputFile,
				int k,
				vector<QrelsLine*>* qrelsForQuery,
			TermsDistributionType termsDistributionType)
{
	ofstream of;
	ofstream qrels;

	std::string qrelsPath = outputFile + ".qrels";
	qrels.open ( qrelsPath.c_str() ); 

	of.open ( outputFile.c_str(), std::ofstream::out ); 
	of << "<parameters>\n";

	std::map<std::string,double> rm_terms;

	// Calculate distance between each of the Discriminative Model words
	std::vector<Term*> &TermFromDiscriminativeModel = firstQueryExpension->ExpensionTerms;
	for (int i = 0 ; i <  TermFromDiscriminativeModel.size() ; ++i)
	{ 
		Term *term = TermFromDiscriminativeModel[i];
		std::string termValue = term->value;
		double weight = term->weight;
		rm_terms.insert(std::make_pair<string,double> (termValue, weight));
	}
	double **distancesMatrix = wordToVecModel->createDistancesMatrix(rm_terms);


	#pragma region Write original query

	std::string queryNumber = firstQueryExpension->QueryId ;
	WriteQueryExpensionHelper::writeOriginalQuery(&of,queryNumber,&firstQueryExpension->OriginalQuery);

	for(unsigned int i2 = 0 ; i2 <qrelsForQuery->size() ; ++i2)
	{
		qrels << queryNumber + " " + (*qrelsForQuery)[i2]->Zero + " " << (*qrelsForQuery)[i2]->DocId + " " << (*qrelsForQuery)[i2]->Score +  "\n";
	}

	#pragma endregion

	ExtendedQuery expansion;
	expansion.QueryId = firstQueryExpension->QueryId;
	expansion.OriginalQuery = firstQueryExpension->OriginalQuery;
	expansion.OriginalQueryWeight = firstQueryExpension->OriginalQueryWeight;
	expansion.ExpensionTermsWeight = 1 - firstQueryExpension->OriginalQueryWeight;
	// for all clusters
	int numberOfExpensionTerms = firstQueryExpension->ExpensionTerms.size();
	for( int i = 0 ; i< numberOfExpensionTerms ; ++i)
	{
		expansion.ExpensionTerms.clear();
		vector<std::pair<int,double> > vcos;

		// get similarity form all words
		for( int j = 0 ; j< numberOfExpensionTerms ; ++j)
		{
			vcos.push_back(std::make_pair<int,double>(j, distancesMatrix[i][j] ));
		}
		// sort the list of similarities
		std::sort(vcos.begin(), vcos.end(), sort_pred);

		// if the term is not in the word2Vec vocabulary, 
		// then all array is zeros, therefore after sort the highest value be zero.
		if( vcos[0].second == 0)
		{
			continue;
		}

		double sum = 0;
		// get the best K tems
		vcos.erase(vcos.begin()+k+1, vcos.end());
		for ( int i1=0; i1<k+1 ; ++i1 )
		{
			int index = vcos[i1].first;
			Term* t =  firstQueryExpension->ExpensionTerms[index]->Copy();
			sum += t->weight;
			expansion.ExpensionTerms.push_back( t );
		}

		// Set probability
		for ( int i1=0; i1<k+1 ; ++i1 )
		{
			Term* t = expansion.ExpensionTerms[i1];
			
			if(termsDistributionType == Uniform)
			{
				double c = expansion.ExpensionTerms.size();
				t->weight = 1/c;
			}
			else if(termsDistributionType == RelativeToRm1)
			{
				t->weight = t->weight / sum;
			}
		}

		queryNumber = firstQueryExpension->QueryId  +"_"  + firstQueryExpension->ExpensionTerms[i]->value;
		WriteQueryExpensionHelper::writeExtendedQuery(&of,queryNumber,&expansion);

	
		for(unsigned int i2 = 0 ; i2 <qrelsForQuery->size() ; ++i2)
		{
			qrels << queryNumber + " " + (*qrelsForQuery)[i2]->Zero + " " << (*qrelsForQuery)[i2]->DocId + " " << (*qrelsForQuery)[i2]->Score +  "\n";
		}

		int length = expansion.ExpensionTerms.size();
		for (unsigned int i = 0; i < length; i++)
		{
			delete expansion.ExpensionTerms[i];
		}
		expansion.ExpensionTerms.clear();
	}

	delete []distancesMatrix;
	//delete distancesMatrix;

	of << "</parameters>";
	of.close(); 

}



static void createQueryExpansionUsingKnnClustering_WordsComeFromDiscriminativeModel(
	string firstQueryExpensionFile,
	string qrelsFileName,
	string outputDir,
	string wordToVecModelFileName,
	int k,
	double originalQueryWeight)
{
	std::cout << "Read First Query Expansion File " << std::endl;
	std::vector<FirstQueryExpension*> *rm =  FirstQueryExpension::readExpensQueryFile(firstQueryExpensionFile);
	QrelsFile QrelsFile;
	std::cout << "Read Qrels File " << std::endl;
	QrelsFile.init(qrelsFileName);
	
	std::cout << "Order Qrels by queries" << std::endl;
	std::map<int,std::vector<QrelsLine*>*> quralMap;
	for(unsigned int i = 0 ; i< QrelsFile.Lines.size() ; i ++ )
	{
		QrelsLine* line = QrelsFile.Lines[i];
		std::map<int,std::vector<QrelsLine*>*>::const_iterator pos = quralMap.find(atoi(line->QueryId.c_str()));
		if ( pos == quralMap.end() ) {
		  // not found
			std::vector<QrelsLine*> *vec = new std::vector<QrelsLine*>();
			vec->push_back(line);
			quralMap.insert( std::make_pair(atoi(line->QueryId.c_str()), vec) ); 
			
		} else {
		  // found
			std::vector<QrelsLine*>* vec = pos->second;
			vec->push_back(line);
		}
	}
	std::cout << "Read Word-To-Vec Model File " << std::endl;
	WordToVecModel wordToVecModel;
	if( ! wordToVecModel.LoadWordToVecModel(wordToVecModelFileName))
	{
		return;
	}

	std::cout << "Start extended all queries : " << std::endl;
	//Pass each extended query
	for (unsigned int j = 0 ; j < rm->size() ; ++j)
    {  
		FirstQueryExpension *firstQueryExpension = (*rm)[j];
		std::cout << "\r" << "	Extend Query " <<  firstQueryExpension->QueryId << std::flush;
		
		// the file name of queries that each query represent cluster.
		std::string  outputFile = outputDir + "/" + firstQueryExpension->QueryId + ".clusters";
		
		firstQueryExpension->OriginalQueryWeight = originalQueryWeight;
		firstQueryExpension->ExpensionTermsWeight = 1 - originalQueryWeight;
		std::map<int,std::vector<QrelsLine*>*>::const_iterator pos = quralMap.find(atoi(firstQueryExpension->QueryId.c_str()));
		if ( pos == quralMap.end() ) {
		  // not found
		} 
		else 
		{
			std::vector<QrelsLine*>* qrelsForQuery = pos->second;
			createQueryExpansionUsingKnnClusteringForQuery_WordsComeFromDiscriminativeModel(firstQueryExpension,&wordToVecModel,outputFile,k,qrelsForQuery,Uniform);
		}
	}

	
}

static void createQueryExpansionUsingKnnClusteringDiffrentW2V_WordsComeFromDiscriminativeModel(
	string firstQueryExpensionFile,
	string qrelsFileName,
	string outputDir,
	vector<string> wordToVecModelFileNameVec,
	int k,
	double originalQueryWeight)
{
	std::cout << "Read First Query Expansion File " << std::endl;
	std::vector<FirstQueryExpension*> *rm =  FirstQueryExpension::readExpensQueryFile(firstQueryExpensionFile);
	QrelsFile QrelsFile;
	std::cout << "Read Qrels File " << std::endl;
	QrelsFile.init(qrelsFileName);
	
	std::cout << "Order Qrels by queries" << std::endl;
	std::map<int,std::vector<QrelsLine*>*> quralMap;
	for(unsigned int i = 0 ; i< QrelsFile.Lines.size() ; i ++ )
	{
		QrelsLine* line = QrelsFile.Lines[i];
		std::map<int,std::vector<QrelsLine*>*>::const_iterator pos = quralMap.find(atoi(line->QueryId.c_str()));
		if ( pos == quralMap.end() ) {
		  // not found
			std::vector<QrelsLine*> *vec = new std::vector<QrelsLine*>();
			vec->push_back(line);
			quralMap.insert( std::make_pair(atoi(line->QueryId.c_str()), vec) ); 
			
		} else {
		  // found
			std::vector<QrelsLine*>* vec = pos->second;
			vec->push_back(line);
		}
	}


	std::cout << "Start extended all queries : " << std::endl;
	//Pass each extended query
	for (unsigned int j = 0 ; j < rm->size() ; ++j)
    {  
		FirstQueryExpension *firstQueryExpension = (*rm)[j];
		std::cout << "\r" << "	Extend Query " <<  firstQueryExpension->QueryId << std::flush;
		
		// the file name of queries that each query represent cluster.
		std::string  outputFile = outputDir + "/" + firstQueryExpension->QueryId + ".clusters";
		
		firstQueryExpension->OriginalQueryWeight = originalQueryWeight;
		firstQueryExpension->ExpensionTermsWeight = 1 - originalQueryWeight;
		std::map<int,std::vector<QrelsLine*>*>::const_iterator pos = quralMap.find(atoi(firstQueryExpension->QueryId.c_str()));
		if ( pos == quralMap.end() ) {
		  // not found
		} 
		else 
		{
			std::vector<QrelsLine*>* qrelsForQuery = pos->second;

			for (int i = 0 ; i < wordToVecModelFileNameVec.size() ; ++i)
			{
				string wordToVecModelFileName = wordToVecModelFileNameVec[i];
				if ( wordToVecModelFileName.find(firstQueryExpension->QueryId) != std::string::npos)
				{
					std::cout << "Read Word-To-Vec Model File for query " << firstQueryExpension->QueryId << " file name is : " << wordToVecModelFileName <<  std::endl;
					WordToVecModel wordToVecModel;
					if( ! wordToVecModel.LoadWordToVecModel(wordToVecModelFileName))
					{
						return;
					}
					createQueryExpansionUsingKnnClusteringForQuery_WordsComeFromDiscriminativeModel(firstQueryExpension,&wordToVecModel,outputFile,k,qrelsForQuery,Uniform);
					wordToVecModelFileNameVec.erase(wordToVecModelFileNameVec.begin()+i);
					break;
				}
				else
				{
					cout << "Can find w2v model for query " << firstQueryExpension->QueryId << std::endl;
				}
			}
			
		}
	}

	
}


static void createClustersScoreFilesUsingKnnClustering_WordsComeFromDiscriminativeModel(
	string firstQueryExpensionFile,
	string outputDir,
	string wordToVecModelFileName,
	int k)
{
	std::cout << "Read First Query Expansion File " << std::endl;
	std::vector<FirstQueryExpension*> *rm =  FirstQueryExpension::readExpensQueryFile(firstQueryExpensionFile);

	std::cout << "Read Word-To-Vec Model File " << std::endl;
	WordToVecModel wordToVecModel;
	if( ! wordToVecModel.LoadWordToVecModel(wordToVecModelFileName))
	{
		return;
	}

	std::cout << "Start calculate cluster score for all queries : " << std::endl;
	//Pass each extended query
	for (unsigned int j = 0 ; j < rm->size() ; ++j)
    {  
		FirstQueryExpension *firstQueryExpension = (*rm)[j];
		std::cout << "\r" << "	Calculate cluster score for Query " <<  firstQueryExpension->QueryId << std::flush;
		std::string  outputFile = outputDir + "/" + firstQueryExpension->QueryId + ".clusters";
		createClustersScoreFilesUsingKnnClusteringForQuery_WordsComeFromDiscriminativeModel(firstQueryExpension,&wordToVecModel,outputFile,k);
	}

}

static void createClustersScoreFilesUsingKnnClusteringDiffrentW2V_WordsComeFromDiscriminativeModel(
	string firstQueryExpensionFile,
	string outputDir,
	vector<string> wordToVecModelVectorFileName,
	int k)
{
	std::cout << "Read First Query Expansion File " << std::endl;
	std::vector<FirstQueryExpension*> *rm =  FirstQueryExpension::readExpensQueryFile(firstQueryExpensionFile);

	std::cout << "Start calculate cluster score for all queries : " << std::endl;
	//Pass each extended query
	for (unsigned int j = 0 ; j < rm->size() ; ++j)
    {  
		FirstQueryExpension *firstQueryExpension = (*rm)[j];
		std::cout << "\r" << "	Calculate cluster score for Query " <<  firstQueryExpension->QueryId << std::flush;
		std::string  outputFile = outputDir + "/" + firstQueryExpension->QueryId + ".clusters";

		for (int i = 0 ; i < wordToVecModelVectorFileName.size() ; ++i)
		{
			string wordToVecModelFileName = wordToVecModelVectorFileName[i];
			if ( wordToVecModelFileName.find(firstQueryExpension->QueryId) != std::string::npos)
			{
				std::cout << "Read Word-To-Vec Model File for query " << firstQueryExpension->QueryId << " file name is : " << wordToVecModelFileName <<  std::endl;
				WordToVecModel wordToVecModel;
				if( ! wordToVecModel.LoadWordToVecModel(wordToVecModelFileName))
				{
					return;
				}
				createClustersScoreFilesUsingKnnClusteringForQuery_WordsComeFromDiscriminativeModel(firstQueryExpension,&wordToVecModel,outputFile,k);
				wordToVecModelVectorFileName.erase(wordToVecModelVectorFileName.begin()+i);
				break;
			}
			else
			{
				cout << "Can find w2v model for query " << firstQueryExpension->QueryId << std::endl;
			}
		}
	}

}


static void createClustersScoreFilesUsingKnnClusteringForQuery_WordsComeFromDiscriminativeModel(
	            FirstQueryExpension *firstQueryExpension,
				WordToVecModel * wordToVecModel,
				string outputFile,
				int k)
{
	ofstream clusterGrade;
	
	std::string clusterGradePath = outputFile + ".clusterGrade";
	clusterGrade.open ( clusterGradePath.c_str() ); 

	std::map<std::string,double> terms;

	// Calculate distance between each of the Discriminative Model words
	std::vector<Term*> &TermFromDiscriminativeModel = firstQueryExpension->ExpensionTerms;
	for (int i = 0 ; i <  TermFromDiscriminativeModel.size() ; ++i)
	{ 
		Term *term = TermFromDiscriminativeModel[i];
		std::string termValue = term->value;
		double weight = term->weight;
		terms.insert(std::make_pair<string,double> (termValue, weight));
	}
	double **distancesMatrix = wordToVecModel->createDistancesMatrix(terms);


	// for all clusters
	int numberOfExpensionTerms = firstQueryExpension->ExpensionTerms.size();
	for( int i = 0 ; i< numberOfExpensionTerms ; ++i)
	{
		vector<std::pair<int,double> > vcos;
		// get similarity form all words
		for( int j = 0 ; j< numberOfExpensionTerms ; ++j)
		{
			vcos.push_back(std::make_pair<int,double>(j, distancesMatrix[i][j] ));
		}
		// sort the list of similarities
		std::sort(vcos.begin(), vcos.end(), sort_pred);

		// if the term is not in the word2Vec vocabulary, 
		// then all array is zeros, therefore after sort the highest value be zero.
		if( vcos[0].second == 0)
		{
			continue;
		}

		double clusterGradeValue = 0;
		// get the best K tems
		vcos.erase(vcos.begin()+k+1, vcos.end());
		for ( int i1=0; i1<k+1 ; ++i1 )
		{
			int index = vcos[i1].first;
			Term* t =  firstQueryExpension->ExpensionTerms[index]->Copy();
			clusterGradeValue += t->weight;
		}
		string clusterNumber = firstQueryExpension->QueryId  + "_"  + firstQueryExpension->ExpensionTerms[i]->value;
		clusterGrade << clusterNumber << " " << clusterGradeValue << "\n";
	}

	delete []distancesMatrix;
	clusterGrade.close(); 
}

static void CreateFilesForGnuplot_WordsComeFromDiscriminativeModel(string pathToDir,vector<string> *fileNameList)
{
	
	std::cout << "Order configurations file ... " << std::endl;
	std::map<string,vector<string>*> orderFileRetrival;
	orderFileByConfig(fileNameList, Retrival  , &orderFileRetrival  );
	std::cout << "After we receive that we have  " << orderFileRetrival.size() << " different configurations " << std::endl;


	std::cout << "For each configuration calculate average for all evaluation measures ... " << std::endl;
	DiscriminativeModelQueryByMultipleConfig discriminativeModelQueryByMultipleConfig;
	map<string , DiscriminativeModelQueryByConfig* >  discriminativeModelQueryByConfigMap ;

	map< string , DiscriminativeModelQueryByMultipleConfig* >  DiscriminativeModelQueryByEvalListByMainConfig;

	std::map<string,vector<string>*>::iterator orderFileRetrivalIt;
	for(orderFileRetrivalIt = orderFileRetrival.begin() ; orderFileRetrivalIt != orderFileRetrival.end(); orderFileRetrivalIt++ )
	{
		std::cout << "        Configuration Name : " << orderFileRetrivalIt->first << " Number of files : " << orderFileRetrivalIt->second->size() << std::endl;
		vector<string> *QueryfileNameList = orderFileRetrivalIt->second;
		DiscriminativeModelQueryByConfig* discriminativeModelQueryByConfig = new DiscriminativeModelQueryByConfig();
		discriminativeModelQueryByConfig->init(QueryfileNameList);
		discriminativeModelQueryByConfigMap.insert(std::make_pair<string , DiscriminativeModelQueryByConfig*>(orderFileRetrivalIt->first,discriminativeModelQueryByConfig));
		discriminativeModelQueryByMultipleConfig.add(discriminativeModelQueryByConfig);

		string mainConfig = getMainConfie(orderFileRetrivalIt->first);
		map< string , DiscriminativeModelQueryByMultipleConfig* >::iterator ByMainConfigIt = DiscriminativeModelQueryByEvalListByMainConfig.find(mainConfig);
		if(ByMainConfigIt != DiscriminativeModelQueryByEvalListByMainConfig.end())
		{
			ByMainConfigIt->second->add(discriminativeModelQueryByConfig);
			std::cout << "                Add Configuration to main Configuration : " <<mainConfig << std::endl;
		}
		else
		{
			DiscriminativeModelQueryByMultipleConfig * discriminativeModelQueryByMultipleConfigByMainConfig = new DiscriminativeModelQueryByMultipleConfig();
			discriminativeModelQueryByMultipleConfigByMainConfig->add(discriminativeModelQueryByConfig);
			DiscriminativeModelQueryByEvalListByMainConfig.insert(std::make_pair<string , DiscriminativeModelQueryByMultipleConfig*>(mainConfig ,discriminativeModelQueryByMultipleConfigByMainConfig));
			std::cout << "                Add Configuration to main Configuration (first time): " << mainConfig << std::endl;
		}

	}
	
	std::map<string,ResultToPrint*> evaluationPerfDocsResult;
	std::vector<string> evaluationNames = discriminativeModelQueryByMultipleConfig.getEvalNames();

	std::cout << "Start - built a evaluation parameter per configuration (before write the file)" << std::endl;	
	for (unsigned int i = 0; i < evaluationNames.size(); i++)
	{
		string evalName = evaluationNames[i];
		ResultToPrint* evalfDocsResult = new ResultToPrint();

		vector<double> *values = new vector<double>();
		vector<double> *scores = new vector<double>();
		discriminativeModelQueryByMultipleConfig.getClusterSortedByValue( evalName , values, scores);

		//printVector("Cluster Performance Average - Sorted By Performance", values);
		//printVector("Cluster Score Average - Sorted By Performance", scores);


		evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Performance Average - Sorted By Performance",values));
		evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Score Average - Sorted By Performance",scores));

		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Correlation - Sorted By Performance",correlation(values,scores)));


		values = new vector<double>();
		scores = new vector<double>();
		discriminativeModelQueryByMultipleConfig.getClusterSortedByScore( evalName , values, scores);

		//printVector("Cluster Performance Average - Sorted By Score", values);
		//printVector("Cluster Score Average - Sorted By Score", scores);

		evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Performance Average - Sorted By Score" ,values));
		evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Score Average - Sorted By Score" ,scores));

		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Correlation - Sorted By Score",correlation(values,scores)));

		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Original Query Value Average",discriminativeModelQueryByMultipleConfig.getOriginalQueryValue(evalName)));


		// for each main configuration
		map< string , DiscriminativeModelQueryByMultipleConfig* >::iterator  DiscriminativeModelQueryByEvalListByMainConfigIt;
		for(DiscriminativeModelQueryByEvalListByMainConfigIt = DiscriminativeModelQueryByEvalListByMainConfig.begin(); DiscriminativeModelQueryByEvalListByMainConfigIt != DiscriminativeModelQueryByEvalListByMainConfig.end(); DiscriminativeModelQueryByEvalListByMainConfigIt++) 
	    {
			string mainConfigName = DiscriminativeModelQueryByEvalListByMainConfigIt->first;
			
			string config;
			if(mainConfigName.find("ModelDocs") != std::string::npos)
			{
				config = getConfigNaem(mainConfigName,"ModelDocs");
			}
			else
			{
				config	= getConfigNaem(mainConfigName);
			}

			DiscriminativeModelQueryByMultipleConfig * discriminativeModelQueryByConfig = DiscriminativeModelQueryByEvalListByMainConfigIt->second;

			vector<double> *values = new vector<double>();
			vector<double> *scores = new vector<double>();
			discriminativeModelQueryByConfig->getClusterSortedByValue( evalName , values, scores);

		//	printVector("Cluster Performance Average - Sorted By Performance - " + config, values);
			//printVector("Cluster Score Average - Sorted By Performance - " + config, scores);

			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Performance - Sorted By Performance  - " + config,values));
			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Score - Sorted By Performance  - " + config,scores));
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Correlation - Sorted By Performance - " + config,correlation(values,scores)));


			values = new vector<double>();
			scores = new vector<double>();
			discriminativeModelQueryByConfig->getClusterSortedByScore( evalName , values, scores);

			//printVector("Cluster Performance Average - Sorted By Score - " + config, values);
			//printVector("Cluster Score Average - Sorted By Score - " + config, scores);


			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Performance - Sorted By Score  - " + config,values));
			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Score - Sorted By Score  - " + config,scores));
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Correlation - Sorted By Score - " + config,correlation(values,scores)));


			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Original Query Value  - " + config ,discriminativeModelQueryByConfig->getOriginalQueryValue(evalName)));
		}

		// for each configuration
		std::map<string,DiscriminativeModelQueryByConfig*>::iterator discriminativeModelQueryByConfigIt;
		for(discriminativeModelQueryByConfigIt = discriminativeModelQueryByConfigMap.begin(); discriminativeModelQueryByConfigIt != discriminativeModelQueryByConfigMap.end(); discriminativeModelQueryByConfigIt++) 
	    {
			string configName = discriminativeModelQueryByConfigIt->first;
			string config = getConfigNaem(configName,"ModelDocs");

			DiscriminativeModelQueryByConfig * discriminativeModelQueryByConfig = discriminativeModelQueryByConfigIt->second;

			vector<double> *values = new vector<double>();
			vector<double> *scores = new vector<double>();
			discriminativeModelQueryByConfig->getClusterSortedByValue( evalName , values, scores);

		//	printVector("Cluster Performance Average - Sorted By Performance - " + config, values);
		//	printVector("Cluster Score Average - Sorted By Performance - " + config, scores);

			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Performance - Sorted By Performance  - " + config,values));
			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Score - Sorted By Performance  - " + config,scores));
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Correlation - Sorted By Performance - " + config,correlation(values,scores)));

			values = new vector<double>();
			scores = new vector<double>();
			discriminativeModelQueryByConfig->getClusterSortedByScore( evalName , values, scores);

		//	printVector("Cluster Performance Average - Sorted By Score - " + config, values);
		//	printVector("Cluster Score Average - Sorted By Score - " + config, scores);

			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Performance - Sorted By Score  - " + config,values));
			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Score - Sorted By Score  - " + config,scores));
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Correlation - Sorted By Score - " + config,correlation(values,scores)));

			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Original Query Value  - " + config ,discriminativeModelQueryByConfig->getOriginalQueryValue(evalName)));
		}




		evaluationPerfDocsResult.insert(std::make_pair<string,ResultToPrint*>(evalName,evalfDocsResult));
	}



	std::cout << "Write files ..." << std::endl;
	std::map<string,ResultToPrint*>::iterator  evaluationPerfDocsResultIterator;
	for(evaluationPerfDocsResultIterator = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIterator != evaluationPerfDocsResult.end(); evaluationPerfDocsResultIterator++) 
	{
		string evalName = evaluationPerfDocsResultIterator->first;
		ResultToPrint* fDocsResultToPlot = evaluationPerfDocsResultIterator->second;
		string path = pathToDir + "/" + evalName + ".dat";
		std::cout << "     write files for : " << evalName + ".dat   ,   " << fDocsResultToPlot->toString()<< std::endl;
		fDocsResultToPlot->writeFileForPlot(path); 
		fDocsResultToPlot->writeCSVFile(path);
	}



	std::map<string,ResultToPrint*>::iterator evaluationPerfDocsResultIt;
	for (evaluationPerfDocsResultIt = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIt != evaluationPerfDocsResult.end() ; evaluationPerfDocsResultIt++)
	{
		evaluationPerfDocsResultIt->second->Clear();
		delete evaluationPerfDocsResultIt->second;
	}
	evaluationPerfDocsResult.clear();
	discriminativeModelQueryByMultipleConfig.Clear();
	for( map< string , DiscriminativeModelQueryByMultipleConfig* >::iterator it  = DiscriminativeModelQueryByEvalListByMainConfig.begin(); 
		it != DiscriminativeModelQueryByEvalListByMainConfig.end(); it++) 
	{
		it->second->Clear();
		delete it->second;
	}
	DiscriminativeModelQueryByEvalListByMainConfig.clear();
	for(std::map< string , DiscriminativeModelQueryByConfig* >::iterator discriminativeModelQueryByConfigMapIt = discriminativeModelQueryByConfigMap.begin(); 
		discriminativeModelQueryByConfigMapIt != discriminativeModelQueryByConfigMap.end(); discriminativeModelQueryByConfigMapIt++) 
	{
		discriminativeModelQueryByConfigMapIt->second->Clear();
		delete discriminativeModelQueryByConfigMapIt->second;
	}
	discriminativeModelQueryByConfigMap.clear();
	for(std::map<string,vector<string>*>::iterator orderFilesIt = orderFileRetrival.begin(); orderFilesIt != orderFileRetrival.end(); orderFilesIt++) 
	{
		delete orderFilesIt->second;
	}
	orderFileRetrival.clear();
	/*
	std::cout << "Order configurations file ... " << std::endl;
	// get for each configuration all the file that belong to him.
	std::map<string,vector<string>*> orderFiles;
	orderFileByConfig(fileNameList,Retrival,&orderFiles); 
	std::cout << "After we receive that we have  " << orderFiles.size() << " different configurations " << std::endl;


	std::cout << "For each configuration calculate average for all evaluation measures ... " << std::endl;
	std::map<string,AvgEvaluateResultFiles*> resultPerConfig; /// key=configuration , vlaue= average all files in the configuration
	vector<AvgEvaluateResultFiles*> avgEvaluateResultFilesList;
	map<string,vector<string>*>::iterator orderFilesIt;
	for(orderFilesIt = orderFiles.begin(); orderFilesIt != orderFiles.end(); orderFilesIt++) 
	{
		std::cout << "        Configuration Name : " << orderFilesIt->first << " Number of files : " << orderFilesIt->second->size() << std::endl;
		vector<string> *localfileNameList = orderFilesIt->second; // list of evaluate files
		AvgEvaluateResultFiles* avgEvaluateResultFiles = new AvgEvaluateResultFiles();
		avgEvaluateResultFiles->init();
	    avgEvaluateResultFiles->AvgFilesToAvg(localfileNameList);
		resultPerConfig.insert(std::make_pair<string,AvgEvaluateResultFiles*> (orderFilesIt->first, avgEvaluateResultFiles)); 
		avgEvaluateResultFilesList.push_back(avgEvaluateResultFiles);
	}

	
	std::cout << "Calculate Average of all configuration files ..." << std::endl;
	// avg of all configuration files
	AvgEvaluateResultFiles avgEvaluateResultFilesAllConfigFiles;
   // avgEvaluateResultFilesAllConfigFiles.AvgFilesToAvg(fileNameList);
	avgEvaluateResultFilesAllConfigFiles.init();
	avgEvaluateResultFilesAllConfigFiles.addAvgEvaluateResultFiles(&(avgEvaluateResultFilesList));


	std::map<string,ResultToPrint*> evaluationPerfDocsResult;
	std::set<string> *evaluationNames = &avgEvaluateResultFilesAllConfigFiles.evaluateResultFile.evaluationNames;
	
	std::vector<string> output(evaluationNames->size());
	std::copy(evaluationNames->begin(), evaluationNames->end(), output.begin());


	std::cout << "Start - built a evaluation parameter per configuration (before write the file)" << std::endl;	
	for (unsigned int i = 0; i < output.size(); i++)
	{
		string evalName = output[i];
		ResultToPrint* evalfDocsResult = new ResultToPrint();
		QueryResult*  resultPerEval = avgEvaluateResultFilesAllConfigFiles.getEvaluateValuesInFile(evalName);
		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("OriginalQueryValue",resultPerEval->originalQueryValue));
		
		// for each configuration
		std::map<string,AvgEvaluateResultFiles*>::iterator resultPerConfigIterator;
		std::cout << "ResultPerConfig size " << resultPerConfig.size() <<  std::endl;	
		for(resultPerConfigIterator = resultPerConfig.begin(); resultPerConfigIterator != resultPerConfig.end(); resultPerConfigIterator++) 
	    {
			string configName = resultPerConfigIterator->first;
			string config = getConfigNaem(configName,"ModelDocs");
			AvgEvaluateResultFiles* clustres = resultPerConfigIterator->second;
			QueryResult*  resultPerEvalAndConfig = clustres->getEvaluateValuesInFile(evalName);
			std::cout << "QueryResult data - " << resultPerEvalAndConfig->toString() << std::endl;	

			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>(config,resultPerEvalAndConfig->getClustresValues()));
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>(config + " - " + "OriginalQueryValue",resultPerEvalAndConfig->originalQueryValue));
			
		}

		evaluationPerfDocsResult.insert(std::make_pair<string,ResultToPrint*>(evalName,evalfDocsResult));
	}


	std::cout << "Write files ..." << std::endl;
	std::map<string,ResultToPrint*>::iterator  evaluationPerfDocsResultIterator;
	for(evaluationPerfDocsResultIterator = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIterator != evaluationPerfDocsResult.end(); evaluationPerfDocsResultIterator++) 
	{
		string evalName = evaluationPerfDocsResultIterator->first;
		ResultToPrint* fDocsResultToPlot = evaluationPerfDocsResultIterator->second;
		string path = pathToDir + "/" + evalName + ".dat";
		std::cout << "     write files for : " << evalName + ".dat   ,   " << fDocsResultToPlot->toString()<< std::endl;
		fDocsResultToPlot->writeFileForPlot(path); 
	}



	std::map<string,ResultToPrint*>::iterator evaluationPerfDocsResultIt;
	for (evaluationPerfDocsResultIt = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIt != evaluationPerfDocsResult.end() ; evaluationPerfDocsResultIt++)
	{
		delete evaluationPerfDocsResultIt->second;
	}
	evaluationPerfDocsResult.clear();

	// free avgEvaluateResult FilesList
	int length = avgEvaluateResultFilesList.size();
	for (int i = 0; i < length; i++)
	{
		avgEvaluateResultFilesList[i]->clear();
	}
	avgEvaluateResultFilesList.clear();
	resultPerConfig.clear();
	avgEvaluateResultFilesAllConfigFiles.clear();

	// free orderFiles map
	for (orderFilesIt = orderFiles.begin(); orderFilesIt != orderFiles.end() ; orderFilesIt++)
	{
		delete orderFilesIt->second;
	}
	orderFiles.clear();
	*/
}

static void CreateFilesForGnuplot_ForClusterScoreAndPerformance(string pathToDir,
														 vector<string> *clustresPerformanceFiles ,
														  vector<string> *clustresScoreFiles)
{
	
	std::cout << "Order configurations file ... " << std::endl;
	std::map<string,vector<string>*> orderFileRetrival;
	orderFileByConfig(clustresPerformanceFiles, Retrival  , &orderFileRetrival  );
	std::cout << "After we receive that we have  " << orderFileRetrival.size() << " different configurations " << std::endl;

	std::cout << "For each configuration calculate average for all evaluation measures ... " << std::endl;
	DiscriminativeModelQueryByMultipleConfig discriminativeModelQueryByMultipleConfig;
	map<string , DiscriminativeModelQueryByConfig* >  discriminativeModelQueryByConfigMap ;

	map< string , DiscriminativeModelQueryByMultipleConfig* >  DiscriminativeModelQueryByEvalListByMainConfig;

	std::map<string,vector<string>*>::iterator orderFileRetrivalIt;
	for(orderFileRetrivalIt = orderFileRetrival.begin() ; orderFileRetrivalIt != orderFileRetrival.end(); orderFileRetrivalIt++ )
	{
		std::cout << "        Configuration Name : " << orderFileRetrivalIt->first << " Number of files : " << orderFileRetrivalIt->second->size() << std::endl;
		vector<string> *QueryfileNameList = orderFileRetrivalIt->second;
		DiscriminativeModelQueryByConfig* discriminativeModelQueryByConfig = new DiscriminativeModelQueryByConfig();
		discriminativeModelQueryByConfig->init(QueryfileNameList,clustresScoreFiles);
		discriminativeModelQueryByConfigMap.insert(std::make_pair<string , DiscriminativeModelQueryByConfig*>(orderFileRetrivalIt->first,discriminativeModelQueryByConfig));
		discriminativeModelQueryByMultipleConfig.add(discriminativeModelQueryByConfig);
	}
	
	std::map<string,ResultToPrint*> evaluationPerfDocsResult;
	std::vector<string> evaluationNames = discriminativeModelQueryByMultipleConfig.getEvalNames();

	std::cout << "Start - built a evaluation parameter per configuration (before write the file)" << std::endl;	
	for (unsigned int i = 0; i < evaluationNames.size(); i++)
	{
		string evalName = evaluationNames[i];
		ResultToPrint* evalfDocsResult = new ResultToPrint();

		vector<double> *values = new vector<double>();
		vector<double> *scores = new vector<double>();
		discriminativeModelQueryByMultipleConfig.getClusterSortedByValue( evalName , values, scores);


		evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Performance Average - Sorted By Performance",values));
		evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Score Average - Sorted By Performance",scores));

		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Correlation - Sorted By Performance",correlation(values,scores)));


		values = new vector<double>();
		scores = new vector<double>();
		discriminativeModelQueryByMultipleConfig.getClusterSortedByScore( evalName , values, scores);


		evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Performance Average - Sorted By Score" ,values));
		evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Score Average - Sorted By Score" ,scores));

		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Correlation - Sorted By Score",correlation(values,scores)));

		evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Original Query Value Average",discriminativeModelQueryByMultipleConfig.getOriginalQueryValue(evalName)));


		// for each configuration
		std::map<string,DiscriminativeModelQueryByConfig*>::iterator discriminativeModelQueryByConfigIt;
		for(discriminativeModelQueryByConfigIt = discriminativeModelQueryByConfigMap.begin(); discriminativeModelQueryByConfigIt != discriminativeModelQueryByConfigMap.end(); discriminativeModelQueryByConfigIt++) 
	    {
			string configName = discriminativeModelQueryByConfigIt->first;
						
			string config;
			if(configName.find("ModelDocs") != std::string::npos)
			{
				config = getConfigNaem(configName,"ModelDocs");
			}
			else
			{
				config	= getConfigNaem(configName , "muRule");
			}
		//	cout << "config  = " << config << std::endl;
			DiscriminativeModelQueryByConfig * discriminativeModelQueryByConfig = discriminativeModelQueryByConfigIt->second;

			vector<double> *values = new vector<double>();
			vector<double> *scores = new vector<double>();
			discriminativeModelQueryByConfig->getClusterSortedByValue( evalName , values, scores);

			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Performance - Sorted By Performance  - " + config,values));
			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Score - Sorted By Performance  - " + config,scores));
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Correlation - Sorted By Performance - " + config,correlation(values,scores)));

			values = new vector<double>();
			scores = new vector<double>();
			discriminativeModelQueryByConfig->getClusterSortedByScore( evalName , values, scores);

			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Performance - Sorted By Score  - " + config,values));
			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>("Cluster Score - Sorted By Score  - " + config,scores));
			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Correlation - Sorted By Score - " + config,correlation(values,scores)));

			evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("Original Query Value  - " + config ,discriminativeModelQueryByConfig->getOriginalQueryValue(evalName)));
		}




		evaluationPerfDocsResult.insert(std::make_pair<string,ResultToPrint*>(evalName,evalfDocsResult));
	}



	std::cout << "Write files ..." << std::endl;
	std::map<string,ResultToPrint*>::iterator  evaluationPerfDocsResultIterator;
	for(evaluationPerfDocsResultIterator = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIterator != evaluationPerfDocsResult.end(); evaluationPerfDocsResultIterator++) 
	{
		string evalName = evaluationPerfDocsResultIterator->first;
		ResultToPrint* fDocsResultToPlot = evaluationPerfDocsResultIterator->second;
		string path = pathToDir + "/" + evalName + ".dat";
		std::cout << "     write files for : " << evalName + ".dat   ,   " << fDocsResultToPlot->toString()<< std::endl;
		fDocsResultToPlot->writeFileForPlot(path); 
		fDocsResultToPlot->writeCSVFile(path);
	}



	std::map<string,ResultToPrint*>::iterator evaluationPerfDocsResultIt;
	for (evaluationPerfDocsResultIt = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIt != evaluationPerfDocsResult.end() ; evaluationPerfDocsResultIt++)
	{
		evaluationPerfDocsResultIt->second->Clear();
		delete evaluationPerfDocsResultIt->second;
	}
	evaluationPerfDocsResult.clear();
	discriminativeModelQueryByMultipleConfig.Clear();
	for( map< string , DiscriminativeModelQueryByMultipleConfig* >::iterator it  = DiscriminativeModelQueryByEvalListByMainConfig.begin(); 
		it != DiscriminativeModelQueryByEvalListByMainConfig.end(); it++) 
	{
		it->second->Clear();
		delete it->second;
	}
	DiscriminativeModelQueryByEvalListByMainConfig.clear();
	for(std::map< string , DiscriminativeModelQueryByConfig* >::iterator discriminativeModelQueryByConfigMapIt = discriminativeModelQueryByConfigMap.begin(); 
		discriminativeModelQueryByConfigMapIt != discriminativeModelQueryByConfigMap.end(); discriminativeModelQueryByConfigMapIt++) 
	{
		discriminativeModelQueryByConfigMapIt->second->Clear();
		delete discriminativeModelQueryByConfigMapIt->second;
	}
	discriminativeModelQueryByConfigMap.clear();
	for(std::map<string,vector<string>*>::iterator orderFilesIt = orderFileRetrival.begin(); orderFilesIt != orderFileRetrival.end(); orderFilesIt++) 
	{
		delete orderFilesIt->second;
	}
	orderFileRetrival.clear();
}


static string buildExtendedQuery_WordsComeFromDiscriminativeModel(string originalQuery , string queryModelFileName , string queryCollectionFeaturesFileName ,int positiveWordsNumber , int negativeWordsNumber )
{
	QueryModel queryModel;
	queryModel.Set(queryModelFileName,queryCollectionFeaturesFileName);
	vector<Term*> wordsWithProbability;
	queryModel.getWordForExpansion(positiveWordsNumber,negativeWordsNumber , &wordsWithProbability);
	
	 string extendedQuery = "#weight( 0.5 #combine( " + originalQuery + " ) " +
			"0.5"  + " #weight(  " + Term::convertTermToString(wordsWithProbability,true,true) + " ) " + " )";

	 int length = wordsWithProbability.size();
	 for (int i = 0; i < length; i++)
	 {
		 delete wordsWithProbability[i];
	 }
	queryModel.Clear();

	return extendedQuery;
}

#pragma region create from the RM1 First Time Query Expansion other First Time Query Expansion file that include Discriminative Model score

static void CreateFromRM1FirstExpansionFirst_OtherExpansionFromDiscriminativeModel( string firstExpansionFromRM1File ,vector<string>  queryModelFileNames , 
																			vector<string>  queryCollectionFeaturesFileNames , string outputFile)
{
	ofstream of;
	of.open ( outputFile.c_str(), std::ofstream::out ); 

	std::cout << "Read First Query Expansion File " << std::endl;
	std::vector<FirstQueryExpension*> *rm =  FirstQueryExpension::readExpensQueryFile(firstExpansionFromRM1File);
	for (unsigned int j = 0 ; j < rm->size() ; ++j)
    {  
		FirstQueryExpension *firstQueryExpension = (*rm)[j];
		std::cout << "\r" << "	Extend Query " <<  firstQueryExpension->QueryId << std::flush;

		string queryModelFileName = getFileNameForQuery(firstQueryExpension->QueryId, &queryModelFileNames );
		string queryCollectionFeaturesFileName = getFileNameForQuery(firstQueryExpension->QueryId, &queryCollectionFeaturesFileNames );
		
		QueryModel queryModel;
		queryModel.Set(queryModelFileName,queryCollectionFeaturesFileName);

		queryModel.SetScoreToTerms(&firstQueryExpension->ExpensionTerms);

		string extendedQuery = WriteQueryExpensionHelper::buildQuery(firstQueryExpension);

		of << firstQueryExpension->QueryId << " " <<  extendedQuery << "\n" ;
	}
	of.close();

}

#pragma endregion


#pragma region Cross Validation - On cluster Grade

static void crossValidationOnClusterScore( vector<string>  *clusterScoreFile , 
								   vector<string>  *clusterPerformance , 
								   int i,
								   string outputFile)
{
	std::cout << "Order Performance file ... " << std::endl;
	std::map<string,vector<string>*> orderPerformanceFile;
	orderFileByConfig(clusterPerformance, Retrival  , &orderPerformanceFile  );
	std::cout << "After we receive that we have  " << orderPerformanceFile.size() << " different Performance configurations " << std::endl;

	std::cout << "Order Score file ... " << std::endl;
	std::map<string,vector<string>*> orderScoreFile;
	orderFileByPos(clusterScoreFile, 2  , &orderScoreFile  );
	std::cout << "After we receive that we have  " << orderScoreFile.size() << " different Score configurations " << std::endl;

	std::cout << "For each configuration calculate average for all evaluation measures ... " << std::endl;
	
	ClusterScorePerAllConfigForCV clusterScorePerAllConfigForCV;
	clusterScorePerAllConfigForCV.init(i);

	std::map<string,vector<string>*>::iterator orderFileRetrivalIt;
	for(orderFileRetrivalIt = orderPerformanceFile.begin() ; orderFileRetrivalIt != orderPerformanceFile.end(); orderFileRetrivalIt++ )
	{
		std::cout << "        Performance Configuration Name : " << orderFileRetrivalIt->first << " Number of files : " << orderFileRetrivalIt->second->size() << std::endl;
		
		vector<string> *QueryfileNameList = orderFileRetrivalIt->second;
		string mainConfig = getMainConfie(orderFileRetrivalIt->first);

		std::map<string,vector<string>*>::iterator it = orderScoreFile.find(mainConfig);
		if(it != orderScoreFile.end() )
		{
			std::cout << "        Score Configuration Name : " << it->first << " Number of files : " << it->second->size() << std::endl;
			DiscriminativeModelQueryByConfig* discriminativeModelQueryByConfig = new DiscriminativeModelQueryByConfig();
			discriminativeModelQueryByConfig->init(QueryfileNameList,it->second);
			clusterScorePerAllConfigForCV.Add(mainConfig, discriminativeModelQueryByConfig);
			discriminativeModelQueryByConfig->Clear();
		}
		else
		{
			cout << "Error - Cannot find score file for config : " << mainConfig << std::endl;
		}
	}
	
	vector<CVPerEval> CVPerEvalList;

	vector<string> evalNames = clusterScorePerAllConfigForCV.evalNames;
	int length = evalNames.size();
	for (int i = 0; i < length; i++)
	{
		string eval = evalNames[i];
		double cvValue = clusterScorePerAllConfigForCV.CalcCV( eval);

		CVPerEval cVPerEval;
		cVPerEval.evalName = eval;
		cVPerEval.CVValue = cvValue;
		CVPerEvalList.push_back(cVPerEval);
	}

	ofstream of;
	string cvOutFile =  outputFile + "/CrossValidationOnClusterScore.txt";
	of.open ( cvOutFile.c_str(), std::ofstream::out ); 
	of << "# evaluation , CV " << std::endl;

	length = CVPerEvalList.size();
	for (int i = 0; i < length; i++)
	{
		ResultToPrint evalfDocsResult;

		string eval = CVPerEvalList[i].evalName;
		double cvValue = CVPerEvalList[i].CVValue;
		of << eval << " " <<  cvValue << std::endl;

		evalfDocsResult.listOfSingleValue.insert(std::make_pair<string,double>("Performance Cross Validation",cvValue));
		string path = outputFile + "/" + eval + ".dat";
		std::cout << "     write files for : " << eval + ".dat " << std::endl;
		evalfDocsResult.writeOnlySingelValue(path); 
	}
	of.close();  


	clusterScorePerAllConfigForCV.Clear();

}



static void bestonAverageOnClusterScore( vector<string>  *clusterScoreFile , 
								   vector<string>  *clusterPerformance , 
								   int i,
								   string outputFile)
{
	std::cout << "Order Performance file ... " << std::endl;
	std::map<string,vector<string>*> orderPerformanceFile;
	orderFileByConfig(clusterPerformance, Retrival  , &orderPerformanceFile  );
	std::cout << "After we receive that we have  " << orderPerformanceFile.size() << " different Performance configurations " << std::endl;

	std::cout << "Order Score file ... " << std::endl;
	std::map<string,vector<string>*> orderScoreFile;
	orderFileByPos(clusterScoreFile, 2  , &orderScoreFile  );
	std::cout << "After we receive that we have  " << orderScoreFile.size() << " different Score configurations " << std::endl;

	std::cout << "For each configuration calculate average for all evaluation measures ... " << std::endl;
	
	ClusterScorePerAllConfigForCV clusterScorePerAllConfigForCV;
	clusterScorePerAllConfigForCV.init(i);

	std::map<string,vector<string>*>::iterator orderFileRetrivalIt;
	for(orderFileRetrivalIt = orderPerformanceFile.begin() ; orderFileRetrivalIt != orderPerformanceFile.end(); orderFileRetrivalIt++ )
	{
		std::cout << "        Performance Configuration Name : " << orderFileRetrivalIt->first << " Number of files : " << orderFileRetrivalIt->second->size() << std::endl;
		
		vector<string> *QueryfileNameList = orderFileRetrivalIt->second;
		string mainConfig = getMainConfie(orderFileRetrivalIt->first);

		std::map<string,vector<string>*>::iterator it = orderScoreFile.find(mainConfig);
		if(it != orderScoreFile.end() )
		{
			std::cout << "        Score Configuration Name : " << it->first << " Number of files : " << it->second->size() << std::endl;
			DiscriminativeModelQueryByConfig* discriminativeModelQueryByConfig = new DiscriminativeModelQueryByConfig();
			discriminativeModelQueryByConfig->init(QueryfileNameList,it->second);
			clusterScorePerAllConfigForCV.Add(mainConfig, discriminativeModelQueryByConfig);
			discriminativeModelQueryByConfig->Clear();
		}
		else
		{
			cout << "Error - Cannot find score file for config : " << mainConfig << std::endl;
		}
	}
	
	vector<CVPerEval> CVPerEvalList;
	cout << "Start calculate Beston Average " << std::endl;
	vector<string> evalNames = clusterScorePerAllConfigForCV.evalNames;
	int length = evalNames.size();
	for (int i = 0; i < length; i++)
	{
		string eval = evalNames[i];
		double cvValue = clusterScorePerAllConfigForCV.CalcBestonAvg( eval);

		CVPerEval cVPerEval;
		cVPerEval.evalName = eval;
		cVPerEval.CVValue = cvValue;
		CVPerEvalList.push_back(cVPerEval);
	}

	ofstream of;
	string cvOutFile =  outputFile + "/BestonAverageOnClusterScore.txt";
	of.open ( cvOutFile.c_str(), std::ofstream::out ); 
	of << "# evaluation , Beston Average  " << std::endl;

	length = CVPerEvalList.size();
	for (int i = 0; i < length; i++)
	{
		ResultToPrint evalfDocsResult;

		string eval = CVPerEvalList[i].evalName;
		double cvValue = CVPerEvalList[i].CVValue;
		of << eval << " " <<  cvValue << std::endl;

		evalfDocsResult.listOfSingleValue.insert(std::make_pair<string,double>("Performance Beston Average",cvValue));
		string path = outputFile + "/" + eval + ".dat";
		std::cout << "     write files for : " << eval + ".dat " << std::endl;
		evalfDocsResult.writeOnlySingelValue(path); 
	}
	of.close();  


	clusterScorePerAllConfigForCV.Clear();

}

#pragma endregion



}CBDM;



#endif