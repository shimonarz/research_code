#ifndef WordInClusterProbability_H
#define WordInClusterProbability_H


#include "GeneralFunctions.h"

#pragma region p(w | c)



typedef struct WordInClusterProbabilityForQuery
{
	double **ProbabilityMatrix;
	map< string , int > ClustersIndexMap;
	map< string , int > TermsIndexMap;

	string queryName;
	ClustersInQuery clusterListPerQuery; // key = query name , value = list of clusters

	void Init(string clusterfileName)
	{
		clusterListPerQuery.init(clusterfileName);

		vector<string> clustersNames;
		clusterListPerQuery.GetClusterNames(&clustersNames);
		MapStringToIndex(&clustersNames,&ClustersIndexMap);


		vector<string> clustersTerms;
		clusterListPerQuery.GetAllClusterTerms(&clustersTerms);
		MapStringToIndex(&clustersTerms, &TermsIndexMap);


		ProbabilityMatrix = new double*[ClustersIndexMap.size()];
		for (unsigned int i = 0; i < ClustersIndexMap.size(); ++i)
				ProbabilityMatrix[i] = new double[TermsIndexMap.size()];

		for (int clustersIndex = 0; clustersIndex < ClustersIndexMap.size(); clustersIndex++)
		{
			for (int termsIndex = 0; termsIndex < TermsIndexMap.size(); termsIndex++)
			{
				ProbabilityMatrix[clustersIndex][termsIndex] = 0;
			}
		}
	}

	void InitProbabilityMatrixFromFile_old(string fileName)
	{
		ifstream finput;
		// Get Number of lines
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return ;
		}
		int numberOfLines = 0;
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			numberOfLines ++;
		}
		finput.close();

		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return ;
		}

		// Read Terms from the first line
		if  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() != 0 ) 
			{
				vector<string> clustersTerms = split(line, ',');
				MapStringToIndex(&clustersTerms, &TermsIndexMap);
			}
		}


		ProbabilityMatrix = new double*[numberOfLines - 1];
		for (unsigned int i = 0; i < numberOfLines - 1; ++i)
				ProbabilityMatrix[i] = new double[TermsIndexMap.size()];

		int clusterIndex = 0;
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			if(line.size() == 0 ) continue;

			vector<string> splited = split(line, ':');
			string clusterName = splited[0];
			ClustersIndexMap.insert( std::make_pair( clusterName , clusterIndex) ); 
		
			string clustersTermsProbability = splited[1];
			vector<string> splitedclustersTermsProbability = split(clustersTermsProbability, ',');
			for (int i = 0; i < splitedclustersTermsProbability.size(); i++)
			{
				ProbabilityMatrix[clusterIndex][i] = convertToDouble(splitedclustersTermsProbability[i]);
			}	

			clusterIndex++;
		}
		finput.close();
	}

	void InitProbabilityMatrixFromFile(string fileName)
	{
		ifstream finput;
		// Get Number of lines
		finput.open ( fileName.c_str() ); 
		if (!finput.good())
		{
			std::cerr << "Can't open file: " << fileName << std::endl;
			return ;
		}
		vector<string> fileLines;
		
		while  ( finput.good() )
		{
			string line;
			getline (finput, line);
			fileLines.push_back(line);
		}
		finput.close();
		int numberOfLines = fileLines.size();

		// Read Terms from the first line
		string line = fileLines[0];
		if(line.size() != 0 ) 
		{
			vector<string> clustersTerms = split(line, ',');
			MapStringToIndex(&clustersTerms, &TermsIndexMap);
		}


		ProbabilityMatrix = new double*[numberOfLines - 1];
		for (unsigned int i = 0; i < numberOfLines - 1; ++i)
				ProbabilityMatrix[i] = new double[TermsIndexMap.size()];

		int clusterIndex = 0;
		for (int i = 1; i < numberOfLines; i++)
		{
			string line = fileLines[i];
			if(line.size() != 0 )
			{
				vector<string> splited = split(line, ':');
				string clusterName = splited[0];
				ClustersIndexMap.insert( std::make_pair( clusterName , clusterIndex) ); 
		
				string clustersTermsProbability = splited[1];
				vector<string> splitedclustersTermsProbability = split(clustersTermsProbability, ',');
				for (int i = 0; i < splitedclustersTermsProbability.size(); i++)
				{
					ProbabilityMatrix[clusterIndex][i] = convertToDouble(splitedclustersTermsProbability[i]);
				}
			clusterIndex++;
			}
		}
		fileLines.clear();
	}

	double GetProbability(string cluster , string term)
	{
		int TermsIndex =  TermsIndexMap.find(term)->second;
		int ClustersIndex =  ClustersIndexMap.find(cluster)->second;
		return ProbabilityMatrix[ClustersIndex][TermsIndex];
	}

	void SetProbability(string cluster , string term , double value)
	{
		int TermsIndex =  TermsIndexMap.find(term)->second;
		int ClustersIndex =  ClustersIndexMap.find(cluster)->second;
		ProbabilityMatrix[ClustersIndex][TermsIndex] = value;

		//cout << "SetProbability : ProbabilityMatrix[ " << ClustersIndex <<  "][" << TermsIndex << "] = " << ProbabilityMatrix[ClustersIndex][TermsIndex] << " " << value << "\n";
	}

	void Softmax_normalize()
	{
		for (int clustersIndex = 0; clustersIndex < ClustersIndexMap.size(); clustersIndex++)
		{
			// Probability = e ^ x
			for (int termsIndex = 0; termsIndex < TermsIndexMap.size(); termsIndex++)
			{
				ProbabilityMatrix[clustersIndex][termsIndex] = exp (ProbabilityMatrix[clustersIndex][termsIndex]);  
			}

			// sum all e ^ x
			double sum = 0;
			for (int termsIndex = 0; termsIndex < TermsIndexMap.size(); termsIndex++)
			{
				sum += ProbabilityMatrix[clustersIndex][termsIndex];
			}

			if(sum == 0 )
			{
				cout << "All the Probability are 0  \n"  ;
				return;
			}
			// e ^ x / Sum(e ^ x)
			for (int termsIndex = 0; termsIndex < TermsIndexMap.size(); termsIndex++)
			{
				ProbabilityMatrix[clustersIndex][termsIndex] = ProbabilityMatrix[clustersIndex][termsIndex] / sum;
			}
		}
	}

	void normalize()
	{
		for (int clustersIndex = 0; clustersIndex < ClustersIndexMap.size(); clustersIndex++)
		{
			double sum = 0;
			for (int termsIndex = 0; termsIndex < TermsIndexMap.size(); termsIndex++)
			{
				sum += ProbabilityMatrix[clustersIndex][termsIndex];
			}
			if(sum == 0 )
			{
				cout << "All the Probability are 0  \n"  ;
				return;
			}
			for (int termsIndex = 0; termsIndex < TermsIndexMap.size(); termsIndex++)
			{
				ProbabilityMatrix[clustersIndex][termsIndex] = ProbabilityMatrix[clustersIndex][termsIndex] / sum;
			}
		}
	}

	void WriteWordInClusterProbabilityToFile(string fileName)
	{
		ofstream of;
		of.open ( fileName.c_str(), std::ofstream::out ); 

		// write Term
		map< string , int >::iterator it_term;
		string termsList = "";
		for(it_term = TermsIndexMap.begin() ; it_term != TermsIndexMap.end() ; ++it_term)
		{
			string term = it_term->first;
			termsList += term + "," ;
		}
		termsList = trimEnd(termsList,",");
		of << termsList << "\n" ;	

		// write clusters and Probability
		map< string , int >::iterator it_cluster;
		for(it_cluster = ClustersIndexMap.begin() ; it_cluster != ClustersIndexMap.end() ; ++it_cluster)
		{
			string cluster = it_cluster->first;
			int cluster_index = it_cluster->second;
			string fullCluster = cluster + ":";
			for(it_term = TermsIndexMap.begin() ; it_term != TermsIndexMap.end() ; ++it_term)
		    {
			    int term_index = it_term->second;
				fullCluster += SSTR(ProbabilityMatrix[cluster_index][term_index]) + "," ;
			}
			fullCluster = trimEnd(fullCluster,",");
			of << fullCluster << "\n" ;	
		}
		of.close();
	}
	
	

	void Clear()
	{
		for (unsigned int i = 0; i < TermsIndexMap.size(); ++i)
				delete ProbabilityMatrix[i] ;
		delete ProbabilityMatrix ;
	}

private:
	void MapStringToIndex(vector<string> *strings,map< string , int > *map )
	{
		int length = strings->size();
		for (int i = 0; i < length; i++)
		{
			map->insert( std::make_pair( (*strings)[i] , i) ); 
		}
	}

	void SetClusterProbability(string cluster , string term , double value)
	{
		int TermsIndex =  TermsIndexMap.find(term)->second;
		int ClustersIndex =  ClustersIndexMap.find(cluster)->second;
		ProbabilityMatrix[TermsIndex][ClustersIndex] = value;
	}
}WordInClusterProbability;

typedef struct WordInClusterProbabilityForAllQuery
{
	map< string , WordInClusterProbabilityForQuery* > ClusterListPerQuery; // key = query name , value = list of clusters

	void init(vector<string> *filesNames)
	{
		FILE_LOG(logDEBUG) << "Start Read Clusters file to initiate  WordInClusterProbabilityCreator structure" ;
		if(filesNames->size() == 0 )
		{
			FILE_LOG(logERROR) << "Cannot Read Clusters file to initiate WordInClusterProbabilityCreator structure - receive 0 files to read";
		}
		for (int i = 0; i < filesNames->size() ; i++)
		{
			string fullFileName = (*filesNames)[i];
			WordInClusterProbabilityForQuery *wordInClusterProbabilityForQuery = new WordInClusterProbabilityForQuery();
			wordInClusterProbabilityForQuery->InitProbabilityMatrixFromFile(fullFileName);
			string fileName = getFileName( fullFileName);
			vector<string> splited = split(fileName, '.');
			string queryName = splited[0];

			cout << '\r' << "Add query " << queryName << " - Progress : " << i+1 <<  '/' <<  filesNames->size() <<
				".     Progress Percent " << (int)(((double)(1+i)/(double)filesNames->size())*100.0)  <<  "%" << std::flush;

			ClusterListPerQuery.insert(std::make_pair< string , WordInClusterProbabilityForQuery* > (queryName,wordInClusterProbabilityForQuery));	
		}
		cout << '\n';
	}

	WordInClusterProbabilityForQuery* GetClusterList(string queryName)
	{
		map< string , WordInClusterProbabilityForQuery* >::iterator it = ClusterListPerQuery.find(queryName);
		if(it != ClusterListPerQuery.end())
		{
			return it->second;
		}
		cout << "Cannot find Query " << queryName << " in WordInClusterProbability list\n";
		return NULL;
	}

	double GetWordInClusterProbability(string query , string cluster , string term ) // P(w|c)
	{
		double probability = 0;
		WordInClusterProbabilityForQuery* probabilityForQuery = GetClusterList(query);
		if(probabilityForQuery == NULL)
		{
			FILE_LOG(logERROR) << "Cannot get Probability for:  " << " query=" << query << ", cluster=" << cluster << ", term=" << term ;
			return 0;
		}
		return probabilityForQuery->GetProbability(cluster,term);
	}

	void Clear()
	{
		std::cout << "Clear all allocation from WordInClusterProbabilityForAllQuery structure" << std::endl;

		map< string , WordInClusterProbabilityForQuery* >::iterator ClusterListPerQuery_it;
		for(ClusterListPerQuery_it = ClusterListPerQuery.begin() ; 
			ClusterListPerQuery_it != ClusterListPerQuery.end() ;
			++ClusterListPerQuery_it)
		{
			ClusterListPerQuery_it->second->Clear();
			delete ClusterListPerQuery_it->second;
		}
		ClusterListPerQuery.clear();
	}

}WordInClusterProbabilityForAllQuery;

typedef struct WordInClusterProbabilityCreator
{
	map< string , WordInClusterProbabilityForQuery* > ClusterListPerQuery; // key = query name , value = list of clusters
	
	
	void init(vector<string> filesNames)
	{
		std::cout << "Start Read Clusters file to initiate  WordInClusterProbabilityCreator structure" << std::endl;
		for (int i = 0; i < filesNames.size() ; i++)
		{
			cout << '\r' << "		Progress : " << i+1 <<  '/' <<  filesNames.size() << ".     Progress Percent " << (int)(((double)(1+i)/(double)filesNames.size())*100.0)  <<  "%" << std::flush;

			string fullFileName = filesNames[i];
			WordInClusterProbabilityForQuery *wordInClusterProbabilityForQuery = new WordInClusterProbabilityForQuery();
			wordInClusterProbabilityForQuery->Init(fullFileName);
			string fileName = getFileName( fullFileName);
			vector<string> splited = split(fileName, '.');
			string queryName = splited[0];
			ClusterListPerQuery.insert(std::make_pair< string , WordInClusterProbabilityForQuery* > (queryName,wordInClusterProbabilityForQuery));
			
		}
		cout << '\n';
	}

	void CreateUniformDistribution(string outputDir)
	{
		std::cout << "Start Create Uniform Distribution for Word-In-Cluster-Probability" << std::endl;
		int j=1;
		map< string , WordInClusterProbabilityForQuery* >::iterator ClusterListPerQuery_it;
		for(ClusterListPerQuery_it = ClusterListPerQuery.begin() ; 
			ClusterListPerQuery_it != ClusterListPerQuery.end() ;
			++ClusterListPerQuery_it)
		{
			cout << '\r' << "		Progress : " << j <<  '/' <<  ClusterListPerQuery.size() << ".     Progress Percent " << (int)(((double)(j)/(double)ClusterListPerQuery.size())*100.0)  <<  "%" << std::flush;

			string queryName = ClusterListPerQuery_it->first;
			map< string , vector<Term> >::iterator ClusterInQueryList_it;
			for(ClusterInQueryList_it = ClusterListPerQuery_it->second->clusterListPerQuery.ClusterInQueryList.begin() ; 
				ClusterInQueryList_it != ClusterListPerQuery_it->second->clusterListPerQuery.ClusterInQueryList.end() ; 
				++ClusterInQueryList_it)
			{
				string clusterMainTerm = ClusterInQueryList_it->first;
				int clusterSize = (ClusterInQueryList_it->second).size();
				for (int i = 0; i < clusterSize ; i++)
				{
					ClusterListPerQuery_it->second->SetProbability(clusterMainTerm,ClusterInQueryList_it->second[i].value , 1.0 / clusterSize );
				}
			}
			string newfileName = outputDir + "/" + queryName + ".UniformQueryClustersDistribution" ;
			ClusterListPerQuery_it->second->normalize();
			ClusterListPerQuery_it->second->WriteWordInClusterProbabilityToFile(newfileName);
			
			j++;
		}
		cout << '\n';
	}

	void CreateProportionalToRM1Distribution(string outputDir, string rm1HighestTermsFiles)
	{
		std::cout << "Start Create Proportional-To-RM1 Distribution for Word-In-Cluster-Probability" << std::endl;

		std::cout << "Read First Query Expansion File " << std::endl;
		std::vector<FirstQueryExpension*> *rm =  FirstQueryExpension::readExpensQueryFile(rm1HighestTermsFiles);
		int j=1;
		std::cout << "Calculate for each cluster - Proportional To RM1 Distribution " << std::endl;
		map< string , WordInClusterProbabilityForQuery* >::iterator ClusterListPerQuery_it;
		for(ClusterListPerQuery_it = ClusterListPerQuery.begin() ; 
			ClusterListPerQuery_it != ClusterListPerQuery.end() ;
			++ClusterListPerQuery_it)
		{
			cout << '\r' << "		Progress : " << j <<  '/' <<  ClusterListPerQuery.size() << ".     Progress Percent " << (int)(((double)(j)/(double)ClusterListPerQuery.size())*100.0)  <<  "%" << std::flush;

			string queryName = ClusterListPerQuery_it->first;

			FirstQueryExpension *firstQueryExpension = NULL;
			for (unsigned int i1 = 0 ; i1 < rm->size() ; ++i1)
			{ 
				FirstQueryExpension *local = (*rm)[i1];
				if( local->QueryId == queryName)
				{
					firstQueryExpension = local;
					break;
				}
			}

			if(firstQueryExpension == NULL)
			{
				cout << "Cannot find query " << queryName << " in First Query Expension files" << std::endl;
				continue;
			}

			
			map< string , vector<Term> >::iterator ClusterInQueryList_it;
			for(ClusterInQueryList_it = ClusterListPerQuery_it->second->clusterListPerQuery.ClusterInQueryList.begin() ; 
				ClusterInQueryList_it != ClusterListPerQuery_it->second->clusterListPerQuery.ClusterInQueryList.end() ; 
				++ClusterInQueryList_it)
			{
				string cluster = ClusterInQueryList_it->first;
				int clusterSize = (ClusterInQueryList_it->second).size();
				for (int i = 0; i < clusterSize ; i++)
				{
					string term = ClusterInQueryList_it->second[i].value;
					Term* termFromrm1 = firstQueryExpension->FindInExpensionTerms(term);
					if(termFromrm1 == NULL)
					{
						cout << "Cannot find for query " << queryName << " the term " << term << std::endl;
						continue;
					}
					ClusterListPerQuery_it->second->SetProbability(cluster,term , termFromrm1->weight );
				}
			}
			string newfileName = outputDir + "/" + queryName + ".ProportionalToRM1Distribution" ;
			ClusterListPerQuery_it->second->normalize();
			ClusterListPerQuery_it->second->WriteWordInClusterProbabilityToFile(newfileName);
			j++;
		}
		cout << '\n';


		for (unsigned int i1 = 0 ; i1 < rm->size() ; ++i1)
		{ 
			FirstQueryExpension *local = (*rm)[i1];
			local->Clear();
			delete local;
		}
		rm->clear();
		delete rm;
	}

	void CreateW2VSimilarityToClusterCentroidDistribution(string outputDir, vector<string> w2vModels)
	{
		std::cout << "Start Create W2V-Similarity-To-Cluster-Centroid Distribution for Word-In-Cluster-Probability" << std::endl;
		WordToVecModel *wordToVecModel;
		
		WordToVecModel globalWordToVecModel;
		std::cout << "There is " << w2vModels.size() << " word to vec models\n"; 
		#pragma region Read Word-To-Vec Model File
		bool isOnlyOneW2vModel = (w2vModels.size() == 1);
		if(isOnlyOneW2vModel)
		{
			std::cout << "Read Word-To-Vec Model File " << std::endl;
			if( ! globalWordToVecModel.LoadWordToVecModel(w2vModels[0]))
			{
				return;
			}
			wordToVecModel = &globalWordToVecModel;
		}
		#pragma endregion


		
		int j=1;
		map< string , WordInClusterProbabilityForQuery* >::iterator ClusterListPerQuery_it;
		for(ClusterListPerQuery_it = ClusterListPerQuery.begin() ; 
			ClusterListPerQuery_it != ClusterListPerQuery.end() ;
			++ClusterListPerQuery_it)
		{
			
			string queryName = ClusterListPerQuery_it->first;

			cout <<   '\r' << queryName << " :	Progress : " << j <<  '/' <<  ClusterListPerQuery.size() << 
				".     Progress Percent " << (int)(((double)(j)/(double)ClusterListPerQuery.size())*100.0)  <<  "%" << std::flush;

			WordToVecModel localWordToVecModel;
			#pragma region Read Word-To-Vec Model File
			if(! isOnlyOneW2vModel)
			{
				string w2vFileName = getW2vModelName(&w2vModels, queryName );
				std::cout << "Read Word-To-Vec Model File : " << w2vFileName <<  std::endl;
				if( w2vFileName != "" )
				{
					if( ! localWordToVecModel.LoadWordToVecModel(w2vFileName) )
					{
						continue;
					}
					wordToVecModel = &localWordToVecModel;
				}
				else
				{
					cout << "Can find w2v model for query " << queryName << std::endl;
					continue;
				}
			}
			#pragma endregion

			map< string , vector<Term> >::iterator ClusterInQueryList_it;
			for(ClusterInQueryList_it = ClusterListPerQuery_it->second->clusterListPerQuery.ClusterInQueryList.begin() ; 
				ClusterInQueryList_it != ClusterListPerQuery_it->second->clusterListPerQuery.ClusterInQueryList.end() ; 
				++ClusterInQueryList_it)
			{
				string cluster = ClusterInQueryList_it->first;
				int clusterSize = (ClusterInQueryList_it->second).size();

				//std::cout << "			cluster : " << cluster << std::endl;
				// Get cluster terms
				vector<string> clusterTerms;
				for (int i = 0; i < clusterSize ; i++)
				{
					string term = ClusterInQueryList_it->second[i].value;
					clusterTerms.push_back(term);
				}
				//std::cout << "			cluster Terms : " << clusterSize << std::endl;
				// calculate cluster Centroid
				float *clusterCentroid =  wordToVecModel->GetCentroid(clusterTerms);

				// calculate similarity between all clusters terms to cluster Centroid
				for (int i = 0; i < clusterSize ; i++)
				{
					string term = ClusterInQueryList_it->second[i].value;
					double weight = wordToVecModel->CalculateSimilarity(term,clusterCentroid);
					weight = exp(weight);
					ClusterListPerQuery_it->second->SetProbability(cluster,term , weight );
				}

				delete  clusterCentroid;
			}
			string newfileName = outputDir + "/" + queryName + ".W2VSimilarityToClusterCentroidDistribution" ;
			ClusterListPerQuery_it->second->normalize();
			ClusterListPerQuery_it->second->WriteWordInClusterProbabilityToFile(newfileName);
			j++;

			if(! isOnlyOneW2vModel)
			{
				localWordToVecModel.Clear();
				wordToVecModel = NULL;
			}
		}
		cout << '\n';
		if(isOnlyOneW2vModel)
		{
			globalWordToVecModel.Clear();
		}
	}



	void CreateW2VSimilarityToClusterCentroidDistributionForAllTerms(string outputDir, vector<string> w2vModels)
	{
		std::cout << "Start Create W2V-Similarity-To-Cluster-Centroid-For-All-Terms Distribution for Word-In-Cluster-Probability" << std::endl;

		WordToVecModel wordToVecModel;

		#pragma region Read Word-To-Vec Model File
		bool isOnlyOneW2vModel = (w2vModels.size() == 1);
		if(isOnlyOneW2vModel)
		{
			std::cout << "Read Word-To-Vec Model File " << std::endl;
			if( ! wordToVecModel.LoadWordToVecModel(w2vModels[0]))
			{
				return;
			}
		}
		#pragma endregion


		int j=1;
		map< string , WordInClusterProbabilityForQuery* >::iterator ClusterListPerQuery_it;
		for(ClusterListPerQuery_it = ClusterListPerQuery.begin() ; 
			ClusterListPerQuery_it != ClusterListPerQuery.end() ;
			++ClusterListPerQuery_it)
		{
			string queryName = ClusterListPerQuery_it->first;

			#pragma region Read Word-To-Vec Model File
			if(! isOnlyOneW2vModel)
			{
				string w2vFileName = getW2vModelName(&w2vModels, queryName );
				std::cout << "Read Word-To-Vec Model File " << std::endl;
				if( w2vFileName != "" )
				{
					if( ! wordToVecModel.LoadWordToVecModel(w2vFileName))
					{
						continue;
					}
				}
				else
				{
					cout << "Can find w2v model for query " << queryName << std::endl;
					continue;
				}
			}
			#pragma endregion

			map< string , vector<Term> >::iterator ClusterInQueryList_it;
			for(ClusterInQueryList_it = ClusterListPerQuery_it->second->clusterListPerQuery.ClusterInQueryList.begin() ; 
				ClusterInQueryList_it != ClusterListPerQuery_it->second->clusterListPerQuery.ClusterInQueryList.end() ; 
				++ClusterInQueryList_it)
			{
				cout << '\r' << "		Progress : " << j <<  '/' <<  ClusterListPerQuery.size() << ".     Progress Percent " << (int)(((double)(j)/(double)ClusterListPerQuery.size())*100.0)  <<  "%" << std::flush;

				string cluster = ClusterInQueryList_it->first;
				int clusterSize = (ClusterInQueryList_it->second).size();

				// Get cluster terms
				vector<string> clusterTerms;
				for (int i = 0; i < clusterSize ; i++)
				{
					string term = ClusterInQueryList_it->second[i].value;
					clusterTerms.push_back(term);
				}
				// calculate cluster Centroid
				float *clusterCentroid =  wordToVecModel.GetCentroid(clusterTerms);

				// calculate similarity between all terms to cluster Centroid
				map< string , int >::iterator trems_it;
				for(trems_it = ClusterListPerQuery_it->second->TermsIndexMap.begin();
					trems_it != ClusterListPerQuery_it->second->TermsIndexMap.end();
					++trems_it)
				{
					string term = trems_it->first;
					double weight = wordToVecModel.CalculateSimilarity(term,clusterCentroid);
					weight = exp(weight);
					ClusterListPerQuery_it->second->SetProbability(cluster,term , weight );
				}
	
				delete  clusterCentroid;
			}
			string newfileName = outputDir + "/" + queryName + ".W2VSimilarityToClusterCentroidDistributionForAllTerms" ;
			ClusterListPerQuery_it->second->normalize();
			ClusterListPerQuery_it->second->WriteWordInClusterProbabilityToFile(newfileName);
			j++;

			if(! isOnlyOneW2vModel)
			{
				wordToVecModel.Clear();
			}
		}
		cout << '\n';
		if(isOnlyOneW2vModel)
		{
			wordToVecModel.Clear();
		}
	}

	void CreateUniformDistributionForAllTerms(string outputDir)
	{
		std::cout << "Start Create Uniform Distribution for Word-In-Cluster-Probability" << std::endl;
		int j=1;
		map< string , WordInClusterProbabilityForQuery* >::iterator ClusterListPerQuery_it;
		for(ClusterListPerQuery_it = ClusterListPerQuery.begin() ; 
			ClusterListPerQuery_it != ClusterListPerQuery.end() ;
			++ClusterListPerQuery_it)
		{
			cout << '\r' << "		Progress : " << j <<  '/' <<  ClusterListPerQuery.size() << ".     Progress Percent " << (int)(((double)(j)/(double)ClusterListPerQuery.size())*100.0)  <<  "%" << std::flush;

			string queryName = ClusterListPerQuery_it->first;
			map< string , vector<Term> >::iterator ClusterInQueryList_it;
			for(ClusterInQueryList_it = ClusterListPerQuery_it->second->clusterListPerQuery.ClusterInQueryList.begin() ; 
				ClusterInQueryList_it != ClusterListPerQuery_it->second->clusterListPerQuery.ClusterInQueryList.end() ; 
				++ClusterInQueryList_it)
			{
				string clusterMainTerm = ClusterInQueryList_it->first;
				map< string , int >::iterator trems_it;
				for(trems_it = ClusterListPerQuery_it->second->TermsIndexMap.begin();
					trems_it != ClusterListPerQuery_it->second->TermsIndexMap.end();
					++trems_it)
				{
					string term = trems_it->first;
					double weight = 1.0 / ClusterListPerQuery_it->second->TermsIndexMap.size();
					ClusterListPerQuery_it->second->SetProbability(clusterMainTerm,term , weight );
				}
		
			}
			string newfileName = outputDir + "/" + queryName + ".UniformQueryClustersDistributionForAllTerms" ;
			ClusterListPerQuery_it->second->normalize();
			ClusterListPerQuery_it->second->WriteWordInClusterProbabilityToFile(newfileName);
			
			j++;
		}
		cout << '\n';
	}

	void CreateProportionalToRM1DistributionForAllTerms(string outputDir, string rm1HighestTermsFiles)
	{
		std::cout << "Start Create Proportional-To-RM1 Distribution for Word-In-Cluster-Probability" << std::endl;

		std::cout << "Read First Query Expansion File " << std::endl;
		std::vector<FirstQueryExpension*> *rm =  FirstQueryExpension::readExpensQueryFile(rm1HighestTermsFiles);
		int j=1;
		std::cout << "Calculate for each cluster - Proportional To RM1 Distribution " << std::endl;
		map< string , WordInClusterProbabilityForQuery* >::iterator ClusterListPerQuery_it;
		for(ClusterListPerQuery_it = ClusterListPerQuery.begin() ; 
			ClusterListPerQuery_it != ClusterListPerQuery.end() ;
			++ClusterListPerQuery_it)
		{
			cout << '\r' << "		Progress : " << j <<  '/' <<  ClusterListPerQuery.size() << ".     Progress Percent " << (int)(((double)(j)/(double)ClusterListPerQuery.size())*100.0)  <<  "%" << std::flush;

			string queryName = ClusterListPerQuery_it->first;

			FirstQueryExpension *firstQueryExpension = NULL;
			for (unsigned int i1 = 0 ; i1 < rm->size() ; ++i1)
			{ 
				FirstQueryExpension *local = (*rm)[i1];
				if( local->QueryId == queryName)
				{
					firstQueryExpension = local;
					break;
				}
			}

			if(firstQueryExpension == NULL)
			{
				cout << "Cannot find query " << queryName << " in First Query Expension files" << std::endl;
				continue;
			}

			
			map< string , vector<Term> >::iterator ClusterInQueryList_it;
			for(ClusterInQueryList_it = ClusterListPerQuery_it->second->clusterListPerQuery.ClusterInQueryList.begin() ; 
				ClusterInQueryList_it != ClusterListPerQuery_it->second->clusterListPerQuery.ClusterInQueryList.end() ; 
				++ClusterInQueryList_it)
			{
				string cluster = ClusterInQueryList_it->first;

				map< string , int >::iterator trems_it;
				for(trems_it = ClusterListPerQuery_it->second->TermsIndexMap.begin();
					trems_it != ClusterListPerQuery_it->second->TermsIndexMap.end();
					++trems_it)
				{
					string term = trems_it->first;
					Term* termFromrm1 = firstQueryExpension->FindInExpensionTerms(term);
					if(termFromrm1 == NULL)
					{
						cout << "Cannot find for query " << queryName << " the term " << term << std::endl;
						continue;
					}
					double weight = termFromrm1->weight;
					ClusterListPerQuery_it->second->SetProbability(cluster,term , weight );
				}
			}
			string newfileName = outputDir + "/" + queryName + ".ProportionalToRM1DistributionForAllTerms" ;
			ClusterListPerQuery_it->second->normalize();
			ClusterListPerQuery_it->second->WriteWordInClusterProbabilityToFile(newfileName);
			j++;
		}
		cout << '\n';


		for (unsigned int i1 = 0 ; i1 < rm->size() ; ++i1)
		{ 
			FirstQueryExpension *local = (*rm)[i1];
			local->Clear();
			delete local;
		}
		rm->clear();
		delete rm;
	}

	void Clear()
	{
		std::cout << "Clear all allocation from WordInClusterProbabilityCreator structure" << std::endl;

		map< string , WordInClusterProbabilityForQuery* >::iterator ClusterListPerQuery_it;
		for(ClusterListPerQuery_it = ClusterListPerQuery.begin() ; 
			ClusterListPerQuery_it != ClusterListPerQuery.end() ;
			++ClusterListPerQuery_it)
		{
			ClusterListPerQuery_it->second->Clear();
			delete ClusterListPerQuery_it->second;
		}
		ClusterListPerQuery.clear();
	}

}WordInClusterProbabilityCreator;

#pragma endregion

#endif