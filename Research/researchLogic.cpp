
#include "research.h"
#include "DataStructer.h"
#include "WordInClusterProbability.h"
#include "Evaluate.h"


void CompareBetweenCBToCBRM3Models_WordComeFromRM1(string pathToDir,string cvPath , vector<string> *fileNameList)
{
	std::cout << "Order configurations file By model ... " << std::endl;
	// get for each configuration all the file that belong to him.
	std::map<string,vector<string>*> orderModelFiles;
	orderFileByConfig(fileNameList,Model,&orderModelFiles); 
	std::cout << "After we receive that we have  " << orderModelFiles.size() << " different models " << std::endl;

	std::cout << "Read cross validation file." << std::endl;
	// rm3 cv data
	Rm3CVFile rm3CVForEvaluations ; 
	rm3CVForEvaluations.init(cvPath);

	std::map<string,ResultToPrint*> evaluationPerfDocsResult;


	std::cout << "Order configurations file By configuration for each model ... " << std::endl;
	std::map<string,vector<string>*>::iterator orderModelFilesIt;

	// fore each model
	for( orderModelFilesIt = orderModelFiles.begin() ; orderModelFilesIt != orderModelFiles.end() ; ++orderModelFilesIt)
	{
		std::map<string,vector<string>*> *orderFiles = new std::map<string,vector<string>*>();
		std::cout << "	Order configurations file By configuration for model :" << orderModelFilesIt->first <<  std::endl;
		orderFileByConfig(orderModelFilesIt->second,Retrival,orderFiles); 

		std::cout << "	For each configuration calculate average for all evaluation measures ... " << std::endl;
		std::map<string,AvgEvaluateResultFiles*> *resultPerConfig = new std::map<string,AvgEvaluateResultFiles*>(); /// key=configuration , vlaue= average all files in the configuration
		map<string,vector<string>*>::iterator orderFilesIt;

		// read and built structure from configuration files
		for(orderFilesIt = orderFiles->begin(); orderFilesIt != orderFiles->end(); orderFilesIt++) 
		{
			std::cout << "        Configuration Name : " << orderFilesIt->first << " Number of files : " << orderFilesIt->second->size() << std::endl;
			vector<string> *localfileNameList = orderFilesIt->second; // list of evaluate files
			AvgEvaluateResultFiles* avgEvaluateResultFiles = new AvgEvaluateResultFiles();
			avgEvaluateResultFiles->init();
			avgEvaluateResultFiles->AvgFilesToAvg(localfileNameList);
			resultPerConfig->insert(std::make_pair<string,AvgEvaluateResultFiles*> (orderFilesIt->first, avgEvaluateResultFiles)); 
		}

		std::set<string> *evaluationNames = &(resultPerConfig->begin()->second->evaluateResultFile.evaluationNames);
		std::vector<string> output(evaluationNames->size());
		std::copy(evaluationNames->begin(), evaluationNames->end(), output.begin());

		// for each evaluation
		for (unsigned int i = 0; i < output.size(); i++)
		{
			string evalName = output[i];
		
			Rm3CVFileLine rm3 = rm3CVForEvaluations.getRm3CVFileLine(evalName);
			double cvRm3100Value = rm3.rm3100CVValue;

			std::map<string,AvgEvaluateResultFiles*>::iterator resultPerConfigIterator;

			unsigned int maxIntersectionIndex = 0;
			string maxIntersectionIndexConfigName = "";

			double maxClusterValue = -1;
			string maxClusterValueConfigName;

			// for each configuration
			for(resultPerConfigIterator = resultPerConfig->begin(); resultPerConfigIterator != resultPerConfig->end(); resultPerConfigIterator++) 
			{
				string configName = resultPerConfigIterator->first;
				AvgEvaluateResultFiles* clustres = resultPerConfigIterator->second;

				QueryResult*  resultPerEvalAndConfig = clustres->getEvaluateValuesInFile(evalName);
				vector<double>* clusters = resultPerEvalAndConfig->getClustresValues();
				
				unsigned int intersectionIndex = 0;
				double clusterValue = (*clusters)[0];

				unsigned int length = clusters->size() ;
				// find The farthest point of intersection with the Cross validation RM3 with 100 terms line.
				for (unsigned int i = 0; i < length -1 ; i++)
				{
					if( (*clusters)[i] >= cvRm3100Value && cvRm3100Value >= (*clusters)[i+1])
					{
						intersectionIndex = i;
					}
				}

				if(intersectionIndex > maxIntersectionIndex )
				{
					maxIntersectionIndex = intersectionIndex;
					maxIntersectionIndexConfigName = resultPerConfigIterator->first;
				}

				// find The highest value of eval for the top rated cluster average.
				if(clusterValue > maxClusterValue )
				{
					maxClusterValue = clusterValue;
					maxClusterValueConfigName = resultPerConfigIterator->first;
				}

			}

			ResultToPrint* evalfDocsResult;

			std::map<string,ResultToPrint*>::iterator it = evaluationPerfDocsResult.find(evalName);
			if(it != evaluationPerfDocsResult.end() )
			{
				evalfDocsResult = it->second;
			}
			else
			{
				evalfDocsResult = new ResultToPrint();

				evalfDocsResult->listOfSingleValue.insert(std::make_pair<string,double>("CV RM3-100 Terms",cvRm3100Value));
	
				evaluationPerfDocsResult.insert(std::make_pair<string,ResultToPrint*>(evalName,evalfDocsResult));
			}

			if(maxIntersectionIndexConfigName == "")
			{
				maxIntersectionIndexConfigName = maxClusterValueConfigName;
			}

			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>(orderModelFilesIt->first + "Farthest Intersection with CV-RM3-100 Terms - " + getConfigNaem(maxIntersectionIndexConfigName),resultPerConfig->find(maxIntersectionIndexConfigName)->second->getEvaluateValuesInFile(evalName)->getClustresValues()));
			evalfDocsResult->listOfMultipleValue.insert(std::make_pair<string,vector<double>*>(orderModelFilesIt->first + "Highest best cluster value - " + getConfigNaem(maxClusterValueConfigName),resultPerConfig->find(maxClusterValueConfigName)->second->getEvaluateValuesInFile(evalName)->getClustresValues()));

		}


		for(orderFilesIt = orderFiles->begin(); orderFilesIt != orderFiles->end(); orderFilesIt++) 
		{
			delete orderFilesIt->second;
		}
		orderFiles->clear();
		delete orderFiles;
	}

	std::cout << "Write files ..." << std::endl;
	std::map<string,ResultToPrint*>::iterator  evaluationPerfDocsResultIterator;
	for(evaluationPerfDocsResultIterator = evaluationPerfDocsResult.begin(); evaluationPerfDocsResultIterator != evaluationPerfDocsResult.end(); evaluationPerfDocsResultIterator++) 
	{
		string evalName = evaluationPerfDocsResultIterator->first;
		ResultToPrint* fDocsResultToPlot = evaluationPerfDocsResultIterator->second;
		string path = pathToDir + "/" + evalName + ".dat";
		std::cout << "     write files for : " << evalName + ".dat   ,   " << fDocsResultToPlot->toString()<< std::endl;
		fDocsResultToPlot->writeFileForPlot(path); 
	}


	for(orderModelFilesIt = orderModelFiles.begin(); orderModelFilesIt != orderModelFiles.end(); orderModelFilesIt++) 
	{
		delete orderModelFilesIt->second;
	}
	orderModelFiles.clear();

	


	
}


void CreateClusterFileForAllQueries(string firstQueryExpensionFile,vector<string> wordToVecModelFileNameVec,int k,string outputDir )
{
	FILE_LOG(logINFO) << "Read First Query Expansion File " ;
	std::vector<FirstQueryExpension*> *rm =  FirstQueryExpension::readExpensQueryFile(firstQueryExpensionFile);

	WordToVecModel wordToVecModel;
	bool isOnlyOneW2vModel = (wordToVecModelFileNameVec.size() == 1);
	if(isOnlyOneW2vModel)
	{
		FILE_LOG(logINFO) << "Read Word-To-Vec Model File " ;
		if( ! wordToVecModel.LoadWordToVecModel(wordToVecModelFileNameVec[0]))
		{
			return;
		}
	}

	FILE_LOG(logINFO)<< "Start clustering all queries : " ;
	for (unsigned int j = 0 ; j < rm->size() ; ++j)
    {  
		FirstQueryExpension *firstQueryExpension = (*rm)[j];
		std::cout << "\r" << "	Clustering Query " <<  firstQueryExpension->QueryId << std::flush;

		// the file name of queries that each query represent cluster.
		std::string  outputFile = outputDir + "/" + firstQueryExpension->QueryId + ".QueryClusters";

		if(isOnlyOneW2vModel)
		{
			CreateClusterFileForQuery(firstQueryExpension,k,&wordToVecModel,outputFile);		
		}
		else
		{
			string w2vFileName = getW2vModelName(&wordToVecModelFileNameVec, firstQueryExpension->QueryId );
			if( w2vFileName != "" )
			{
				std::cout << "Read Word-To-Vec Model File for query " << firstQueryExpension->QueryId << " file name is : " << w2vFileName <<  std::endl;
				WordToVecModel queryWordToVecModel;
				if( ! queryWordToVecModel.LoadWordToVecModel(w2vFileName))
				{
					cout << "Can load w2v model for query " << firstQueryExpension->QueryId << std::endl;
					break;
				}
				CreateClusterFileForQuery(firstQueryExpension,k,&queryWordToVecModel,outputFile);	
				queryWordToVecModel.Clear();
			}
			else
			{
				cout << "Can find w2v model for query " << firstQueryExpension->QueryId << std::endl;
			}
		}

	}
	for (unsigned int i1 = 0 ; i1 < rm->size() ; ++i1)
	{ 
			FirstQueryExpension *local = (*rm)[i1];
			local->Clear();
			delete local;
	}
	rm->clear();
	delete rm;
}
void CreateClusterFileForQuery(FirstQueryExpension *firstQueryExpension,int k,WordToVecModel *wordToVecModel,string outputFile)
{
	ofstream of;

	of.open ( outputFile.c_str(), std::ofstream::out ); 
	
	std::map<std::string,double> rm_terms;

	// Calculate distance between each of the Discriminative Model words
	FILE_LOG(logINFO) << "Calculate distance between each of the Model words" ;
	std::vector<Term*> &TermFromDiscriminativeModel = firstQueryExpension->ExpensionTerms;
	for (unsigned int i = 0 ; i <  TermFromDiscriminativeModel.size() ; ++i)
	{ 
		Term *term = TermFromDiscriminativeModel[i];
		std::string termValue = term->value;
		double weight = term->weight;
		rm_terms.insert(std::make_pair<string,double> (termValue, weight));
	}
	double **distancesMatrix = wordToVecModel->createDistancesMatrix(rm_terms);
	
	std::string queryNumber = firstQueryExpension->QueryId ;


	#pragma region Write Clusers query with: RM3 100 terms and Cluster of K Terms
	FILE_LOG(logINFO) << "Write Clusters query with: RM3 100 terms and Cluster of K Terms" ;
	// for all clusters
	unsigned int numberOfExpensionTerms = firstQueryExpension->ExpensionTerms.size();
	for( unsigned int i = 0 ; i< numberOfExpensionTerms ; ++i)
	{
		string clusterMainTerm = firstQueryExpension->ExpensionTerms[i]->value;

		vector<std::pair<int,double> > vcos;

		// get similarity form all words
		FILE_LOG(logDEBUG) << "get similarity form all words" ;
		for( int j = 0 ; j< numberOfExpensionTerms ; ++j)
		{
			vcos.push_back(std::make_pair<int,double>(j, distancesMatrix[i][j] ));
		}
		FILE_LOG(logDEBUG) << "sort the list of similarities" ;
		// sort the list of similarities
		std::sort(vcos.begin(), vcos.end(), sort_pred);

		// if the term is not in the word2Vec vocabulary, 
		// then all array is zeros, therefore after sort the highest value be zero.
		if( vcos[0].second == 0)
		{
			continue;
		}

		double sum = 0;
		// get the best K tems
		FILE_LOG(logDEBUG) << "get the best K tems" ;
		vcos.erase(vcos.begin()+k+1, vcos.end());

		string fullCluster = clusterMainTerm + ":";
		for ( int i1=0; i1<k+1 ; ++i1 )
		{
			int index = vcos[i1].first;
			fullCluster += firstQueryExpension->ExpensionTerms[index]->value + "," ;
		}
		fullCluster = trimEnd(fullCluster,",");
		FILE_LOG(logDEBUG) << "Write Cluster on file " ;
		of << fullCluster << "\n" ;	
	}

	#pragma endregion

	delete []distancesMatrix;

	of.close(); 
}
void CreateQrelFileForAllQueryClusers(vector<string> clustersfilesNames , string qrelsFileName , string outputDir)
{

	QrelsFile QrelsFile;
	std::cout << "Read Qrels File " << std::endl;
	QrelsFile.init(qrelsFileName);
	QrelsFile.SetScoreToOneOrZero();
	
	std::cout << "Order Qrels by queries" << std::endl;
	std::map<string,std::vector<QrelsLine*>*> quralMap;
	for(unsigned int i = 0 ; i< QrelsFile.Lines.size() ; i ++ )
	{
		QrelsLine* line = QrelsFile.Lines[i];
		std::map<string,std::vector<QrelsLine*>*>::const_iterator pos = quralMap.find(line->QueryId);
		if ( pos == quralMap.end() ) {
		  // not found
			std::vector<QrelsLine*> *vec = new std::vector<QrelsLine*>();
			vec->push_back(line);
			quralMap.insert( std::make_pair(line->QueryId, vec) ); 
			
		} else {
		  // found
			std::vector<QrelsLine*>* vec = pos->second;
			vec->push_back(line);
		}
	}

	QueriesClusters queriesClusters;
	queriesClusters.init(&clustersfilesNames);

	map< string , ClustersInQuery >::iterator it;
	for(it = queriesClusters.ClusterListPerQuery.begin() ; it != queriesClusters.ClusterListPerQuery.end() ; ++it)
	{
		string queryName = it->first;
		ClustersInQuery *clusterListPerQuery = &it->second;;

		string fileName = outputDir + "/" + queryName + ".qrels" ;
		std::map<string,std::vector<QrelsLine*>*>::const_iterator pos = quralMap.find(queryName);
		if ( pos == quralMap.end() ) {
		  // not found
		} 
		else 
		{
			std::vector<QrelsLine*>* qrelsForQuery = pos->second;
			ofstream qrels;
			qrels.open ( fileName.c_str() ); 
			/*// Write regular query
			for(unsigned int i2 = 0 ; i2 <qrelsForQuery->size() ; ++i2)
			{
				qrels << queryName + " " + (*qrelsForQuery)[i2]->Zero + " " << (*qrelsForQuery)[i2]->DocId + " " << (*qrelsForQuery)[i2]->Score +  "\n";
			}
			// Write rm310 query
			for(unsigned int i2 = 0 ; i2 <qrelsForQuery->size() ; ++i2)
			{
				qrels << queryName + "_rm3_10_Term" + " " + (*qrelsForQuery)[i2]->Zero + " " << (*qrelsForQuery)[i2]->DocId + " " << (*qrelsForQuery)[i2]->Score +  "\n";
			}
			// Write rm3100 query
			for(unsigned int i2 = 0 ; i2 <qrelsForQuery->size() ; ++i2)
			{
				qrels << queryName + "_rm3_All_Term" + " " + (*qrelsForQuery)[i2]->Zero + " " << (*qrelsForQuery)[i2]->DocId + " " << (*qrelsForQuery)[i2]->Score +  "\n";
			}*/
			map< string , vector<Term> >::iterator it;
			for(it = clusterListPerQuery->ClusterInQueryList.begin() ; it != clusterListPerQuery->ClusterInQueryList.end() ; ++it)
			{
				string cluster = it->first;
				string fullClusterName = GetClusterQueryFullName(queryName , cluster );
				for(unsigned int i2 = 0 ; i2 <qrelsForQuery->size() ; ++i2)
				{
					qrels << fullClusterName + " " + (*qrelsForQuery)[i2]->Zero + " " << (*qrelsForQuery)[i2]->DocId + " " << (*qrelsForQuery)[i2]->Score +  "\n";
				}
			}
			qrels.close(); 
		}

	}
	QrelsFile.Clear();
	queriesClusters.Clear();
}
void CreateQueriesFileWithWorkingSetDocno(string reRankingQueries, string outputDir ,vector<string> *clustersQueryFileList )
{
	QueriesFileWithWorkingSetDocno reRankingFile;
	reRankingFile.readFile(reRankingQueries);
	for (int i = 0; i < clustersQueryFileList->size(); i++)
	{
		string currentClustersQueryFile = (*clustersQueryFileList)[i];
		QueriesFileWithWorkingSetDocno newClustersQueryFile;
		newClustersQueryFile.readFileWitoutWorkingSetDocno(currentClustersQueryFile);
		string queryName = getFileName_Query(currentClustersQueryFile);
		vector<string> *workingSetDocnoList = &reRankingFile.WorkingSetDocnoPerQuery.find(queryName)->second;
		newClustersQueryFile.AddWorkingSetDocnoListForAllQuery(workingSetDocnoList);
		string fileName = getFileName(currentClustersQueryFile);
		string newFileName = outputDir + "/" + fileName;
		newClustersQueryFile.WriteFile(newFileName);
	}
}

void CreateQueriesFileWithWorkingSetDocno(string reRankingQueries, string outputDir ,string currentClustersQueryFile )
{
	QueriesFileWithWorkingSetDocno reRankingFile;
	reRankingFile.readFile(reRankingQueries);

	QueriesFileWithWorkingSetDocno newClustersQueryFile;
	newClustersQueryFile.readFileWitoutWorkingSetDocno(currentClustersQueryFile);
	vector<string> queriesNames;
	newClustersQueryFile.getQueriesNames(&queriesNames);

	for (int i = 0; i < queriesNames.size(); i++)
	{
		string queryName = queriesNames[i];
		vector<string> *workingSetDocnoList = &reRankingFile.WorkingSetDocnoPerQuery.find(queryName)->second;
		newClustersQueryFile.AddWorkingSetDocnoListToQuery(queryName , workingSetDocnoList);
	}

	string fileName = getFileName(currentClustersQueryFile);
	string newFileName = outputDir + "/reRanking" + fileName;
	newClustersQueryFile.WriteFile(newFileName);
	
}


