#ifndef WordToVec_H
#define WordToVec_H

#include <string.h>
#include <string>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <set>
#include <sstream>
#include <algorithm>
#include "math.h"
#include <stdarg.h>
#include <functional> 
#include <cctype>
#include <locale>
#include <stdlib.h> 

using std::map;
using std::vector;
using std::string;
using std::cout;
using std::cerr;


inline double Cosine(float* pv1, float* pv2, long long dim)
{
	double ip(0.0),l1(0.0),l2(0.0);
	for (long long i=0; i<dim; i++)
	{       
		ip += pv1[i]*pv2[i];
		l1 += pv1[i]*pv1[i];
		l2 += pv2[i]*pv2[i];
	}
	return ( ip == 0 || l1 ==0 || l2 ==0 ) ? 0 : ip/(sqrt(l1)*sqrt(l2));
}

inline void Centroid(float* centroidVec,int vectorElements , vector<float*> vectors)
{
	int numberOfVectors = vectors.size();
	
	if(numberOfVectors == 0)
	{
		cout << "Received 0 vector to calculate Centroid" <<std::endl;
		return;
	}
	for (int elementIndex = 0; elementIndex < vectorElements; elementIndex++)
	{
		centroidVec[elementIndex] = 0.0;
		for (int vectorIndex = 0; vectorIndex < numberOfVectors; vectorIndex++)
		{
			centroidVec[elementIndex] += vectors[vectorIndex][elementIndex];
		}
		centroidVec[elementIndex] = centroidVec[elementIndex] / numberOfVectors;
	}
}

typedef struct WordToVecModel
{
	std::map<std::string,float*> voc_to_line;
	//std::map<float*,const std::string*> ptr_to_voc;
	float* data;
	long long dim;
	long long size;
	
	bool LoadWordToVecModel(string wordToVecModelFileName )
	{
		const int cnst_max_term_len=50;

		FILE*  model_file;
		model_file = fopen(wordToVecModelFileName.c_str(), "rb");
		if (model_file == NULL) {
			printf("Model file not found\n");
			return false;
		}

		voc_to_line.clear();
		fscanf(model_file, "%lld", &size);
		fscanf(model_file, "%lld", &dim);

		data=new float[dim*size];
		float* ptr=data;

		int max_w(cnst_max_term_len);
		char* tmp=new char[max_w];
		long line=1;
		while (!feof( model_file ) )
		{
			memset(tmp,0,max_w);
			int a = 0;
			while (1) {
				char c = fgetc(model_file);
				if (c == ' ') break;
				tmp[a] = c;
				if (feof(model_file)) break;
				if ((a < max_w) && (tmp[a] != '\n')) a++;
			}

			std::string term(tmp);


			if ((!(term.empty())) && (term!=" "))
			{
				voc_to_line.insert( std::make_pair(term, ptr) ); 
				//ptr_to_voc.insert( std::make_pair<float*,const string* >(ptr, &(voc_to_line.find(term)->first)) ); 
			}
			fread(ptr, sizeof(float), dim, model_file);
			ptr = ptr+dim;
			line++;
		}
		cout << '\n';
		
		cout << "The vocabulary size in WordToVecModel is : " << voc_to_line.size() << "\n";

		//delete[] tmp;
		fclose(model_file);

		/*int i = 0;
		for ( std::map<std::string,float*>::iterator citvoc = voc_to_line.begin();
			citvoc != voc_to_line.end() ; citvoc ++ )
		{
			if( start_with(citvoc->first  , "aden"))
			{
				cout << "Start with a : " << citvoc->first << " at index " << i << "\n";
			}
			i++;
		}*/
		

		return true;
	}

	double ** createDistancesMatrix(const std::map<std::string,double>& rm_terms,double **matrix = NULL)
	{
		std::map<string, float*> rep;
		std::vector<string> terms;
		for ( map<string,double>::const_iterator cit = rm_terms.begin();
			cit != rm_terms.end(); cit++ )
		{
			terms.push_back(cit->first);
		}
		size_t cluster_id(1);
		GetVectorsForLines( rep, terms);

		double **distancesMatrix;
		if(matrix == NULL)
		{
			distancesMatrix = new double*[terms.size()];
			for (unsigned int i = 0; i < terms.size(); ++i)
				distancesMatrix[i] = new double[terms.size()];
		}
		else
		{
			distancesMatrix = matrix;
		}


		for( unsigned int i = 0 ; i< terms.size() ; ++i)
		{
			for( unsigned int j = 0 ; j< terms.size() ; ++j)
			{
				double cos = 0;
				// not found
				if( (rep.find(terms[i]) == rep.end()) || (rep.find(terms[j]) == rep.end()) )
				{
					cos = 0;
				} 
				else 
				{
					float * t1Array = rep.find(terms[i])->second;
					float * t2Array = rep.find(terms[j])->second;
					cos = Cosine( t1Array, t2Array, dim);
				}
				distancesMatrix[i][j] = cos;
				distancesMatrix[j][i] = cos;
			}
		}


		std::map<string, float*>::iterator it;
		for (it = rep.begin(); it != rep.end(); it++)
		{
			delete it->second;
		}

		return distancesMatrix;
	}

	double CalculateSimilarity(float* queryCentroid,float *clusterCentroid)
	{
		return Cosine( queryCentroid, clusterCentroid, dim);
	}

	double CalculateSimilarity(string term,float *clusterCentroid)
	{
		std::map<std::string,float*>::const_iterator citvoc = voc_to_line.find(term);
		if (citvoc == voc_to_line.end() )
		{
			std::cout << "can't find term in vocabulary, skipping: |" << term << "|" <<std::endl;
			return -2;
		}
		return Cosine( citvoc->second, clusterCentroid, dim);
	}

	double CalculateSimilarity(string term1,string term2)
	{
		std::map<std::string,float*>::const_iterator term1Vec = voc_to_line.find(term1);
		if (term1Vec == voc_to_line.end() )
		{
			std::cout << "can't find term in vocabulary, skipping: |" << term1 << "|" <<std::endl;
			return -2;
		}

		std::map<std::string,float*>::const_iterator term2Vec = voc_to_line.find(term2);
		if (term2Vec == voc_to_line.end() )
		{
			std::cout << "can't find term in vocabulary, skipping: |" << term2 << "|" <<std::endl;
			return -2;
		}

		return Cosine( term1Vec->second, term2Vec->second, dim);
	}


	float* GetCentroid(const vector<string>& terms)
	{
		
		vector<float*> vectors;
		for ( vector<string>::const_iterator cit = terms.begin(); cit != terms.end(); cit++)
		{
			string term = *cit;
			std::map<std::string,float*>::const_iterator citvoc = voc_to_line.find(term);
			if (citvoc == voc_to_line.end() )
			{
				std::cout << "can't find term in vocabulary, skipping: |" << term << "|" << std::endl;
				continue;
			}
			float* ptr = new float[dim];       
			memcpy( ptr, citvoc->second, sizeof(float)*dim);
			vectors.push_back(ptr);
		}         
		float* centroidVec = new float[dim];  
		for (int i = 0; i < dim; i++)
		{
			centroidVec[i] = 0;
		}
		Centroid( centroidVec, dim , vectors);

		for (int i = 0; i < vectors.size(); i++)
		{
			delete vectors[i];
		}

		return centroidVec;
	}

	void GetVectorsForLines( std::map<string, float*>& rep,	const vector<string>& terms	)
	{       
		// the positions saved are from the start
		for ( vector<string>::const_iterator cit = terms.begin(); cit != terms.end(); cit++)
		{
			string term = *cit;
			std::map<std::string,float*>::const_iterator citvoc = voc_to_line.find(term);
			if (citvoc == voc_to_line.end() )
			{
				std::cout << "can't find term in vocabulary, skipping: |" << *cit << "|" << std::endl;
				continue;
			}

			float* ptr = new float[dim];       
			memcpy( ptr, citvoc->second, sizeof(float)*dim);
			rep.insert( std::make_pair(*cit, ptr) );
		}                  
	}

	void Clear()
	{
		//ptr_to_voc.clear();
		voc_to_line.clear();
		delete data;
		dim = size = 0;
	}

}WordToVecModel;

#endif