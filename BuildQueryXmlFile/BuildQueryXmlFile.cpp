
#include <stdio.h>

void BuildQueryXmlFile(char* fileName, char* index, int fbDocs , int fbTerms , int fbMu , int muRule , float fbOrigWeight)
{

	FILE *f = fopen(fileName, "w");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		return;
	}
	fprintf(f, "<parameters>\n");
	fprintf(f, "<memory>1G</memory>\n");
	fprintf(f, "<index>%s</index>\n", index);
	fprintf(f, "<count>1000</count>\n");
	fprintf(f, "<trecFormat>true</trecFormat>\n");
	fprintf(f, "<fbDocs>%d</fbDocs>\n",fbDocs);
	fprintf(f, "<fbTerms>%d</fbTerms>\n",fbTerms);
	fprintf(f, "<fbMu>%d</fbMu>\n",fbMu);
	fprintf(f, "<rule>method:dir,mu:%d</rule>\n",muRule);
	fprintf(f, "<fbOrigWeight>%.2</fbOrigWeight>\n",fbOrigWeight);
	fprintf(f, "<threads>4</threads>\n");
	fprintf(f, "<printQuery>true</printQuery>\n");
	fprintf(f, "</parameters>\n");

	fclose(f);
}


void main(int argc, char *argv[])
{
	if(argc < 4)
	{
		return ;
	}

	// argv[0] = program name
	// argv[1] = index path
	// argv[2] = corpus Name ( AP, WSJ ... )
	// argv[3] = Ouput dirctory

	

	char fileName[200];
	char* index = argv[1]; 
	int fbDocsList[] = {10, 25, 50}; 
	int fbTermsList[] = {100}; 
	int fbMuList[] = { 0 , 1000 } ; 
	int muRuleList[] = { 0 , 1000 } ; 
	float fbOrigWeightList[] = { 0.0 , 0.2, 1.0};

	for(int i1 = 0 ; i1 < sizeof(fbDocsList)/sizeof(fbDocsList[0]) ; i1 ++ )
	{
		for(int i2 = 0 ; i2 < sizeof(fbTermsList)/sizeof(fbTermsList[0]) ; i2 ++)
		{
			for(int i3 = 0 ; i3 < sizeof(fbMuList)/sizeof(fbMuList[0]) ; i3 ++)
			{
				for(int i4 = 0 ; i4 < sizeof(muRuleList)/sizeof(muRuleList[0]) ; i4 ++)
				{
					for(int i5 = 0 ; i5 < sizeof(fbOrigWeightList)/sizeof(fbOrigWeightList[0]) ; i5 ++)
					{
						int fbDocs = fbDocsList[i1];
						int fbTerms = fbTermsList[i2];
						int fbMu = fbMuList[i3];
						int muRule = muRuleList[i4];
						float fbOrigWeight = fbOrigWeightList[i5];

						sprintf(fileName,"%s\\%s_fbDocs_%d_fbTerms_%d_fbMu_%d_muRule_%d_fbOrigWeight_%.2f_.xml",argv[3],argv[2],fbDocs ,  fbTerms ,  fbMu ,  muRule ,  fbOrigWeight);

						BuildQueryXmlFile(fileName,  index,  fbDocs ,  fbTerms ,  fbMu ,  muRule ,  fbOrigWeight);
					}
				}
			}
		}
	}
}

