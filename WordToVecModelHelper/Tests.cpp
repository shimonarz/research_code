#include <iostream>
#include <cstdlib>
#include <windows.h>

#include "Tests.h"
#include "functions.h"

using namespace std;



void getAllFileFromDirectory(string dirFullPath ,vector<string> *listOfFiles )
{
	WIN32_FIND_DATA file_data;
	HANDLE hFindFileNameEvaluation = FindFirstFile((dirFullPath+ "/*").c_str() , &file_data);
	if (hFindFileNameEvaluation != INVALID_HANDLE_VALUE) 
	{
		do 
		{
			string fileName = file_data.cFileName;
			if(fileName != "." && fileName != ".." )
			{
				std::cout << "file Name = " << fileName << std::endl ;
				string fullFileName = dirFullPath + "/" + fileName;
				listOfFiles->push_back(fullFileName);
			}
		}
		while (FindNextFile(hFindFileNameEvaluation, &file_data));
		FindClose(hFindFileNameEvaluation);
	}
}

// Check inputs

void tese_buildNewRm3ClustresQuery2()
{
	ClusterRM3ExpansionTerms terms;
	terms.QueryId = "051";

	// 0.5 #combine( #combine( leveraged buyouts ) ) 
	// 0.3 #weight(  0.2 "s" 0.2 "kravi" 0.2 "force" 0.2 "company" 0.2 "new" ) 
	// 0.2 #weight(  0.1 "s" 0.1 "kravi" 0.1 "force" 0.1 "1" 0.1 "early" 0.1 "company" 0.1 "corp" 0.1 "new" 0.1 "3" 0.1 "tax") )</text>

	// Original Query
	terms.OriginalQueryWeight = 0.5;
	Term t1  ;
	t1.value = "leveraged";
	t1.weight = 0.0;
	Term t2  ;
	t2.value = "buyouts";
	t2.weight = 0.0;
	terms.OriginalQuery.push_back(&t1);
	terms.OriginalQuery.push_back(&t2);


	// rm1
	terms.Rm1TermsWeight = 0.3;
	Term t1r  ;
	t1r.value = "s";
	t1r.weight = 0.2;
	Term t2r  ;
	t2r.value = "kravi";
	t2r.weight = 0.2;
	Term t3r  ;
	t3r.value = "force";
	t3r.weight = 0.2;
	Term t4r  ;
	t4r.value = "company";
	t4r.weight = 0.2;
	Term t5r  ;
	t5r.value = "new";
	t5r.weight = 0.2;
	terms.Rm1Terms.push_back(&t1r);
	terms.Rm1Terms.push_back(&t2r);
	terms.Rm1Terms.push_back(&t3r);
	terms.Rm1Terms.push_back(&t4r);
	terms.Rm1Terms.push_back(&t5r);
	
	// 0.2 #weight(  0.1 "s" 0.1 "kravi" 0.1 "force" 0.1 "1" 0.1 "early" 0.1 "company" 0.1 "corp" 0.1 "new" 0.1 "3" 0.1 "tax") )</text>
	// Cluster TermsW
	terms.ClusterTermsWeight = 0.2;
	Term t1c  ;
	t1c.value = "s";
	t1c.weight = 0.1;
	Term t2c  ;
	t2c.value = "kravi";
	t2c.weight = 0.1;
	Term t3c  ;
	t3c.value = "force";
	t3c.weight = 0.1;
	Term t4c  ;
	t4c.value = "1";
	t4c.weight = 0.1;
	Term t5c  ;
	t5c.value = "early";
	t5c.weight = 0.1;
	Term t6c  ;
	t6c.value = "company";
	t6c.weight = 0.1;
	Term t7c  ;
	t7c.value = "corp";
	t7c.weight = 0.1;
	Term t8c  ;
	t8c.value = "new";
	t8c.weight = 0.1;
	Term t9c  ;
	t9c.value = "3";
	t9c.weight = 0.1;
	Term t10c  ;
	t10c.value = "tax";
	t10c.weight = 0.1;
	terms.ClusterTerms.push_back(&t1c);
	terms.ClusterTerms.push_back(&t2c);
	terms.ClusterTerms.push_back(&t3c);
	terms.ClusterTerms.push_back(&t4c);
	terms.ClusterTerms.push_back(&t5c);
	terms.ClusterTerms.push_back(&t6c);
	terms.ClusterTerms.push_back(&t7c);
	terms.ClusterTerms.push_back(&t8c);
	terms.ClusterTerms.push_back(&t9c);
	terms.ClusterTerms.push_back(&t10c);
	
	terms.OriginalQueryWeight = 0.5;
	terms.ClusterTermsWeight = 0.2;
	terms.Rm1TermsWeight = 0.3;
	std::string res1 = buildNewRm3ClustresQuery(&terms);

	terms.OriginalQueryWeight = 0;
	terms.ClusterTermsWeight = 0.2;
	terms.Rm1TermsWeight = 0.3;
	std::string res2 = buildNewRm3ClustresQuery(&terms);

	terms.OriginalQueryWeight = 0.5;
	terms.ClusterTermsWeight = 0;
	terms.Rm1TermsWeight = 0.3;
	std::string res3 = buildNewRm3ClustresQuery(&terms);

	terms.OriginalQueryWeight = 0.5;
	terms.ClusterTermsWeight = 0.2;
	terms.Rm1TermsWeight = 0;
	std::string res4 = buildNewRm3ClustresQuery(&terms);

	terms.OriginalQueryWeight = 0.5;
	terms.ClusterTermsWeight = 0;
	terms.Rm1TermsWeight = 0;
	std::string res5 = buildNewRm3ClustresQuery(&terms);

	terms.OriginalQueryWeight = 0;
	terms.ClusterTermsWeight = 0;
	terms.Rm1TermsWeight = 0.3;
	std::string res6 = buildNewRm3ClustresQuery(&terms);

}

	
void main()
{
    //tese_buildNewRm3ClustresQuery2();
}

