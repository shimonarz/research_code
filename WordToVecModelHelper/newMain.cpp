
/* 
* File:   main.cpp
* Author: anna
*
* Created on April 16, 2016, 9:21 PM
*/

#include <cstdlib>
#include <iostream>

#include <stdio.h>
#include<stdlib.h>
#include <string.h>
#include <math.h>
#include "functions.h"
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include<limits.h>
#include<errno.h>
using namespace std;


/* Definitions */
typedef  bool (*CheckInputDelegate)(int, char**);
typedef  void (*ExecuteOperationDelegate)();

typedef struct GeneralVariables 
{
	std::string model;
	std::string input;
	std::string command;
	float* data;
	long long dim;
	long long size;
	std::map<std::string,float*> voc_to_line;
	std::map<float*,const std::string*> ptr_to_voc;

}GeneralVariables;
typedef struct Operation 
{
	bool needTosetTheVocabulary;
	CheckInputDelegate checkInput;
	ExecuteOperationDelegate execute;
	char **argv;

}Operation;







/* Functions */
void SetTheCurrentOperation(int argc, char *argv[]);
void WriteToFile( const std::string& name, const std::vector<float>& vout, const std::string& path);
void PrintClusterFeatures( const std::string& path_to_output, const string& qid, size_t clust_id,
						  float avg_cosine_cluster_terms, 
						  float clust_query_cent_sim,
						  float clust_query_cent_corpus,
						  const std::map<string,float>& dist_to_qterm);

/* Global Variables */
Operation operation;
GeneralVariables generalVariables;





int mai2n(int argc, char *argv[])
{
	if (argc <3 ) {
		cout << "Usage (1): model-file input-file cosine" << std::endl;
		cout << "Usage (2): model-file input-file KNN k-neigb normalize-1/0" << std::endl;
		cout << "Usage (3): model-file subset-file ScoreSubset normalize-1/0" << std::endl;
		cout << "Usage (4): model-file input-file KNN-terms k-neigb" << std::endl;
		cout << "Usage (5): model-file input-file KNN-all-no-sort norm path_for_output" << std::endl;
		cout << "Usage (6): model-file Vocabulary" << std::endl;
		cout << "Usage (7): model-file input-file KNN-all-no-sort-terms path_for_output" << std::endl;
		cout << "Usage (8): model-file query-file Centroid path_for_output" << std::endl;
		cout << "Usage (9): model-file input-file KNN-RM path_for_output" << std::endl;
		cout << "Usage (10): model-file query-file ClusterFeatures path_to_clusters path_to_output" << std::endl; 
		cout << "Usage (11): model-file input-file(q_i|w) Translation" << std::endl;//TransNormFile
		cout << "Usage (12): model-file TranslationNorm" << std::endl;

		return 0;
	}

	SetTheCurrentOperation(argc,argv);
	if(operation.checkInput(argc,argv) == false)
	{
		std::cerr << "Try again with the right format of parameters." << std::endl;
		return EXIT_FAILURE;
	}
	if(operation.needTosetTheVocabulary == true)
	{
		FILE*  model_file;
		model_file = fopen(generalVariables.model.c_str(), "rb");
		if (model_file == NULL) {
			printf("Model file not found\n");
			return EXIT_FAILURE;
		}
		LoadVocabulary( model_file, generalVariables.dim, generalVariables.size, generalVariables.voc_to_line, generalVariables.ptr_to_voc, generalVariables.data);
		fclose(model_file);
	}
	operation.execute();

	if(operation.needTosetTheVocabulary == true)
	{
		delete[] generalVariables.data;
		generalVariables.voc_to_line.clear();
		generalVariables.ptr_to_voc.clear();
	}

	//system("PAUSE");
	return EXIT_SUCCESS;

}


bool checkInput_ReturnTrue(int argc , char **argv)
{
	return true;
}


#pragma region Method unused

/* cosine */
// model-file input-file cosine
bool checkCosineInput(int argc , char **argv)
{
	if(argc != 4) // argv : [0] = program name , [1] = model file name , [2] = input file name , [4] = cosine command
	{
		std::cerr << "Expect a three input parameters ( model-file   input-file   cosine) , recive " << argc - 1 << std::endl;
		return false; 
	}
	ifstream file(argv[2]);
	if (!file)
	{
		std::cerr << "Can't open input file: " << argv[2] << std::endl;
		return false;
	}
	return true;
}
void cosine()
{
	ifstream* finput = NULL;
	finput = new ifstream( generalVariables.input.c_str() );
	string line;
	while  ( finput->good() )
	{
		getline (*finput, line);
		std::stringstream os(line);
		std::string term1, term2;
		os >> term1;
		os >> term2;

		if (!term1.empty()&&!term2.empty()){
			double cos = CalcCosine( term1, term2, generalVariables.voc_to_line, generalVariables.data, generalVariables.dim );
			cout << term1 << " - " << term2 << " : " << cos << std::endl;
		}
	}

	finput->close();
	delete finput;
}


/* KNN */
//  model-file input-file KNN k-neigb normalize-1/0
bool checkKnnInput(int argc , char **argv)
{
	if(argc != 6) // argv : [0] = program name , [1] = model file name , [2] = input file name , [3] = KNN command , [4] = k-neigb , [5] = normalize-1/0
	{
		std::cerr << "Expect a three input parameters ( model-file   input-file   KNN   k-neigb   normalize-1/0) , recive " << argc - 1 << std::endl;
		return false; 
	}
	ifstream file(argv[2]);
	if (!file)
	{
		std::cerr << "Can't open input file: " << argv[2] << std::endl;
		return false;
	}

	char * temp;
	errno = 0;
	int k =(int) strtol(argv[4], &temp, 10);
	if (temp == argv[4] || *temp != '\0' || ((k == LONG_MIN || k == LONG_MAX) && errno == ERANGE))
	{
		std::cerr << "Could not convert " << argv[4] << " to number" <<  std::endl;
		return false;
	}
	errno = 0;
	int norm =(int) strtol(argv[5], &temp, 10);
	if (temp == argv[4] || *temp != '\0' || ((norm == LONG_MIN || norm == LONG_MAX) && errno == ERANGE))
	{
		std::cerr << "Could not convert " << argv[5] << " to number" <<  std::endl;
		return false;
	}
	if(norm != 1 && norm != 0)
	{
		std::cerr << "normalize must be 1 or 0" << argv[5] << " to number" <<  std::endl;
		return false;
	}

	return true;
}
void knn()
{
	ifstream* finput = NULL;
	finput = new ifstream( generalVariables.input.c_str() );
	std::string str_k = operation.argv[4];
	istringstream ss1(str_k);
	int k(0);
	ss1 >> k;
	std::string bool_norm = operation.argv[5];
	istringstream ss2(bool_norm);
	bool norm(false);
	ss2 >> norm;
	std::vector<std::pair<string,float> > vout;
	while  ( finput->good() )
	{
		string query;
		getline (*finput, query);
		cout << "query = " << query <<" "<< std::endl;
		vector<std::string> query_vector;
		parseLine( query, query_vector );     
		if (query.empty()) { break; }
		std::string qid=query_vector[0];
		query_vector.erase(query_vector.begin(),query_vector.begin()+1);

		KNN( query_vector, generalVariables.voc_to_line, generalVariables.ptr_to_voc, generalVariables.dim, generalVariables.size, k, vout, norm, generalVariables.data );
		int i=1;
		for (size_t sz = 0; sz < vout.size(); sz++)
		{
			cout << qid <<" " << vout[sz].first << " " << vout[sz].second << " " << i << std::endl;
			i++;
		}
	}

	finput->close();
	delete finput;
}

/* ScoreSubset*/
// model-file subset-file ScoreSubset normalize-1/0
bool checkscoreSubsetInput(int argc , char **argv)
{
	if(argc != 5) // argv : [0] = program name , [1] = model file name , [2] = subset-file name , [3] = ScoreSubset command , [4] = normalize-1/0
	{
		std::cerr << "Expect a three input parameters ( model-file   subset-file   ScoreSubset  normalize-1/0) , recive " << argc - 1 << std::endl;
		return false; 
	}
	ifstream file(argv[2]);
	if (!file)
	{
		std::cerr << "Can't open subset file: " << argv[2] << std::endl;
		return false;
	}

	char * temp;
	errno = 0;
	int norm =(int) strtol(argv[4], &temp, 10);
	if (temp == argv[4] || *temp != '\0' || ((norm == LONG_MIN || norm == LONG_MAX) && errno == ERANGE))
	{
		std::cerr << "Could not convert " << argv[5] << " to number" <<  std::endl;
		return false;
	}
	if(norm != 1 && norm != 0)
	{
		std::cerr << "normalize must be 1 or 0" << argv[5] << " to number" <<  std::endl;
		return false;
	}

	return true;
}
void scoreSubset()
{
	ifstream* finput = NULL;
	finput = new ifstream( generalVariables.input.c_str() );

	string line;
	std::string bool_norm = operation.argv[4];
	istringstream ss2(bool_norm);
	bool norm(false);
	ss2 >> norm;
	string query;
	getline (*finput, query);
	while  ( finput->good() )
	{//file format:
		//            // ! qid qterm1 qterm2 qterm3....
		std::vector<std::pair<string,float> > vout;
		std::vector<std::string> subset;
		vector<std::string> query_vector;
		parseLine( query, query_vector );     
		if (query_vector[0]!="!") 
		{
			//              // cerr << "bad format, can't read query " << std::endl;
			break;
		}
		std::string qid=query_vector[1];            
		query_vector.erase(query_vector.begin(),query_vector.begin()+2);
		while  ( finput->good() ){
			getline (*finput, line);
			if (line.empty() ) continue;
			std::stringstream os(line);
			std::string term1;
			os >> term1;
			if (term1=="!")
			{  query=line;
			break;
			}
			subset.push_back(term1);
		}

		ScoreSubset( query_vector, generalVariables.voc_to_line, generalVariables.dim, vout, norm, subset, generalVariables.data);
		int i=1;
		for (std::vector<std::pair<string,float> >::const_iterator cit=vout.begin();
			cit != vout.end(); cit++)
		{
			cout << qid + " " << cit->first << " " << cit->second << " " << i << std::endl;   
			i++;      
		}
	}

	finput->close();
	delete finput;
}

/* KNN-terms */
//model-file input-file KNN-terms k-neigb
bool checkKnnTermsInput(int argc , char **argv)
{
	if(argc != 5) // argv : [0] = program name , [1] = model file name , [2] = input-file name , [3] = KNN-terms command , [4] = k-neigb
	{
		std::cerr << "Expect a three input parameters ( model-file   input-file   KNN-terms  k-neigb) , recive " << argc - 1 << std::endl;
		return false; 
	}
	ifstream file(argv[2]);
	if (!file)
	{
		std::cerr << "Can't open subset file: " << argv[2] << std::endl;
		return false;
	}

	char * temp;
	errno = 0;
	int k =(int) strtol(argv[4], &temp, 10);
	if (temp == argv[4] || *temp != '\0' || ((k == LONG_MIN || k == LONG_MAX) && errno == ERANGE))
	{
		std::cerr << "Could not convert " << argv[4] << " to number" <<  std::endl;
		return false;
	}

	return true;
}
void knnTerms()
{
	ifstream* finput = NULL;
	finput = new ifstream( generalVariables.input.c_str() );

	string line;
	std::string str_k = operation.argv[4];
	istringstream ss1(str_k);
	int k(0);
	ss1 >> k; // k neighbours to find

	while  ( finput->good() )
	{//file format:
		// qid qterm1 qterm2 qterm3....

		string query;
		getline (*finput, query);
		vector<std::string> query_vector;
		parseLine( query, query_vector );     
		if ( query.empty() ) { break; }
		std::string qid = query_vector[0];
		query_vector.erase( query_vector.begin(), query_vector.begin()+1 );    

		KNN_term( query_vector, generalVariables.voc_to_line, generalVariables.ptr_to_voc, generalVariables.dim, generalVariables.size, qid, k, generalVariables.data);
	}

	finput->close();
	delete finput;
}

/* KNN-all-no-sort */
// model-file input-file KNN-all-no-sort norm path_for_output
bool checkKnnAllNoSortInput(int argc , char **argv)
{
	if(argc != 6) // argv : [0] = program name , [1] = model file name , [2] = input-file name , [3] =  KNN-all-no-sort command , [4] = norm , [5] = path for output 
	{
		std::cerr << "Expect a three input parameters ( model-file   input-file   KNN-all-no-sort  norm  path_for_output  ) , receive " << argc - 1 << std::endl;
		return false; 
	}
	ifstream file(argv[2]);
	if (!file)
	{
		std::cerr << "Can't open subset file: " << argv[2] << std::endl;
		return false;
	}

	char * temp;
	errno = 0;
	int norm =(int) strtol(argv[4], &temp, 10);
	if (temp == argv[4] || *temp != '\0' || ((norm == LONG_MIN || norm == LONG_MAX) && errno == ERANGE))
	{
		std::cerr << "Could not convert " << argv[5] << " to number" <<  std::endl;
		return false;
	}
	if(norm != 1 && norm != 0)
	{
		std::cerr << "normalize must be 1 or 0" << argv[5] << " to number" <<  std::endl;
		return false;
	}

	ifstream path(argv[5]);
	if (!path)
	{
		std::cerr << "Can't open path for output : " << argv[5] << std::endl;
		return false;
	}

	return true;
}
void knnAllNoSort()
{
	ifstream* finput = NULL;
	finput = new ifstream( generalVariables.input.c_str() );

	std::string bool_norm = operation.argv[4];
	istringstream ss2(bool_norm);
	bool norm(false);
	ss2 >> norm;
	std::string path=operation.argv[5];

	while  ( finput->good() )
	{
		string query;
		getline (*finput, query);
		vector<std::string> query_vector;
		parseLine( query, query_vector );     
		if (query.empty()) { break; }
		std::string qid=query_vector[0];
		query_vector.erase(query_vector.begin(),query_vector.begin()+1);
		std::vector<float> vout;
		KNN_all_no_sort( query_vector,generalVariables.voc_to_line, generalVariables.ptr_to_voc, generalVariables.dim, generalVariables.size, vout, norm, generalVariables.data );
		string snorm;
		//reversed as always
		if (norm)
			snorm = "notnorm";
		else
			snorm = "norm";
		WriteToFile( qid + "." + snorm, vout, path ); //To implement!!!
	}

	finput->close();
	delete finput;
}

/*Vocabulary*/
// model-file Vocabulary
bool checkVocabularyInput(int argc , char **argv)
{
	if(argc != 3) // argv : [0] = program name , [1] = model file name , [2] = Vocabulary command 
	{
		std::cerr << "Expect a three input parameters ( model-file   Vocabulary) , recive " << argc - 1 << std::endl;
		return false; 
	}
	ifstream file(argv[1]);
	if (!file)
	{
		std::cerr << "Can't open model file: " << argv[2] << std::endl;
		return false;
	}
	return true;
}
void vocabulary()
{
	FILE*  model_file;
	model_file = fopen(generalVariables.model.c_str(), "rb");
	PrintVocabulary( model_file, generalVariables.dim, generalVariables.size);
	fclose(model_file);
}

/* KNN-all-no-sort-terms */
// model-file input-file KNN-all-no-sort-terms path_for_output
bool checkKnnAllNoSortTermsInput(int argc , char **argv)
{
	if(argc != 5) // argv : [0] = program name , [1] = model file name , [2] = input-file name , [3] =  KNN-all-no-sort-terms command , [4] = path for output 
	{
		std::cerr << "Expect a three input parameters ( model-file   input-file   KNN-all-no-sort-terms  path_for_output  ) , receive " << argc - 1 << std::endl;
		return false; 
	}
	ifstream file(argv[2]);
	if (!file)
	{
		std::cerr << "Can't open subset file: " << argv[2] << std::endl;
		return false;
	}

	ifstream path(argv[4]);
	if (!path)
	{
		std::cerr << "Can't open path for output : " << argv[4] << std::endl;
		return false;
	}

	return true;
}
void knnAllNoSortTerms()
{
	ifstream* finput = NULL;
	finput = new ifstream( generalVariables.input.c_str() );

	std::string path=operation.argv[4];

	while  ( finput->good() )
	{
		string query;
		getline (*finput, query);
		vector<std::string> query_vector;
		parseLine( query, query_vector );     
		if (query.empty()) { break; }
		std::string qid=query_vector[0];
		query_vector.erase(query_vector.begin(),query_vector.begin()+1);
		for (size_t i=0; i<query_vector.size();i++)
		{
			std::vector<float> vout;
			KNN_term_no_sort( query_vector[i], generalVariables.voc_to_line, generalVariables.ptr_to_voc, generalVariables.dim, generalVariables.size, vout,generalVariables.data );
			if (vout.size()>0) WriteToFile( qid + "." + query_vector[i], vout, path ); 
		}
	}

	finput->close();
	delete finput;
}

/* Centroid */
// model-file query-file Centroid path_for_output
bool checkCentroidInput(int argc , char **argv)
{
	if(argc != 5) // argv : [0] = program name , [1] = model file name , [2] = query-file name , [3] =  Centroid command , [4] = path for output 
	{
		std::cerr << "Expect a three input parameters ( model-file   query-file   Centroid  path_for_output  ) , receive " << argc - 1 << std::endl;
		return false; 
	}
	ifstream file(argv[2]);
	if (!file)
	{
		std::cerr << "Can't open query file: " << argv[2] << std::endl;
		return false;
	}

	ifstream path(argv[4]);
	if (!path)
	{
		std::cerr << "Can't open path for output : " << argv[4] << std::endl;
		return false;
	}

	return true;
}
void centroid()
{
	ifstream* finput = NULL;
	finput = new ifstream( generalVariables.input.c_str() );

	std::string path=operation.argv[4];
	float* centroid = GetCentroid( generalVariables.dim, generalVariables.size, generalVariables.data, generalVariables.ptr_to_voc );
	ofstream of_query;
	of_query.open ( (path + "/centroid_query_sim" ).c_str() );
	ofstream of_terms;
	of_terms.open ( (path + "/centroid_terms_sim" ).c_str() );

	while  ( finput->good() )
	{
		string query;
		getline (*finput, query);
		// Here we need to call a function to calculate similarity between the entire query and the centroid
		//and output it to an appropriate file
		vector<std::string> query_vector;
		parseLine( query, query_vector );     
		if (query.empty()) { break; }
		std::string qid=query_vector[0];
		query_vector.erase(query_vector.begin(),query_vector.begin()+1);
		float simqcent = SimQueryCentroid( query_vector, generalVariables.voc_to_line, generalVariables.ptr_to_voc, 
			generalVariables.dim,  generalVariables.size, generalVariables.data, centroid);
		of_query << qid << " " << simqcent << std::endl;
		for (size_t i=0; i<query_vector.size();i++)
		{
			//
			vector<std::string> temp;
			temp.push_back(query_vector[i]);
			simqcent = SimQueryCentroid( query_vector, generalVariables.voc_to_line,generalVariables.ptr_to_voc, 
				generalVariables.dim,  generalVariables.size, generalVariables.data, centroid);
			of_terms << qid << " " << query_vector[i] << " " << simqcent << std::endl;
		}
	}
	delete[] centroid;
	of_query.close();
	of_terms.close();

	finput->close();
	delete finput;
}

/* KNN-RM */
// model-file input-file KNN-RM path_for_output
bool checkKnnRmInput(int argc , char **argv)
{
	if(argc != 5) // argv : [0] = program name , [1] = model file name , [2] = input-file name , [3] =  KNN-RM command , [4] = path for output 
	{
		std::cerr << "Expect a three input parameters ( model-file   input-file   KNN-RM  path_for_output  ) , receive " << argc - 1 << std::endl;
		return false; 
	}
	ifstream file(argv[2]);
	if (!file)
	{
		std::cerr << "Can't open input file: " << argv[2] << std::endl;
		return false;
	}

	/*ifstream path(argv[4]);
	if (!path)
	{
		std::cerr << "Can't open path for output : " << argv[4] << std::endl;
		return false;
	}*/

	return true;
}
void knnRm()
{
	ifstream* finput = NULL;
	finput = new ifstream( generalVariables.input.c_str() );

	std::string path=operation.argv[4];
	// Input format: qid term RM-prob
	string qid, next, term;
	double prob(0.0);
	*finput >> next >> term >> prob;
	while  ( finput->good() )
	{
		//we read all the terms in for this query
		qid=next;
		map<string,double> rm_terms;

		while ((  next == qid )&& ( finput->good() ))
		{
			rm_terms.insert(make_pair<string,double> (term, prob));
			*finput >> next >> term >> prob; 
		}
		if ( ! ( finput->good() ))
			rm_terms.insert(make_pair<string,double> (term, prob));
		cerr << "loaded " << rm_terms.size() << " terms for query " << qid << std::endl;
		KNN_RM( rm_terms, generalVariables.voc_to_line, generalVariables.ptr_to_voc, generalVariables.dim, generalVariables.size, generalVariables.data, path + "/" + qid + ".clusters" );
	}

	finput->close();
	delete finput;
}

/* ClusterFeatures */
// model-file query-file ClusterFeatures path_to_clusters path_to_output
bool checkClusterFeaturesInput(int argc , char **argv)
{
	if(argc != 6) // argv : [0] = program name , [1] = model file name , [2] = query-file name , [3] =  ClusterFeatures command , [4] = path_to_clusters , [5] = path for output 
	{
		std::cerr << "Expect a three input parameters ( model-file   query-file   ClusterFeatures   path_to_clusters  path_for_output  ) , receive " << argc - 1 << std::endl;
		return false; 
	}
	ifstream file(argv[2]);
	if (!file)
	{
		std::cerr << "Can't open query file: " << argv[2] << std::endl;
		return false;
	}

	ifstream pathToclusters(argv[4]);
	if (!pathToclusters)
	{
		std::cerr << "Can't open path for clusters : " << argv[4] << std::endl;
		return false;
	}

	ifstream path(argv[5]);
	if (!path)
	{
		std::cerr << "Can't open path for output : " << argv[4] << std::endl;
		return false;
	}

	return true;
}
void clusterFeatures()
{
	ifstream* finput = NULL;
	finput = new ifstream( generalVariables.input.c_str() );

	std::string path_to_clusters=operation.argv[4];
	std::string path_to_output=operation.argv[5];
	float* centroid = GetCentroid( generalVariables.dim, generalVariables.size, generalVariables.data, generalVariables.ptr_to_voc );

	while  ( finput->good() )
	{
		string query;
		getline (*finput, query);

		vector<std::string> query_vector;
		parseLine( query, query_vector );     
		if (query.empty()) { break; }
		std::string qid=query_vector[0];
		query_vector.erase(query_vector.begin(),query_vector.begin()+1);
		std::map<size_t,std::vector<string> > clusters;
		LoadClusters( path_to_clusters + "/" + qid + ".clusters", clusters, 100); ///!!!!Attention
		//  for ( std::map<size_t,std::vector<string> >::const_iterator cit = clusters.begin(); cit != clusters.end(); cit++)
		//            {
		//                size_t id = cit->first;
		//                for (size_t i=0; i<cit->second.size(); i++)
		//                {
		//                    cout << id << " " << cit->second[i]  << std::endl;  
		//                }    
		//            }
		std::map<string, float*> rep_query;
		float* query_cent=GetClusterCentroid(query_vector, generalVariables.voc_to_line, generalVariables.ptr_to_voc, generalVariables.dim, generalVariables.size, generalVariables.data, rep_query );

		for (size_t ic=1; ic <= clusters.size(); ic++)
		{
			std::map<string, float*> rep_cluster;
			//assumption  - the data was normalized previously - in function that returns collection centroid
			float* clust_cent = GetClusterCentroid( clusters[ic], generalVariables.voc_to_line, generalVariables.ptr_to_voc, generalVariables.dim, generalVariables.size, generalVariables.data, rep_cluster );
			float avg_cosine_cluster_terms = GetAvgCosineSim(rep_cluster, clust_cent, generalVariables.dim,path_to_output, qid, ic);
			float clust_query_cent_sim = Cosine( query_cent, clust_cent, generalVariables.dim);  
			float clust_cent_corpus = Cosine ( clust_cent, centroid, generalVariables.dim);
			//  cerr << "similarities: "<< avg_cosine_cluster_terms << " " << clust_query_cent_sim << " " << clust_cent_corpus << std::endl;
			std::map<string,float> dist_to_qterm;         
			for (std::map<string, float*>::const_iterator cit2=rep_query.begin(); cit2 != rep_query.end(); cit2++)
			{
				dist_to_qterm.insert(std::make_pair<string,float>(cit2->first,Cosine(cit2->second,clust_cent,generalVariables.dim)));
			}
			PrintClusterFeatures( path_to_output, qid, ic,
				avg_cosine_cluster_terms, clust_query_cent_sim, clust_cent_corpus, dist_to_qterm);

			delete[] clust_cent;
		}
		delete[] query_cent;
	}
	delete[] centroid;

	finput->close();
	delete finput;

}


/* TranslationNorm */
// model-file TranslationNorm
void translationNorm()
{
	for (std::map<std::string,float*>::const_iterator cit = generalVariables.voc_to_line.begin();
		cit != generalVariables.voc_to_line.end(); cit++)
	{
		double norm = GetNormalizerOverCorpus( cit->first, generalVariables.voc_to_line, generalVariables.data,  generalVariables.dim, generalVariables.size );
		cout << cit->first << " " << norm << std::endl;
	}
}

/*typedef struct Term
{
	std::string value;
	float* vec;
	double weight;
}Term;

typedef struct Rm1Terms
{
	std::string QueryId;
	std::vector<Term*> OriginalQuery;
	float OriginalQueryWeight;
	std::vector<Term*> TermFromRm1;
	float TermsWeight;
}Rm1Terms;*/


#pragma endregion 


/* knn_rm1_Terms */
//model-file expended-query-file knn_rm1_Terms k-neigb output_directory
bool checKnnRm1TermsInput(int argc , char **argv)
{
	// argv : 
	//[0] = program name , 
	//[1] = model file name , 
	//[2] = expended-query-file , 
	//[3] = knn_rm1_Terms command , 
	//[4] = k-neigb , 
	//[5] = ProbabilityType  , 
	//[6] = output directory
	//[7] = qrels File

	if(argc != 8) 
	{
		std::cerr << "Expect a three input parameters ( model-file   input-file   KNN-terms  k-neigb) , receive " << argc - 1 << std::endl;
		return false; 
	}
	ifstream file(argv[2]);
	if (!file)
	{
		std::cerr << "Can't open subset file: " << argv[2] << std::endl;
		return false;
	}

	char * temp;
	errno = 0;
	int k =(int) strtol(argv[4], &temp, 10);
	if (temp == argv[4] || *temp != '\0' || ((k == LONG_MIN || k == LONG_MAX) && errno == ERANGE))
	{
		std::cerr << "Could not convert " << argv[4] << " to number" <<  std::endl;
		return false;
	}

	return true;
}
/*void knnRm1Terms()
{
	// read file the "IndriRunQuery_BuildEtendedQuery" was created.
	std::vector<Rm1Terms*> *rm =  readExpensQueryFile(generalVariables.input);
	QrelsFile* QrelsFile = readQrelsFile(operation.argv[6]);


	std::string str_k = operation.argv[4];
	istringstream ss1(str_k);
	int k(0);
	ss1 >> k;

	//Pass each extended query
	for (int j = 0 ; j < rm->size() ; ++j)
    {  
		Rm1Terms *rm1Terms = (*rm)[j];
		char path[100];
		sprintf(path,"%s/",operation.argv[5]);

		// the file name of queries that each query represent cluster.
		std::string  outputFile = path + rm1Terms->QueryId + ".clusters";

		// run on the extended query KNN algorithm and create new file of queries that each query represent cluster.
		knn_rm1_Terms( rm1Terms, 
			generalVariables.voc_to_line, 
			generalVariables.ptr_to_voc, 
			generalVariables.dim, 
			generalVariables.size, 
			generalVariables.data, 
			outputFile,
			k);


	}

	delete rm;
}*/

void knnRm1Terms()
{
	//$WordToVecHelper $WordToVecIndex $OutputFile knn_rm1_Terms $k $probabilityType $ExpandedQuerysFolder $qrelsFile
	// read file the "IndriRunQuery_BuildEtendedQuery" was created.
	std::vector<Rm1Terms*> *rm =  readExpensQueryFile(generalVariables.input);
	QrelsFile* QrelsFile = readQrelsFile(operation.argv[7]);
	std::map<int,std::vector<QrelsLine*>*> quralMap;
	for(int i = 0 ; i< QrelsFile->Lines.size() ; i ++ )
	{
		QrelsLine* line = QrelsFile->Lines[i];
		std::map<int,std::vector<QrelsLine*>*>::const_iterator pos = quralMap.find(atoi(line->QueryId.c_str()));
		if ( pos == quralMap.end() ) {
		  // not found
			std::vector<QrelsLine*> *vec = new std::vector<QrelsLine*>();
			vec->push_back(line);
			quralMap.insert( std::make_pair(atoi(line->QueryId.c_str()), vec) ); 
			
		} else {
		  // found
			std::vector<QrelsLine*>* vec = pos->second;
			vec->push_back(line);
		}
	}


	std::string str_k = operation.argv[4];
	istringstream ss1(str_k);
	int k(0);
	ss1 >> k;


	istringstream sstype(operation.argv[5]);
	int typeNum(0);
	sstype >> typeNum;

	//Pass each extended query
	for (int j = 0 ; j < rm->size() ; ++j)
    {  
		Rm1Terms *rm1Terms = (*rm)[j];
		char path[300];
		sprintf(path,"%s/",operation.argv[6]);

		printf("Extend Query %s\n",rm1Terms->QueryId.c_str());
		// the file name of queries that each query represent cluster.
		std::string  outputFile = path + rm1Terms->QueryId + ".clusters";

		std::map<int,std::vector<QrelsLine*>*>::const_iterator pos = quralMap.find(atoi(rm1Terms->QueryId.c_str()));
		if ( pos == quralMap.end() ) {
		  printf("There is no relevant feedback for query %s\n",rm1Terms->QueryId.c_str());
		} else {
		  // found

			std::vector<QrelsLine*>* vec = pos->second;
			// run on the extended query KNN algorithm and create new file of queries that each query represent cluster.
			KnnRm1TermsProbabilityType type = typeNum==0 ? Uniform : RelativeToRm1;
			
			printf("KnnRm1TermsProbabilityType is  %s\n",(typeNum==0 ? "Uniform" : "RelativeToRm1"));

		knn_rm1_Terms( rm1Terms, 
			generalVariables.voc_to_line, 
			generalVariables.ptr_to_voc, 
			generalVariables.dim, 
			generalVariables.size, 
			generalVariables.data, 
			outputFile,
			k,
			type,
			vec);

		}

		


	}

	delete rm;
}




bool checNewRm3ModleUsingKnnRm1Terms(int argc , char **argv)
{
	// argv : 
	//[0] = program name , 
	//[1] = model file name , 
	//[2] = expended-query-file , 
	//[3] = knn_rm1_Terms command , 
	//[4] = k-neigb , 
	//[5] = ProbabilityType  , 
	//[6] = output directory
	//[7] = qrels File
	//[8] = originalQueryWeight
	//[9] = clusterTermsWeight
	if(argc != 10) 
	{
		std::cerr << "Expect a three input parameters ( model-file   input-file   KNN-terms  k-neigb) , receive " << argc - 1 << std::endl;
		return false; 
	}
	ifstream file(argv[2]);
	if (!file)
	{
		std::cerr << "Can't open subset file: " << argv[2] << std::endl;
		return false;
	}

	char * temp;
	errno = 0;
	int k =(int) strtol(argv[4], &temp, 10);
	if (temp == argv[4] || *temp != '\0' || ((k == LONG_MIN || k == LONG_MAX) && errno == ERANGE))
	{
		std::cerr << "Could not convert " << argv[4] << " to number" <<  std::endl;
		return false;
	}

	return true;
}

void newRm3ModleUsingKnnRm1Terms()
{

    // argv : 
	//[0] = program name , 
	//[1] = model file name , 
	//[2] = expended-query-file , 
	//[3] = knn_rm1_Terms command , 
	//[4] = k-neigb , 
	//[5] = ProbabilityType  , 
	//[6] = output directory
	//[7] = qrels File
	//[8] = originalQueryWeight
	//[9] = clusterTermsWeight


	//$WordToVecHelper $WordToVecIndex $OutputFile knn_rm1_Terms $k $probabilityType $ExpandedQuerysFolder $qrelsFile
	// read file the "IndriRunQuery_BuildEtendedQuery" was created.
	std::vector<Rm1Terms*> *rm =  readExpensQueryFile(generalVariables.input);
	QrelsFile* QrelsFile = readQrelsFile(operation.argv[7]);
	std::map<int,std::vector<QrelsLine*>*> quralMap;
	for(int i = 0 ; i< QrelsFile->Lines.size() ; i ++ )
	{
		QrelsLine* line = QrelsFile->Lines[i];
		std::map<int,std::vector<QrelsLine*>*>::const_iterator pos = quralMap.find(atoi(line->QueryId.c_str()));
		if ( pos == quralMap.end() ) {
		  // not found
			std::vector<QrelsLine*> *vec = new std::vector<QrelsLine*>();
			vec->push_back(line);
			quralMap.insert( std::make_pair(atoi(line->QueryId.c_str()), vec) ); 
			
		} else {
		  // found
			std::vector<QrelsLine*>* vec = pos->second;
			vec->push_back(line);
		}
	}


	std::string str_k = operation.argv[4];
	istringstream ss1(str_k);
	int k(0);
	ss1 >> k;
	
	istringstream sstype(operation.argv[5]);
	int typeNum(0);
	sstype >> typeNum;
	
	istringstream ssoriginalQueryWeight(operation.argv[8]);
	double originalQueryWeight(0);
	ssoriginalQueryWeight >> originalQueryWeight;

	istringstream ssclusterTermsWeight(operation.argv[9]);
	double clusterTermsWeight(0);
	ssclusterTermsWeight >> clusterTermsWeight;
		
	//Pass each extended query
	for (int j = 0 ; j < rm->size() ; ++j)
    {  
		Rm1Terms *rm1Terms = (*rm)[j];
		char path[300];
		sprintf(path,"%s/",operation.argv[6]);

		printf("Extend Query %s\n",rm1Terms->QueryId.c_str());
		// the file name of queries that each query represent cluster.
		std::string  outputFile = path + rm1Terms->QueryId + ".clusters";

		std::map<int,std::vector<QrelsLine*>*>::const_iterator pos = quralMap.find(atoi(rm1Terms->QueryId.c_str()));
		if ( pos == quralMap.end() ) {
		  // not found
		} else {
		  // found

			std::vector<QrelsLine*>* vec = pos->second;
			// run on the extended query KNN algorithm and create new file of queries that each query represent cluster.
			KnnRm1TermsProbabilityType type = typeNum==0 ? Uniform : RelativeToRm1;
			
			printf("KnnRm1TermsProbabilityType is  %s (type value is %d)\n",(typeNum==0 ? "Uniform" : "RelativeToRm1"), typeNum);

		new_rm3_modle_using_knn_rm1_Terms( rm1Terms, 
			generalVariables.voc_to_line, 
			generalVariables.ptr_to_voc, 
			generalVariables.dim, 
			generalVariables.size, 
			generalVariables.data, 
			outputFile,
			k,
			type,
			originalQueryWeight,
			clusterTermsWeight,
			vec);

		}

		


	}

	delete rm;
}


bool checkCreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModel(int argc , char **argv)
{
	return true;
}
void createQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModel()
{

}


/*
void knnRm1Terms()
{
	// read file the "IndriRunQuery_BuildEtendedQuery" was created.
	std::vector<Rm1Terms*> *rm =  readExpensQueryFile(generalVariables.input);

	std::string str_k = operation.argv[4];
	istringstream ss1(str_k);
	int k(0);
	ss1 >> k;
	std::string path = operation.argv[5];
	std::vector<ExpendedQuery*>* expendedQueryList = getListOfExpendedQuerys2( rm, 
			generalVariables.voc_to_line, 
			generalVariables.ptr_to_voc, 
			generalVariables.dim, 
			generalVariables.size, 
			generalVariables.data, 
			k);
	createQueryFile(expendedQueryList, 1, path);

	for(int i = 0 ; i<expendedQueryList->size() ; ++i)
	{
		delete &expendedQueryList[i];
	}
	delete expendedQueryList;
	delete rm;
}*/


void SetTheCurrentOperation(int argc, char *argv[])
{
	generalVariables.model = argv[1];
	generalVariables.input =  argv[2];
	generalVariables.command =  argv[3];
	generalVariables.dim = 0;
	generalVariables.size = 0;
	generalVariables.data = NULL;

	operation.needTosetTheVocabulary = true;
	operation.argv = argv;

	if (generalVariables.input=="Vocabulary") 
	{ 
		operation.needTosetTheVocabulary = false;
		operation.checkInput = checkVocabularyInput;
		operation.execute = vocabulary;
	}
	else if (generalVariables.input == "TranslationNorm" )
	{
		operation.checkInput = checkInput_ReturnTrue;
		operation.execute = translationNorm;
	}

	std::string command = generalVariables.command; 

	if (command=="cosine")
	{
		operation.checkInput = checkCosineInput;
		operation.execute = cosine;
	}
	else if (command=="KNN")
	{
		operation.checkInput = checkKnnInput;
		operation.execute = knn;
	}
	else if (command=="ScoreSubset")
	{
		operation.checkInput = checkscoreSubsetInput;
		operation.execute = scoreSubset;
	}
	else if (command=="KNN-terms")
	{
		operation.checkInput = checkKnnTermsInput;
		operation.execute = knnTerms;
	}
	else if (command=="KNN-all-no-sort")
	{
		operation.checkInput = checkKnnAllNoSortInput;
		operation.execute = knnAllNoSort;
	}
	else if (command=="KNN-all-no-sort-terms")
	{
		operation.checkInput = checkKnnAllNoSortTermsInput;
		operation.execute = knnAllNoSortTerms;
	}
	else if (command=="Centroid")
	{
		operation.checkInput = checkCentroidInput;
		operation.execute = centroid;
	}
	else if (command=="KNN-RM")
	{
		operation.checkInput = checkKnnRmInput;
		operation.execute = knnRm;
	}
	else if (command=="ClusterFeatures")
	{
		operation.checkInput = checkClusterFeaturesInput;
		operation.execute = clusterFeatures;
	}
	else if (command=="knn_rm1_Terms")
	{
		operation.checkInput = checKnnRm1TermsInput;
		operation.execute = knnRm1Terms;
	}
	else if (command=="New_Rm3_Modle_Using_Knn_Rm1_Terms")
	{
		operation.checkInput = checNewRm3ModleUsingKnnRm1Terms;
		operation.execute = newRm3ModleUsingKnnRm1Terms;
	}
	else if (command=="createQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModel")
	{
		operation.checkInput = checkCreateQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModel;
		operation.execute = createQueryExpansionUsingKnnClusteringWordsComeFromDiscriminativeModel;
	}

}


void WriteToFile( const std::string& name, const std::vector<float>& vout, const std::string& path)
{
	ofstream of;

	of.open ( (path + "/" + name ).c_str() );

	for ( std::vector<float>::const_iterator cit=vout.begin(); 
		cit!= vout.end(); 
		cit++)
	{
		of << *cit << std::endl;
	}
	of.close();

}

void PrintClusterFeatures( const std::string& path_to_output, const string& qid, size_t clust_id,
						  float avg_cosine_cluster_terms, 
						  float clust_query_cent_sim,
						  float clust_query_cent_corpus,
						  const std::map<string,float>& dist_to_qterm)
{
	ofstream of;
	of.open ( (path_to_output + "/cluster_centroid_features" ).c_str(), std::ofstream::out | std::ofstream::app );
	of << qid << " " << clust_id << " " << avg_cosine_cluster_terms << " " << clust_query_cent_sim << " " << clust_query_cent_corpus << std::endl;
	of.close();
	ofstream of2;
	of2.open ( (path_to_output + "/sim_cluster_centroid_query_term" ).c_str(), std::ofstream::out | std::ofstream::app );
	for ( std::map<string,float>::const_iterator cit = dist_to_qterm.begin(); cit != dist_to_qterm.end(); cit++)
	{    
		of2 << qid << " " << clust_id << " " << cit->first << " " << cit->second << std::endl;
	}
	of2.close();
} 