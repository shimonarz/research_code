/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   functions.h
 * Author: anna
 *
 * Created on April 16, 2016, 9:23 PM
 */



#ifndef W2V_FUNC_H
#define W2V_FUNC_H

#include <string>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <set>
#include <sstream>
#include <algorithm>

using std::ifstream;
using std::ofstream;
using std::map;
using std::vector;
using std::string;
using std::cout;
using std::cerr;

///////////////////////////// shimon adding  - Start///////////////////////////////////////////

inline void split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
}

inline std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}


struct smaller 
{
	inline bool operator()(const std::pair<float*,float>& p1, const std::pair<float*,float>& p2)
	{
		if ( p1.second > p2.second ) return true;
		return false;
	}   
};

struct smallerStrPair 
{
	inline bool operator()(const std::pair<string,float>& p1, const std::pair<string,float>& p2)
	{
		if ( p1.second > p2.second ) return true;
		return false;
	}   
};

struct smallerStringDoublePair 
{
	inline bool operator()(const std::pair<string,double>& p1, const std::pair<string,double>& p2)
	{
		if ( p1.second > p2.second ) return true;
		return false;
	}   
};

struct smallerDoublePair 
{
	inline bool operator()(const std::pair<double,double>& p1, const std::pair<double,double>& p2)
	{
		if ( p1.second > p2.second ) return true;
		return false;
	}   
};





/***********************************************************************************************/
typedef struct Term
{
	std::string value;
	float* vec;
	double weight;

	 bool operator () ( const Term& m ) const
    {
        return m.value == value;
    }

	 bool operator==(const Term& m  ) const
	 {
		return m.value == value;
	 } 

	 Term* Copy()
	 {
		 Term* t = new Term();
		 t->value = value;
		 t->weight = weight;
		 t->vec = vec;
		 return t;
	 }

}Term;

typedef struct Rm1Terms
{
	std::string QueryId;
	std::vector<Term*> OriginalQuery;
	float OriginalQueryWeight;
	std::vector<Term*> TermFromRm1;
	float TermsWeight;

	void init()
	{
		QueryId = "";
		OriginalQueryWeight = 0.0;
		TermsWeight = 0.0;
	}

}Rm1Terms;

// Cluster RM3 Expansion Terms
typedef struct ClusterRM3ExpansionTerms
{
	std::string QueryId;
	
	float OriginalQueryWeight;
	std::vector<Term*> OriginalQuery;
	
	float Rm1TermsWeight;
	std::vector<Term*> Rm1Terms;

	float ClusterTermsWeight;
	std::vector<Term*> ClusterTerms;

}ClusterRM3ExpansionTerms;



typedef struct ExpendedQuery
{
	std::string QueryId;
	std::string OriginalQuery;
	std::vector<std::string> Querys; 
}ExpendedQuery;



typedef struct QrelsLine
{
	std::string QueryId;
	std::string Zero;
	std::string DocId;
	std::string Score;
}QrelsLine;


typedef struct QrelsFile
{
	std::vector<QrelsLine*> Lines; 
}QrelsFile;



std::vector<Rm1Terms*>* readExpensQueryFile( std::string fileName);
Rm1Terms* parse_query( const std::string& query );
double ** createDistancesMatrix(const std::map<std::string,double>& rm_terms,
				   const std::map<std::string,float*>& voc_to_line, 
				   long long dim, 
				   float* data ,
				   double **matrix = NULL);

//void knn_rm1_Terms( Rm1Terms *rm1Terms,
//				   const std::map<std::string,float*>& voc_to_line, 
//				   const std::map<float*, const std::string* >& ptr_to_voc, 
//				   long long dim, 
//				   long long size,
//				   float* data, 
//				   const std::string& path_to_file,
//				   int k);
std::string buildQuery(Rm1Terms *terms);
std::string convertTermToString(std::vector<Term*> terms , bool withWeight = false, bool withQuotationMark  = false );
void createQueryFile(std::vector<ExpendedQuery*> *expendedQueryList,int numberOfClusters,std::string  path);
std::vector<ExpendedQuery*>* getListOfExpendedQuerys( std::vector<Rm1Terms*> *rm ,
													const std::map<std::string,float*>& voc_to_line, 
													const std::map<float*, const std::string* >& ptr_to_voc, 
													long long dim, 
													long long size,
													float* data, 
													int k);
//std::vector<ExpendedQuery*>* getListOfExpendedQuerys2( std::vector<Rm1Terms*> *rm ,
//													const std::map<std::string,float*>& voc_to_line, 
//													const std::map<float*, const std::string* >& ptr_to_voc, 
//													long long dim, 
//													long long size,
//													float* data, 
//													int k);
QrelsLine* parse_Qrels_line(std::string qrelsLine);
QrelsFile* readQrelsFile(std::string fileName);


enum KnnRm1TermsProbabilityType
{
	Uniform,
	RelativeToRm1
};
void knn_rm1_Terms( Rm1Terms *rm1Terms ,
				   const std::map<std::string,float*>& voc_to_line, 
				   const std::map<float*, const std::string* >& ptr_to_voc, 
				   long long dim, 
				   long long size,
				   float* data, 
				   const std::string& path_to_file ,
				   int k,
				   KnnRm1TermsProbabilityType type,
				   std::vector<QrelsLine*>* vec);

void new_rm3_modle_using_knn_rm1_Terms( Rm1Terms *rm1Terms ,
				   const std::map<std::string,float*>& voc_to_line, 
				   const std::map<float*, const std::string* >& ptr_to_voc, 
				   long long dim, 
				   long long size,
				   float* data, 
				   const std::string& path_to_file ,
				   int k,
				   KnnRm1TermsProbabilityType type,
				   double originalQueryWeight,
				   double clusterTermsWeight,
				   std::vector<QrelsLine*>* vec);

bool sort_pred(const std::pair<int,double>& left, const std::pair<int,double>& right);

std::string buildNewRm3ClustresQuery(ClusterRM3ExpansionTerms *terms);



///////////////////////////// shimon adding - End ///////////////////////////////////////////



void CalcAllCosine( long long dim, long long size, float* ptr, 
                    std::vector<std::pair<float*,float> >& vcos, float* data );


void GetSumRepresentation(std::map<std::string, float*>& rep,
                         long long dim,
                         float*& ptr);

void parseLine( const std::string& line, std::vector<std::string>& terms );

void GetVectorsForLines( std::map<std::string, float*>& rep,
                         long long dim,
                         const std::map<std::string,float*>& voc_to_line,
                         const std::vector<std::string>& terms,
                         float* data);
                         
void LineToTerm(  const std::map<std::string,float*>& voc_to_line,
                  const std::vector<std::string>& terms, 
                  std::map<float*,std::string>& line_to_term);

void LoadVocabulary( FILE* model_file, 
                     long long& dim, long long& size,
                     std::map<std::string, float*>& voc_to_line,
                     std::map<float*, const std::string* >& ptr_to_voc,
                     float*& data );
                     
void PrintVocabulary( FILE* model_file,
                    long long& dim, long long& size);
                     
double CalcCosine(const std::string& term1, 
                  const std::string& term2,
                  const std::map<std::string,float*>& voc_to_line,
                  float* data,
                  long long dim); 

//double GetNormalizedProb(const std::string& term1, const std::string& term2, const std::map<std::string,float*>&  voc_to_line, 
//                        float*  data, long long dim, long long size, double& cos);
  
double GetNormalizerOverCorpus( const std::string& term, const std::map<std::string,float*>&  voc_to_line,
                                float* data, long long dim, long long size );
void LoadRestrictedVocabulary( const std::string& filename, std::set<std::string>& voc );

void GetNormalizedProbs(const std::string& qterm, 
                        const std::map<std::string,float*>&  voc_to_line,
                        float* data, long long dim, long long size, 
                        std::vector<std::pair<std::string,double> >& terms, const std::set<std::string>& voc);//, const std::map<std::string,double>& normCorpus);
void ReadNormTrans(std::map<std::string,double>& normCorpus, const std::string& transFile);

void KNN(  const std::vector<std::string>& terms,
           const std::map<std::string,float*>& voc_to_line,
           const std::map<float*, const std::string*>& ptr_to_voc,
           long long dim, long long size, int k,
           std::vector<std::pair<std::string,float> >& vout,
           bool norm,
           float* data );
           
void KNN_all_no_sort( const std::vector<std::string>& terms, 
                      const std::map<std::string,float*>& voc_to_line, 
                      const std::map<float*, const std::string*>& ptr_to_voc, 
                      long long dim,
                      long long size,
                      std::vector<float>& vout, 
                      bool norm, float* data );
                      
void KNN_term_no_sort(const std::string& term, 
                      const std::map<std::string,float*>& voc_to_line, 
                      const std::map<float*, const std::string*>& ptr_to_voc, 
                      long long dim,
                      long long size,
                      std::vector<float>& vout, 
                      float* data );
		   
float Cosine(float* pv1, float* pv2, long long dim); 

void NormVectors(std::map<std::string, float*>& rep, long long dim);

void ScoreSubset( const std::vector<std::string>& terms,
                  const std::map<std::string,float*>& voc_to_line,
                  long long dim, std::vector<std::pair<std::string,float> >& vout,
                  bool norm,
                  const std::vector<std::string>& subset,
                  float* data);
                  
void KNN_term( const std::vector<std::string>& terms,
               const std::map<std::string,float*>& voc_to_line, 
               const std::map<float*, const std::string* >& ptr_to_voc,
               long long dim, long long size,
               const std::string& qid,
               int k, float* data );
               
float* GetCentroid( long long dim, long long size, float* data, const std::map<float*, const std::string* >& ptr_to_voc );

float SimQueryCentroid( const std::vector<std::string>& terms,
                        const std::map<std::string,float*>& voc_to_line, 
                        const std::map<float*, const std::string* >& ptr_to_voc, 
                        long long dim, long long size,
                        float* norm_data, float* centroid);
  
float* GetClusterCentroid( const std::vector<std::string>& cluster,
                          const std::map<std::string,float*>& voc_to_line, 
                          const std::map<float*, const std::string* >& ptr_to_voc, 
                          long long dim, long long size,
                          float* norm_data,
                          std::map<std::string, float*>& rep);
                          
float GetAvgCosineSim( const std::map<std::string, float*>& rep, float* cent, long long dim, 
                       const std::string& path_to_output, const std::string& qid, size_t clust_id );
                        
                        
void LoadClusters( const std::string& path, std::map<size_t,std::vector<std::string> >& clusters, size_t size);

void KNN_RM( const std::map<std::string,double>& rm_terms,
             const std::map<std::string,float*>& voc_to_line, 
             const std::map<float*, const std::string* >& ptr_to_voc, 
             long long dim, long long size,
             float* data, const std::string& path );
         

void CalcAllCosine( long long dim,
                    float* ptr,
                    std::vector<std::pair<std::string,float> >& vcos,
                    const std::map<std::string,float*>& rep_subset);
                    
void CleanSubset( std::map<std::string, float*>& rep );

void InitSubset(  long long dim,
                  std::map<std::string, float*>& rep,
                  const std::map<std::string, float*>& voc_to_line,
                  const std::vector<std::string>& terms, 
                  std::map<long long, std::string>& line_to_term);
                  

void InitSubset(  long long dim,
                  std::map<std::string, float*>& rep,
                  const std::map<std::string,float*>& voc_to_line,
                  const std::vector<std::string>& terms );


#endif
