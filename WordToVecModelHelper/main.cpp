
/* 
* File:   main.cpp
* Author: anna
*
* Created on April 16, 2016, 9:21 PM
*/

#include <cstdlib>
#include <iostream>

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "functions.h"
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
using namespace std;

/*
void WriteToFile( const std::string& name, const std::vector<float>& vout, const std::string& path)
{
	ofstream of;

	of.open ( (path + "/" + name ).c_str() );

	for ( std::vector<float>::const_iterator cit=vout.begin(); 
		cit!= vout.end(); 
		cit++)
	{
		of << *cit << std::endl;
	}
	of.close();

}
void PrintClusterFeatures( const std::string& path_to_output, const string& qid, size_t clust_id,
						  float avg_cosine_cluster_terms, 
						  float clust_query_cent_sim,
						  float clust_query_cent_corpus,
						  const std::map<string,float>& dist_to_qterm)
{
	ofstream of;
	of.open ( (path_to_output + "/cluster_centroid_features" ).c_str(), std::ofstream::out | std::ofstream::app );
	of << qid << " " << clust_id << " " << avg_cosine_cluster_terms << " " << clust_query_cent_sim << " " << clust_query_cent_corpus << std::endl;
	of.close();
	ofstream of2;
	of2.open ( (path_to_output + "/sim_cluster_centroid_query_term" ).c_str(), std::ofstream::out | std::ofstream::app );
	for ( std::map<string,float>::const_iterator cit = dist_to_qterm.begin(); cit != dist_to_qterm.end(); cit++)
	{    
		of2 << qid << " " << clust_id << " " << cit->first << " " << cit->second << std::endl;
	}
	of2.close();
}          




int main_123(int argc, char *argv[])
{

	if (argc <3 ) {
		cout << "Usage (1): model-file input-file cosine" << std::endl;
		cout << "Usage (2): model-file input-file KNN k-neigb normalize-1/0" << std::endl;
		cout << "Usage (3): model-file subset-file ScoreSubset normalize-1/0" << std::endl;
		cout << "Usage (4): model-file input-file KNN-terms k-neigb" << std::endl;
		cout << "Usage (5): model-file input-file KNN-all-no-sort norm path_for_output" << std::endl;
		cout << "Usage (6): model-file Vocabulary" << std::endl;
		cout << "Usage (7): model-file input-file KNN-all-no-sort-terms path_for_output" << std::endl;
		cout << "Usage (8): model-file query-file Centroid path_for_output" << std::endl;
		cout << "Usage (9): model-file input-file KNN-RM path_for_output" << std::endl;
		cout << "Usage (10): model-file query-file ClusterFeatures path_to_clusters path_to_output" << std::endl; 
		cout << "Usage (11): model-file input-file(q_i|w) Translation" << std::endl;//TransNormFile
		cout << "Usage (12): model-file TranslationNorm" << std::endl;

		return 0;
	}




	std::string model = argv[1];
	std::cout << model << std::endl;
	std::string input = argv[2];
	ifstream* finput = NULL;
	std::string command;
	std::map<std::string,float*> voc_to_line;
	std::map<float*,const std::string*> ptr_to_voc;
	float* data=NULL;
	long long dim(0), size(0);
	FILE*  model_file;
	model_file = fopen(model.c_str(), "rb");
	if (model_file == NULL) {
		printf("Model file not found\n");
		return -1;
	}

	if ((input != "Vocabulary")&&(input !="TranslationNorm" ))
	{
		command = argv[3];
		finput = new ifstream( input.c_str() );
		if ( !finput->is_open() )
		{
			std::cerr << "Can't open input file: " << input << std::endl;
			return 1;
		}
		LoadVocabulary( model_file, dim, size, voc_to_line, ptr_to_voc, data);
		fclose(model_file);

	}
	else if (input=="Vocabulary") { //here we process vocabulary command
		PrintVocabulary( model_file, dim, size);
		fclose(model_file);
	}
	else if (input == "TranslationNorm" )
	{
		LoadVocabulary( model_file, dim, size, voc_to_line, ptr_to_voc, data);
		fclose(model_file);
		for (std::map<std::string,float*>::const_iterator cit = voc_to_line.begin();
			cit != voc_to_line.end(); cit++){
				double norm = GetNormalizerOverCorpus( cit->first, voc_to_line, data,  dim, size );
				cout << cit->first << " " << norm << std::endl;
		}

	}


	if (command=="cosine")
	{
		string line;
		while  ( finput->good() )
		{
			getline (*finput, line);
			std::stringstream os(line);
			std::string term1, term2;
			os >> term1;
			os >> term2;

			if (!term1.empty()&&!term2.empty()){
				double cos = CalcCosine( term1, term2, voc_to_line, data, dim );
				cout << term1 << " - " << term2 << " : " << cos << std::endl;
			}
		}
	}

	else if (command=="KNN")
	{
		std::string str_k = argv[4];
		istringstream ss1(str_k);
		int k(0);
		ss1 >> k;
		std::string bool_norm = argv[5];
		istringstream ss2(bool_norm);
		bool norm(false);
		ss2 >> norm;
		std::vector<std::pair<string,float> > vout;
		while  ( finput->good() )
		{
			string query;
			getline (*finput, query);
			cout << "query = " << query <<" "<< std::endl;
			vector<std::string> query_vector;
			parseLine( query, query_vector );     
			if (query.empty()) { break; }
			std::string qid=query_vector[0];
			query_vector.erase(query_vector.begin(),query_vector.begin()+1);

			KNN( query_vector, voc_to_line, ptr_to_voc, dim, size, k, vout, norm, data );
			int i=1;
			for (size_t sz = 0; sz < vout.size(); sz++)
			{
				cout << qid <<" " << vout[sz].first << " " << vout[sz].second << " " << i << std::endl;
				i++;
			}

		}
	}

	else if (command=="ScoreSubset")
	{
		string line;
		std::string bool_norm = argv[4];
		istringstream ss2(bool_norm);
		bool norm(false);
		ss2 >> norm;
		string query;
		getline (*finput, query);
		while  ( finput->good() )
		{//file format:
			//            // ! qid qterm1 qterm2 qterm3....
			std::vector<std::pair<string,float> > vout;
			std::vector<std::string> subset;
			vector<std::string> query_vector;
			parseLine( query, query_vector );     
			if (query_vector[0]!="!") 
			{
				//              // cerr << "bad format, can't read query " << std::endl;
				break;
			}
			std::string qid=query_vector[1];            
			query_vector.erase(query_vector.begin(),query_vector.begin()+2);
			while  ( finput->good() ){
				getline (*finput, line);
				if (line.empty() ) continue;
				std::stringstream os(line);
				std::string term1;
				os >> term1;
				if (term1=="!")
				{  query=line;
				break;
				}
				subset.push_back(term1);
			}

			ScoreSubset( query_vector, voc_to_line, dim, vout, norm, subset, data);
			int i=1;
			for (std::vector<std::pair<string,float> >::const_iterator cit=vout.begin();
				cit != vout.end(); cit++)
			{
				cout << qid + " " << cit->first << " " << cit->second << " " << i << std::endl;   
				i++;      
			}
		}
	}
	else if (command=="KNN-terms")
	{
		string line;
		std::string str_k = argv[4];
		istringstream ss1(str_k);
		int k(0);
		ss1 >> k; // k neighbours to find

		while  ( finput->good() )
		{//file format:
			// qid qterm1 qterm2 qterm3....

			string query;
			getline (*finput, query);
			vector<std::string> query_vector;
			parseLine( query, query_vector );     
			if ( query.empty() ) { break; }
			std::string qid = query_vector[0];
			query_vector.erase( query_vector.begin(), query_vector.begin()+1 );    

			KNN_term( query_vector, voc_to_line, ptr_to_voc, dim, size, qid, k, data);
		}
	}
	else if (command=="KNN-all-no-sort")
	{
		std::string bool_norm = argv[4];
		istringstream ss2(bool_norm);
		bool norm(false);
		ss2 >> norm;
		std::string path=argv[5];

		while  ( finput->good() )
		{
			string query;
			getline (*finput, query);
			vector<std::string> query_vector;
			parseLine( query, query_vector );     
			if (query.empty()) { break; }
			std::string qid=query_vector[0];
			query_vector.erase(query_vector.begin(),query_vector.begin()+1);
			std::vector<float> vout;
			KNN_all_no_sort( query_vector, voc_to_line, ptr_to_voc, dim, size, vout, norm, data );
			string snorm;
			//reversed as always
			if (norm)
				snorm = "notnorm";
			else
				snorm = "norm";
			WriteToFile( qid + "." + snorm, vout, path ); //To implement!!!
		}
	}
	else if (command=="KNN-all-no-sort-terms")
	{

		std::string path=argv[4];

		while  ( finput->good() )
		{
			string query;
			getline (*finput, query);
			vector<std::string> query_vector;
			parseLine( query, query_vector );     
			if (query.empty()) { break; }
			std::string qid=query_vector[0];
			query_vector.erase(query_vector.begin(),query_vector.begin()+1);
			for (size_t i=0; i<query_vector.size();i++)
			{
				std::vector<float> vout;
				KNN_term_no_sort( query_vector[i], voc_to_line, ptr_to_voc, dim, size, vout,data );
				if (vout.size()>0) WriteToFile( qid + "." + query_vector[i], vout, path ); 
			}
		}
	}
	else if (command=="Centroid")
	{

		std::string path=argv[4];

		float* centroid = GetCentroid( dim, size, data, ptr_to_voc );

		ofstream of_query;

		of_query.open ( (path + "/centroid_query_sim" ).c_str() );

		ofstream of_terms;

		of_terms.open ( (path + "/centroid_terms_sim" ).c_str() );


		while  ( finput->good() )
		{
			string query;
			getline (*finput, query);
			// Here we need to call a function to calculate similarity between the entire query and the centroid
			//and output it to an appropriate file
			vector<std::string> query_vector;
			parseLine( query, query_vector );     
			if (query.empty()) { break; }
			std::string qid=query_vector[0];
			query_vector.erase(query_vector.begin(),query_vector.begin()+1);
			float simqcent = SimQueryCentroid( query_vector, voc_to_line, ptr_to_voc, 
				dim,  size, data, centroid);
			of_query << qid << " " << simqcent << std::endl;
			for (size_t i=0; i<query_vector.size();i++)
			{
				//
				vector<std::string> temp;
				temp.push_back(query_vector[i]);
				simqcent = SimQueryCentroid( query_vector, voc_to_line, ptr_to_voc, 
					dim,  size, data, centroid);
				of_terms << qid << " " << query_vector[i] << " " << simqcent << std::endl;
			}
		}
		delete[] centroid;
		of_query.close();
		of_terms.close();

	}
	else if (command=="KNN-RM")
	{

		std::string path=argv[4];
		// Input format: qid term RM-prob
		string qid, next, term;
		double prob(0.0);
		*finput >> next >> term >> prob;
		while  ( finput->good() )
		{
			//we read all the terms in for this query
			qid=next;
			map<string,double> rm_terms;

			while ((  next == qid )&& ( finput->good() ))
			{
				rm_terms.insert(make_pair<string,double> (term, prob));
				*finput >> next >> term >> prob; 
			}
			if ( ! ( finput->good() ))
				rm_terms.insert(make_pair<string,double> (term, prob));
			cerr << "loaded " << rm_terms.size() << " terms for query " << qid << std::endl;
			KNN_RM( rm_terms, voc_to_line, ptr_to_voc, dim, size, data, path + "/" + qid + ".clusters" );
		}

	}
	else if (command=="ClusterFeatures")
	{//model-file query-file ClusterFeatures path_to_clusters path_to_output" << std::endl; 

		std::string path_to_clusters=argv[4];
		std::string path_to_output=argv[5];
		float* centroid = GetCentroid( dim, size, data, ptr_to_voc );

		while  ( finput->good() )
		{
			string query;
			getline (*finput, query);

			vector<std::string> query_vector;
			parseLine( query, query_vector );     
			if (query.empty()) { break; }
			std::string qid=query_vector[0];
			query_vector.erase(query_vector.begin(),query_vector.begin()+1);
			std::map<size_t,std::vector<string> > clusters;
			LoadClusters( path_to_clusters + "/" + qid + ".clusters", clusters, 100); ///!!!!Attention
			//  for ( std::map<size_t,std::vector<string> >::const_iterator cit = clusters.begin(); cit != clusters.end(); cit++)
			//            {
			//                size_t id = cit->first;
			//                for (size_t i=0; i<cit->second.size(); i++)
			//                {
			//                    cout << id << " " << cit->second[i]  << std::endl;  
			//                }    
			//            }
			std::map<string, float*> rep_query;
			float* query_cent=GetClusterCentroid(query_vector, voc_to_line, ptr_to_voc, dim, size, data, rep_query );

			for (size_t ic=1; ic <= clusters.size(); ic++)
			{
				std::map<string, float*> rep_cluster;
				//assumption  - the data was normalized previously - in function that returns collection centroid
				float* clust_cent = GetClusterCentroid( clusters[ic], voc_to_line, ptr_to_voc, dim, size, data, rep_cluster );
				float avg_cosine_cluster_terms = GetAvgCosineSim(rep_cluster, clust_cent, dim,path_to_output, qid, ic);
				float clust_query_cent_sim = Cosine( query_cent, clust_cent, dim);  
				float clust_cent_corpus = Cosine ( clust_cent, centroid, dim);
				//  cerr << "similarities: "<< avg_cosine_cluster_terms << " " << clust_query_cent_sim << " " << clust_cent_corpus << std::endl;
				std::map<string,float> dist_to_qterm;         
				for (std::map<string, float*>::const_iterator cit2=rep_query.begin(); cit2 != rep_query.end(); cit2++)
				{
					dist_to_qterm.insert(std::make_pair<string,float>(cit2->first,Cosine(cit2->second,clust_cent,dim)));
				}
				PrintClusterFeatures( path_to_output, qid, ic,
					avg_cosine_cluster_terms, clust_query_cent_sim, clust_cent_corpus, dist_to_qterm);

				delete[] clust_cent;
			}
			delete[] query_cent;
		}
		delete[] centroid;
	}
	if (command=="Translation")
	{
		string vocFile = argv[4];
		std::set<string> rest_voc;
		LoadRestrictedVocabulary( vocFile, rest_voc);

		while  ( finput->good() )
		{
			string query;
			getline (*finput, query);

			vector<std::string> query_vector;
			parseLine( query, query_vector );     
			if (query.empty()) { break; }
			std::string qid = query_vector[0];
			query_vector.erase(query_vector.begin(),query_vector.begin()+1);

			for ( vector<std::string>::const_iterator cit = query_vector.begin(); cit != query_vector.end(); cit++ )
			{
				std::vector<std::pair<string,double> > terms;
				//       cerr << " before get norm prob" << normCorpus["school"] << std::endl;
				GetNormalizedProbs(*cit, voc_to_line, data, dim, size, terms, rest_voc);//, normCorpus );
				for (std::vector<std::pair<string,double> >::const_iterator cit2 = terms.begin();
					cit2 != terms.end(); cit2++ ){
						cout << qid << " " << *cit << " " <<  cit2->first << " " << cit2->second << std::endl;
				}
				//GetNormalizedProb(*cit, voc_to_line, data, dim, size );
				//cout << cos << " " << norm << " " << exp(cos)/norm << std::endl;
			}
		}
	}

	delete[] data;
	if (finput)
		finput->close();
	delete finput;
	system("PAUSE");
	return EXIT_SUCCESS;
}

*/