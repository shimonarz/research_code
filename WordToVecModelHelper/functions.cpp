/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

#include "functions.h"
#include "math.h"
#include <sstream>
#include <algorithm>
#include <string.h>
#include <set>
#include <iostream>
#include <iostream>
#include <string>
#include <stdlib.h>     /* atoi */

using std::ifstream;
using std::ofstream;
using std::map;
using std::vector;
using std::string;
using std::cout;
using std::cerr;

const int cnst_max_term_len=50;



void GetVectorsForLines( std::map<string, float*>& rep,
						long long dim,
						const std::map<std::string,float*>& voc_to_line,
						const vector<string>& terms,
						float* data)
{       
	// the positions saved are from the start
	for ( vector<string>::const_iterator cit = terms.begin(); 
		cit != terms.end(); cit++)
	{
		//  rewind(model_file);
		std::map<std::string,float*>::const_iterator citvoc = voc_to_line.find(*cit);
		if (citvoc == voc_to_line.end() )
		{
			std::cerr << "can't find term in vocabulary, skipping: " << *cit << std::endl;
			continue;
		}

		float* ptr = new float[dim];       
		memcpy( ptr, citvoc->second, sizeof(float)*dim);
		rep.insert( std::make_pair(*cit, ptr) );
	}                  
}

void parseLine( const std::string& line, vector<string>& terms )
{
	terms.clear();
	std::stringstream os(line);
	while (os.good())
	{
		string term;
		os >> term;
		if (term.empty()) break;
		terms.push_back(term);
	}
}

void NormVector(float* vec, long long dim)
{
	float l2(0.0);
	for (long long i=0; i < dim; i++)
	{
		l2+= (vec[i]*vec[i]);
	}    
	l2 = sqrt( l2 );
	for (long long i=0; i < dim; i++)
	{
		vec[i]=vec[i]/l2;
	} 

}

void NormVectors(std::map<string, float*>& rep, long long dim)
{
	for(std::map<string, float*>::const_iterator cit = rep.begin();
		cit != rep.end(); cit++)
	{
		NormVector( cit->second, dim);
	}
}

void GetSumRepresentation(std::map<string, float*>& rep,
						  long long dim,
						  float*& ptr)
{
	ptr = new float[dim];
	memset(ptr,0,dim*sizeof(float));

	for(std::map<string, float*>::const_iterator cit = rep.begin();
		cit != rep.end(); cit++)
	{
		for (long long i=0; i < dim; i++)
		{
			ptr[i]+=cit->second[i];
		}    
	}
}

void CalcAllCosine( long long dim,  long long size, float* ptr, 
				   vector<std::pair<float*,float> >& vcos, float* data ) 
{
	vcos.clear();
	float* ptr2=data;
	for (long long i=0; i< size; i++)
	{          
		vcos.push_back(std::make_pair<float*,float>(ptr2, Cosine( ptr, ptr2, dim )));
		ptr2=ptr2+dim;
	}
}

void CalcAllCosine( long long dim,  long long size, float* ptr, 
				   vector<float>& vcos, float* data ) 
{
	vcos.clear();
	float* ptr2=data;
	for (long long i=0; i< size; i++)
	{          
		vcos.push_back(Cosine( ptr, ptr2, dim ));
		ptr2=ptr2+dim;
	}
}


//double GetNormalizedProb(const std::string& term1, const std::string& term2,
//                     const std::map<std::string,float*>&  voc_to_line,
//                     float*  data, long long dim, long long size, double& cos )
//{
//    double norm(0.0);   
//    cos = CalcCosine(term1, term2, voc_to_line, data, dim);
//    //Now we want the normalizer for the second term
//    std::map<std::string,float*>::const_iterator cit = voc_to_line.find( term2 );
//    if (cit==voc_to_line.end())
//       return -1;
//     float* ptr2 = cit->second;
//     float* ptr1 = data;
//     for (long long i=0; i< size; i++)
//     {          
//          norm += exp(Cosine( ptr1, ptr2, dim ));
//          ptr1 = ptr1 + dim;
//     }    
//     return norm;
//}

double GetNormalizerOverCorpus( const string& term, const std::map<std::string,float*>&  voc_to_line,
							   float* data, long long dim, long long size )
{
	//Now we want the normalizer for the second term
	std::map<std::string,float*>::const_iterator cit = voc_to_line.find( term );
	if (cit==voc_to_line.end())
		return -1;
	std::map<std::string,float*>::const_iterator cit_avoid = voc_to_line.find( "</s>" );
	float* ptr2 = cit->second;
	float* ptr1 = data;
	double norm(0.0);
	for (long long i=0; i< size; i++)
	{          
		if (ptr1==cit_avoid->second) 
		{  
			ptr1 = ptr1 + dim;
			continue;
		}
		norm += exp(Cosine( ptr1, ptr2, dim ));
		ptr1 = ptr1 + dim;
	}    
	return norm;
}

void ReadNormTrans(map<std::string,double>& normCorpus, const std::string& transFile)
{
	ifstream in(transFile.c_str());
	while (in.good())
	{
		string term;
		double prob(0.0);
		in >> term >> prob;
		normCorpus.insert(std::make_pair<string,double>(term,prob));
	}
	in.close();
}
void LoadRestrictedVocabulary( const std::string& filename, std::set<std::string>& voc )
{
	ifstream in(filename.c_str(), ifstream::in );
	while ( in.good() )
	{
		string term;
		long long c1(0), c2(0);
		in >> term >> c1 >> c2;
		voc.insert(term);
	}

	in.close();
}


void GetNormalizedProbs(const std::string& qterm, 
						const std::map<std::string,float*>&  voc_to_line,
						float* data, long long dim, long long size, std::vector<std::pair<string,double> >& terms,
						const std::set<std::string>& voc)//,
						//         const map<std::string,double>& normCorpus)
{    
	if ( voc.find(qterm) == voc.end() )
	{
		terms.push_back(std::make_pair<string,double>(qterm,1.0));
		return;
	}
	std::map<std::string,float*>::const_iterator iter = voc_to_line.find( qterm );
	if (iter==voc_to_line.end())
		return;
	float* ptr_qterm = iter->second;
	for (std::map<std::string,float*>::const_iterator cit = voc_to_line.begin(); cit != voc_to_line.end(); cit++){
		if (cit->first=="</s>") continue;
		if ( voc.find(cit->first) == voc.end() )
			continue;
		double sim = Cosine( ptr_qterm, cit->second, dim );  //exp(Cosine( ptr_qterm, cit->second, dim ));
		//  double norm = normCorpus.at(cit->first);
		terms.push_back(std::make_pair<string,double>(cit->first, sim));///norm)); No normalization
	}
	std::sort(terms.begin(), terms.end(), smallerStrPair());
	terms.erase(terms.begin()+10, terms.end());
	//Normalize
	double sum(0.0);
	for ( std::vector<std::pair<string,double> >::iterator it = terms.begin(); it != terms.end(); it++ )
	{
		//    double norm = GetNormalizerOverCorpus( it->first, voc_to_line, data, dim, size );
		//    it->second = it->second/norm;
		sum += it->second;
	}
	for ( std::vector<std::pair<string,double> >::iterator it = terms.begin(); it != terms.end(); it++ )
	{
		it->second = it->second/sum;
	}
}

void KNN(  const vector<string>& terms,
		 const std::map<std::string,float*>& voc_to_line,
		 const std::map<float*, const std::string*>& ptr_to_voc,
		 long long dim, long long size, int k,
		 std::vector<std::pair<string,float> >& vout,
		 bool norm,
		 float* data )
{     
	std::map<string, float*> rep;

	GetVectorsForLines( rep, dim, voc_to_line, terms, data);
	if (norm)
		NormVectors(rep, dim);
	float* psum(NULL);
	GetSumRepresentation( rep, dim, psum);

	std::vector< std::pair<float*,float> > add_cosine;

	CalcAllCosine( dim, size, psum, add_cosine, data);
	std::sort(add_cosine.begin(),add_cosine.end(),smaller());
	if ( k < add_cosine.size() )
		add_cosine.erase(add_cosine.begin()+k+1,add_cosine.end());

	vout.clear();
	int i=1; //ptr_to_voc
	vector<std::pair<float*,float> >::const_iterator cit=add_cosine.begin();
	while ( (cit !=add_cosine.end()) && (i<=k)) 
	{ //insert here to vout
		string str = *ptr_to_voc.find(cit->first)->second;      
		if ( str != "</s>")
		{
			vout.push_back( std::make_pair<string,float>( str, cit->second ));
			i++;
		}
		cit++;
	}
	//release memory of rep
	CleanSubset(rep);
	delete[] psum;
}

void KNN_all_no_sort( const vector<string>& terms, 
					 const std::map<std::string,float*>& voc_to_line, 
					 const std::map<float*, const std::string*>& ptr_to_voc, 
					 long long dim,
					 long long size,
					 std::vector<float>& vout, 
					 bool norm, float* data )
{

	std::map<string, float*> rep;

	GetVectorsForLines( rep, dim, voc_to_line, terms, data);
	if (norm)
		NormVectors(rep, dim);
	float* psum(NULL);
	GetSumRepresentation( rep, dim, psum);
	vout.clear();  
	CalcAllCosine( dim, size, psum, vout, data);
	//release memory of rep
	CleanSubset(rep);
	delete[] psum;
}

float* GetCentroid( long long dim, long long size,
				   float* data,
				   const std::map<float*, const std::string* >& ptr_to_voc )
{

	float* centroid = new float[dim];
	memset(centroid,0,dim*sizeof(float));
	float* ptr=data;
	//need to iterate data
	for ( size_t i=0; i<size; i++)
	{
		std::map<float*, const std::string* >::const_iterator cit = ptr_to_voc.find( ptr);
		if ((cit!=ptr_to_voc.end())&&( (*cit->second)!="</s>"))
		{
			NormVector(ptr, dim );                    
			for ( size_t j=0; j<dim; j++)
			{
				centroid[j] += ptr[j];
			}
		}
		ptr = ptr+dim;
	}
	return centroid;      
}

float SimQueryCentroid( const std::vector<std::string>& terms,
					   const std::map<std::string,float*>& voc_to_line, 
					   const std::map<float*, const std::string* >& ptr_to_voc, 
					   long long dim, long long size,
					   float* norm_data, float* centroid)
{

	std::map<string, float*> rep;

	GetVectorsForLines( rep, dim, voc_to_line, terms, norm_data);

	float* psum(NULL);
	GetSumRepresentation( rep, dim, psum);

	return Cosine(psum,centroid,dim);


}
double CalcCosine(const std::string& term1, 
				  const std::string& term2,
				  const std::map<std::string,float*>& voc_to_line,
				  float* data,
				  long long dim)
{
	std::map<std::string,float*>::const_iterator cit = voc_to_line.find( term1 );
	if (cit==voc_to_line.end())
		return -1;
	float* ptr1 = cit->second;
	cit = voc_to_line.find( term2 );
	if (cit==voc_to_line.end())
		return -1;
	float* ptr2 = cit->second;  

	double cos = Cosine(ptr1,ptr2,dim);
	return cos;
}

void PrintVocabulary( FILE* model_file,
					 long long& dim, long long& size)
{
	fscanf(model_file, "%lld", &size);
	fscanf(model_file, "%lld", &dim);
	//cerr<< "size: " << size << " dim: " << dim  <<std::endl;       
	int max_w(cnst_max_term_len);
	char* tmp=new char[max_w];
	float* ptr=new float[dim];
	long line=0;
	while ((!feof( model_file ) )&&(line<size))
	{
		memset(tmp,0,max_w);
		int a = 0;
		while (1) {
			char c = fgetc(model_file);
			if (c == ' ') break;
			tmp[a] = c;
			if (feof(model_file)) break;
			if ((a < max_w) && (tmp[a] != '\n')) a++;
		}

		std::string term(tmp);
		if ((!(term.empty())) && (term!=" "))
		{
			std::cout << term << std::endl;
		}
		fread(ptr, sizeof(float), dim, model_file);
		line++;
	} 
	delete[] tmp;
	delete[] ptr;

}
void LoadVocabulary( FILE* model_file, 
					long long& dim, long long& size,
					std::map<std::string,float*>& voc_to_line,
					std::map<float*, const std::string* >& ptr_to_voc,
					float*& data )
{
	voc_to_line.clear();
	fscanf(model_file, "%lld", &size);
	fscanf(model_file, "%lld", &dim);

	data=new float[dim*size];
	float* ptr=data;

	int max_w(cnst_max_term_len);
	char* tmp=new char[max_w];
	long line=1;
	while (!feof( model_file ) )
	{
		memset(tmp,0,max_w);
		int a = 0;
		while (1) {
			char c = fgetc(model_file);
			if (c == ' ') break;
			tmp[a] = c;
			if (feof(model_file)) break;
			if ((a < max_w) && (tmp[a] != '\n')) a++;
		}

		std::string term(tmp);


		if ((!(term.empty())) && (term!=" "))
		{
			voc_to_line.insert( std::make_pair(term, ptr) ); 
			//std::make_pair<float*,const string* >(ptr, &(voc_to_line.find(term)->first));
			ptr_to_voc.insert( std::make_pair<float*,const string* >(ptr, &(voc_to_line.find(term)->first)) ); 
		}
		fread(ptr, sizeof(float), dim, model_file);
		ptr = ptr+dim;
		line++;
	}

	delete[] tmp;
}

float Cosine(float* pv1, float* pv2, long long dim)
{
	double ip(0.0),l1(0.0),l2(0.0);
	for (long long i=0; i<dim; i++)
	{       
		ip += pv1[i]*pv2[i];
		l1 += pv1[i]*pv1[i];
		l2 += pv2[i]*pv2[i];
	}
	return ip/(sqrt(l1)*sqrt(l2));
}

void InitSubset(  long long dim,
				std::map<string, float*>& rep,
				const std::map<std::string, float*>& voc_to_line,
				const vector<string>& terms,
				float* data )
{
	//assumption rep is newly allocated.. Not cleaning it
	for ( vector<string>::const_iterator cit = terms.begin(); 
		cit != terms.end(); cit++)
	{
		std::map<std::string,float*>::const_iterator citvoc = voc_to_line.find(*cit);
		if (citvoc == voc_to_line.end() )
			continue;
		float* ptr = new float[dim];  
		memcpy( ptr, citvoc->second, sizeof(float)*dim);
		rep.insert( std::make_pair(citvoc->first, ptr) );
	}       
}

void CleanSubset( std::map<string, float*>& rep )
{
	//release memory of rep
	for(std::map<string, float*>::iterator it = rep.begin();
		it != rep.end(); it++)
	{
		delete[] it->second;
	}

}

void CalcAllCosine( long long dim, 
				   float* ptr,
				   std::vector<std::pair<string,float> >& vcos,
				   const std::map<string,float*>& rep_subset)
{
	vcos.clear();

	for (std::map<string, float*>::const_iterator cit=rep_subset.begin();
		cit!= rep_subset.end(); cit++ )
	{ 
		vcos.push_back(std::make_pair<string,float>(cit->first, Cosine( ptr, cit->second, dim )));
	}
}


void ScoreSubset( const vector<string>& terms,
				 const std::map<std::string,float*>& voc_to_line,
				 long long dim, std::vector<std::pair<string,float> >& vout,
				 bool norm,
				 const std::vector<std::string>& subset,
				 float* data)
{ 
	//this for the query terms
	std::map<string, float*> rep;
	GetVectorsForLines( rep, dim, voc_to_line, terms, data );
	std::map<string, float*> rep_subset;
	std::map<long long, string> line_to_term;
	GetVectorsForLines( rep_subset, dim, voc_to_line, subset, data );
	//  LineToTerm( voc_to_line, subset, line_to_term);   //commented to compile   
	if (norm)
		NormVectors(rep, dim);
	float* psum(NULL);
	GetSumRepresentation( rep, dim, psum);
	vout.clear();
	CalcAllCosine( dim, psum, vout, rep_subset);
	std::sort(vout.begin(), vout.end(), smallerStrPair());

	CleanSubset (rep);
	CleanSubset (rep_subset);
	//release memory of sum
	delete[] psum;
}


void KNN_term( const vector<string>& terms, 
			  const std::map<std::string,float*>& voc_to_line, 
			  const std::map<float*, const std::string*>& ptr_to_voc,
			  long long dim,
			  long long size,
			  const std::string& qid, 
			  int k,
			  float* data)
{
	//   this for the query terms
	std::map<string, float*> rep;  
	GetVectorsForLines( rep, dim, voc_to_line, terms, data);

	for ( std::map<string, float*>::const_iterator rep_it=rep.begin();
		rep_it != rep.end(); rep_it++)
	{
		vector<std::pair<float*,float> > vcos;

		CalcAllCosine( dim, size, rep_it->second, vcos, data);
		std::sort(vcos.begin(), vcos.end(), smaller());
		vcos.erase(vcos.begin()+k+1, vcos.end());
		int i=0;
		vector<std::pair<float*,float> >::const_iterator cit=vcos.begin();
		while ( (cit !=vcos.end()) && (i<k)) { 
			string str = *ptr_to_voc.find(cit->first)->second;      
			if ( str != "</s>")
			{
				cout << qid + " " +  rep_it->first + " " << str << " " << cit->second << " " << i << std::endl; 
				i++;
			}         
			cit++;
		}   
	}
	CleanSubset (rep);
}

void KNN_term_no_sort(const std::string& term, 
					  const std::map<std::string,float*>& voc_to_line, 
					  const std::map<float*, const std::string*>& ptr_to_voc, 
					  long long dim,
					  long long size,
					  std::vector<float>& vout, 
					  float* data )
{
	//first of all we check whether the term is in vocabulary:
	if (voc_to_line.find(term)==voc_to_line.end())
	{
		std::cerr<< "Term: " + term + " out of vocabulary" << std::endl;
		return;
	}

	std::map<string, float*> rep;
	std::vector<string> terms;
	terms.push_back(term);
	GetVectorsForLines( rep, dim, voc_to_line, terms, data);

	vout.clear();  
	CalcAllCosine( dim, size, rep[term], vout, data);
	//release memory of rep
	CleanSubset(rep);
}

void PrintDistancesToFile( const std::string& path,
						  const vector<std::pair<string,float> >& distances,
						  size_t cluster_id,
						  const map<string,double>& rm_terms )
{
	ofstream of;

	of.open ( path.c_str(), std::ofstream::out | std::ofstream::app ); //append mode

	for ( vector<std::pair<string,float> >::const_iterator cit=distances.begin(); 
		cit!= distances.end(); 
		cit++)
	{
		of << cluster_id << " " << cit->first << " " << cit->second << " " << rm_terms.find(cit->first)->second << std::endl;
	}

	of.close();            
}

void LoadClusters( const std::string& path, std::map<size_t,std::vector<string> >& clusters, size_t size)
{
	ifstream in;
	in.open( path.c_str() );
	clusters.clear();
	size_t cluster_id_curr(1);
	size_t cluster_id(0);
	string term;
	double cos(0.0), rm(0.0);
	in >> cluster_id >> term; //Attention!!!>> cos >> rm;
	while (in.good())
	{
		clusters.insert( std::make_pair<size_t,std::vector<string> >( cluster_id_curr, vector<string>() ));   

		for (size_t i=0; i<size; i++)
		{           
			if (cluster_id!=cluster_id_curr) break;
			if (! in.good()) break;
			clusters[cluster_id].push_back(term);
			in >> cluster_id >> term; //Attention!!! >> cos >> rm;  
		}
		while ( (cluster_id==cluster_id_curr)&&(in.good()))
		{
			in >> cluster_id >> term;  //Attention!!! >> cos >> rm;   
		}
		cluster_id_curr=cluster_id;
	}
	in.close();
}

float GetAvgCosineSim( const std::map<string, float*>& rep, float* cent, long long dim, 
					  const std::string& path_to_output, const string& qid, size_t clust_id)
{
	float cosine(0.0);      
	size_t count(0);
	ofstream of;
	of.open ( (path_to_output + "/sim_cluster_cent" ).c_str(), std::ofstream::out | std::ofstream::app );

	for ( std::map<string, float*>::const_iterator cit = rep.begin(); cit != rep.end(); cit++)
	{
		float ptcos = Cosine(cit->second, cent, dim); 
		cosine += ptcos;
		of << qid << " " << clust_id << " " << cit->first << " " << ptcos << std::endl;
		count++;
	}
	of.close();
	return cosine/count;

}


float* GetClusterCentroid( const std::vector<std::string>& cluster,
						  const std::map<std::string,float*>& voc_to_line, 
						  const std::map<float*, const std::string* >& ptr_to_voc, 
						  long long dim, long long size,
						  float* norm_data, //the data is normalized!!!!
						  std::map<string, float*>& rep ) 
{
	GetVectorsForLines( rep, dim, voc_to_line, cluster, norm_data);
	float* ptr= new float[dim];
	GetSumRepresentation( rep, dim, ptr );
	return ptr;

}

void KNN_RM( const map<string,double>& rm_terms,
			const std::map<std::string,float*>& voc_to_line, 
			const std::map<float*, const std::string* >& ptr_to_voc, 
			long long dim, long long size,
			float* data, const std::string& path_to_file )
{
	std::map<string, float*> rep;
	std::vector<string> terms;
	for ( map<string,double>::const_iterator cit = rm_terms.begin();
		cit != rm_terms.end(); cit++ )
	{
		terms.push_back(cit->first);
	}
	size_t cluster_id(1);
	GetVectorsForLines( rep, dim, voc_to_line, terms, data);
	for ( std::map<string, float*>::const_iterator cit1=rep.begin();
		cit1 != rep.end(); cit1++ )
	{
		float* focal = cit1->second;
		vector<std::pair<string,float> > distances;
		for ( std::map<string, float*>::const_iterator cit2=rep.begin();
			cit2 != rep.end(); cit2++ )
		{
			float cos = Cosine( focal, cit2->second, dim);
			distances.push_back( std::make_pair<string,float>(cit2->first, cos) );
		}
		std::sort(distances.begin(), distances.end(), smallerStrPair());
		PrintDistancesToFile( path_to_file, distances, cluster_id, rm_terms );
		cluster_id++;
	}   
}






///////////////////////////// shimon adding  - Start///////////////////////////////////////////


// Built extended query file from knn clusters

//void knn_rm1_Terms( Rm1Terms *rm1Terms ,
//				   const std::map<std::string,float*>& voc_to_line, 
//				   const std::map<float*, const std::string* >& ptr_to_voc, 
//				   long long dim, 
//				   long long size,
//				   float* data, 
//				   const std::string& path_to_file ,
//				   int k)
//{
//	ofstream of;
//
//	of.open ( path_to_file.c_str(), std::ofstream::out ); //append mode
//	of << "<parameters>\n";
//
//	std::map<std::string,double> rm_terms;
//
//	std::vector<Term*> &TermFromRm1 = rm1Terms->TermFromRm1;
//	for (int i = 0 ; i <  TermFromRm1.size() ; ++i)
//	{ 
//		Term *term = TermFromRm1[i];
//		std::string termValue = term->value;
//		double weight = term->weight;
//		rm_terms.insert(std::make_pair<string,double> (termValue, weight));
//	}
//	double **distancesMatrix = createDistancesMatrix(rm_terms,voc_to_line,dim,data);
//
//
//	Rm1Terms expansion;
//
//	expansion.QueryId = rm1Terms->QueryId;
//	expansion.OriginalQuery = rm1Terms->OriginalQuery;
//	expansion.OriginalQueryWeight = rm1Terms->OriginalQueryWeight;
//	expansion.TermsWeight = rm1Terms->TermsWeight;
//
//
//	for( int i = 0 ; i< TermFromRm1.size() ; ++i)
//	{
//		expansion.TermFromRm1.clear();
//		vector<std::pair<int,double> > vcos;
//
//		for( int j = 0 ; j< TermFromRm1.size() ; ++j)
//		{
//			vcos.push_back(std::make_pair<int,double>(j, distancesMatrix[i][j] ));
//		}
//
//		std::sort(vcos.begin(), vcos.end(), [](std::pair<int,double> &left, std::pair<int,double> &right) 
//		{
//			return left.second > right.second;
//		});
//
//		// if the term is not in the word2Vec vocabulary, then all array is zeros, therefore after sort the highest value be zero.
//		if( vcos[0].second == 0)
//		{
//			continue;
//		}
//
//		double sum = 0;
//		vcos.erase(vcos.begin()+k+1, vcos.end());
//		for ( int i1=0; i1<k+1 ; ++i1 )
//		{
//			int index = vcos[i1].first;
//			Term* t =  rm1Terms->TermFromRm1[index];
//			sum += t->weight;
//			expansion.TermFromRm1.push_back( t );
//		}
//
//		for ( int i1=0; i1<k+1 ; ++i1 )
//		{
//			Term* t = expansion.TermFromRm1[i1];
//			t->weight = t->weight / sum;
//		}
//
//		std::string query = buildQuery(&expansion);
//
//		of << "<query>\n";
//		of << "<number>" + rm1Terms->QueryId  +"_"  + TermFromRm1[i]->value + "</number>\n";
//		of << "<text>" + query + "</text>\n";
//		of << "</query>\n";
//	}
//
//	delete []distancesMatrix;
//	//delete distancesMatrix;
//
//	of << "</parameters>";
//	of.close();  
//}

std::vector<ExpendedQuery*>* getListOfExpendedQuerys( std::vector<Rm1Terms*> *rm ,
													 const std::map<std::string,float*>& voc_to_line, 
													 const std::map<float*, const std::string* >& ptr_to_voc, 
													 long long dim, 
													 long long size,
													 float* data, 
													 int k)
{
	std::map<std::string,double> rm_terms;
	std::vector<ExpendedQuery*> *expendedQueryList = new std::vector<ExpendedQuery*>();

	for (int rm1TermsIndex = 0 ; rm1TermsIndex < rm->size() ; ++rm1TermsIndex)
	{  
		Rm1Terms *rm1Terms = (*rm)[rm1TermsIndex];
		std::vector<Term*> &TermFromRm1 = rm1Terms->TermFromRm1;
		ExpendedQuery* expendedQuery = new ExpendedQuery();
		expendedQuery->QueryId = rm1Terms->QueryId;
		expendedQuery->OriginalQuery = "#combine( "  + convertTermToString(rm1Terms->OriginalQuery) + ")";

		for (int i = 0 ; i <  TermFromRm1.size() ; ++i)
		{ 
			Term *term = TermFromRm1[i];
			std::string termValue = term->value;
			double weight = term->weight;
			rm_terms.insert(std::make_pair<string,double> (termValue, weight));
		}
		double **distancesMatrix = createDistancesMatrix(rm_terms,voc_to_line,dim,data);

		Rm1Terms expansion;

		expansion.QueryId = rm1Terms->QueryId;
		expansion.OriginalQuery = rm1Terms->OriginalQuery;
		expansion.OriginalQueryWeight = rm1Terms->OriginalQueryWeight;
		expansion.TermsWeight = rm1Terms->TermsWeight;

		for( int i = 0 ; i< TermFromRm1.size() ; ++i)
		{
			expansion.TermFromRm1.clear();
			vector<std::pair<int,double> > vcos;

			for( int j = 0 ; j< TermFromRm1.size() ; ++j)
			{
				vcos.push_back(std::make_pair<int,double>(j, distancesMatrix[i][j] ));
			}
			std::sort(vcos.begin(), vcos.end(), sort_pred);

			/*std::sort(vcos.begin(), vcos.end(), 
				[](std::pair<int,double> &left, std::pair<int,double> &right) -> bool{
						return left.second > right.second;	});*/

			// if the term is not in the word2Vec vocabulary, then all array is zeros, therefore after sort the highest value be zero.
			if( vcos[0].second == 0)
			{
				continue;
			}

			double sum = 0;
			vcos.erase(vcos.begin()+k+1, vcos.end());
			for ( int i1=0; i1<k+1 ; ++i1 )
			{
				int index = vcos[i1].first;
				Term* t =  rm1Terms->TermFromRm1[index];
				sum += t->weight;
				expansion.TermFromRm1.push_back( t );
			}

			for ( int i1=0; i1<k+1 ; ++i1 )
			{
				Term* t = expansion.TermFromRm1[i1];
				t->weight = t->weight / sum;
			}

			std::string query = buildQuery(&expansion);

			expendedQuery->Querys.push_back(query);
		}
		expendedQueryList->push_back(expendedQuery);

		delete []distancesMatrix;
	}
	return  expendedQueryList;
}

void createQueryFile(std::vector<ExpendedQuery*> *expendedQueryList,int numberOfClusters,std::string  path)
{
	for (int i = 0 ; i < numberOfClusters ; ++i)
	{
		char ch[100];
		sprintf(ch , "%d" ,i );
		std::string  outputFile = path + "/" +  ch + ".clusters";

		ofstream of;
		of.open ( outputFile.c_str(), std::ofstream::out ); 
		of << "<parameters>\n";

		for (int index = 0 ; index < expendedQueryList->size() ; ++index)
		{ 
			ExpendedQuery *expendedQuery = (*expendedQueryList)[index];

			of << "<query>\n";
			of << "<number>" + expendedQuery->QueryId + "</number>\n";
			of << "<text>" + expendedQuery->Querys[i] + "</text>\n";
			of << "</query>\n";

		}

		of << "</parameters>";
		of.close();

	}


}


std::string buildQuery(Rm1Terms *terms)
{
	std::string result;

	char originalQueryWeightStr[40];
	sprintf(originalQueryWeightStr,"%.32f",terms->OriginalQueryWeight);
	std::ostringstream originalQueryWeight;
	originalQueryWeight << originalQueryWeightStr;

	char termsWeightStr[40];
	sprintf(termsWeightStr,"%.32f",terms->TermsWeight);
	std::ostringstream termsWeight;
	termsWeight << termsWeightStr;

	result = "#weight( " + 
		originalQueryWeight.str() + 
		" #combine( #combine( "  + 
		convertTermToString(terms->OriginalQuery) + ") ) " + 
		termsWeight.str() + 
		" #weight(  " + 
		convertTermToString(terms->TermFromRm1,true,true) + " ) )";

	return result;
}


std::string buildNewRm3ClustresQuery2(ClusterRM3ExpansionTerms *terms)
{
	std::string result;

	char originalQueryWeightStr[40];
	sprintf(originalQueryWeightStr,"%.32f",terms->OriginalQueryWeight);
	std::ostringstream originalQueryWeight;
	originalQueryWeight << originalQueryWeightStr;


	char clusterTermsWeightStr[40];
	sprintf(clusterTermsWeightStr,"%.32f",terms->ClusterTermsWeight);
	std::ostringstream clusterTermsWeight;
	clusterTermsWeight << clusterTermsWeightStr;

	char rm1TermsWeightStr[40];
	sprintf(rm1TermsWeightStr,"%.32f",terms->Rm1TermsWeight);
	std::ostringstream rm1TermsWeight;
	rm1TermsWeight << rm1TermsWeightStr;



	result = "#weight( " + 
		originalQueryWeight.str() + " #combine( #combine( "  + 	convertTermToString(terms->OriginalQuery) + ") ) " + 
		clusterTermsWeight.str()  + " #weight(  " + convertTermToString(terms->ClusterTerms,true,true) + " ) " + 
		rm1TermsWeight.str()      + " #weight(  " + convertTermToString(terms->Rm1Terms,true,true) + " ) " + 
		" )";

	return result;
}

std::string buildNewRm3ClustresQuery(ClusterRM3ExpansionTerms *terms)
{
	std::string result;

	double originalQueryWeight = terms->OriginalQueryWeight;
	double expansionQueryWeight = terms->ClusterTermsWeight + terms->Rm1TermsWeight;

	char originalQueryWeightStr[40];
	sprintf(originalQueryWeightStr,"%.32f",originalQueryWeight);
	std::ostringstream originalQueryWeightStream;
	originalQueryWeightStream << originalQueryWeightStr;


	char expansionQueryWeightStr[40];
	sprintf(expansionQueryWeightStr,"%.32f",expansionQueryWeight);
	std::ostringstream expansionQueryWeightStream;
	expansionQueryWeightStream << expansionQueryWeightStr;

	if(terms->ClusterTermsWeight == 0 && terms->Rm1TermsWeight == 0)
	{
		// no expansion 
		result = "#combine( "  + 	convertTermToString(terms->OriginalQuery) + " )" ;
	}
	else if(terms->ClusterTermsWeight == 0 && terms->Rm1TermsWeight != 0)
	{
		// only Rm1 expansion
		result = "#weight( " + 
			originalQueryWeightStream.str() + " #combine( #combine( "  + 	convertTermToString(terms->OriginalQuery) + ") ) " + 
			expansionQueryWeightStream.str()  + " #weight(  " + convertTermToString(terms->Rm1Terms,true,true) + " ) " + " )";
	}
	else if( terms->ClusterTermsWeight != 0 && terms->Rm1TermsWeight == 0)
	{
		// only Cluster expansion
		result = "#weight( " + 
			originalQueryWeightStream.str() + " #combine( #combine( "  + 	convertTermToString(terms->OriginalQuery) + ") ) " + 
			expansionQueryWeightStream.str()  + " #weight(  " + convertTermToString(terms->ClusterTerms,true,true) + " ) " + " )";

	}
	else
	{
		// Rm1 and Cluster expansion
		std::vector<Term*> margeTerms;
		// rm1 values
		int length = terms->Rm1Terms.size();
		for (int i = 0; i < length; i++)
		{
			Term* t = terms->Rm1Terms[i];
			Term* newTerm = t->Copy();
			newTerm->weight = ( terms->Rm1TermsWeight / expansionQueryWeight) * t->weight;
			margeTerms.push_back(newTerm);
		}

		length = terms->ClusterTerms.size();
		for (int i = 0; i < length; i++)
		{
			Term* t = terms->ClusterTerms[i];
			Term* foundT = NULL;
			std::vector<Term*>::iterator it =  margeTerms.begin();
			for (it =  margeTerms.begin() ; it != margeTerms.end(); it++)
			{
				if((*it)->value == t->value)
				{
					foundT = *it;
					break;
				}
			}

			if(foundT != NULL)
			{
				foundT->weight += ( terms->ClusterTermsWeight / expansionQueryWeight) * t->weight;
			}
			else
			{
				t->weight = ( terms->ClusterTermsWeight / expansionQueryWeight) * t->weight;
				margeTerms.push_back(t);
			}

		}

		result = "#weight( " + 
			originalQueryWeightStream.str() + " #combine( #combine( "  + 	convertTermToString(terms->OriginalQuery) + ") ) " + 
			expansionQueryWeightStream.str()  + " #weight(  " + convertTermToString(margeTerms,true,true) + " ) " + " )";
	}



	return result;
}


std::string convertTermToString(std::vector<Term*> terms , bool withWeight , bool withQuotationMark )
{
	std::string result;
	char str[40];
	for(int i=0 ; i<terms.size() ; ++i)
	{
		Term *t = terms[i];
		if( withWeight )
		{
			sprintf(str,"%.32f",t->weight);
			std::ostringstream wight;
			wight << str;
			result += wight.str() + " ";
		}
		if(withQuotationMark)
		{
			result += "\"" +  t->value + "\"" + " ";
		}
		else
		{
			result += t->value + " ";
		}

	}
	return result;
}

Rm1Terms* parse_query( const std::string& query )
{

	std::vector<std::string> qset;
	qset.clear();
	std::string qcopy( query);
	for ( size_t i=0; i<qcopy.size(); i++ )
	{
		if ( (qcopy[i]=='(')||(qcopy[i]==')')||(qcopy[i]==',')||(qcopy[i]=='"')||(qcopy[i]==':')) 
		{
			qcopy[i]=' ';
		}
	}
	std::istringstream oss (qcopy);
	std::string temp;

	bool isWeight = false;
	bool isOriginalQuery = true;
	//oss << qcopy;
	while (oss.good())
	{
		std::string temp;
		oss >> temp;
		if ( temp.empty()) continue;    
		qset.push_back( temp );
	}


	Rm1Terms*  rm1Terms= new Rm1Terms();
	std::string::size_type sz;

	rm1Terms->QueryId = qset[0];
	rm1Terms->OriginalQueryWeight = atof (qset[2].c_str()); 

	int j = 5;

	while(qset[j + 1].compare("#weight") != 0)
	{
		Term* term = new Term();
		term->value = qset[j];
		term->vec = NULL;
		term->weight = 0;
		rm1Terms->OriginalQuery.push_back(term);
		j++;
	}

	rm1Terms->TermsWeight = atof (qset[j].c_str());
	j+=2;

	while(j < qset.size() )
	{
		Term* term = new Term();
		term->value = qset[j+1];
		term->vec = NULL;
		term->weight = atof (qset[j].c_str());  
		rm1Terms->TermFromRm1.push_back(term);
		j+=2;
	}
	return rm1Terms;

}

std::vector<Rm1Terms*>* readExpensQueryFile( std::string fileName)
{
	ifstream finput;
	finput.open ( fileName.c_str() ); 
	
	if (!finput.good())
	{
		std::cerr << "Can't open file: " << fileName << std::endl;
		return NULL;
	}

	std::vector<Rm1Terms*>* rm1Terms = new std::vector<Rm1Terms*>();

	while  ( finput.good() )
	{
		string line;
		getline (finput, line);
		if(line.size() == 0 ) continue;
		rm1Terms->push_back(parse_query(line) );		
	}

	finput.close();
	return rm1Terms;
}

double ** createDistancesMatrix(const std::map<std::string,double>& rm_terms,
								const std::map<std::string,float*>& voc_to_line, 
								long long dim, 
								float* data,
								double **matrix)
{
	std::map<string, float*> rep;
	std::vector<string> terms;
	for ( map<string,double>::const_iterator cit = rm_terms.begin();
		cit != rm_terms.end(); cit++ )
	{
		terms.push_back(cit->first);
	}
	size_t cluster_id(1);
	GetVectorsForLines( rep, dim, voc_to_line, terms, data);

	double **distancesMatrix;
	if(matrix == NULL)
	{
		distancesMatrix = new double*[terms.size()];
		for (int i = 0; i < terms.size(); ++i)
			distancesMatrix[i] = new double[terms.size()];
	}
	else
	{
		distancesMatrix = matrix;
	}


	for( int i = 0 ; i< terms.size() ; ++i)
	{
		for( int j = 0 ; j< terms.size() ; ++j)
		{
			float cos = 0;
			// not found
			if( (rep.find(terms[i]) == rep.end()) || (rep.find(terms[j]) == rep.end()) )
			{
				cos = 0;
			} 
			else 
			{
				float * t1Array = rep.find(terms[i])->second;
				float * t2Array = rep.find(terms[j])->second;
				cos = Cosine( t1Array, t2Array, dim);
			}


			distancesMatrix[i][j] = cos;
			distancesMatrix[j][i] = cos;
		}
	}

	return distancesMatrix;
}

//std::vector<ExpendedQuery*>* getListOfExpendedQuerys2( std::vector<Rm1Terms*> *rm ,
//													  const std::map<std::string,float*>& voc_to_line, 
//													  const std::map<float*, const std::string* >& ptr_to_voc, 
//													  long long dim, 
//													  long long size,
//													  float* data, 
//													  int k)
//{
//	std::map<std::string,double> rm_terms;
//	std::vector<ExpendedQuery*> *expendedQueryList = new std::vector<ExpendedQuery*>();
//
//	for (int rm1TermsIndex = 0 ; rm1TermsIndex < rm->size() ; ++rm1TermsIndex)
//	{  
//		Rm1Terms *rm1Terms = (*rm)[rm1TermsIndex];
//		std::vector<Term*> &TermFromRm1 = rm1Terms->TermFromRm1;
//		ExpendedQuery* expendedQuery = new ExpendedQuery();
//		expendedQuery->QueryId = rm1Terms->QueryId;
//		expendedQuery->OriginalQuery = "#combine( "  + convertTermToString(rm1Terms->OriginalQuery) + ")";
//		Rm1Terms expansion;
//		expansion.QueryId = rm1Terms->QueryId;
//		expansion.OriginalQuery = rm1Terms->OriginalQuery;
//		expansion.OriginalQueryWeight = rm1Terms->OriginalQueryWeight;
//		expansion.TermsWeight = rm1Terms->TermsWeight;
//		double sum = 0;
//		for ( int i1=0; i1<k+1 ; ++i1 )
//		{
//			Term* t =  rm1Terms->TermFromRm1[i1];
//			sum += t->weight;
//			expansion.TermFromRm1.push_back( t );
//		}
//		std::string query = buildQuery(&expansion);
//		expendedQuery->Querys.push_back(query);
//		expendedQueryList->push_back(expendedQuery);
//	}
//	return  expendedQueryList;
//}





QrelsFile* readQrelsFile(std::string fileName)
{
	ifstream finput;
	finput.open ( fileName.c_str() ); 
	
	if (!finput.good())
	{
		std::cerr << "Can't open file: " << fileName << std::endl;
		return NULL;
	}

	QrelsFile* qrelsFile = new QrelsFile();
	while  ( finput.good() )
	{
		string line;
		getline (finput, line);
		if(line.size() == 0 ) continue;
		qrelsFile->Lines.push_back(parse_Qrels_line(line) );
	}

	finput.close();
	return qrelsFile;
}

QrelsLine* parse_Qrels_line(std::string qrelsLine)
{
	QrelsLine* lineData = new QrelsLine();
	std::string qcopy( qrelsLine);
	std::istringstream oss (qcopy);
	std::string temp;

	if ( ! oss.good() ) {
		delete lineData;
		return NULL; 
	}

	std::string queryId;
	oss >> queryId;
	if ( queryId.empty()) {
		delete lineData;
		return NULL;    
	}

	std::string zero;
	oss >> zero;
	if ( zero.empty()) {
		delete lineData;
		return NULL;    
	}

	std::string docId;
	oss >> docId;
	if ( docId.empty()) {
		delete lineData;
		return NULL;    
	}

	std::string docScore;
	oss >> docScore;
	if ( docScore.empty()) {
		delete lineData;
		return NULL;    
	}

	lineData->QueryId = queryId;
	lineData->Zero = zero;
	lineData->DocId = docId;
	lineData->Score = docScore;

	return lineData;
}

bool sort_pred(const std::pair<int,double>& left, const std::pair<int,double>& right)
    {
    return left.second > right.second;
    }

void knn_rm1_Terms( Rm1Terms *rm1Terms ,
				   const std::map<std::string,float*>& voc_to_line, 
				   const std::map<float*, const std::string* >& ptr_to_voc, 
				   long long dim, 
				   long long size,
				   float* data, 
				   const std::string& path_to_file ,
				   int k,
				   KnnRm1TermsProbabilityType type,
				   std::vector<QrelsLine*>* vec)
{
	ofstream of;
	ofstream qrels;

	std::string qrelsPath = path_to_file + ".qrels";
	qrels.open ( qrelsPath.c_str() ); 

	of.open ( path_to_file.c_str(), std::ofstream::out ); 
	of << "<parameters>\n";

	std::map<std::string,double> rm_terms;

	std::vector<Term*> &TermFromRm1 = rm1Terms->TermFromRm1;
	for (int i = 0 ; i <  TermFromRm1.size() ; ++i)
	{ 
		Term *term = TermFromRm1[i];
		std::string termValue = term->value;
		double weight = term->weight;
		rm_terms.insert(std::make_pair<string,double> (termValue, weight));
	}
	double **distancesMatrix = createDistancesMatrix(rm_terms,voc_to_line,dim,data);


	Rm1Terms expansion;

	expansion.QueryId = rm1Terms->QueryId;
	expansion.OriginalQuery = rm1Terms->OriginalQuery;
	expansion.OriginalQueryWeight = rm1Terms->OriginalQueryWeight;
	expansion.TermsWeight = rm1Terms->TermsWeight;

	#pragma region for original query
	std::string queryNumber = rm1Terms->QueryId ;
	of << "<query>\n";
	of << "<number>" + queryNumber + "</number>\n";
	of << "<text> #combine( "  + convertTermToString(rm1Terms->OriginalQuery) + ") </text>\n";
	of << "</query>\n";
	for(int i2 = 0 ; i2 <vec->size() ; ++i2)
	{
		qrels << queryNumber + " " + (*vec)[i2]->Zero + " " << (*vec)[i2]->DocId + " " << (*vec)[i2]->Score +  "\n";
	}

	// for rm3  query  10 terms
	double sum = 0;
	unsigned int len = (unsigned int)(rm1Terms->TermFromRm1).size();
	for ( int i1=0; i1<10 && i1<len ; ++i1 )
	{
		
		Term* t =  rm1Terms->TermFromRm1[i1];
		sum += t->weight;
		expansion.TermFromRm1.push_back( t );
	}
	for ( int i1=0; i1<10 && i1<len ; ++i1 )
	{
		Term* t = expansion.TermFromRm1[i1];
		t->weight = t->weight / sum;
	}
	std::string query = buildQuery(&expansion);
	queryNumber = rm1Terms->QueryId  +"_rm3_10"  ;
	of << "<query>\n";
	of << "<number>" + queryNumber + "</number>\n";
	of << "<text>" + query + "</text>\n";
	of << "</query>\n";
	for(int i2 = 0 ; i2 <vec->size() ; ++i2)
	{
		qrels << queryNumber + " " + (*vec)[i2]->Zero + " " << (*vec)[i2]->DocId + " " << (*vec)[i2]->Score +  "\n";
	}
	expansion.TermFromRm1.clear();
	#pragma endregion 

	#pragma region for rm3 query  100 terms
	sum = 0;
	for ( int i1=0; i1<100 && i1<len; ++i1 )
	{
		Term* t =  rm1Terms->TermFromRm1[i1];
		sum += t->weight;
		expansion.TermFromRm1.push_back( t );
	}
	for ( int i1=0; i1<100 && i1<len; ++i1 )
	{
		Term* t = expansion.TermFromRm1[i1];
		t->weight = t->weight / sum;
	}
	query = buildQuery(&expansion);
	queryNumber = rm1Terms->QueryId  +"_rm3_100"  ;
	of << "<query>\n";
	of << "<number>" + queryNumber + "</number>\n";
	of << "<text>" + query + "</text>\n";
	of << "</query>\n";
	for(int i2 = 0 ; i2 <vec->size() ; ++i2)
	{
		qrels << queryNumber + " " + (*vec)[i2]->Zero + " " << (*vec)[i2]->DocId + " " << (*vec)[i2]->Score +  "\n";
	}

	#pragma endregion 


	// for all clusters
	for( int i = 0 ; i< TermFromRm1.size() ; ++i)
	{
		expansion.TermFromRm1.clear();
		vector<std::pair<int,double> > vcos;

		for( int j = 0 ; j< TermFromRm1.size() ; ++j)
		{
			vcos.push_back(std::make_pair<int,double>(j, distancesMatrix[i][j] ));
		}

		std::sort(vcos.begin(), vcos.end(), sort_pred);

		// if the term is not in the word2Vec vocabulary, then all array is zeros, therefore after sort the highest value be zero.
		if( vcos[0].second == 0)
		{
			continue;
		}

		double sum = 0;
		vcos.erase(vcos.begin()+k+1, vcos.end());
		for ( int i1=0; i1<k+1 ; ++i1 )
		{
			int index = vcos[i1].first;
			Term* t =  rm1Terms->TermFromRm1[index];
			sum += t->weight;
			expansion.TermFromRm1.push_back( t );
		}

		for ( int i1=0; i1<k+1 ; ++i1 )
		{
			Term* t = expansion.TermFromRm1[i1];
			
			if(type == Uniform)
			{
				double c = expansion.TermFromRm1.size();
				t->weight = 1/c;
			}
			else if(type == RelativeToRm1)
			{
				t->weight = t->weight / sum;
			}
	
			
		}

		std::string query = buildQuery(&expansion);

		queryNumber = rm1Terms->QueryId  +"_"  + TermFromRm1[i]->value;

		of << "<query>\n";
		of << "<number>" + queryNumber + "</number>\n";
		of << "<text>" + query + "</text>\n";
		of << "</query>\n";

		for(int i2 = 0 ; i2 <vec->size() ; ++i2)
		{
			qrels << queryNumber + " " + (*vec)[i2]->Zero + " " << (*vec)[i2]->DocId + " " << (*vec)[i2]->Score +  "\n";
		}

	}

	delete []distancesMatrix;
	//delete distancesMatrix;

	of << "</parameters>";
	of.close();  
}








void new_rm3_modle_using_knn_rm1_Terms( Rm1Terms *rm1Terms ,
				   const std::map<std::string,float*>& voc_to_line, 
				   const std::map<float*, const std::string* >& ptr_to_voc, 
				   long long dim, 
				   long long size,
				   float* data, 
				   const std::string& path_to_file ,
				   int k,
				   KnnRm1TermsProbabilityType type,
				   double originalQueryWeight,
				   double clusterTermsWeight,
				   std::vector<QrelsLine*>* vec)
{
	ofstream of;
	ofstream qrels;

	std::string qrelsPath = path_to_file + ".qrels";
	qrels.open ( qrelsPath.c_str() ); 

	of.open ( path_to_file.c_str(), std::ofstream::out ); 
	of << "<parameters>\n";

	std::map<std::string,double> rm_terms;

	std::vector<Term*> &TermFromRm1 = rm1Terms->TermFromRm1;
	for (int i = 0 ; i <  TermFromRm1.size() ; ++i)
	{ 
		Term *term = TermFromRm1[i];
		std::string termValue = term->value;
		double weight = term->weight;
		rm_terms.insert(std::make_pair<string,double> (termValue, weight));
	}
	double **distancesMatrix = createDistancesMatrix(rm_terms,voc_to_line,dim,data);


	ClusterRM3ExpansionTerms expansion;

	expansion.QueryId = rm1Terms->QueryId;

	expansion.OriginalQuery = rm1Terms->OriginalQuery;
	expansion.Rm1Terms = rm1Terms->TermFromRm1;
	expansion.OriginalQueryWeight = originalQueryWeight;
	expansion.ClusterTermsWeight = clusterTermsWeight;
	expansion.Rm1TermsWeight = 1 - originalQueryWeight - clusterTermsWeight;


	#pragma region for original query
	Rm1Terms localRm1Terms;
	localRm1Terms.QueryId = rm1Terms->QueryId;
	localRm1Terms.OriginalQuery = rm1Terms->OriginalQuery;
	localRm1Terms.OriginalQueryWeight = originalQueryWeight;
	localRm1Terms.TermsWeight = 1 - originalQueryWeight;

	std::string queryNumber = rm1Terms->QueryId ;
	of << "<query>\n";
	of << "<number>" + queryNumber + "</number>\n";
	of << "<text> #combine( "  + convertTermToString(rm1Terms->OriginalQuery) + ") </text>\n";
	of << "</query>\n";
	for(int i2 = 0 ; i2 <vec->size() ; ++i2)
	{
		qrels << queryNumber + " " + (*vec)[i2]->Zero + " " << (*vec)[i2]->DocId + " " << (*vec)[i2]->Score +  "\n";
	}

	// for rm3  query  10 terms
	double sum = 0;
	unsigned int len = (unsigned int)(rm1Terms->TermFromRm1).size();
	for ( int i1=0; i1<10 && i1<len ; ++i1 )
	{
		
		Term* t =  rm1Terms->TermFromRm1[i1];
		sum += t->weight;
		localRm1Terms.TermFromRm1.push_back( t );
	}
	for ( int i1=0; i1<10 && i1<len ; ++i1 )
	{
		Term* t = localRm1Terms.TermFromRm1[i1];
		t->weight = t->weight / sum;
	}
	std::string query = buildQuery(&localRm1Terms);
	queryNumber = rm1Terms->QueryId  +"_rm3_10"  ;
	of << "<query>\n";
	of << "<number>" + queryNumber + "</number>\n";
	of << "<text>" + query + "</text>\n";
	of << "</query>\n";
	for(int i2 = 0 ; i2 <vec->size() ; ++i2)
	{
		qrels << queryNumber + " " + (*vec)[i2]->Zero + " " << (*vec)[i2]->DocId + " " << (*vec)[i2]->Score +  "\n";
	}
	localRm1Terms.TermFromRm1.clear();
	#pragma endregion 

	#pragma region for rm3 query  100 terms
	sum = 0;
	for ( int i1=0; i1<100 && i1<len; ++i1 )
	{
		Term* t =  rm1Terms->TermFromRm1[i1];
		sum += t->weight;
		localRm1Terms.TermFromRm1.push_back( t );
	}
	for ( int i1=0; i1<100 && i1<len; ++i1 )
	{
		Term* t = localRm1Terms.TermFromRm1[i1];
		t->weight = t->weight / sum;
	}
	query = buildQuery(&localRm1Terms);
	queryNumber = rm1Terms->QueryId  +"_rm3_100"  ;
	of << "<query>\n";
	of << "<number>" + queryNumber + "</number>\n";
	of << "<text>" + query + "</text>\n";
	of << "</query>\n";
	for(int i2 = 0 ; i2 <vec->size() ; ++i2)
	{
		qrels << queryNumber + " " + (*vec)[i2]->Zero + " " << (*vec)[i2]->DocId + " " << (*vec)[i2]->Score +  "\n";
	}

	#pragma endregion 



	// for all clusters - build new rm3-100
	for( int i = 0 ; i< TermFromRm1.size() ; ++i)
	{
		expansion.ClusterTerms.clear();
		vector<std::pair<int,double> > vcos;

		for( int j = 0 ; j< TermFromRm1.size() ; ++j)
		{
			vcos.push_back(std::make_pair<int,double>(j, distancesMatrix[i][j] ));
		}

		std::sort(vcos.begin(), vcos.end(), sort_pred);

		// if the term is not in the word2Vec vocabulary, then all array is zeros, therefore after sort the highest value be zero.
		if( vcos[0].second == 0)
		{
			continue;
		}

		double sum = 0;
		vcos.erase(vcos.begin()+k+1, vcos.end());


		for ( int i1=0; i1<k+1 ; ++i1 )
		{
			int index = vcos[i1].first;
			Term* t =  rm1Terms->TermFromRm1[index];
			sum += t->weight;
			if(t == NULL)
			{
				std::cout << "Receive Term that is NULL " << std::endl;
			}
			else
			{
				Term *tNew = t->Copy();
				expansion.ClusterTerms.push_back( tNew );
			}
			
		}

		for ( int i1=0; i1<k+1 ; ++i1 )
		{
			Term* t = expansion.ClusterTerms[i1];
			if(type == Uniform)
			{
				double c = expansion.ClusterTerms.size();
				t->weight = 1/c;
			}
			else if(type == RelativeToRm1)
			{
				t->weight = t->weight / sum;
			}
		}

		std::string query = buildNewRm3ClustresQuery(&expansion);

		queryNumber = rm1Terms->QueryId  +"_"  + TermFromRm1[i]->value;

		of << "<query>\n";
		of << "<number>" + queryNumber + "</number>\n";
		of << "<text>" + query + "</text>\n";
		of << "</query>\n";

		for(int i2 = 0 ; i2 <vec->size() ; ++i2)
		{
			qrels << queryNumber + " " + (*vec)[i2]->Zero + " " << (*vec)[i2]->DocId + " " << (*vec)[i2]->Score +  "\n";
		}

	}

	
	delete []distancesMatrix;
	//delete distancesMatrix;

	of << "</parameters>";
	of.close();  
}




